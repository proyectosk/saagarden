/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mario
 */
public class ConectionDBMario {

    /**
     * Se encarga de establecer(abrir) la conexion con la BD
     * 
     * @return La conexion
     */
    public static Connection getConnection() {

        String url;
        String dbName;
        String userName;
        String password;

        url = "jdbc:mysql://localhost:3306/";
        dbName = "gnklmex_consolidada2?useUnicode=true&characterEncoding=utf-8";
        userName = "root";
        password = "eve9397";

        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager
                    .getConnection(url + dbName, userName, password);

            return con;
        } catch (Exception e) {
            Logger.getLogger(ConectionDB.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
    }

    /**
     * Se encarga de terminar(cerrar) la conexion
     * @param con La conexion
     */
    public static void close(Connection con) {
        try {
            con.close();
        } catch (SQLException e) {
            Logger.getLogger(ConectionDB.class.getName()).log(Level.SEVERE, null, e);
        }
    }

}
