/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Correo;

import conn.ConectionDB;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.sql.ResultSet;

/**
 *
 * @author Americo
 */
public class CorreoPedido1 {

    /**
     *
     * @param folio
     */
    public static void enviaCorreo(int folio) {
        ConectionDB conBD = new ConectionDB();

        try {
            /* TODO output your page here. You may use following sample code. */
            Properties props = new Properties();
            props.setProperty("mail.smtp.host", "smtp.gmail.com");
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.user", "ricardo.wence@gnkl.mx");
            props.setProperty("mail.smtp.auth", "true");

            // Preparamos la sesion
            Session session = Session.getDefaultInstance(props);

            // Construimos el mensaje
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress("ricardo.wence@gnkl.mx"));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("ricardo.wence@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("alejandro.centeno@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("juan.ortiz@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("ricardo.sanchez@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("salvador.negrete.rodea@gmail.com"));//Aqui se pone la direccion a donde se enviara el correo
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("anibal.rincon@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("david.olvera@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            message.setSubject("Nuevo pedido solicitado GARDEN HIGHPRO / GNK Logística");
            String mensaje = "";
            try {
                conBD.conectar();

                String consulta = "SELECT m.F_ClaPro clave, m.F_DesPro descrip, ft.F_Cant, ft.F_FecEnt, u.F_NomCli,ft.F_IdFact FROM tb_facttemp AS ft, tb_lote AS l INNER JOIN tb_medica AS m ON l.F_ClaPro = m.F_ClaPro, tb_uniatn AS u WHERE ft.F_IdLot = l.F_IdLote AND u.F_ClaCli = ft.F_ClaCli and F_IdFact='" + folio + "'";

                ResultSet rset = conBD.consulta(consulta);
                if (rset.next()) {
                    mensaje = mensaje + "Se acaba de solicitar el siguiente Pedido: " + rset.getString("F_IdFact") + "\n";
                    mensaje = mensaje + "Punto de Entrega: " + rset.getString("F_NomCli") + "\n"
                            + "Fecha de Entrega: " + rset.getString("F_FecEnt") + "\n";
                }

                mensaje = mensaje + "Clave\t\t\tDescripción\t\t\t\t\t\tCantidad\n ";
                rset = conBD.consulta(consulta);
                while (rset.next()) {
                    mensaje = mensaje + rset.getString("clave") + "\t\t" + rset.getString("descrip") + "\t\t\t\t\t\t" + rset.getString("ft.F_Cant") + "\n\n";
                }
                conBD.cierraConexion();
            } catch (Exception e) {
                System.out.println(e);
            }
            //mensaje += "\n\nCORREO CON CONTENIDO DE PRUEBAS";

            message.setText(mensaje);

            // Lo enviamos.
            Transport t = session.getTransport("smtp");
            t.connect("ricardo.wence@gnkl.mx", "ricardo.wence+111");
            t.sendMessage(message, message.getAllRecipients());

            // Cierre.
            t.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
