/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Correo;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import conn.*;
import java.sql.ResultSet;

/**
 *
 * @author Americo
 */
public class CancelaCompra {

    ConectionDB obj = new ConectionDB();

    public void cancelaCompra(String folio, String usuario) {
        try {
            /* TODO output your page here. You may use following sample code. */
            Properties props = new Properties();
            props.setProperty("mail.smtp.host", "smtp.gmail.com");
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.user", "ricardo.wence@gnkl.mx");
            props.setProperty("mail.smtp.auth", "true");

            // Preparamos la sesion
            Session session = Session.getDefaultInstance(props);

            boolean ala165 = true;
            // Construimos el mensaje
            MimeMessage message = new MimeMessage(session);
            if (ala165) {

                message.addRecipient(Message.RecipientType.TO, new InternetAddress("ricardo.wence@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("alejandro.centeno@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("juan.ortiz@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("ricardo.sanchez@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("salvador.negrete.rodea@gmail.com"));//Aqui se pone la direccion a donde se enviara el correo
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("anibal.rincon@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("david.olvera@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            } else {
                message.addRecipient(Message.RecipientType.TO, new InternetAddress("ricardo.wence@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
                message.addRecipient(Message.RecipientType.TO, new InternetAddress("mario.uribe@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo

            }
            message.setSubject("Cancelación de ORDEN DE REPOSICIÓN DE INVENTARIO GARDEN HIGHPRO / GNK Logística");
            System.out.println("Cancelación de ORDEN DE REPOSICIÓN DE INVENTARIO / GNK Logística");
            String user = "";
            try {
                obj.conectar();
                ResultSet rset = obj.consulta("select F_Usuario from tb_usuariosisem where F_IdUsu = '" + usuario + "' ");
                while (rset.next()) {
                    user = rset.getString(1);
                }
                obj.cierraConexion();
            } catch (Exception e) {
            }
            String mensaje = "CANCELACIÓN\nSe acaba de cancelar la siguiente ORDEN DE COMPRA: " + folio + " por el Usuario: " + user + "\n";
            try {
                obj.conectar();
                ResultSet rset = obj.consulta("select F_Observaciones from tb_obscancela where F_NoCompra = '" + folio + "' ");
                while (rset.next()) {
                    mensaje = mensaje + "Observaciones: " + rset.getString(1) + "\n";
                }
                obj.cierraConexion();
            } catch (Exception e) {
            }
            try {
                obj.conectar();
                ResultSet rset = obj.consulta("select p.F_FecSur, p.F_HorSur, pro.F_NomPro, u.F_Usuario from tb_pedidoisem p, tb_proveedor pro, tb_usuariosisem u where u.F_IdUsu = p.F_IdUsu and p.F_Provee = pro.F_ClaProve and  F_NoCompra = '" + folio + "' group by pro.F_NomPro ");
                while (rset.next()) {
                    mensaje = mensaje + "Proveedor: " + rset.getString(3) + "\n"
                            + "Fecha de Entrega: " + rset.getString(1) + " " + rset.getString(2) + "\n"
                            + "Orden capurada por: " + rset.getString(4) + "\n";
                }
                obj.cierraConexion();
            } catch (Exception e) {
            }
            mensaje = mensaje + "Clave\t\t\tCantidad\n";
            try {
                obj.conectar();
                ResultSet rset = obj.consulta("select F_Clave, F_Cant, F_Obser from tb_pedidoisem where F_NoCompra = '" + folio + "' ");
                while (rset.next()) {
                    mensaje = mensaje + rset.getString(1) + "\t\t" + rset.getString(2) + "\t\t" + rset.getString(3) + "\n";
                }
                obj.cierraConexion();
            } catch (Exception e) {
            }
            System.out.println(mensaje);
            mensaje+="\n\n";
            message.setText(mensaje);

            // Lo enviamos.
            Transport t = session.getTransport("smtp");
            t.connect("ricardo.wence@gnkl.mx", "ricardo.wence+111");
            t.sendMessage(message, message.getAllRecipients());

            // Cierre.
            t.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
