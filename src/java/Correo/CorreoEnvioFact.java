/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Correo;

import conn.ConectionDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mario
 */
public class CorreoEnvioFact {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    public static void enviaCorreo(String folio) {
        ConectionDB conBD = new ConectionDB();

        try {
            /* TODO output your page here. You may use following sample code. */
            Properties props = new Properties();
            props.setProperty("mail.smtp.host", "smtp.gmail.com");
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.user", "ricardo.wence@gnkl.mx");
            props.setProperty("mail.smtp.auth", "true");

            // Preparamos la sesion
            Session session = Session.getDefaultInstance(props);

            // Construimos el mensaje
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress("ricardo.wence@gnkl.mx"));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("ricardo.wence@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("alejandro.centeno@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("juan.ortiz@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("ricardo.sanchez@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("salvador.negrete.rodea@gmail.com"));//Aqui se pone la direccion a donde se enviara el correo
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("anibal.rincon@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("david.olvera@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            
            message.setSubject("Factura del Folio: "+folio+" en camino vía correo / GARDEN HIGHPRO A GNK Logística");
            String mensaje="";
            try {
                mensaje = mensaje + "La factura del Pedido: " + folio + " se encuentra en camino\n";
                conBD.conectar();

                String consulta = "SELECT u.F_ClaCli, u.F_NomCli, f.F_ClaDoc ,f.F_FecEnt FROM tb_factura AS f INNER JOIN tb_uniatn AS u ON f.F_ClaCli = u.F_ClaCli WHERE f.F_ClaDoc =" + folio + " GROUP BY f.F_ClaDoc";

                ResultSet rset = conBD.consulta(consulta);
                if (rset.next()) {
                    mensaje = mensaje + "Punto de Entrega: " + rset.getString("F_NomCli") + "\n"
                            + "Fecha de Entrega: " + rset.getString("F_FecEnt") + "\n";
                }
                conBD.cierraConexion();
            } catch (Exception e) {
                System.out.println(e);
            }
            //mensaje += "\n\nCORREO CON CONTENIDO DE PRUEBAS";

            message.setText(mensaje);

            // Lo enviamos.
            Transport t = session.getTransport("smtp");
            t.connect("ricardo.wence@gnkl.mx", "ricardo.wence+111");
            t.sendMessage(message, message.getAllRecipients());

            // Cierre.
            t.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    /*protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ConectionDB conBD = new ConectionDB();

        PrintWriter out = response.getWriter();

        String pedido = request.getParameter("pedido");
        try {
            /* TODO output your page here. You may use following sample code. */
      /*      Properties props = new Properties();
            props.setProperty("mail.smtp.host", "smtp.gmail.com");
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.user", "ricardo.wence@gnkl.mx");
            props.setProperty("mail.smtp.auth", "true");

            // Preparamos la sesion
            Session session = Session.getDefaultInstance(props);

            // Construimos el mensaje
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress("ricardo.wence@gnkl.mx"));
            //message.addRecipient(Message.RecipientType.TO, new InternetAddress("ricardo.wence@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            //message.addRecipient(Message.RecipientType.TO, new InternetAddress("mario.uribe@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            //message.addRecipient(Message.RecipientType.TO, new InternetAddress("vicente.flores@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            //message.addRecipient(Message.RecipientType.TO, new InternetAddress("guillermo.gomez@gnkl.mx "));//Aqui se pone la direccion a donde se enviara el correo
            //message.addRecipient(Message.RecipientType.TO, new InternetAddress("juan.ortiz@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            //message.addRecipient(Message.RecipientType.TO, new InternetAddress("manuel.olvera@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            //message.addRecipient(Message.RecipientType.TO, new InternetAddress("alejandro.centeno@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("anibal.rincon@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            //message.addRecipient(Message.RecipientType.TO, new InternetAddress("david.olvera@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            
            message.setSubject("Factura en camino vía correo / ITUPHARMA A GNK Logística");
            String mensaje = "";
            try {

                mensaje = mensaje + "La factura del Pedido: " + pedido + " se encuentra en camino\n";
                conBD.conectar();

                String consulta = "SELECT u.F_ClaCli, u.F_NomCli, f.F_ClaDoc ,f.F_FecEnt FROM tb_factura AS f INNER JOIN tb_uniatn AS u ON f.F_ClaCli = u.F_ClaCli WHERE f.F_ClaDoc =" + pedido + " GROUP BY f.F_ClaDoc";

                ResultSet rset = conBD.consulta(consulta);
                if (rset.next()) {
                    mensaje = mensaje + "Punto de Entrega: " + rset.getString("F_NomCli") + "\n"
                            + "Fecha de Entrega: " + rset.getString("F_FecEnt") + "\n";
                }
            } catch (Exception e) {
                System.out.println(e);
            }
                      //  mensaje+="\n\nCORREO CON CONTENIDO DE PRUEBAS";

            message.setText(mensaje);

            // Lo enviamos.
            Transport t = session.getTransport("smtp");
            t.connect("ricardo.wence@gnkl.mx", "ricardo.wence+111");
            t.sendMessage(message, message.getAllRecipients());

            // Cierre.
            t.close();
            out.println("<script type=\"text/javascript\">\n" +
            "        var ventana = window.self;\n" +
            "        ventana.opener = window.self;\n" +
            "        setTimeout(\"window.close()\", 500);\n" +
            "\n" +
            "    </script>");
            out.close();
             
            out.println("<script>window.history.back()</script>");
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
         out.println("<script>window.history.back()</script>");
    }*/

   

}
