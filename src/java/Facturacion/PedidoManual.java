/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Facturacion;

import Correo.CorreoEnvioFact;
import Correo.CorreoPedido1;
import Modula.RequerimientoModula;
import conn.ConectionDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mario
 */
public class PedidoManual extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String accion;
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        accion = request.getParameter("accion");
        ConectionDB con = new ConectionDB();
        HttpSession sesion = request.getSession();

        if (accion == null) {
            accion = "";
        }

        //Eliminar Registros lotes seleccionados
        try {
            if (!request.getParameter("accionEliminar3").equals("")) {

                //sesion.setAttribute("F_IndGlobal", null);
                con.conectar();
                ResultSet rset = con.consulta("select * from tb_facttemp where F_Id = '" + request.getParameter("accionEliminar3") + "'");
                while (rset.next()) {
                    con.insertar("insert into tb_facttemp_elim values ('" + rset.getString(1) + "','" + rset.getString(2) + "','" + rset.getString(3) + "','" + rset.getString(4) + "','" + rset.getString(5) + "','" + rset.getString(6) + "','" + rset.getString(7) + "', '" + rset.getString(8) + "', NOW())");
                }
                con.insertar("delete from tb_facttemp where F_Id = '" + request.getParameter("accionEliminar3") + "' ");
                con.cierraConexion();
                out.println("<script>alert('Clave Eliminada Correctamente')</script>");
                out.println("<script>window.location='pedidoManual2.jsp'</script>");
                sesion.setAttribute("ClaProFM", "");
                sesion.setAttribute("DesPro", "");
                sesion.setAttribute("DesProFM", "");
                sesion.setAttribute("ExistenciaFM", "");
            }

        } catch (Exception e) {
        }

        //Elimina insumo del pedido
        if (request.getParameter("accionEliminarIns") != null) {
            if (!request.getParameter("accionEliminarIns").equals("")) {
                try {
                    con.conectar();
                    ResultSet rset = con.consulta("select * from tb_facttemp where F_Id = '" + request.getParameter("accionEliminarIns") + "'");
                    while (rset.next()) {
                        con.insertar("insert into tb_facttemp_elim values ('" + rset.getString(1) + "','" + rset.getString(2) + "','" + rset.getString(3) + "','" + rset.getString(4) + "','" + rset.getString(5) + "','" + rset.getString(6) + "','" + rset.getString(7) + "', '" + rset.getString(8) + "', NOW())");
                    }
                    con.insertar("delete from tb_facttemp where F_Id = '" + request.getParameter("accionEliminarIns") + "' ");
                    con.cierraConexion();
                    out.println("<script>alert('Clave Eliminada Correctamente')</script>");
                    out.println("<script>window.location='pedidoManual2.jsp'</script>");
                } catch (SQLException ex) {
                    Logger.getLogger(PedidoManual.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        //cancela el pedido
        if (accion.equals("CancelarFactura")) {
            try {
                con.conectar();
                ResultSet rset = con.consulta("select * from tb_facttemp where F_IdFact = '" + request.getParameter("F_IndGlob") + "'");
                while (rset.next()) {
                    con.insertar("insert into tb_facttemp_elim values ('" + rset.getString(1) + "','" + rset.getString(2) + "','" + rset.getString(3) + "','" + rset.getString(4) + "','" + rset.getString(5) + "','" + rset.getString(6) + "','" + rset.getString(7) + "', '" + (String) sesion.getAttribute("Usuario") + "', NOW())");
                }
                con.insertar("delete from tb_facttemp where F_IdFact = '" + request.getParameter("F_IndGlob") + "' ");
                con.cierraConexion();
                sesion.setAttribute("F_IndGlobal", null);
                sesion.setAttribute("ClaCliFM", "");
                sesion.setAttribute("DesCliFM", "");
                sesion.setAttribute("FechaEntFM", "");
                sesion.setAttribute("Hora", "");
                sesion.setAttribute("nombrePAQ", "");
                sesion.setAttribute("DirecPAQ", "");
                sesion.setAttribute("ClaProFM", "");
                sesion.setAttribute("DesProFM", "");
                sesion.setAttribute("ClaGenCliFM", null);
                sesion.setAttribute("nombrePE", null);

                out.println("<script>alert('Factura Eliminada Correctamente')</script>");
                out.println("<script>window.location='pedidoManual2.jsp'</script>");
            } catch (SQLException e) {
            }
        }

        //confirma el pedido
        /*if (accion.equals("ConfirmarFactura")) {
         try {
         con.conectar();
         int folio = Integer.parseInt((String) sesion.getAttribute("F_IndGlobal"));
         RequerimientoModula reqMod = new RequerimientoModula();
         con.insertar("update tb_facttemp set F_StsFact = '0' where F_IdFact = '" + folio + "' ");
         con.cierraConexion();
         CorreoPedido1.enviaCorreo(folio);

         sesion.setAttribute("F_IndGlobal", null);
         sesion.setAttribute("ClaCliFM", "");
         sesion.setAttribute("FechaEntFM", "");
         sesion.setAttribute("ClaProFM", "");
         sesion.setAttribute("DesProFM", "");
         response.sendRedirect("pedidoManual2.jsp");
         } catch (SQLException e) {
         System.out.println("ERROR SQL:" + e.getMessage());
         } catch (NumberFormatException e) {
         System.out.println("ERROR NumberFormatException:" + e.getMessage());
         } catch (IOException e) {
         System.out.println("ERROR IOException:" + e.getMessage());
         }
         }*/
        //confirma el pedido
        if (accion.equals("ConfirmarFactura")) {
            try {
                con.conectar();
                int folio = Integer.parseInt((String) sesion.getAttribute("F_IndGlobal"));
                //RequerimientoModula reqMod = new RequerimientoModula();
                String Paqueteria = request.getParameter("paquete");
                String Cobro = request.getParameter("cobro");
                if ((!(Paqueteria == "")) && ((!(Cobro == "")))) {
                    con.insertar("update tb_facttemp set F_StsFact = '0',F_TipoEnvio='" + Paqueteria + "',F_TerminoCap=NOW(),F_TipoCobro='"+Cobro+"' where F_IdFact = '" + folio + "' ");
                    con.cierraConexion();
                    CorreoPedido1.enviaCorreo(folio);

                    sesion.setAttribute("F_IndGlobal", null);
                    sesion.setAttribute("ClaCliFM", "");
                    sesion.setAttribute("DesCliFM", "");
                    sesion.setAttribute("FechaEntFM", "");
                    sesion.setAttribute("Hora", "");
                    sesion.setAttribute("nombrePAQ", "");
                    sesion.setAttribute("DirecPAQ", "");
                    sesion.setAttribute("ClaProFM", "");
                    sesion.setAttribute("DesProFM", "");
                    sesion.setAttribute("ClaGenCliFM", null);
                    sesion.setAttribute("nombrePE", null);
                    response.sendRedirect("pedidoManual2.jsp");
                } else {
                    out.println("<script>alert('Selecciona Forma Envío y/o Tipo Envío')</script>");
                    out.println("<script>window.history.back()</script>");
                }
            } catch (SQLException e) {
                System.out.println("ERROR SQL:" + e.getMessage());
            } catch (NumberFormatException e) {
                System.out.println("ERROR NumberFormatException:" + e.getMessage());
            } catch (IOException e) {
                System.out.println("ERROR IOException:" + e.getMessage());
            }
        }

        //busca los puntos de entrega
        if (accion.equals("buscarCliente")) {
            try {
                con.conectar();

                String clacliente = request.getParameter("ClaCli");
                List<String[]> claves = new ArrayList<String[]>();
                ResultSet rset = con.consulta("select F_ClaCli, F_Direc, F_ClaveGen,F_NomCli from tb_uniatn where F_ClaveGen = '" + clacliente + "'");
                while (rset.next()) {
                    String[] detalle = new String[2];
                    detalle[0] = rset.getString(1);
                    detalle[1] = rset.getString(2);
                    claves.add(detalle);
                    sesion.setAttribute("ClaCliFM", rset.getString("F_ClaveGen"));
                    sesion.setAttribute("DesCliFM", rset.getString("F_NomCli"));
                    System.out.println("Descli" + rset.getString("F_NomCli") + "Clav" + rset.getString("F_ClaveGen"));
                }
                request.setAttribute("puntosEntrega", claves);

                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(PedidoManual.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.getRequestDispatcher("pedidoManual2.jsp").forward(request, response);

        }

        //busca el insumo por clave
        /*if (accion.equals("buscar")) {

         int existencia = 0, cantTemp = 0, existenciaVirtual;

         try {
         /**
         * Proceso de generación de Concentrado Global
         */

        /* String F_IndGlobal = (String) sesion.getAttribute("F_IndGlobal");
         if (F_IndGlobal == null) {
         sesion.setAttribute("F_IndGlobal", dameIndGlobal() + "");
         }
         con.conectar();
         ResultSet rset = con.consulta("SELECT m.F_ClaPro, m.F_DesPro, l.F_ClaLot, l.F_FolLot, DATE_FORMAT(l.F_FecCad, '%d/%m/%Y') FROM tb_medica m, tb_lote l WHERE m.F_ClaPro = l.F_ClaPro AND m.F_ClaPro = '" + request.getParameter("ClaPro") + "' GROUP BY m.F_ClaPro;");
         while (rset.next()) {
         sesion.setAttribute("DesProFM", rset.getString(2));
         }
         ResultSet rset2 = con.consulta("SELECT l.F_ClaPro, l.F_ExiLot, ft.F_IdLot, l.F_IdLote, SUM(ft.F_Cant) apartado FROM tb_facttemp AS ft, tb_lote AS l WHERE ft.F_StsFact < 5 AND ft.F_IdLot = l.F_IdLote AND l.F_ClaPro='" + request.getParameter("ClaPro") + "' GROUP BY l.F_ClaPro");
         while (rset2.next()) {
         cantTemp = rset2.getInt("apartado");
         }
         rset2 = con.consulta("SELECT SUM(l.F_ExiLot) from tb_lote as l WHERE l.F_ClaPro='" + request.getParameter("ClaPro") + "'");
         while (rset2.next()) {
         existencia = rset2.getInt(1);
         }

         existenciaVirtual = existencia - cantTemp;
         System.out.println(existenciaVirtual + "=" + existencia + "-" + cantTemp);

         sesion.setAttribute("ExistenciaFM", existenciaVirtual + "");

         if (sesion.getAttribute("ClaGenCliFM") == null) {
         sesion.setAttribute("ClaGenCliFM", request.getParameter("puntosEntrega"));
         }
         rset = con.consulta("select F_Direc from tb_uniatn where F_ClaCli = '" + sesion.getAttribute("ClaGenCliFM") + "'");
         while (rset.next()) {
         sesion.setAttribute("nombrePE", rset.getString(1));
         }
                
         /*if (request.getParameter("nombrePE").equals("")){
         sesion.setAttribute("nombrePE", request.getParameter("puntosEntrega"));
         }else{
         sesion.setAttribute("nombrePE", request.getParameter("nombrePE"));
         }*/
        /*rset = con.consulta("select F_Direc from tb_uniatn where F_ClaCli = '" + sesion.getAttribute("ClaGenCliFM") + "'");
         while (rset.next()) {
         sesion.setAttribute("nombrePE", rset.getString(1));
         }*/
        /*con.cierraConexion();
         //sesion.setAttribute("ClaCliFM", request.getParameter("ClaCli"));
         sesion.setAttribute("FechaEntFM", request.getParameter("FechaEnt"));
         sesion.setAttribute("ClaProFM", request.getParameter("ClaPro"));
         response.sendRedirect("pedidoManual2.jsp");

         } catch (SQLException ex) {
         Logger.getLogger(PedidoManual.class.getName()).log(Level.SEVERE, null, ex);
         }

         }*/
        //busca el insumo por clave
        if (accion.equals("buscar")) {

            int existencia = 0, cantTemp = 0, existenciaVirtual;

            try {
                /**
                 * Proceso de generación de Concentrado Global
                 */

                String F_IndGlobal = (String) sesion.getAttribute("F_IndGlobal");
                if (F_IndGlobal == null) {
                    sesion.setAttribute("F_IndGlobal", dameIndGlobal() + "");
                }
                con.conectar();
                ResultSet rset = con.consulta("SELECT m.F_ClaPro, m.F_DesPro, l.F_ClaLot, l.F_FolLot, DATE_FORMAT(l.F_FecCad, '%d/%m/%Y') FROM tb_medica m, tb_lote l WHERE m.F_ClaPro = l.F_ClaPro AND m.F_ClaPro = '" + request.getParameter("ClaPro") + "' GROUP BY m.F_ClaPro;");
                while (rset.next()) {
                    sesion.setAttribute("DesProFM", rset.getString(2));
                }
                ResultSet rset2 = con.consulta("SELECT l.F_ClaPro, l.F_ExiLot, ft.F_IdLot, l.F_IdLote, SUM(ft.F_Cant) apartado FROM tb_facttemp AS ft, tb_lote AS l WHERE ft.F_StsFact < 5 AND ft.F_IdLot = l.F_IdLote AND l.F_ClaPro='" + request.getParameter("ClaPro") + "' GROUP BY l.F_ClaPro");
                while (rset2.next()) {
                    cantTemp = rset2.getInt("apartado");
                }
                rset2 = con.consulta("SELECT SUM(l.F_ExiLot) from tb_lote as l WHERE l.F_ClaPro='" + request.getParameter("ClaPro") + "'");
                while (rset2.next()) {
                    existencia = rset2.getInt(1);
                }

                existenciaVirtual = existencia - cantTemp;
                System.out.println(existenciaVirtual + "=" + existencia + "-" + cantTemp);

                sesion.setAttribute("ExistenciaFM", existenciaVirtual + "");
                System.out.println("l" + sesion.getAttribute("ClaGenCliFM"));
                if (sesion.getAttribute("ClaGenCliFM") == null) {
                    sesion.setAttribute("ClaGenCliFM", request.getParameter("puntosEntrega"));
                }
                System.out.println(request.getParameter("puntosEntrega"));

                rset = con.consulta("select F_Direc,F_Transporte,F_DirTrans from tb_uniatn where F_ClaCli = '" + request.getParameter("puntosEntrega") + "'");
                while (rset.next()) {
                    sesion.setAttribute("nombrePE", rset.getString(1));
                    sesion.setAttribute("nombrePAQ", rset.getString(2));
                    sesion.setAttribute("DirecPAQ", rset.getString(3));
                }
                /*if (request.getParameter("nombrePE").equals("")){
                 sesion.setAttribute("nombrePE", request.getParameter("puntosEntrega"));
                 }else{
                 sesion.setAttribute("nombrePE", request.getParameter("nombrePE"));
                 }*/
                con.cierraConexion();
                //sesion.setAttribute("ClaCliFM", request.getParameter("ClaCli"));
                sesion.setAttribute("FechaEntFM", request.getParameter("FechaEnt"));
                sesion.setAttribute("ClaProFM", request.getParameter("ClaPro"));
                sesion.setAttribute("Hora", request.getParameter("hora"));
                response.sendRedirect("pedidoManual2.jsp");

            } catch (SQLException ex) {
                Logger.getLogger(PedidoAuto.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        //busca el insumo por clave Transferencias
        if (accion.equals("buscarT")) {

            int existencia = 0, cantTemp = 0, existenciaVirtual;

            try {
                /**
                 * Proceso de generación de Concentrado Global
                 */

                String F_IndGlobal = (String) sesion.getAttribute("F_IndGlobal");
                if (F_IndGlobal == null) {
                    sesion.setAttribute("F_IndGlobal", dameIndGlobal() + "");
                }
                con.conectar();
                ResultSet rset = con.consulta("SELECT m.F_ClaPro, m.F_DesPro, l.F_ClaLot, l.F_FolLot, DATE_FORMAT(l.F_FecCad, '%d/%m/%Y') FROM tb_medica m, tb_lote l WHERE m.F_ClaPro = l.F_ClaPro AND m.F_ClaPro = '" + request.getParameter("ClaPro") + "' GROUP BY m.F_ClaPro;");
                while (rset.next()) {
                    sesion.setAttribute("DesProFM1", rset.getString(2));
                }
                ResultSet rset2 = con.consulta("SELECT l.F_ClaPro, l.F_ExiLot, ft.F_IdLot, l.F_IdLote, SUM(ft.F_Cant) apartado FROM tb_facttemp AS ft, tb_lote AS l WHERE ft.F_StsFact < 5 AND ft.F_IdLot = l.F_IdLote AND l.F_ClaPro='" + request.getParameter("ClaPro") + "' GROUP BY l.F_ClaPro");
                while (rset2.next()) {
                    cantTemp = rset2.getInt("apartado");
                }
                rset2 = con.consulta("SELECT SUM(l.F_ExiLot) from tb_lote as l WHERE l.F_ClaPro='" + request.getParameter("ClaPro") + "'");
                while (rset2.next()) {
                    existencia = rset2.getInt(1);
                }

                existenciaVirtual = existencia - cantTemp;
                System.out.println(existenciaVirtual + "=" + existencia + "-" + cantTemp);

                sesion.setAttribute("ExistenciaFM1", existenciaVirtual + "");

                if (sesion.getAttribute("ClaGenCliFM1") == null) {
                    sesion.setAttribute("ClaGenCliFM1", request.getParameter("puntosEntrega"));
                }
                rset = con.consulta("select F_Direc from tb_uniatn where F_ClaCli = '" + sesion.getAttribute("ClaGenCliFM") + "'");
                while (rset.next()) {
                    sesion.setAttribute("nombrePE", rset.getString(1));
                }

                con.cierraConexion();
                //sesion.setAttribute("ClaCliFM", request.getParameter("ClaCli"));
                sesion.setAttribute("FechaEntFM", request.getParameter("FechaEnt"));
                sesion.setAttribute("ClaProFM1", request.getParameter("ClaPro"));
                response.sendRedirect("Transferir.jsp");

            } catch (SQLException ex) {
                Logger.getLogger(PedidoManual.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        //selecciona la clave en caso de que una descripcion cuente con mas de una clave
        if (accion.equals("selectClave")) {

            int existencia = 0, cantTemp = 0, existenciaVirtual;

            try {
                /**
                 * Proceso de generación de Concentrado Global
                 */

                String F_IndGlobal = (String) sesion.getAttribute("F_IndGlobal");
                if (F_IndGlobal == null) {
                    sesion.setAttribute("F_IndGlobal", dameIndGlobal() + "");
                }
                con.conectar();
                ResultSet rset = con.consulta("SELECT m.F_ClaPro, m.F_DesPro, l.F_ClaLot, l.F_FolLot, DATE_FORMAT(l.F_FecCad, '%d/%m/%Y') FROM tb_medica m, tb_lote l WHERE m.F_ClaPro = l.F_ClaPro AND m.F_ClaPro = '" + request.getParameter("claves") + "' GROUP BY m.F_ClaPro;");
                while (rset.next()) {
                    sesion.setAttribute("DesProFM", rset.getString(2));
                }
                ResultSet rset2 = con.consulta("SELECT l.F_ClaPro, l.F_ExiLot, ft.F_IdLot, l.F_IdLote, SUM(ft.F_Cant) apartado FROM tb_facttemp AS ft, tb_lote AS l WHERE ft.F_StsFact < 5 AND ft.F_IdLot = l.F_IdLote AND l.F_ClaPro='" + request.getParameter("claves") + "' GROUP BY l.F_ClaPro");
                while (rset2.next()) {
                    cantTemp = rset2.getInt("apartado");
                }
                rset2 = con.consulta("SELECT SUM(l.F_ExiLot) from tb_lote as l WHERE l.F_ClaPro='" + request.getParameter("claves") + "'");
                while (rset2.next()) {
                    existencia = rset2.getInt(1);
                }

                existenciaVirtual = existencia - cantTemp;
                System.out.println(existenciaVirtual + "=" + existencia + "-" + cantTemp);

                sesion.setAttribute("ExistenciaFM", existenciaVirtual + "");

                con.cierraConexion();
                sesion.setAttribute("ClaProFM", request.getParameter("claves"));
                response.sendRedirect("pedidoManual2.jsp");

            } catch (SQLException ex) {
                Logger.getLogger(PedidoManual.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        //busca el insumo por descripcion
        if (accion.equals("buscarDesc")) {

            int existencia = 0, cantTemp = 0, existenciaVirtual;
            sesion.setAttribute("DesProFM", "");
            sesion.setAttribute("ClaProFM", "");
            sesion.setAttribute("ExistenciaFM", "");

            try {
                /**
                 * Proceso de generación de Concentrado Global
                 */
                con.conectar();

                List<String> claves = new ArrayList<String>();
                ResultSet rset = con.consulta("select m.F_ClaPro, m.F_DesPro from tb_medica m,tb_lote l where m.F_ClaPro=l.F_ClaPro AND m.F_DesPro = '" + request.getParameter("descripcion") + "' GROUP BY m.F_ClaPro");
                while (rset.next()) {
                    claves.add(rset.getString(1));
                    sesion.setAttribute("clave", rset.getString(1));
                    sesion.setAttribute("descripcion", rset.getString(2));
                }
                int ban = 0;
                System.out.println("Claves_ :" + claves.size());
                if (claves.size() == 1) {
                    ban = 1;
                    String F_IndGlobal = (String) sesion.getAttribute("F_IndGlobal");
                    if (F_IndGlobal == null) {
                        sesion.setAttribute("F_IndGlobal", dameIndGlobal() + "");
                    }
                    con.conectar();
                    rset = con.consulta("SELECT m.F_ClaPro, m.F_DesPro, l.F_ClaLot, l.F_FolLot, DATE_FORMAT(l.F_FecCad, '%d/%m/%Y') FROM tb_medica m, tb_lote l WHERE m.F_ClaPro = l.F_ClaPro AND m.F_ClaPro = '" + sesion.getAttribute("clave") + "' GROUP BY m.F_ClaPro;");
                    while (rset.next()) {
                        sesion.setAttribute("ClaProFM", rset.getString(1));
                        sesion.setAttribute("DesProFM", rset.getString(2));
                    }
                    ResultSet rset2 = con.consulta("SELECT l.F_ClaPro, l.F_ExiLot, ft.F_IdLot, l.F_IdLote, SUM(ft.F_Cant) apartado FROM tb_facttemp AS ft, tb_lote AS l WHERE ft.F_StsFact < 5 AND ft.F_IdLot = l.F_IdLote AND l.F_ClaPro='" + sesion.getAttribute("clave") + "' GROUP BY l.F_ClaPro");
                    while (rset2.next()) {
                        cantTemp = rset2.getInt("apartado");
                    }
                    rset2 = con.consulta("SELECT SUM(l.F_ExiLot) from tb_lote as l WHERE l.F_ClaPro='" + sesion.getAttribute("clave") + "'");
                    while (rset2.next()) {
                        existencia = rset2.getInt(1);
                    }

                    existenciaVirtual = existencia - cantTemp;
                    System.out.println(existenciaVirtual + "=" + existencia + "-" + cantTemp);

                    sesion.setAttribute("ExistenciaFM", existenciaVirtual + "");

                    if (sesion.getAttribute("ClaGenCliFM") == null) {
                        sesion.setAttribute("ClaGenCliFM", request.getParameter("puntosEntrega"));
                    }
                    /*rset = con.consulta("select F_Direc from tb_uniatn where F_ClaCli = '" + sesion.getAttribute("ClaGenCliFM") + "'");
                     while (rset.next()) {
                     sesion.setAttribute("nombrePE", rset.getString(1));
                     }*/
                    if (request.getParameter("nombrePE").equals("")) {
                        sesion.setAttribute("nombrePE", request.getParameter("puntosEntrega"));
                    } else {
                        sesion.setAttribute("nombrePE", request.getParameter("nombrePE"));
                    }

                    con.cierraConexion();

                    sesion.setAttribute("FechaEntFM", request.getParameter("FechaEnt"));
                    response.sendRedirect("pedidoManual2.jsp");
                } else {
                    //mas de uno
                    System.out.println("qui");
                    request.setAttribute("masdeuno", "si");
                    request.setAttribute("claves", claves);

                    String F_IndGlobal = (String) sesion.getAttribute("F_IndGlobal");
                    if (F_IndGlobal == null) {
                        sesion.setAttribute("F_IndGlobal", dameIndGlobal() + "");
                    }

                    if (sesion.getAttribute("ClaGenCliFM") == null) {
                        sesion.setAttribute("ClaGenCliFM", request.getParameter("puntosEntrega"));
                    }
                    rset = con.consulta("select F_Direc from tb_uniatn where F_ClaCli = '" + sesion.getAttribute("ClaGenCliFM") + "'");
                    while (rset.next()) {
                        sesion.setAttribute("nombrePE", rset.getString(1));
                    }

                    con.cierraConexion();
                    //sesion.setAttribute("ClaCliFM", request.getParameter("ClaCli"));

                    sesion.setAttribute("FechaEntFM", request.getParameter("FechaEnt"));
                    sesion.setAttribute("Hora", request.getParameter("hora"));
                    request.getRequestDispatcher("pedidoManual2.jsp").forward(request, response);

                }

            } catch (SQLException ex) {
                Logger.getLogger(PedidoManual.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        //busca el insumo por descripcion Transferenias
        if (accion.equals("buscarDescT")) {

            int existencia = 0, cantTemp = 0, existenciaVirtual;
            sesion.setAttribute("DesProFM1", "");
            sesion.setAttribute("ClaProFM1", "");
            sesion.setAttribute("ExistenciaFM1", "");

            try {
                /**
                 * Proceso de generación de Concentrado Global
                 */
                con.conectar();

                List<String> claves = new ArrayList<String>();
                ResultSet rset = con.consulta("select m.F_ClaPro, m.F_DesPro from tb_medica m,tb_lote l where m.F_ClaPro=l.F_ClaPro AND m.F_DesPro = '" + request.getParameter("descripcion") + "' GROUP BY m.F_ClaPro");
                while (rset.next()) {
                    claves.add(rset.getString(1));
                    sesion.setAttribute("clave", rset.getString(1));
                    sesion.setAttribute("descripcion", rset.getString(2));
                }
                int ban = 0;
                System.out.println("Claves_ :" + claves.size());
                if (claves.size() == 1) {
                    ban = 1;
                    String F_IndGlobal = (String) sesion.getAttribute("F_IndGlobal");
                    if (F_IndGlobal == null) {
                        sesion.setAttribute("F_IndGlobal", dameIndGlobal() + "");
                    }
                    con.conectar();
                    rset = con.consulta("SELECT m.F_ClaPro, m.F_DesPro, l.F_ClaLot, l.F_FolLot, DATE_FORMAT(l.F_FecCad, '%d/%m/%Y') FROM tb_medica m, tb_lote l WHERE m.F_ClaPro = l.F_ClaPro AND m.F_ClaPro = '" + sesion.getAttribute("clave") + "' GROUP BY m.F_ClaPro;");
                    while (rset.next()) {
                        sesion.setAttribute("ClaProFM1", rset.getString(1));
                        sesion.setAttribute("DesProFM1", rset.getString(2));
                    }
                    ResultSet rset2 = con.consulta("SELECT l.F_ClaPro, l.F_ExiLot, ft.F_IdLot, l.F_IdLote, SUM(ft.F_Cant) apartado FROM tb_facttemp AS ft, tb_lote AS l WHERE ft.F_StsFact < 5 AND ft.F_IdLot = l.F_IdLote AND l.F_ClaPro='" + sesion.getAttribute("clave") + "' GROUP BY l.F_ClaPro");
                    while (rset2.next()) {
                        cantTemp = rset2.getInt("apartado");
                    }
                    rset2 = con.consulta("SELECT SUM(l.F_ExiLot) from tb_lote as l WHERE l.F_ClaPro='" + sesion.getAttribute("clave") + "'");
                    while (rset2.next()) {
                        existencia = rset2.getInt(1);
                    }

                    existenciaVirtual = existencia - cantTemp;
                    System.out.println(existenciaVirtual + "=" + existencia + "-" + cantTemp);

                    sesion.setAttribute("ExistenciaFM1", existenciaVirtual + "");

                    if (sesion.getAttribute("ClaGenCliFM") == null) {
                        sesion.setAttribute("ClaGenCliFM", request.getParameter("puntosEntrega"));
                    }
                    rset = con.consulta("select F_Direc from tb_uniatn where F_ClaCli = '" + sesion.getAttribute("ClaGenCliFM") + "'");
                    while (rset.next()) {
                        sesion.setAttribute("nombrePE", rset.getString(1));
                    }

                    con.cierraConexion();

                    sesion.setAttribute("FechaEntFM", request.getParameter("FechaEnt"));
                    response.sendRedirect("Transferir.jsp");
                } else {
                    //mas de uno
                    System.out.println("qui");
                    request.setAttribute("masdeuno", "si");
                    request.setAttribute("claves", claves);

                    String F_IndGlobal = (String) sesion.getAttribute("F_IndGlobal");
                    if (F_IndGlobal == null) {
                        sesion.setAttribute("F_IndGlobal", dameIndGlobal() + "");
                    }

                    if (sesion.getAttribute("ClaGenCliFM") == null) {
                        sesion.setAttribute("ClaGenCliFM", request.getParameter("puntosEntrega"));
                    }
                    rset = con.consulta("select F_Direc from tb_uniatn where F_ClaCli = '" + sesion.getAttribute("ClaGenCliFM") + "'");
                    while (rset.next()) {
                        sesion.setAttribute("nombrePE", rset.getString(1));
                    }

                    con.cierraConexion();
                    //sesion.setAttribute("ClaCliFM", request.getParameter("ClaCli"));

                    sesion.setAttribute("FechaEntFM", request.getParameter("FechaEnt"));
                    request.getRequestDispatcher("Transferir.jsp").forward(request, response);

                }

            } catch (SQLException ex) {
                Logger.getLogger(PedidoManual.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        //Selecciona Multiples lotes
        if (accion.equals("SelectMultples")) {

            try {
                con.conectar();
                int Solicitado = 0, folio = 0, Ban = 0;
                String Observaciones = "", cliente = "", nombrePAQ = "", DirecPAQ = "", Hora = "";
                String[] claveschk = request.getParameterValues("checkRemis");
                if (claveschk == null) {
                    Ban = 0;
                } else {
                    Ban = 1;
                }
                if (Ban == 1) {
                    String F_CLaCli = (String) sesion.getAttribute("ClaCliFM");
                    String F_CLaCli2 = (String) sesion.getAttribute("ClaCliFM");
                    cliente = (String) sesion.getAttribute("ClaGenCliFM");// cliente del pedido  
                    nombrePAQ = (String) sesion.getAttribute("nombrePAQ");
                    DirecPAQ = (String) sesion.getAttribute("DirecPAQ");
                    Hora = (String) sesion.getAttribute("Hora");
                    String[] F_CLaCliArray = F_CLaCli.split(" - ");
                    F_CLaCli = F_CLaCliArray[0];
                    byte[] a = request.getParameter("Observaciones").getBytes("ISO-8859-1");
                    Observaciones = (new String(a, "UTF-8")).toUpperCase();

                    //if (Largo > 0){
                    String Cantidad = request.getParameter("unidad1");
                    //String ClaCli = request.getParameter("ClaCli");
                    String FechaEnt = request.getParameter("FechaEnt");
                    String usua = request.getParameter("usua");
                    Solicitado = Integer.parseInt(Cantidad);
                    folio = Integer.parseInt(request.getParameter("folio"));

                    System.out.println("Cant--" + Cantidad);

                    int posi = 0, posi1 = 0, posi2 = 0, Existencia = 0, Diferencia = 0, F_IdLote = 0, F_FolLot = 0, CantUtil = 0;
                    int F_Cant = 0, F_Id = 0, Facturar = 0;
                    String Clave = "", Lote = "", Cadu = "", Cant = "";
                    for (int i = 0; i < claveschk.length; i++) {
                        System.out.println("claveschk1: " + claveschk[i]);
                        posi = claveschk[i].indexOf(';');
                        posi1 = claveschk[i].indexOf(':');
                        posi2 = claveschk[i].indexOf('&');

                        Clave = claveschk[i].substring(0, posi);
                        Lote = claveschk[i].substring(posi + 1, posi1);
                        Cadu = claveschk[i].substring(posi1 + 1, posi2);
                        Cant = claveschk[i].substring(posi2 + 1);
                        System.out.println(Clave + "--" + Lote + "--" + Cadu + "--" + Cant);
                        ResultSet Consulta = con.consulta("SELECT F_IdLote,F_FolLot,F_ExiLot FROM tb_lote WHERE F_ClaPro='" + Clave + "' AND F_ClaLot='" + Lote + "' AND F_FecCad='" + Cadu + "' AND F_ExiLot>0");
                        while (Consulta.next()) {
                            F_IdLote = Consulta.getInt(1);
                            F_FolLot = Consulta.getInt(2);
                            Existencia = Consulta.getInt(3);

                            ResultSet Util = con.consulta("SELECT SUM(F_Cant) FROM tb_facttemp WHERE F_IdLot='" + F_IdLote + "' AND F_StsFact<'5'");
                            if (Util.next()) {
                                CantUtil = Util.getInt(1);
                            }
                            Existencia = Existencia - CantUtil;
                            if (Existencia > 0) {
                                if (Solicitado > 0) {
                                    if (Solicitado >= Existencia) {
                                        Diferencia = Solicitado - Existencia;
                                        ResultSet RLote = con.consulta("SELECT F_Id,F_Cant FROM tb_facttemp WHERE F_IdLot='" + F_IdLote + "' AND F_IdFact='" + folio + "';");
                                        if (RLote.next()) {
                                            F_Id = RLote.getInt(1);
                                            F_Cant = RLote.getInt(2);
                                        }
                                        if (F_Cant > 0) {
                                            Facturar = Existencia + F_Cant;
                                            con.actualizar("update tb_facttemp set F_Cant='" + Facturar + "' where F_Id='" + F_Id + "' and F_IdLot='" + F_IdLote + "' AND F_IdFact='" + folio + "'");
                                        } else {
                                            con.actualizar("insert into tb_facttemp values ('" + folio + "','" + cliente + "','" + F_IdLote + "','" + Existencia + "','" + FechaEnt + "','3',0,'" + usua + "','" + Existencia + "','0','" + nombrePAQ + "','" + DirecPAQ + "','" + Hora + "','" + Observaciones + "','',NOW(),'')");
                                        }
                                        Solicitado = Diferencia;
                                        Facturar = 0;
                                    } else {
                                        ResultSet RLote = con.consulta("SELECT F_Id,F_Cant FROM tb_facttemp WHERE F_IdLot='" + F_IdLote + "' AND F_IdFact='" + folio + "';");
                                        if (RLote.next()) {
                                            F_Id = RLote.getInt(1);
                                            F_Cant = RLote.getInt(2);
                                        }
                                        if (F_Cant > 0) {
                                            Facturar = Solicitado + F_Cant;
                                            con.actualizar("update tb_facttemp set F_Cant='" + Facturar + "' where F_Id='" + F_Id + "' and F_IdLot='" + F_IdLote + "' AND F_IdFact='" + folio + "'");
                                        } else {
                                            con.actualizar("insert into tb_facttemp values ('" + folio + "','" + cliente + "','" + F_IdLote + "','" + Solicitado + "','" + FechaEnt + "','3',0,'" + usua + "','" + Solicitado + "','0','" + nombrePAQ + "','" + DirecPAQ + "','" + Hora + "','" + Observaciones + "','',NOW(),'')");
                                        }
                                        Solicitado = 0;
                                        Facturar = 0;
                                    }
                                }
                            }
                        }
                    }
                    sesion.setAttribute("ClaCliFM", F_CLaCli2);
                    sesion.setAttribute("ClaProFM", "");
                    sesion.setAttribute("DesPro", "");
                    sesion.setAttribute("DesProFM", "");
                    sesion.setAttribute("ExistenciaFM", "");
                    response.sendRedirect("pedidoManual2.jsp");
                    /*
                     out.println(" <script>window.open('reportes/multiplesRemis.jsp?Impresora="+Impresora+"&User="+sesion.getAttribute("nombre")+"', '', 'width=1200,height=800,left=50,top=50,toolbar=no'); </script>");
                     out.println("<script>window.history.back()</script>");
                     out.println("remisionesReImp:" +remisionesReImp);
                     */
                } else {
                    out.println("<script>alert('Seecciona Lote')</script>");
                    out.println("<script>window.history.back()</script>");
                }
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(PedidoManual.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        //Transferir cantidad
        if (accion.equals("CantidadTrans")) {
            try {
                con.conectar();
                int Exist = 0, folio = 0, CanTrans = 0, ConEnt = 0, ConSal = 0;
                String Entrar = "", ClaveSel = "", NombreU = "";

                String F_CLaCli = (String) sesion.getAttribute("ClaCliFM");
                String F_CLaCli2 = (String) sesion.getAttribute("ClaCliFM");

                String FechaEnt = request.getParameter("FechaEnt");
                String usua = request.getParameter("usua");
                Exist = Integer.parseInt(request.getParameter("Exist"));
                folio = Integer.parseInt(request.getParameter("folio"));
                CanTrans = Integer.parseInt(request.getParameter("CanTrans"));
                Entrar = request.getParameter("ClaveSel2");
                ClaveSel = request.getParameter("ClaveSel");

                String EntrarP = Entrar.substring(0, 2);
                String SalidaP = ClaveSel.substring(0, 2);

                if (EntrarP.equals(SalidaP)) {
                    out.println("<script>alert('No se puede Transferir, Favor de verificar las Claves')</script>");
                    out.println("<script>window.history.back()</script>");
                } else {

                    if ((EntrarP.equals("GB")) && (SalidaP.equals("PR"))) {
                        ConEnt = 5;
                        ConSal = 53;
                    } else {
                        ConEnt = 4;
                        ConSal = 52;
                    }

                    ResultSet User = con.consulta("SELECT F_Usuario FROM tb_usuariosisem WHERE F_IdUsu='" + usua + "'");
                    if (User.next()) {
                        NombreU = User.getString(1);
                    }

                    System.out.println("Exist--" + Exist + "CanTrans--" + CanTrans);

                    if (CanTrans > 0) {

                        if (CanTrans > Exist) {
                            out.println("<script>alert('Cantidad Transferir es Mayor al Existente')</script>");
                            out.println("<script>window.history.back()</script>");
                        } else {
                            int F_IdLote = 0, F_ExiLot = 0, FacTem = 0, Existen = 0, Restante = 0, F_FolLot = 0, F_IdLoteL = 0, F_ExiLotL = 0, Suma = 0, IndiceL = 0;
                            int IndiceT = 0;
                            double CostoSL = 0.0, CostoEL = 0.0, MontoS = 0.0, MontoE = 0.0;
                            String F_Ubica = "", F_FolLotL = "";
                            ResultSet Clave = con.consulta("SELECT F_IdLote,F_ExiLot,F_Ubica,F_ClaLot,F_FecCad,F_ClaOrg,F_FecFab,F_Cb,F_ClaMar,F_Origen,F_ClaPrv,F_FolLot FROM tb_lote WHERE F_ClaPro='" + ClaveSel + "' AND F_ExiLot>0 ORDER BY F_FecCad,F_ClaLot ASC");
                            while (Clave.next()) {
                                F_IdLote = Clave.getInt(1);
                                F_ExiLot = Clave.getInt(2);
                                F_Ubica = Clave.getString(3);

                                ResultSet Fact = con.consulta("SELECT SUM(F_Cant) FROM tb_facttemp WHERE F_IdLot='" + F_IdLote + "' AND  F_StsFact <5");
                                if (Fact.next()) {
                                    FacTem = Fact.getInt(1);
                                }
                                Existen = F_ExiLot - FacTem;

                                if (Existen > 0) {
                                    if (CanTrans >= Existen) {
                                        Restante = CanTrans - Existen;

                                        con.actualizar("UPDATE tb_lote SET F_ExiLot='0' WHERE F_IdLote='" + F_IdLote + "'");

                                        ResultSet FolioL = con.consulta("SELECT F_FolLot,F_IdLote,F_ExiLot FROM tb_lote WHERE F_ClaPro='" + Entrar + "' AND F_ClaLot='" + Clave.getString(4) + "' AND F_FecCad='" + Clave.getString(5) + "'");
                                        if (FolioL.next()) {
                                            F_FolLotL = FolioL.getString(1);
                                            //F_IdLoteL = FolioL.getInt(2);
                                            //F_ExiLotL = FolioL.getInt(3);
                                        }

                                        ResultSet CostoS = con.consulta("SELECT F_Costo FROM tb_medica WHERE F_ClaPro='" + ClaveSel + "'");
                                        if (CostoS.next()) {
                                            CostoSL = CostoS.getDouble(1);
                                        }
                                        MontoS = CostoSL * Existen;

                                        ResultSet CostoE = con.consulta("SELECT F_Costo FROM tb_medica WHERE F_ClaPro='" + Entrar + "'");
                                        if (CostoE.next()) {
                                            CostoEL = CostoE.getDouble(1);
                                        }
                                        MontoE = CostoEL * Existen;

                                        if (F_FolLotL != "") {

                                            ResultSet UbiFol = con.consulta("SELECT F_FolLot,F_IdLote,F_ExiLot FROM tb_lote WHERE F_FolLot='" + F_FolLotL + "' and F_Ubica='" + F_Ubica + "'");
                                            if (UbiFol.next()) {
                                                F_IdLoteL = UbiFol.getInt(2);
                                                F_ExiLotL = UbiFol.getInt(3);
                                            }

                                            if (F_IdLoteL != 0) {
                                                Suma = F_ExiLotL + Existen;
                                                con.actualizar("UPDATE tb_lote SET F_ExiLot='" + Suma + "' WHERE F_IdLote='" + F_IdLoteL + "'");
                                                con.actualizar("INSERT INTO tb_movinv VALUES(0,CURDATE(),'0','" + ConSal + "','" + ClaveSel + "','" + Existen + "','" + CostoSL + "','" + MontoS + "','-1','" + Clave.getString(12) + "','" + F_Ubica + "','" + Clave.getString(11) + "',curtime(),'" + NombreU + "');");
                                                con.actualizar("INSERT INTO tb_movinv VALUES(0,CURDATE(),'0','" + ConEnt + "','" + Entrar + "','" + Existen + "','" + CostoEL + "','" + MontoE + "','1','" + Clave.getString(12) + "','" + F_Ubica + "','" + Clave.getString(11) + "',curtime(),'" + NombreU + "');");

                                                CanTrans = Restante;

                                            } else {
                                                ResultSet Indice = con.consulta("SELECT F_IndLote FROM tb_indice");
                                                if (Indice.next()) {
                                                    IndiceL = Indice.getInt(1);
                                                }
                                                IndiceT = IndiceL + 1;

                                                con.insertar("INSERT INTO tb_lote VALUES(0,'" + Entrar + "','" + Clave.getString(4) + "','" + Clave.getString(5) + "','" + Existen + "','" + IndiceL + "','" + Clave.getString(6) + "','" + F_Ubica + "','" + Clave.getString(7) + "','" + Clave.getString(8) + "','" + Clave.getString(9) + "','" + Clave.getString(10) + "','" + Clave.getString(11) + "','0')");

                                                con.actualizar("UPDATE tb_indice SET F_IndLote='" + IndiceT + "'");

                                                con.actualizar("INSERT INTO tb_movinv VALUES(0,CURDATE(),'0','" + ConSal + "','" + ClaveSel + "','" + Existen + "','" + CostoSL + "','" + MontoS + "','-1','" + Clave.getString(12) + "','" + F_Ubica + "','" + Clave.getString(11) + "',curtime(),'" + NombreU + "');");
                                                con.actualizar("INSERT INTO tb_movinv VALUES(0,CURDATE(),'0','" + ConEnt + "','" + Entrar + "','" + Existen + "','" + CostoEL + "','" + MontoE + "','1','" + Clave.getString(12) + "','" + F_Ubica + "','" + Clave.getString(11) + "',curtime(),'" + NombreU + "');");

                                                CanTrans = Restante;
                                            }

                                        } else {
                                            ResultSet Indice = con.consulta("SELECT F_IndLote FROM tb_indice");
                                            if (Indice.next()) {
                                                IndiceL = Indice.getInt(1);
                                            }
                                            IndiceT = IndiceL + 1;

                                            con.insertar("INSERT INTO tb_lote VALUES(0,'" + Entrar + "','" + Clave.getString(4) + "','" + Clave.getString(5) + "','" + Existen + "','" + IndiceL + "','" + Clave.getString(6) + "','" + F_Ubica + "','" + Clave.getString(7) + "','" + Clave.getString(8) + "','" + Clave.getString(9) + "','" + Clave.getString(10) + "','" + Clave.getString(11) + "','0')");

                                            con.actualizar("UPDATE tb_indice SET F_IndLote='" + IndiceT + "'");

                                            con.actualizar("INSERT INTO tb_movinv VALUES(0,CURDATE(),'0','" + ConSal + "','" + ClaveSel + "','" + Existen + "','" + CostoSL + "','" + MontoS + "','-1','" + Clave.getString(12) + "','" + F_Ubica + "','" + Clave.getString(11) + "',curtime(),'" + NombreU + "');");
                                            con.actualizar("INSERT INTO tb_movinv VALUES(0,CURDATE(),'0','" + ConEnt + "','" + Entrar + "','" + Existen + "','" + CostoEL + "','" + MontoE + "','1','" + Clave.getString(12) + "','" + F_Ubica + "','" + Clave.getString(11) + "',curtime(),'" + NombreU + "');");

                                            CanTrans = Restante;
                                        }

                                    } else {

                                        Restante = Existen - CanTrans;
                                        con.actualizar("UPDATE tb_lote SET F_ExiLot='" + Restante + "' WHERE F_IdLote='" + F_IdLote + "'");

                                        ResultSet FolioL = con.consulta("SELECT F_FolLot,F_IdLote,F_ExiLot FROM tb_lote WHERE F_ClaPro='" + Entrar + "' AND F_ClaLot='" + Clave.getString(4) + "' AND F_FecCad='" + Clave.getString(5) + "'");
                                        if (FolioL.next()) {
                                            F_FolLotL = FolioL.getString(1);
                                            //F_IdLoteL = FolioL.getInt(2);
                                            //F_ExiLotL = FolioL.getInt(3);
                                        }

                                        ResultSet UbiFol = con.consulta("SELECT F_FolLot,F_IdLote,F_ExiLot FROM tb_lote WHERE F_FolLot='" + F_FolLotL + "' and F_Ubica='" + F_Ubica + "'");
                                        if (UbiFol.next()) {
                                            F_IdLoteL = UbiFol.getInt(2);
                                            F_ExiLotL = UbiFol.getInt(3);
                                        }
                                        ResultSet CostoS = con.consulta("SELECT F_Costo FROM tb_medica WHERE F_ClaPro='" + ClaveSel + "'");
                                        if (CostoS.next()) {
                                            CostoSL = CostoS.getDouble(1);
                                        }
                                        MontoS = CostoSL * CanTrans;

                                        ResultSet CostoE = con.consulta("SELECT F_Costo FROM tb_medica WHERE F_ClaPro='" + Entrar + "'");
                                        if (CostoE.next()) {
                                            CostoEL = CostoE.getDouble(1);
                                        }
                                        MontoE = CostoEL * CanTrans;

                                        if (F_FolLotL != "") {
                                            if (F_IdLoteL != 0) {
                                                Suma = F_ExiLotL + CanTrans;
                                                con.actualizar("UPDATE tb_lote SET F_ExiLot='" + Suma + "' WHERE F_IdLote='" + F_IdLoteL + "'");

                                                con.actualizar("INSERT INTO tb_movinv VALUES(0,CURDATE(),'0','" + ConSal + "','" + ClaveSel + "','" + CanTrans + "','" + CostoSL + "','" + MontoS + "','-1','" + Clave.getString(12) + "','" + F_Ubica + "','" + Clave.getString(11) + "',curtime(),'" + NombreU + "');");
                                                con.actualizar("INSERT INTO tb_movinv VALUES(0,CURDATE(),'0','" + ConEnt + "','" + Entrar + "','" + CanTrans + "','" + CostoEL + "','" + MontoE + "','1','" + Clave.getString(12) + "','" + F_Ubica + "','" + Clave.getString(11) + "',curtime(),'" + NombreU + "');");

                                                CanTrans = 0;
                                            } else {
                                                ResultSet Indice = con.consulta("SELECT F_IndLote FROM tb_indice");
                                                if (Indice.next()) {
                                                    IndiceL = Indice.getInt(1);
                                                }
                                                IndiceT = IndiceL + 1;

                                                con.insertar("INSERT INTO tb_lote VALUES(0,'" + Entrar + "','" + Clave.getString(4) + "','" + Clave.getString(5) + "','" + CanTrans + "','" + IndiceL + "','" + Clave.getString(6) + "','" + F_Ubica + "','" + Clave.getString(7) + "','" + Clave.getString(8) + "','" + Clave.getString(9) + "','" + Clave.getString(10) + "','" + Clave.getString(11) + "','0')");

                                                con.actualizar("UPDATE tb_indice SET F_IndLote='" + IndiceT + "'");

                                                con.actualizar("INSERT INTO tb_movinv VALUES(0,CURDATE(),'0','" + ConSal + "','" + ClaveSel + "','" + CanTrans + "','" + CostoSL + "','" + MontoS + "','-1','" + Clave.getString(12) + "','" + F_Ubica + "','" + Clave.getString(11) + "',curtime(),'" + NombreU + "');");
                                                con.actualizar("INSERT INTO tb_movinv VALUES(0,CURDATE(),'0','" + ConEnt + "','" + Entrar + "','" + CanTrans + "','" + CostoEL + "','" + MontoE + "','1','" + Clave.getString(12) + "','" + F_Ubica + "','" + Clave.getString(11) + "',curtime(),'" + NombreU + "');");

                                                CanTrans = 0;

                                            }

                                        } else {
                                            ResultSet Indice = con.consulta("SELECT F_IndLote FROM tb_indice");
                                            if (Indice.next()) {
                                                IndiceL = Indice.getInt(1);
                                            }
                                            IndiceT = IndiceL + 1;

                                            con.insertar("INSERT INTO tb_lote VALUES(0,'" + Entrar + "','" + Clave.getString(4) + "','" + Clave.getString(5) + "','" + CanTrans + "','" + IndiceL + "','" + Clave.getString(6) + "','" + F_Ubica + "','" + Clave.getString(7) + "','" + Clave.getString(8) + "','" + Clave.getString(9) + "','" + Clave.getString(10) + "','" + Clave.getString(11) + "','0')");

                                            con.actualizar("UPDATE tb_indice SET F_IndLote='" + IndiceT + "'");

                                            con.actualizar("INSERT INTO tb_movinv VALUES(0,CURDATE(),'0','" + ConSal + "','" + ClaveSel + "','" + CanTrans + "','" + CostoSL + "','" + MontoS + "','-1','" + Clave.getString(12) + "','" + F_Ubica + "','" + Clave.getString(11) + "',curtime(),'" + NombreU + "');");
                                            con.actualizar("INSERT INTO tb_movinv VALUES(0,CURDATE(),'0','" + ConEnt + "','" + Entrar + "','" + CanTrans + "','" + CostoEL + "','" + MontoE + "','1','" + Clave.getString(12) + "','" + F_Ubica + "','" + Clave.getString(11) + "',curtime(),'" + NombreU + "');");

                                            CanTrans = 0;
                                        }
                                    }
                                }

                            }

                            sesion.setAttribute("ClaCliFM", F_CLaCli2);
                            sesion.setAttribute("ClaProFM", "");
                            sesion.setAttribute("DesPro", "");
                            sesion.setAttribute("DesProFM", "");
                            sesion.setAttribute("ExistenciaFM", "");
                            response.sendRedirect("pedidoManual2.jsp");
                            //response.sendRedirect("PedidoManual2.jsp");
                        }
                    } else {
                        out.println("<script>alert('Cantidad Transferir debe se Mayor a 0')</script>");
                        out.println("<script>window.history.back()</script>");
                    }
                }
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(PedidoManual.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        // Seleccionar Lotes
        if (accion.equals("SeleccionaLote")) {

            int Cantidad = 0, Existencias = 0;
            String ClaveSel = "";
            response.setContentType("text/html");

            Cantidad = Integer.parseInt(request.getParameter("Cantidad"));
            ClaveSel = request.getParameter("ClaveSel");
            Existencias = Integer.parseInt(request.getParameter("ClaveSel"));
            System.out.println("Cant:" + Cantidad + "Exi:" + Existencias);
            if (Cantidad > Existencias) {
                out.println("<script>alert('Cantidad Solicitada es Mayor al Existente')</script>");
                out.println("<script>window.history.back()</script>");
            } else {
                //out.println("<script>alert('Cantidad Solicitada es Menor al Existente')</script>");
                request.setAttribute("Cantidad", request.getParameter("Cantidad"));
                request.getRequestDispatcher("facturacionManualLote.jsp").forward(request, response);
            }

            //request.getRequestDispatcher("facturacion/facturacionManualSelecLote2.jsp").forward(request, response);
        }

        // Seleccionar Lotes
        if (accion.equals("Transferir")) {
            int Cantidad = 0, Existencias = 0;
            String ClaveSel = "";
            response.setContentType("text/html");
            request.getRequestDispatcher("Transferir.jsp").forward(request, response);
        }

        //agrega el insumo con la cantidad indicada
        if (accion.equals("agregar")) {
            String claPro, cliente, fechaEnt, F_IndGlobal, usuario;
            int piezas, existencia, diferencia;

            piezas = Integer.parseInt(request.getParameter("Cantidad"));//cantidad del pedido
            usuario = (String) sesion.getAttribute("Usuario");//usuario que solicita el pedido
            claPro = (String) sesion.getAttribute("ClaProFM");//clave del producto seleccionado
            fechaEnt = (String) sesion.getAttribute("FechaEntFM");//fecha de la entrega
            cliente = (String) sesion.getAttribute("ClaGenCliFM");// cliente del pedido
            F_IndGlobal = (String) sesion.getAttribute("F_IndGlobal"); // inidice del pedido

            try {

                con.conectar();

                /**
                 * Se genera un duplicado de la tabla tb_lote para generar el
                 * proceso de descuento en ella
                 */
                con.insertar("delete from tb_lotetemp");
                con.insertar("insert into tb_lotetemp select * from tb_lote");

                /**
                 * Se leen los insusmo y las cantidades solicitadas en tb_unireq
                 */
                //INICIO DE CONSULTA MYSQL
                /**
                 * Búsqueda del insumo y sus ubicaciones tomando en cuenta
                 * primeras caducidades
                 */
                String consulta = "SELECT L.F_FecCad AS F_FecCad,L.F_FolLot AS F_FolLot,(L.F_ExiLot) AS F_ExiLot, M.F_TipMed AS F_TipMed, M.F_Costo AS F_Costo, L.F_Ubica AS F_Ubica, C.F_ProVee AS F_ProVee, F_ClaLot,F_IdLote FROM tb_lotetemp L INNER JOIN tb_medica M ON L.F_ClaPro=M.F_ClaPro INNER JOIN tb_compra C ON L.F_FolLot=C.F_Lote WHERE L.F_ClaPro='" + claPro + "' AND L.F_ExiLot>'0' AND L.F_Ubica NOT IN ('REJA_DEVOL','PROX_CADUCA')  GROUP BY L.F_IdLote ORDER BY L.F_FecCad,L.F_IdLote ASC";
                ResultSet rsBusquedaInsumo = con.consulta(consulta);
                while (rsBusquedaInsumo.next()) {
                    /**
                     * Mientras que lo solicitado siga siendo mayor que 0
                     */
                    if (piezas > 0) {
                        /**
                         * Obtenemos información de la tabla de lote
                         */
                        String IdLote = rsBusquedaInsumo.getString("F_IdLote");
                        existencia = Integer.parseInt(rsBusquedaInsumo.getString("F_ExiLot"));
                        ResultSet rset2 = con.consulta("select sum(F_Cant) from tb_facttemp where F_IdLot = '" + IdLote + "' and F_StsFact!=5");
                        /**
                         * Se calcula la existencia restando lo que se tiene
                         * apartado para ese F_IdLote
                         */
                        while (rset2.next()) {
                            existencia = existencia - rset2.getInt(1);
                        }

                        if (existencia > 0) {
                            if (piezas > existencia) {
                                /**
                                 * Si las piezas que se solicitan son más de las
                                 * que se tienen en la ubicacion encontrada se
                                 * atualizarán las exitencias a 0 se tomarán
                                 * todas y se insertará en la tabla tb_facttemp
                                 * la referencia la diferencia se convertirá en
                                 * lo restante de la clave
                                 */
                                diferencia = piezas - existencia;
                                con.actualizar("UPDATE tb_lotetemp SET F_ExiLot='0' WHERE F_IdLote='" + IdLote + "'");
                                con.insertar("insert into tb_facttemp values('" + F_IndGlobal + "','" + cliente + "','" + IdLote + "','" + existencia + "','" + fechaEnt + "','3','0','" + usuario + "','0','0')");
                                piezas = diferencia;
                            } else {
                                /**
                                 * Si la existencia es mayor a lo solicitado se
                                 * actualiza a la diferencia, se inserta en
                                 * facttemp lo solicitado y las piezas se
                                 * vuelven 0
                                 */
                                diferencia = existencia - piezas;
                                if (diferencia >= 0) {

                                    if (piezas >= 0) {
                                        con.insertar("INSERT INTO tb_facttemp VALUES('" + F_IndGlobal + "','" + cliente + "','" + IdLote + "','" + piezas + "','" + fechaEnt + "','3','0','" + usuario + "','0','0')");
                                        con.actualizar("UPDATE tb_lotetemp SET F_ExiLot='" + diferencia + "' WHERE F_IdLote='" + IdLote + "'");
                                    }
                                }
                                piezas = 0;
                            }
                        }
                        //}
                    }
                }

                /**
                 * Se va actualizando cada registro a F_Status 2 para indicar
                 * que fué procesado correctamente
                 */
                con.cierraConexion();
                out.println("<script>window.location='pedidoManual2.jsp'</script>");

            } catch (SQLException e) {
                System.out.println(e);
                System.out.println(e.getLocalizedMessage());
            } catch (NumberFormatException e) {
                System.out.println(e);
                System.out.println(e.getLocalizedMessage());
            }
        }
        /**
         * **MODIFICA STATUS Y NO GUÍA***
         */
        if (accion.equals("CambiO")) {
            try {
                con.conectar();
                String folio = request.getParameter("Folio");
                String sts = request.getParameter("Envio1");
                String guia = request.getParameter("Guia1");
                String FechaCli = request.getParameter("fechacli");
                if (!(sts == "") || (!(guia == "")) || (!(FechaCli == ""))) {
                    if (!(sts == "")) {
                        con.actualizar("update tb_foliosval set F_StsEnvio='" + sts + "' where F_FolVal='" + folio + "'");
                    }
                    if (!(guia == "")) {
                        con.actualizar("update tb_foliosval set F_Guia='" + guia + "' where F_FolVal='" + folio + "'");
                    }
                    if (!(FechaCli == "")) {
                        con.actualizar("update tb_foliosval set F_FecCliente='" + FechaCli + "' where F_FolVal='" + folio + "'");
                    }
                    out.println("<script>window.location='verPedidosVal.jsp'</script>");
                } else {
                    out.println("<script>alert('Ingrese Datos')</script>");
                    out.println("<script>window.history.back()</script>");
                }

                con.cierraConexion();

            } catch (Exception e) {
            }

        }
        /**
         * * FIN CAMBIO **
         */

        /**
         * **MODIFICA CAJAS KG***
         */
        if (accion.equals("CambiOCKG")) {
            try {
                con.conectar();
                String folio = request.getParameter("Folio");
                String cajas = request.getParameter("Cajas2");
                String kg = request.getParameter("KG2");
                String Sts = request.getParameter("STS2");

                if (!(cajas == "") || (!(kg == "")) || (!(Sts == ""))) {

                    if (cajas != "") {
                        con.actualizar("update tb_cajaskg set F_Cajas='" + cajas + "' where F_Cladoc='" + folio + "'");
                    }

                    if (kg != "") {
                        con.actualizar("update tb_cajaskg set F_Kg='" + kg + "' where F_Cladoc='" + folio + "'");
                    }

                    if (Sts != "") {
                        con.actualizar("update tb_cajaskg set F_Sts='" + Sts + "' where F_Cladoc='" + folio + "'");

                        if (Sts.equals("Entregado")) {
                            con.actualizar("update tb_cajaskg set F_Fecha=CURDATE(),F_Time=CURTIME() where F_Cladoc='" + folio + "'");
                        } else {
                            con.actualizar("update tb_cajaskg set F_Fecha='0000-00-00',F_Time='00:00:00' where F_Cladoc='" + folio + "'");
                        }
                    }

                    out.println("<script>window.location='verCajasKg.jsp'</script>");
                } else {
                    out.println("<script>alert('Ingrese Datos')</script>");
                    out.println("<script>window.history.back()</script>");
                }

                con.cierraConexion();

            } catch (Exception e) {
            }

        }
        /**
         * * FIN CAMBIO CAJAS KG**
         */

        System.out.println("action: " + accion);
        /**
         * *** Envío Correo Factura ****
         */
        if (accion.equals("EnvioCorreo")) {
            try {

                con.conectar();
                String folio = request.getParameter("Folio");
                String Factu = request.getParameter("Factu");
                //String Archivos = request.getParameter("file1");

                System.out.println("Fol:" + folio + " Fact:" + Factu);
                con.actualizar("UPDATE tb_foliosval SET F_FecFactura=NOW(),F_Factura='" + Factu + "' WHERE F_FolVal='" + folio + "';");
                CorreoEnvioFact.enviaCorreo(folio);
                //EnvioCorreoFact.enviaCorreo(folio);
                //out.println("<script>window.location='CorreoEnvioFact?pedido="+folio+"'</script>");
                out.println("<script>window.history.back()</script>");
                con.cierraConexion();
            } catch (Exception e) {
            }
        }
        /**
         * *** Fin Envío Correo Factura ****
         */
    }

    public int dameIndGlobal() throws SQLException {
        ConectionDB con = new ConectionDB();
        con.conectar();
        int FolioFactura = 0, FolFact;
        ResultSet FolioFact = con.consulta("SELECT F_IndGlobal FROM tb_indice");
        while (FolioFact.next()) {
            FolioFactura = Integer.parseInt(FolioFact.getString("F_IndGlobal"));
        }
        FolFact = FolioFactura + 1;
        con.actualizar("update tb_indice set F_IndGlobal='" + FolFact + "'");
        ResultSet rset = con.consulta("select MAX(F_IdFact) from tb_facttemp");
        int idMax = 0;
        while (rset.next()) {
            idMax = rset.getInt(1);
        }
        while (FolioFactura <= idMax) {
            FolioFactura++;
            FolFact = FolioFactura + 1;
            con.actualizar("update tb_indice set F_IndGlobal='" + FolFact + "'");
        }
        con.cierraConexion();
        return FolioFactura;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
