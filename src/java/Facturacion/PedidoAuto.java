/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Facturacion;

import Correo.CorreoPedido1;
import Modula.RequerimientoModula;
import conn.ConectionDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mario
 */
public class PedidoAuto extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String accion;
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        accion = request.getParameter("accion");
        ConectionDB con = new ConectionDB();
        HttpSession sesion = request.getSession();

        if (accion == null) {
            accion = "";
        }

        //Elimina insumo del pedido
        if (request.getParameter("accionEliminarIns") != null) {
            if (!request.getParameter("accionEliminarIns").equals("")) {
                try {
                    con.conectar();
                    ResultSet rset = con.consulta("select * from tb_facttemp where F_Id = '" + request.getParameter("accionEliminarIns") + "'");
                    while (rset.next()) {
                        con.insertar("insert into tb_facttemp_elim values ('" + rset.getString(1) + "','" + rset.getString(2) + "','" + rset.getString(3) + "','" + rset.getString(4) + "','" + rset.getString(5) + "','" + rset.getString(6) + "','" + rset.getString(7) + "', '" + (String) sesion.getAttribute("nombre") + "', NOW())");
                    }
                    con.insertar("delete from tb_facttemp where F_Id = '" + request.getParameter("accionEliminarIns") + "' ");
                    con.cierraConexion();
                    out.println("<script>alert('Clave Eliminada Correctamente')</script>");
                    out.println("<script>window.location='pedidoAutomatico.jsp'</script>");
                } catch (SQLException ex) {
                    Logger.getLogger(PedidoAuto.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        //cancela el pedido
        if (accion.equals("CancelarFactura")) {
            try {
                con.conectar();
                ResultSet rset = con.consulta("select * from tb_facttemp where F_IdFact = '" + request.getParameter("F_IndGlob") + "'");
                while (rset.next()) {
                    con.insertar("insert into tb_facttemp_elim values ('" + rset.getString(1) + "','" + rset.getString(2) + "','" + rset.getString(3) + "','" + rset.getString(4) + "','" + rset.getString(5) + "','" + rset.getString(6) + "','" + rset.getString(7) + "', '" + (String) sesion.getAttribute("nombre") + "', NOW())");
                }
                con.insertar("delete from tb_facttemp where F_ClaCLi = '" + request.getParameter("F_IndGlob") + "' ");
                con.cierraConexion();
                sesion.setAttribute("F_IndGlobal", null);

                //nuevo
                sesion.setAttribute("ClaGenCliFM", null);
                sesion.setAttribute("nombrePE", null);
                //
                out.println("<script>alert('Factura Eliminada Correctamente')</script>");
                out.println("<script>window.location='pedidoAutomatico.jsp'</script>");
            } catch (SQLException e) {
            }
        }

        //confirma el pedido
        if (accion.equals("ConfirmarFactura")) {
            try {
                con.conectar();
                int folio = Integer.parseInt((String) sesion.getAttribute("F_IndGlobal"));
                RequerimientoModula reqMod = new RequerimientoModula();
                con.insertar("update tb_facttemp set F_StsFact = '0' where F_IdFact = '" + folio + "' ");
                con.cierraConexion();
                CorreoPedido1.enviaCorreo(folio);

                sesion.setAttribute("F_IndGlobal", null);
                sesion.setAttribute("ClaCliFM", "");
                sesion.setAttribute("FechaEntFM", "");
                sesion.setAttribute("ClaProFM", "");
                sesion.setAttribute("DesProFM", "");
                sesion.setAttribute("ClaGenCliFM", null);
                sesion.setAttribute("nombrePE", null);
                response.sendRedirect("pedidoAutomatico.jsp");
            } catch (SQLException e) {
                System.out.println("ERROR SQL:" + e.getMessage());
            } catch (NumberFormatException e) {
                System.out.println("ERROR NumberFormatException:" + e.getMessage());
            } catch (IOException e) {
                System.out.println("ERROR IOException:" + e.getMessage());
            }
        }

        //busca los puntos de entrega
        if (accion.equals("buscarCliente")) {
            try {
                con.conectar();

                String clacliente = request.getParameter("ClaCli");
                List<String[]> claves = new ArrayList<String[]>();
                ResultSet rset = con.consulta("select F_ClaCli, F_Direc, F_ClaveGen,F_NomCli from tb_uniatn where F_ClaveGen = '" + clacliente + "'");
                while (rset.next()) {
                    String[] detalle = new String[2];
                    detalle[0] = rset.getString(1);
                    detalle[1] = rset.getString(2);
                    claves.add(detalle);
                    sesion.setAttribute("ClaCliFM", rset.getString("F_ClaveGen"));
                    sesion.setAttribute("DesCliFM", rset.getString("F_NomCli"));
                }
                request.setAttribute("puntosEntrega", claves);
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(PedidoAuto.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.getRequestDispatcher("pedidoAutomatico.jsp").forward(request, response);

        }

        //busca el insumo por clave
        if (accion.equals("buscar")) {

            int existencia = 0, cantTemp = 0, existenciaVirtual;

            try {
                /**
                 * Proceso de generación de Concentrado Global
                 */

                String F_IndGlobal = (String) sesion.getAttribute("F_IndGlobal");
                if (F_IndGlobal == null) {
                    sesion.setAttribute("F_IndGlobal", dameIndGlobal() + "");
                }
                con.conectar();
                ResultSet rset = con.consulta("SELECT m.F_ClaPro, m.F_DesPro, l.F_ClaLot, l.F_FolLot, DATE_FORMAT(l.F_FecCad, '%d/%m/%Y') FROM tb_medica m, tb_lote l WHERE m.F_ClaPro = l.F_ClaPro AND m.F_ClaPro = '" + request.getParameter("ClaPro") + "' GROUP BY m.F_ClaPro;");
                while (rset.next()) {
                    sesion.setAttribute("DesProFM", rset.getString(2));
                }
                ResultSet rset2 = con.consulta("SELECT l.F_ClaPro, l.F_ExiLot, ft.F_IdLot, l.F_IdLote, SUM(ft.F_Cant) apartado FROM tb_facttemp AS ft, tb_lote AS l WHERE ft.F_StsFact < 5 AND ft.F_IdLot = l.F_IdLote AND l.F_ClaPro='" + request.getParameter("ClaPro") + "' GROUP BY l.F_ClaPro");
                while (rset2.next()) {
                    cantTemp = rset2.getInt("apartado");
                }
                rset2 = con.consulta("SELECT SUM(l.F_ExiLot) from tb_lote as l WHERE l.F_ClaPro='" + request.getParameter("ClaPro") + "'");
                while (rset2.next()) {
                    existencia = rset2.getInt(1);
                }

                existenciaVirtual = existencia - cantTemp;
                System.out.println(existenciaVirtual + "=" + existencia + "-" + cantTemp);

                sesion.setAttribute("ExistenciaFM", existenciaVirtual + "");
                System.out.println("l"+sesion.getAttribute("ClaGenCliFM"));
                if (sesion.getAttribute("ClaGenCliFM") == null) {
                    sesion.setAttribute("ClaGenCliFM", request.getParameter("puntosEntrega"));
                }
                rset = con.consulta("select F_Direc from tb_uniatn where F_ClaCli = '" + sesion.getAttribute("ClaGenCliFM") + "'");
                while (rset.next()) {
                    sesion.setAttribute("nombrePE", rset.getString(1));
                }

                con.cierraConexion();
                //sesion.setAttribute("ClaCliFM", request.getParameter("ClaCli"));
                sesion.setAttribute("FechaEntFM", request.getParameter("FechaEnt"));
                sesion.setAttribute("ClaProFM", request.getParameter("ClaPro"));
                response.sendRedirect("pedidoAutomatico.jsp");

            } catch (SQLException ex) {
                Logger.getLogger(PedidoAuto.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        //selecciona la clave en caso de que una descripcion cuente con mas de una clave
        if (accion.equals("selectClave")) {

            int existencia = 0, cantTemp = 0, existenciaVirtual;

            try {
                /**
                 * Proceso de generación de Concentrado Global
                 */

                String F_IndGlobal = (String) sesion.getAttribute("F_IndGlobal");
                if (F_IndGlobal == null) {
                    sesion.setAttribute("F_IndGlobal", dameIndGlobal() + "");
                }
                con.conectar();
                ResultSet rset = con.consulta("SELECT m.F_ClaPro, m.F_DesPro, l.F_ClaLot, l.F_FolLot, DATE_FORMAT(l.F_FecCad, '%d/%m/%Y') FROM tb_medica m, tb_lote l WHERE m.F_ClaPro = l.F_ClaPro AND m.F_ClaPro = '" + request.getParameter("claves") + "' GROUP BY m.F_ClaPro;");
                while (rset.next()) {
                    sesion.setAttribute("DesProFM", rset.getString(2));
                }
                ResultSet rset2 = con.consulta("SELECT l.F_ClaPro, l.F_ExiLot, ft.F_IdLot, l.F_IdLote, SUM(ft.F_Cant) apartado FROM tb_facttemp AS ft, tb_lote AS l WHERE ft.F_StsFact < 5 AND ft.F_IdLot = l.F_IdLote AND l.F_ClaPro='" + request.getParameter("claves") + "' GROUP BY l.F_ClaPro");
                while (rset2.next()) {
                    cantTemp = rset2.getInt("apartado");
                }
                rset2 = con.consulta("SELECT SUM(l.F_ExiLot) from tb_lote as l WHERE l.F_ClaPro='" + request.getParameter("claves") + "'");
                while (rset2.next()) {
                    existencia = rset2.getInt(1);
                }

                existenciaVirtual = existencia - cantTemp;
                System.out.println(existenciaVirtual + "=" + existencia + "-" + cantTemp);

                sesion.setAttribute("ExistenciaFM", existenciaVirtual + "");

                con.cierraConexion();
                sesion.setAttribute("ClaProFM", request.getParameter("claves"));
                response.sendRedirect("pedidoAutomatico.jsp");

            } catch (SQLException ex) {
                Logger.getLogger(PedidoAuto.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        //busca el insumo por descripcion
        if (accion.equals("buscarDesc")) {

            int existencia = 0, cantTemp = 0, existenciaVirtual;
            sesion.setAttribute("DesProFM", "");
            sesion.setAttribute("ClaProFM", "");
            sesion.setAttribute("ExistenciaFM", "");

            try {
                /**
                 * Proceso de generación de Concentrado Global
                 */
                con.conectar();

                List<String> claves = new ArrayList<String>();
                ResultSet rset = con.consulta("select m.F_ClaPro, m.F_DesPro from tb_medica m,tb_lote l where m.F_ClaPro=l.F_ClaPro AND m.F_DesPro = '" + request.getParameter("descripcion") + "' GROUP BY m.F_ClaPro");
                while (rset.next()) {
                    claves.add(rset.getString(1));
                    sesion.setAttribute("clave", rset.getString(1));
                    sesion.setAttribute("descripcion", rset.getString(2));
                }
                int ban = 0;
                System.out.println("Claves_ :" + claves.size());
                if (claves.size() == 1) {
                    ban = 1;
                    String F_IndGlobal = (String) sesion.getAttribute("F_IndGlobal");
                    if (F_IndGlobal == null) {
                        sesion.setAttribute("F_IndGlobal", dameIndGlobal() + "");
                    }
                    con.conectar();
                    rset = con.consulta("SELECT m.F_ClaPro, m.F_DesPro, l.F_ClaLot, l.F_FolLot, DATE_FORMAT(l.F_FecCad, '%d/%m/%Y') FROM tb_medica m, tb_lote l WHERE m.F_ClaPro = l.F_ClaPro AND m.F_ClaPro = '" + sesion.getAttribute("clave") + "' GROUP BY m.F_ClaPro;");
                    while (rset.next()) {
                        sesion.setAttribute("ClaProFM", rset.getString(1));
                        sesion.setAttribute("DesProFM", rset.getString(2));
                    }
                    ResultSet rset2 = con.consulta("SELECT l.F_ClaPro, l.F_ExiLot, ft.F_IdLot, l.F_IdLote, SUM(ft.F_Cant) apartado FROM tb_facttemp AS ft, tb_lote AS l WHERE ft.F_StsFact < 5 AND ft.F_IdLot = l.F_IdLote AND l.F_ClaPro='" + sesion.getAttribute("clave") + "' GROUP BY l.F_ClaPro");
                    while (rset2.next()) {
                        cantTemp = rset2.getInt("apartado");
                    }
                    rset2 = con.consulta("SELECT SUM(l.F_ExiLot) from tb_lote as l WHERE l.F_ClaPro='" + sesion.getAttribute("clave") + "'");
                    while (rset2.next()) {
                        existencia = rset2.getInt(1);
                    }

                    existenciaVirtual = existencia - cantTemp;
                    System.out.println(existenciaVirtual + "=" + existencia + "-" + cantTemp);

                    sesion.setAttribute("ExistenciaFM", existenciaVirtual + "");

                    if (sesion.getAttribute("ClaGenCliFM") == null) {
                        sesion.setAttribute("ClaGenCliFM", request.getParameter("puntosEntrega"));
                    }
                    rset = con.consulta("select F_Direc from tb_uniatn where F_ClaCli = '" + sesion.getAttribute("ClaGenCliFM") + "'");
                    while (rset.next()) {
                        sesion.setAttribute("nombrePE", rset.getString(1));
                    }

                    con.cierraConexion();

                    sesion.setAttribute("FechaEntFM", request.getParameter("FechaEnt"));
                    response.sendRedirect("pedidoAutomatico.jsp");
                } else {
                    //mas de uno
                    System.out.println("qui");
                    request.setAttribute("masdeuno", "si");
                    request.setAttribute("claves", claves);

                    String F_IndGlobal = (String) sesion.getAttribute("F_IndGlobal");
                    if (F_IndGlobal == null) {
                        sesion.setAttribute("F_IndGlobal", dameIndGlobal() + "");
                    }

                    if (sesion.getAttribute("ClaGenCliFM") == null) {
                        sesion.setAttribute("ClaGenCliFM", request.getParameter("puntosEntrega"));
                    }
                    rset = con.consulta("select F_Direc from tb_uniatn where F_ClaCli = '" + sesion.getAttribute("ClaGenCliFM") + "'");
                    while (rset.next()) {
                        sesion.setAttribute("nombrePE", rset.getString(1));
                    }

                    con.cierraConexion();
                    //sesion.setAttribute("ClaCliFM", request.getParameter("ClaCli"));

                    sesion.setAttribute("FechaEntFM", request.getParameter("FechaEnt"));
                    request.getRequestDispatcher("pedidoAutomatico.jsp").forward(request, response);

                }

            } catch (SQLException ex) {
                Logger.getLogger(PedidoAuto.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        //agrega el insumo con la cantidad indicada
        if (accion.equals("agregar")) {
            String claPro, cliente, fechaEnt, F_IndGlobal, usuario;
            int piezas, existencia, diferencia;

            piezas = Integer.parseInt(request.getParameter("Cantidad"));//cantidad del pedido
            usuario = (String) sesion.getAttribute("Usuario");//usuario que solicita el pedido
            claPro = (String) sesion.getAttribute("ClaProFM");//clave del producto seleccionado
            fechaEnt = (String) sesion.getAttribute("FechaEntFM");//fecha de la entrega
            cliente = (String) sesion.getAttribute("ClaGenCliFM");// cliente del pedido
            F_IndGlobal = (String) sesion.getAttribute("F_IndGlobal"); // inidice del pedido

            try {

                con.conectar();

                /**
                 * Se genera un duplicado de la tabla tb_lote para generar el
                 * proceso de descuento en ella
                 */
                con.insertar("delete from tb_lotetemp");
                con.insertar("insert into tb_lotetemp select * from tb_lote");

                /**
                 * Se leen los insusmo y las cantidades solicitadas en tb_unireq
                 */
                //INICIO DE CONSULTA MYSQL
                /**
                 * Búsqueda del insumo y sus ubicaciones tomando en cuenta
                 * primeras caducidades
                 */
                String consulta = "SELECT L.F_FecCad AS F_FecCad,L.F_FolLot AS F_FolLot,(L.F_ExiLot) AS F_ExiLot, M.F_TipMed AS F_TipMed, M.F_Costo AS F_Costo, L.F_Ubica AS F_Ubica, C.F_ProVee AS F_ProVee, F_ClaLot,F_IdLote FROM tb_lotetemp L INNER JOIN tb_medica M ON L.F_ClaPro=M.F_ClaPro INNER JOIN tb_compra C ON L.F_FolLot=C.F_Lote WHERE L.F_ClaPro='" + claPro + "' AND L.F_ExiLot>'0' AND L.F_Ubica NOT IN ('REJA_DEVOL','PROX_CADUCA')  GROUP BY L.F_IdLote ORDER BY L.F_FecCad,L.F_IdLote ASC";
                ResultSet rsBusquedaInsumo = con.consulta(consulta);
                while (rsBusquedaInsumo.next()) {
                    /**
                     * Mientras que lo solicitado siga siendo mayor que 0
                     */
                    if (piezas > 0) {
                        /**
                         * Obtenemos información de la tabla de lote
                         */
                        String IdLote = rsBusquedaInsumo.getString("F_IdLote");
                        existencia = Integer.parseInt(rsBusquedaInsumo.getString("F_ExiLot"));
                        ResultSet rset2 = con.consulta("select sum(F_Cant) from tb_facttemp where F_IdLot = '" + IdLote + "' and F_StsFact!=5");
                        /**
                         * Se calcula la existencia restando lo que se tiene
                         * apartado para ese F_IdLote
                         */
                        while (rset2.next()) {
                            existencia = existencia - rset2.getInt(1);
                        }

                        if (existencia > 0) {
                            if (piezas > existencia) {
                                /**
                                 * Si las piezas que se solicitan son más de las
                                 * que se tienen en la ubicacion encontrada se
                                 * atualizarán las exitencias a 0 se tomarán
                                 * todas y se insertará en la tabla tb_facttemp
                                 * la referencia la diferencia se convertirá en
                                 * lo restante de la clave
                                 */
                                diferencia = piezas - existencia;
                                con.actualizar("UPDATE tb_lotetemp SET F_ExiLot='0' WHERE F_IdLote='" + IdLote + "'");
                                con.insertar("insert into tb_facttemp values('" + F_IndGlobal + "','" + cliente + "','" + IdLote + "','" + existencia + "','" + fechaEnt + "','3','0','" + usuario + "','0','0')");
                                piezas = diferencia;
                            } else {
                                /**
                                 * Si la existencia es mayor a lo solicitado se
                                 * actualiza a la diferencia, se inserta en
                                 * facttemp lo solicitado y las piezas se
                                 * vuelven 0
                                 */
                                diferencia = existencia - piezas;
                                if (diferencia >= 0) {

                                    if (piezas >= 0) {
                                        con.insertar("INSERT INTO tb_facttemp VALUES('" + F_IndGlobal + "','" + cliente + "','" + IdLote + "','" + piezas + "','" + fechaEnt + "','3','0','" + usuario + "','0','0')");
                                        con.actualizar("UPDATE tb_lotetemp SET F_ExiLot='" + diferencia + "' WHERE F_IdLote='" + IdLote + "'");
                                    }
                                }
                                piezas = 0;
                            }
                        }
                        //}
                    }
                }

                /**
                 * Se va actualizando cada registro a F_Status 2 para indicar
                 * que fué procesado correctamente
                 */
                con.cierraConexion();
                out.println("<script>window.location='pedidoAutomatico.jsp'</script>");

            } catch (SQLException e) {
                System.out.println(e);
                System.out.println(e.getLocalizedMessage());
            } catch (NumberFormatException e) {
                System.out.println(e);
                System.out.println(e.getLocalizedMessage());
            }
        }

    }

    public int dameIndGlobal() throws SQLException {
        ConectionDB con = new ConectionDB();
        con.conectar();
        int FolioFactura = 0, FolFact;
        ResultSet FolioFact = con.consulta("SELECT F_IndGlobal FROM tb_indice");
        while (FolioFact.next()) {
            FolioFactura = Integer.parseInt(FolioFact.getString("F_IndGlobal"));
        }
        FolFact = FolioFactura + 1;
        con.actualizar("update tb_indice set F_IndGlobal='" + FolFact + "'");
        ResultSet rset = con.consulta("select MAX(F_IdFact) from tb_facttemp");
        int idMax = 0;
        while (rset.next()) {
            idMax = rset.getInt(1);
        }
        while (FolioFactura <= idMax) {
            FolioFactura++;
            FolFact = FolioFactura + 1;
            con.actualizar("update tb_indice set F_IndGlobal='" + FolFact + "'");
        }
        con.cierraConexion();
        return FolioFactura;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
