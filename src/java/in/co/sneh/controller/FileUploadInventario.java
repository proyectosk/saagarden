package in.co.sneh.controller;

import LeeExcel.LeeExcelInventario;
import in.co.sneh.model.FileUpload;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import java.io.InputStream;
import java.io.PrintWriter;
import org.apache.commons.fileupload.FileItemIterator;

/**
 *
 * @author Mario
 */
public class FileUploadInventario extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LeeExcelInventario lee = new LeeExcelInventario();
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        boolean isMultiPart = ServletFileUpload.isMultipartContent(request);
        if (isMultiPart) {
            ServletFileUpload upload = new ServletFileUpload();
            try {
                FileItemIterator itr = upload.getItemIterator(request);
                while (itr.hasNext()) {
                    FileItemStream item = itr.next();
                    if (item.isFormField()) {
                        /**
                         * Si es de un campo (que no sea el del archivo)
                         */
                        String fielName = item.getFieldName();
                        InputStream is = item.openStream();
                        byte[] b = new byte[is.available()];
                        is.read(b);
                        String value = new String(b);
                        response.getWriter().println(fielName + ":" + value + "<br/>");
                    } else {
                        /**
                         * Si es del archivo
                         */
                        String path = getServletContext().getRealPath("/");
                        if (FileUpload.processFile(path, item)) {
                            if (lee.obtieneArchivo(path, item.getName())) {
                                out.println("<script>alert('Se cargó el Excel Correctamente')</script>");
                                out.println("<script>window.location='inventarioExcel.jsp'</script>");
                            }
                        }
                    }
                }
            } catch (FileUploadException fue) {
            }
            out.println("<script>alert('No se pudo cargar el Folio, verifique las celdas')</script>");
            out.println("<script>window.location='inventarioExcel.jsp'</script>");
        }
    }
}
