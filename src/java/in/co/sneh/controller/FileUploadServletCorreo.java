/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.co.sneh.controller;

import LeeExcel.LeeExcel;
import conn.ConectionDB;
import in.co.sneh.model.FileUploadCorreo;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import java.io.InputStream;
import java.io.PrintWriter;
import org.apache.commons.fileupload.FileItemIterator;

/**
 *
 * @author CEDIS TOLUCA3
 */
public class FileUploadServletCorreo extends HttpServlet {    
    ConectionDB con = new ConectionDB();
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //ConectionDB con = new ConectionDB();
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        //String Unidad = "";

        boolean isMultiPart = ServletFileUpload.isMultipartContent(request);
        if (isMultiPart) {
            ServletFileUpload upload = new ServletFileUpload();
            try {
               String Folio = "", Factura = "", Archivos = "", Folio1 = "", Factura1 = "", Archivos1 = "";
               
                FileItemIterator itr = upload.getItemIterator(request);
                while (itr.hasNext()) {
                    FileItemStream item = itr.next();
                    if (item.isFormField()) {
                        /**
                         * Si es de un campo (que no sea el del archivo)
                         */
                        String fielName = item.getFieldName();
                        InputStream is = item.openStream();
                        byte[] b = new byte[is.available()];
                        is.read(b);
                        String value = new String(b);
                        response.getWriter().println(fielName + ":" + value + "<br/>");
                        
                        
                        if(Folio == ""){
                            if(fielName.equals("Folio")){Folio = value;}
                        }
                         if(Factura == ""){
                             if(fielName.equals("Factu")){Factura = value;}
                         }
                         if(Archivos == ""){
                             if(fielName.equals("ima1")){Archivos = value;}
                         }
                         
                        /**
                         * Si es del archivo
                         */
                        if(Folio != ""){
                            Folio1 = Folio;
                        }
                        if(Factura !=""){
                            Factura1 = Factura;
                        }
                        if(Archivos !=""){
                            Archivos1 = Archivos;
                        }
                        String path = getServletContext().getRealPath("/");
                        if (FileUploadCorreo.processFile(path, item)) {
                            //response.getWriter().println("file uploaded successfully");
                             try{
                                    con.conectar();
                                    if(Archivos1 !=""){
                                        con.insertar("INSERT INTO tb_imagenes VALUES('"+Folio1+"','"+Factura1+"','"+Archivos1+"',0)");
                                    }
                                    con.cierraConexion();
                                }catch(Exception e){

                                }
                                out.println("<script>alert('Se envío Correctamente el Correo')</script>");
                                out.println("<script>window.location='verPedidosFacturarItu.jsp'</script>");
                            
                            //response.sendRedirect("cargaFotosCensos.jsp");
                        } else {
                            //response.getWriter().println("file uploading falied");
                            //response.sendRedirect("cargaFotosCensos.jsp");
                        }
                    }
                }
            } catch (FileUploadException fue) {
            }
            out.println("<script>alert('No se pudo enviar el correo')</script>");
            out.println("<script>window.location='verPedidosFacturarItu.jsp'</script>");
            //response.sendRedirect("carga.jsp");
        }

        /*
         * Para insertar el excel en tablas
         */
    }
}
