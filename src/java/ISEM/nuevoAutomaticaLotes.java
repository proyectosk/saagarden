/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ISEM;

import Correo.CorreoOCAbierto;
import conn.ConectionDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Americo
 */
public class nuevoAutomaticaLotes extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     *
     * Clase para ingresar compras.
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ConectionDB con = new ConectionDB();
        CorreoOCAbierto correo = new CorreoOCAbierto();
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession sesion = request.getSession(true);
        int F_IndCom = 0;
        try {
            /**
             * En la parte de verificar las Órdenes de Reposición de Inventario,
             * para eliminarlas.
             */
            if (request.getParameter("accion").equals("EliminarVerifica")) {
                try {
                    con.conectar();
                    try {
                        con.insertar("delete from tb_compratemp where F_FolRemi = '" + request.getParameter("vRemi") + "' and F_OrdCom = '" + request.getParameter("vOrden") + "' ");
                    } catch (Exception e) {
                    }
                    con.cierraConexion();
                    sesion.setAttribute("vOrden", "");
                    sesion.setAttribute("vRemi", "");
                    response.sendRedirect("verificarCompraAuto.jsp");
                } catch (Exception e) {
                }
            }
            /**
             * Para guardar la compra, se deja abierta para que no modifique el
             * campo F_Recibido en la tabla tb_pedidoisem y se pueda continuar
             * capturando de manera posterior.
             *
             */
            if (request.getParameter("accion").equals("GuardarAbiertaVerifica")) {
                try {
                    con.conectar();
                    //consql.conectar();
                    String F_ClaPro, F_Lote, F_FecCad, F_FecFab, F_Marca, F_Provee, F_Cb, F_Tarimas, F_Costo, F_ImpTo, F_ComTot, F_User, F_FolRemi, F_OrdCom;
                    String FolioLote = "", ExiLote = "", F_Caja , F_Piezas , F_Obser = null;

                    int ExiLot, cantidad, sumalote, FolLot, FolioLot, F_IndComT, F_Origen, F_OrigenC, FolMov, FolioMovi = 0;

                    //VARIABLES SQL SERVER
                    String F_Numero = "";
                    int  Contar;
                    //CONSULTA MYSQL INDICE DE COMPRA
                    ResultSet rset_IndF = con.consulta("SELECT F_IndCom FROM tb_indice");
                    while (rset_IndF.next()) {
                        F_IndCom = Integer.parseInt(rset_IndF.getString("F_IndCom"));
                    }
                    F_IndComT = F_IndCom + 1;
                    con.actualizar("update tb_indice set F_IndCom='" + F_IndComT + "'");

                    Contar = F_Numero.length();

                    /*
                     *Consulta a compra temporal (MySQL)
                     *con base en fecha y usuario
                     */
                    ResultSet rsetDatos = con.consulta("SELECT F_ClaPro, F_Lote, F_FecCad,DATE_FORMAT(F_FecCad,'%d/%m/%Y') AS FECAD, F_FecFab, F_Marca, F_Provee, F_Cb, F_Tarimas, F_Cajas, F_Pz, F_Resto, F_Costo,F_ImpTo, F_ComTot, F_FolRemi, F_OrdCom, F_ClaOrg, F_User, F_Obser,F_Origen FROM tb_compratemp WHERE F_OrdCom='" + request.getParameter("vOrden") + "' and F_FolRemi = '" + request.getParameter("vRemi") + "' ");
                    while (rsetDatos.next()) {
                        F_ClaPro = rsetDatos.getString("F_ClaPro");
                        F_Lote = rsetDatos.getString("F_Lote").toUpperCase();
                        F_FecCad = rsetDatos.getString("F_FecCad");
                        F_FecFab = rsetDatos.getString("F_FecFab");
                        F_Marca = rsetDatos.getString("F_Marca");
                        F_Provee = rsetDatos.getString("F_Provee");
                        F_Cb = rsetDatos.getString("F_Cb");
                        F_Tarimas = rsetDatos.getString("F_Tarimas");
                        F_Caja = rsetDatos.getString("F_Cajas");
                        F_Piezas = rsetDatos.getString("F_Pz");
                        String F_PiezasLote = F_Piezas;
                        if (F_ClaPro.equals("4186")) {
                            F_PiezasLote = (Integer.parseInt(F_Piezas) / 30) + "";
                        }
                        F_Costo = rsetDatos.getString("F_Costo");
                        F_ImpTo = rsetDatos.getString("F_ImpTo");
                        F_ComTot = rsetDatos.getString("F_ComTot");
                        F_FolRemi = rsetDatos.getString("F_FolRemi");
                        F_OrdCom = rsetDatos.getString("F_OrdCom");
                        F_Origen = Integer.parseInt(rsetDatos.getString("F_ClaOrg"));
                        F_User = rsetDatos.getString("F_User");
                        F_OrigenC = Integer.parseInt(rsetDatos.getString("F_Origen"));
                        String Ubicacion = "NUEVA";

                        ResultSet rset = con.consulta("select F_Tipo from tb_tipomed where F_ClaPro = '" + F_ClaPro + "'");
                        while (rset.next()) {
                            Ubicacion = rset.getString(1);
                        }
                        try {
                            byte[] a = rsetDatos.getString("F_Obser").getBytes("ISO-8859-1");
                            F_Obser = (new String(a, "UTF-8")).toUpperCase();
                        } catch (Exception e) {
                            System.out.println(e.getMessage());
                        }

                        // CONSULTA MYSQL
                        /*
                         *Se extrae fol_lote de F_FolLot para agregar o generar uno nuevo
                         */
                        ResultSet rsetLote = con.consulta("SELECT F_FolLot FROM tb_lote WHERE F_ClaPro='" + F_ClaPro + "' and F_ClaLot='" + F_Lote + "' and F_FecCad='" + F_FecCad + "' and F_Origen='" + F_OrigenC + "';");
                        while (rsetLote.next()) {
                            //System.out.println(rset.getString("F_FolLot"));
                            FolioLote = rsetLote.getString("F_FolLot");
                        }

                        if (!(FolioLote.isEmpty())) {//Lote existente
                            ResultSet rset_fol = con.consulta("SELECT F_ExiLot FROM tb_lote WHERE F_FolLot='" + FolioLote + "' and F_Ubica='" + Ubicacion + "'");
                            while (rset_fol.next()) {
                                ExiLote = rset_fol.getString("F_ExiLot");
                            }
                            if (ExiLote.isEmpty()) { //Lote sin ubicacion
                                con.insertar("insert into tb_lote values (0,'" + F_ClaPro + "','" + F_Lote + "','" + F_FecCad + "','" + F_PiezasLote + "','" + FolioLote + "','" + F_Origen + "','" + Ubicacion + "','" + F_FecFab + "','" + F_Cb + "','" + F_Marca + "','2','" + F_Origen + "','0')");
                            } else { //Lote con ubicacion
                                ExiLot = Integer.parseInt(ExiLote);
                                cantidad = Integer.parseInt(F_PiezasLote);
                                sumalote = ExiLot + cantidad;
                                con.actualizar("update tb_lote set F_ExiLot='" + sumalote + "' where F_FolLot='" + FolioLote + "' and F_Ubica='" + Ubicacion + "'");
                            }
                        } else { //Lote Inexistente
                            ResultSet rset_Ind = con.consulta("SELECT F_IndLote FROM tb_indice");
                            while (rset_Ind.next()) {
                                FolioLote = rset_Ind.getString("F_IndLote");
                                FolLot = Integer.parseInt(rset_Ind.getString("F_IndLote"));
                                con.insertar("insert into tb_lote values (0,'" + F_ClaPro + "','" + F_Lote + "','" + F_FecCad + "','" + F_PiezasLote + "','" + FolioLote + "','" + F_Origen + "','" + Ubicacion + "','" + F_FecFab + "','" + F_Cb + "','" + F_Marca + "','2','" + F_Origen + "','0')");
                                FolioLot = FolLot + 1;
                                con.actualizar("update tb_indice set F_IndLote='" + FolioLot + "'");
                            }

                        }
                        //FIN CONSULTA MYSQL


                        //CONSULTA INDICE MOVIMIENTO MYSQL
                        ResultSet FolioMov = con.consulta("SELECT F_IndMov FROM tb_indice");
                        while (FolioMov.next()) {
                            FolioMovi = Integer.parseInt(FolioMov.getString("F_IndMov"));
                        }
                        FolMov = FolioMovi + 1;
                        con.actualizar("update tb_indice set F_IndMov='" + FolMov + "'");
                        //FIN CONSULTA MYSQL
                        con.insertar("insert into tb_movinv values (0,curdate(),'" + F_IndCom + "','1', '" + F_ClaPro + "', '" + F_PiezasLote + "', '" + F_Costo + "', '" + F_ComTot + "' ,'1', '" + FolioLote + "', 'NUEVA', '" + F_Provee + "',curtime(),'" + F_User + "') ");
                        con.insertar("insert into tb_compra values (0,'" + F_IndCom + "','" + F_Provee + "','A',curdate(), '" + F_ClaPro + "', '" + F_Piezas + "', '" + F_Costo + "', '" + F_Caja + "', '0', '" + F_Tarimas + "', '" + F_ImpTo + "' ,'" + F_ComTot + "', '" + FolioLote + "', '" + F_FolRemi + "', '" + F_OrdCom + "', '" + F_Origen + "', '" + F_Cb + "', curtime(), '" + F_User + "','" + F_Obser + "','0') ");

                        FolioLote = "";
                    }
                    correo.enviaCorreo(request.getParameter("vOrden"));
                    
                    con.actualizar("delete from tb_compratemp where F_OrdCom = '" + request.getParameter("vOrden") + "' and F_FolRemi = '" + request.getParameter("vRemi") + "'");
                    con.cierraConexion();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

                sesion.setAttribute("vOrden", "");
                sesion.setAttribute("vRemi", "");
                out.println("<script>alert('Compra ABIERTA realizada, datos transferidos correctamente')</script>");
                out.println("<script>window.location='verificarCompraAuto.jsp'</script>");

            }

            /**
             * Para guardar la compra, en este caso se pasa el F_Recibido a 1
             * para indicar que se ingresó completamente la clave
             *
             */
            if (request.getParameter("accion").equals("GuardarVerifica")) {
                try {
                    con.conectar();
                    String F_ClaPro, F_Lote, F_FecCad, F_FecFab, F_Marca, F_Provee, F_Cb, F_Tarimas, F_Costo, F_ImpTo, F_ComTot, F_User, F_FolRemi, F_OrdCom;
                    String FolioLote = "", ExiLote = "", F_Caja, F_Piezas, F_Obser = "",costo;

                    int ExiLot, cantidad, sumalote, FolLot, FolioLot, F_IndComT, F_Origen , FolMov, F_OrigenC , FolioMovi = 0;

                    //CONSULTA MYSQL INDICE DE COMPRA
                    ResultSet rset_IndF = con.consulta("SELECT F_IndCom FROM tb_indice");
                    while (rset_IndF.next()) {
                        F_IndCom = Integer.parseInt(rset_IndF.getString("F_IndCom"));
                    }
                    F_IndComT = F_IndCom + 1;
                    con.actualizar("update tb_indice set F_IndCom='" + F_IndComT + "'");

                    /*
                     *Consulta a compra temporal (MySQL)
                     *con base en fecha y usuario
                     */
                    ResultSet rsetDatos = con.consulta("SELECT F_ClaPro, F_Lote, F_FecCad,DATE_FORMAT(F_FecCad,'%d/%m/%Y') AS FECAD, F_FecFab, F_Marca, F_Provee, F_Cb, F_Tarimas, F_Cajas, F_Pz, F_Resto, F_Costo,F_ImpTo, F_ComTot, F_FolRemi, F_OrdCom, F_ClaOrg, F_User, F_Obser,F_Origen FROM tb_compratemp WHERE F_OrdCom='" + request.getParameter("vOrden") + "' and F_FolRemi = '" + request.getParameter("vRemi") + "' ");
                    while (rsetDatos.next()) {
                        F_ClaPro = rsetDatos.getString("F_ClaPro");
                        F_Lote = rsetDatos.getString("F_Lote").toUpperCase();
                        F_FecCad = rsetDatos.getString("F_FecCad");
                        F_FecFab = rsetDatos.getString("F_FecFab");
                        F_Marca = rsetDatos.getString("F_Marca");
                        F_Provee = rsetDatos.getString("F_Provee");
                        F_Cb = rsetDatos.getString("F_Cb");
                        F_Tarimas = rsetDatos.getString("F_Tarimas");
                        F_Caja = rsetDatos.getString("F_Cajas");
                        F_Piezas = rsetDatos.getString("F_Pz");
                        String F_PiezasLote = F_Piezas;
                        F_Costo = rsetDatos.getString("F_Costo");
                        F_ImpTo = rsetDatos.getString("F_ImpTo");
                        F_ComTot = rsetDatos.getString("F_ComTot");
                        F_FolRemi = rsetDatos.getString("F_FolRemi");
                        F_OrdCom = rsetDatos.getString("F_OrdCom");
                        F_Origen = Integer.parseInt(rsetDatos.getString("F_ClaOrg"));
                        F_User = rsetDatos.getString("F_User");
                        F_OrigenC = Integer.parseInt(rsetDatos.getString("F_Origen"));
                        String Ubicacion = "NUEVA";

                        ResultSet rset = con.consulta("select F_Tipo from tb_tipomed where F_ClaPro = '" + F_ClaPro + "'");
                        while (rset.next()) {
                            Ubicacion = rset.getString(1);
                        }
                        try {
                            byte[] a = rsetDatos.getString("F_Obser").getBytes("ISO-8859-1");
                            F_Obser = (new String(a, "UTF-8")).toUpperCase();
                        } catch (Exception e) {
                            System.out.println(e.getMessage());
                        }

                        // CONSULTA MYSQL
                        /*
                         *Se extrae fol_lote de F_FolLot para agregar o generar uno nuevo
                         */
                        ResultSet rsetLote = con.consulta("SELECT F_FolLot FROM tb_lote WHERE F_ClaPro='" + F_ClaPro + "' and F_ClaLot='" + F_Lote + "' and F_FecCad='" + F_FecCad + "' and F_Origen='" + F_OrigenC + "';");
                        while (rsetLote.next()) {
                            FolioLote = rsetLote.getString("F_FolLot");
                        }

                        if (!(FolioLote.equals(""))) {//Lote existente
                            ResultSet rset_fol = con.consulta("SELECT F_ExiLot FROM tb_lote WHERE F_FolLot='" + FolioLote + "' and F_Ubica='" + Ubicacion + "'");
                            while (rset_fol.next()) {
                                ExiLote = rset_fol.getString("F_ExiLot");
                            }
                            if (!(ExiLote.equals(""))) { //Lote con ubicacion
                                ExiLot = Integer.parseInt(ExiLote);
                                cantidad = Integer.parseInt(F_PiezasLote);
                                sumalote = ExiLot + cantidad;
                                con.actualizar("update tb_lote set F_ExiLot='" + sumalote + "' where F_FolLot='" + FolioLote + "' and F_Ubica='" + Ubicacion + "'");
                            } else { //Lote sin ubicacion
                                con.insertar("insert into tb_lote values (0,'" + F_ClaPro + "','" + F_Lote + "','" + F_FecCad + "','" + F_PiezasLote + "','" + FolioLote + "','" + F_Origen + "','" + Ubicacion + "','" + F_FecFab + "','" + F_Cb + "','" + F_Marca + "','0','0','0')");
                            }
                        } else { //Lote Inexistente
                            ResultSet rset_Ind = con.consulta("SELECT F_IndLote FROM tb_indice");
                            while (rset_Ind.next()) {
                                FolioLote = rset_Ind.getString("F_IndLote");
                                FolLot = Integer.parseInt(rset_Ind.getString("F_IndLote"));
                                con.insertar("insert into tb_lote values (0,'" + F_ClaPro + "','" + F_Lote + "','" + F_FecCad + "','" + F_PiezasLote + "','" + FolioLote + "','" + F_Origen + "','" + Ubicacion + "','" + F_FecFab + "','" + F_Cb + "','" + F_Marca + "','0','0','0')");
                                FolioLot = FolLot + 1;
                                con.actualizar("update tb_indice set F_IndLote='" + FolioLot + "'");
                            }

                        }
                        //FIN CONSULTA MYSQL

                        //CONSULTA INDICE MOVIMIENTO MYSQL
                        ResultSet FolioMov = con.consulta("SELECT F_IndMov FROM tb_indice");
                        while (FolioMov.next()) {
                            FolioMovi = Integer.parseInt(FolioMov.getString("F_IndMov"));
                        }
                        FolMov = FolioMovi + 1;
                        con.actualizar("update tb_indice set F_IndMov='" + FolMov + "'");
                        //FIN CONSULTA MYSQL
                        con.insertar("insert into tb_movinv values (0,curdate(),'" + F_IndCom + "','1', '" + F_ClaPro + "', '" + F_PiezasLote + "', '" + F_Costo + "', '" + F_ComTot + "' ,'1', '" + FolioLote + "', 'NUEVA', '" + F_Provee + "',curtime(),'" + F_User + "') ");
                        con.insertar("insert into tb_compra values (0,'" + F_IndCom + "','" + F_Provee + "','A',curdate(), '" + F_ClaPro + "', '" + F_Piezas + "', '" + F_Costo + "', '" + F_Caja + "', '0', '" + F_Tarimas + "', '" + F_ImpTo + "' ,'" + F_ComTot + "', '" + FolioLote + "', '" + F_FolRemi + "', '" + F_OrdCom + "', '" + F_Origen + "', '" + F_Cb + "', curtime(), '" + F_User + "','" + F_Obser + "','0') ");
                        FolioLote = "";
                        con.actualizar("update tb_pedidoisem set F_Recibido = '1' where F_NoCompra = '" + F_OrdCom + "' and F_Clave = '" + F_ClaPro + "' ");
                    }
                    correo.enviaCorreoCerrado(request.getParameter("vOrden"));
                    con.actualizar("delete from tb_compratemp where F_OrdCom = '" + request.getParameter("vOrden") + "' and F_FolRemi = '" + request.getParameter("vRemi") + "'");
                    con.cierraConexion();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

                sesion.setAttribute("vOrden", "");
                sesion.setAttribute("vRemi", "");
                out.println("<script>alert('Compra realizada, datos transferidos correctamente')</script>");
                out.println("<script>window.location='verificarCompraAuto.jsp'</script>");
            }
            /**
             * Para eliminar de tb_compratemp los registros referentes a una
             * ORDEN DE REPOSICIÓN DE INVENTARIO
             */
            if (request.getParameter("accion").equals("Eliminar")) {

                try {
                    con.conectar();

                    try {
                        con.insertar("delete from tb_compratemp where F_OrdCom = '" + request.getParameter("fol_gnkl") + "' ");
                    } catch (Exception e) {

                    }
                    con.cierraConexion();
                    request.getSession().setAttribute("folio", "");
                    request.getSession().setAttribute("fecha", "");
                    request.getSession().setAttribute("folio_remi", "");
                    request.getSession().setAttribute("orden", "");
                    request.getSession().setAttribute("provee", "");
                    request.getSession().setAttribute("recib", "");
                    request.getSession().setAttribute("entrega", "");
                    request.getSession().setAttribute("origen", "");
                    request.getSession().setAttribute("coincide", "");
                    request.getSession().setAttribute("observaciones", "");
                    request.getSession().setAttribute("clave", "");
                    request.getSession().setAttribute("descrip", "");
                } catch (Exception e) {
                }
                sesion.setAttribute("posClave", "0");
                sesion.setAttribute("NoCompra", "0");
                out.println("<script>alert('Compra cancelada')</script>");
                out.println("<script>window.location='compraAuto2.jsp'</script>");
            }

            /**
             *
             * Metodo anterior que se utilizaba para confirmar directamente las
             * compras, ya no se utiliza ya que se manejan Confirmacion Abierta
             * o Cerrada
             *
             */
            if (request.getParameter("accion").equals("Guardar")) {
                try {
                    con.conectar();
                    //consql.conectar();
                    String F_ClaPro, F_Lote, F_FecCad, F_FecFab, F_Marca, F_Provee , F_Cb , F_Tarimas , F_Costo , F_ImpTo , F_ComTot , F_User , F_FolRemi, F_OrdCom = "";
                    String FolioLote = "", ExiLote = "", F_Caja , F_Piezas , F_Obser = "";

                    int ExiLot , cantidad , sumalote , FolLot , FolioLot , F_IndComT , F_Origen , FolMov , FolioMovi = 0;

                    //VARIABLES SQL SERVER
                    String F_Numero = "";
                    int Contar;
                    //CONSULTA MYSQL INDICE DE COMPRA
                    ResultSet rset_IndF = con.consulta("SELECT F_IndCom FROM tb_indice");
                    while (rset_IndF.next()) {
                        F_IndCom = Integer.parseInt(rset_IndF.getString("F_IndCom"));
                    }
                    F_IndComT = F_IndCom + 1;
                    con.actualizar("update tb_indice set F_IndCom='" + F_IndComT + "'");

                    Contar = F_Numero.length();


                    /*
                     *Consulta a compra temporal (MySQL)
                     *con base en fecha y usuario
                     */
                    ResultSet rsetDatos = con.consulta("SELECT F_ClaPro, F_Lote, F_FecCad,DATE_FORMAT(F_FecCad,'%d/%m/%Y') AS FECAD, F_FecFab, F_Marca, F_Provee, F_Cb, F_Tarimas, F_Cajas, F_Pz, F_Resto, F_Costo,F_ImpTo, F_ComTot, F_FolRemi, F_OrdCom, F_ClaOrg, F_User, F_Obser FROM tb_compratemp WHERE F_OrdCom='" + request.getParameter("fol_gnkl") + "'");
                    while (rsetDatos.next()) {
                        F_ClaPro = rsetDatos.getString("F_ClaPro");
                        F_Lote = rsetDatos.getString("F_Lote").toUpperCase();
                        F_FecCad = rsetDatos.getString("F_FecCad");
                        F_FecFab = rsetDatos.getString("F_FecFab");
                        F_Marca = rsetDatos.getString("F_Marca");
                        F_Provee = rsetDatos.getString("F_Provee");
                        F_Cb = rsetDatos.getString("F_Cb");
                        F_Tarimas = rsetDatos.getString("F_Tarimas");
                        F_Caja = rsetDatos.getString("F_Cajas");
                        F_Piezas = rsetDatos.getString("F_Pz");
                        F_Costo = rsetDatos.getString("F_Costo");
                        F_ImpTo = rsetDatos.getString("F_ImpTo");
                        F_ComTot = rsetDatos.getString("F_ComTot");
                        F_FolRemi = rsetDatos.getString("F_FolRemi");
                        F_OrdCom = rsetDatos.getString("F_OrdCom");
                        F_Origen = Integer.parseInt(rsetDatos.getString("F_ClaOrg"));
                        F_User = rsetDatos.getString("F_User");
                        try {
                            byte[] a = rsetDatos.getString("F_Obser").getBytes("ISO-8859-1");
                            F_Obser = (new String(a, "UTF-8")).toUpperCase();
                        } catch (Exception e) {
                            System.out.println(e.getMessage());
                        }

                        // CONSULTA MYSQL
                        /*
                         *Se extrae fol_lote de F_FolLot para agregar o generar uno nuevo
                         */
                        ResultSet rsetLote = con.consulta("SELECT F_FolLot FROM tb_lote WHERE F_ClaPro='" + F_ClaPro + "' and F_ClaLot='" + F_Lote + "' and F_FecCad='" + F_FecCad + "' and F_ClaOrg='" + F_Origen + "' and F_ClaMar='" + F_Marca + "'");
                        while (rsetLote.next()) {
                            FolioLote = rsetLote.getString("F_FolLot");
                        }

                        if (!(FolioLote.equals(""))) {//Lote existente
                            ResultSet rset_fol = con.consulta("SELECT F_ExiLot FROM tb_lote WHERE F_FolLot='" + FolioLote + "' and F_Ubica='NUEVA'");
                            while (rset_fol.next()) {
                                ExiLote = rset_fol.getString("F_ExiLot");
                            }
                            if (!(ExiLote.equals(""))) { //Lote con ubicacion
                                ExiLot = Integer.parseInt(ExiLote);
                                cantidad = Integer.parseInt(F_Piezas);
                                sumalote = ExiLot + cantidad;
                                con.actualizar("update tb_lote set F_ExiLot='" + sumalote + "' where F_FolLot='" + FolioLote + "' and F_Ubica='NUEVA'");
                            } else { //Lote sin ubicacion
                                con.insertar("insert into tb_lote values (0,'" + F_ClaPro + "','" + F_Lote + "','" + F_FecCad + "','" + F_Piezas + "','" + FolioLote + "','" + F_Origen + "','NUEVA','" + F_FecFab + "','" + F_Cb + "','" + F_Marca + "')");
                                con.insertar("insert into tb_lote_repisem values (0,'" + F_ClaPro + "','" + F_Lote + "','" + F_FecCad + "','" + F_Piezas + "','" + FolioLote + "','" + F_Origen + "','NUEVA','" + F_FecFab + "','" + F_Cb + "','" + F_Marca + "')");
                            }
                        } else { //Lote Inexistente
                            ResultSet rset_Ind = con.consulta("SELECT F_IndLote FROM tb_indice");
                            while (rset_Ind.next()) {
                                FolioLote = rset_Ind.getString("F_IndLote");
                                FolLot = Integer.parseInt(rset_Ind.getString("F_IndLote"));
                                con.insertar("insert into tb_lote values (0,'" + F_ClaPro + "','" + F_Lote + "','" + F_FecCad + "','" + F_Piezas + "','" + FolioLote + "','" + F_Origen + "','NUEVA','" + F_FecFab + "','" + F_Cb + "','" + F_Marca + "')");
                                FolioLot = FolLot + 1;
                                con.actualizar("update tb_indice set F_IndLote='" + FolioLot + "'");
                            }

                        }
  
                        ResultSet FolioMov = con.consulta("SELECT F_IndMov FROM tb_indice");
                        while (FolioMov.next()) {
                            FolioMovi = Integer.parseInt(FolioMov.getString("F_IndMov"));
                        }
                        FolMov = FolioMovi + 1;
                        con.actualizar("update tb_indice set F_IndMov='" + FolMov + "'");
                        con.insertar("insert into tb_movinv values (0,curdate(),'" + F_IndCom + "','1', '" + F_ClaPro + "', '" + F_Piezas + "', '" + F_Costo + "', '" + F_ComTot + "' ,'1', '" + FolioLote + "', 'NUEVA', '" + F_Provee + "',curtime(),'" + F_User + "') ");
                        con.insertar("insert into tb_compra values (0,'" + F_IndCom + "','" + F_Provee + "','A',curdate(), '" + F_ClaPro + "', '" + F_Piezas + "', '" + F_Costo + "', '" + F_Caja + "', '0', '" + F_Tarimas + "', '" + F_ImpTo + "' ,'" + F_ComTot + "', '" + FolioLote + "', '" + F_FolRemi + "', '" + F_OrdCom + "', '" + F_Origen + "', '" + F_Cb + "', curtime(), '" + F_User + "','" + F_Obser + "','0') ");

                        FolioLote = "";
                    }

                    con.actualizar("delete from tb_compratemp where F_OrdCom = '" + F_OrdCom + "'");
                    con.actualizar("update tb_pedidoisem set F_Recibido = '1' where F_NoCompra = '" + F_OrdCom + "'");
                    con.cierraConexion();
                    //consql.cierraConexion();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

                request.getSession().setAttribute("folio", "");
                request.getSession().setAttribute("fecha", "");
                request.getSession().setAttribute("folio_remi", "");
                request.getSession().setAttribute("orden", "");
                request.getSession().setAttribute("provee", "");
                request.getSession().setAttribute("recib", "");
                request.getSession().setAttribute("entrega", "");
                request.getSession().setAttribute("origen", "");
                request.getSession().setAttribute("coincide", "");
                request.getSession().setAttribute("observaciones", "");
                request.getSession().setAttribute("clave", "");
                request.getSession().setAttribute("descrip", "");
                request.getSession().setAttribute("cuenta", "");
                request.getSession().setAttribute("cb", "");
                request.getSession().setAttribute("codbar2", "");
                request.getSession().setAttribute("Marca", "");
                request.getSession().setAttribute("PresPro", "");

                out.println("<script>alert('Compra realizada, datos transferidos correctamente')</script>");
            }

            /**
             * Cuando no se tenían las verificaciones por auditores se
             * confirmaba directamente
             */
            if (request.getParameter("accion").equals("GuardarAbierta")) {
                try {
                    con.conectar();
                    //consql.conectar();
                    String F_ClaPro , F_Lote , F_FecCad , F_FecFab , F_Marca , F_Provee , F_Cb , F_Tarimas , F_Costo , F_ImpTo , F_ComTot = "", F_User = "", F_FolRemi = "", F_OrdCom = "";
                    String FolioLote = "", ExiLote = "", F_Caja , F_Piezas, F_Obser = "";

                    int ExiLot = 0, cantidad = 0, sumalote = 0, FolLot = 0, FolioLot = 0, F_IndComT = 0, F_Origen = 0, FolMov = 0, FolioMovi = 0;

                    //VARIABLES SQL SERVER
                    String F_Numero = "";
                    int Contar;
                    //CONSULTA MYSQL INDICE DE COMPRA
                    ResultSet rset_IndF = con.consulta("SELECT F_IndCom FROM tb_indice");
                    while (rset_IndF.next()) {
                        F_IndCom = Integer.parseInt(rset_IndF.getString("F_IndCom"));
                    }
                    F_IndComT = F_IndCom + 1;
                    con.actualizar("update tb_indice set F_IndCom='" + F_IndComT + "'");
                        //FIN MYSQL
                    Contar = F_Numero.length();

                    /*
                     *Consulta a compra temporal (MySQL)
                     *con base en fecha y usuario
                     */
                    ResultSet rsetDatos = con.consulta("SELECT F_ClaPro, F_Lote, F_FecCad,DATE_FORMAT(F_FecCad,'%d/%m/%Y') AS FECAD, F_FecFab, F_Marca, F_Provee, F_Cb, F_Tarimas, F_Cajas, F_Pz, F_Resto, F_Costo,F_ImpTo, F_ComTot, F_FolRemi, F_OrdCom, F_ClaOrg, F_User, F_Obser FROM tb_compratemp WHERE F_OrdCom='" + request.getParameter("fol_gnkl") + "'");
                    while (rsetDatos.next()) {
                        F_ClaPro = rsetDatos.getString("F_ClaPro");
                        F_Lote = rsetDatos.getString("F_Lote").toUpperCase();
                        F_FecCad = rsetDatos.getString("F_FecCad");
                        F_FecFab = rsetDatos.getString("F_FecFab");
                        F_Marca = rsetDatos.getString("F_Marca");
                        F_Provee = rsetDatos.getString("F_Provee");
                        F_Cb = rsetDatos.getString("F_Cb");
                        F_Tarimas = rsetDatos.getString("F_Tarimas");
                        F_Caja = rsetDatos.getString("F_Cajas");
                        F_Piezas = rsetDatos.getString("F_Pz");
                        F_Costo = rsetDatos.getString("F_Costo");
                        F_ImpTo = rsetDatos.getString("F_ImpTo");
                        F_ComTot = rsetDatos.getString("F_ComTot");
                        F_FolRemi = rsetDatos.getString("F_FolRemi");
                        F_OrdCom = rsetDatos.getString("F_OrdCom");
                        F_Origen = Integer.parseInt(rsetDatos.getString("F_ClaOrg"));
                        F_User = rsetDatos.getString("F_User");
                        try {
                            byte[] a = rsetDatos.getString("F_Obser").getBytes("ISO-8859-1");
                            F_Obser = (new String(a, "UTF-8")).toUpperCase();
                        } catch (Exception e) {
                            System.out.println(e.getMessage());
                        }

                        // CONSULTA MYSQL
                        /*
                         *Se extrae fol_lote de F_FolLot para agregar o generar uno nuevo
                         */
                        ResultSet rsetLote = con.consulta("SELECT F_FolLot FROM tb_lote WHERE F_ClaPro='" + F_ClaPro + "' and F_ClaLot='" + F_Lote + "' and F_FecCad='" + F_FecCad + "' and F_ClaOrg='" + F_Origen + "' and F_ClaMar='" + F_Marca + "'");
                        while (rsetLote.next()) {
                            FolioLote = rsetLote.getString("F_FolLot");
                        }

                        if (!(FolioLote.equals(""))) {//Lote existente
                            ResultSet rset_fol = con.consulta("SELECT F_ExiLot FROM tb_lote WHERE F_FolLot='" + FolioLote + "' and F_Ubica='NUEVA'");
                            while (rset_fol.next()) {
                                ExiLote = rset_fol.getString("F_ExiLot");
                            }
                            if (!(ExiLote.equals(""))) { //Lote con ubicacion
                                ExiLot = Integer.parseInt(ExiLote);
                                cantidad = Integer.parseInt(F_Piezas);
                                sumalote = ExiLot + cantidad;
                                con.actualizar("update tb_lote set F_ExiLot='" + sumalote + "' where F_FolLot='" + FolioLote + "' and F_Ubica='NUEVA'");
                            } else { //Lote sin ubicacion
                                con.insertar("insert into tb_lote values (0,'" + F_ClaPro + "','" + F_Lote + "','" + F_FecCad + "','" + F_Piezas + "','" + FolioLote + "','" + F_Origen + "','NUEVA','" + F_FecFab + "','" + F_Cb + "','" + F_Marca + "')");
                                con.insertar("insert into tb_lote_repisem values (0,'" + F_ClaPro + "','" + F_Lote + "','" + F_FecCad + "','" + F_Piezas + "','" + FolioLote + "','" + F_Origen + "','NUEVA','" + F_FecFab + "','" + F_Cb + "','" + F_Marca + "')");
                            }
                        } else { //Lote Inexistente
                            ResultSet rset_Ind = con.consulta("SELECT F_IndLote FROM tb_indice");
                            while (rset_Ind.next()) {
                                FolioLote = rset_Ind.getString("F_IndLote");
                                FolLot = Integer.parseInt(rset_Ind.getString("F_IndLote"));
                                con.insertar("insert into tb_lote values (0,'" + F_ClaPro + "','" + F_Lote + "','" + F_FecCad + "','" + F_Piezas + "','" + FolioLote + "','" + F_Origen + "','NUEVA','" + F_FecFab + "','" + F_Cb + "','" + F_Marca + "')");
                                con.insertar("insert into tb_lote_repisem values (0,'" + F_ClaPro + "','" + F_Lote + "','" + F_FecCad + "','" + F_Piezas + "','" + FolioLote + "','" + F_Origen + "','NUEVA','" + F_FecFab + "','" + F_Cb + "','" + F_Marca + "')");
                                FolioLot = FolLot + 1;
                                con.actualizar("update tb_indice set F_IndLote='" + FolioLot + "'");
                            }

                        }
                        ResultSet FolioMov = con.consulta("SELECT F_IndMov FROM tb_indice");
                        while (FolioMov.next()) {
                            FolioMovi = Integer.parseInt(FolioMov.getString("F_IndMov"));
                        }
                        FolMov = FolioMovi + 1;
                        con.actualizar("update tb_indice set F_IndMov='" + FolMov + "'");
                         
                        con.insertar("insert into tb_movinv values (0,curdate(),'" + F_IndCom + "','1', '" + F_ClaPro + "', '" + F_Piezas + "', '" + F_Costo + "', '" + F_ComTot + "' ,'1', '" + FolioLote + "', 'NUEVA', '" + F_Provee + "',curtime(),'" + F_User + "') ");
                        con.insertar("insert into tb_compra values (0,'" + F_IndCom + "','" + F_Provee + "','A',curdate(), '" + F_ClaPro + "', '" + F_Piezas + "', '" + F_Costo + "', '" + F_Caja + "', '0', '" + F_Tarimas + "', '" + F_ImpTo + "' ,'" + F_ComTot + "', '" + FolioLote + "', '" + F_FolRemi + "', '" + F_OrdCom + "', '" + F_Origen + "', '" + F_Cb + "', curtime(), '" + F_User + "','" + F_Obser + "','0') ");
                        FolioLote = "";
                    }

                    con.actualizar("delete from tb_compratemp where F_OrdCom = '" + F_OrdCom + "'");
                    con.cierraConexion();
                    //consql.cierraConexion();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

                request.getSession().setAttribute("folio", "");
                request.getSession().setAttribute("fecha", "");
                request.getSession().setAttribute("folio_remi", "");
                request.getSession().setAttribute("orden", "");
                request.getSession().setAttribute("provee", "");
                request.getSession().setAttribute("recib", "");
                request.getSession().setAttribute("entrega", "");
                request.getSession().setAttribute("origen", "");
                request.getSession().setAttribute("coincide", "");
                request.getSession().setAttribute("observaciones", "");
                request.getSession().setAttribute("clave", "");
                request.getSession().setAttribute("descrip", "");
                request.getSession().setAttribute("cuenta", "");
                request.getSession().setAttribute("cb", "");
                request.getSession().setAttribute("codbar2", "");
                request.getSession().setAttribute("Marca", "");
                request.getSession().setAttribute("PresPro", "");

                out.println("<script>alert('Compra realizada, datos transferidos correctamente')</script>");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        sesion.setAttribute("posClave", "0");
        sesion.setAttribute("NoCompra", "0");
        out.println("<script>window.open('reimpReporte.jsp?fol_gnkl=" + F_IndCom + "','_blank')</script>");
        out.println("<script>window.open('reimp_marbete.jsp?fol_gnkl=" + F_IndCom + "','_blank')</script>");
        out.println("<script>window.location='compraAuto2.jsp'</script>");
    }

    

    public String dame7car(String clave) {
        try {
            int largoClave = clave.length();
            int espacios = 7 - largoClave;
            for (int i = 1; i <= espacios; i++) {
                clave = " " + clave;
            }
        } catch (Exception e) {
        }
        return clave;
    }

    public String dame5car(String clave) {
        try {
            int largoClave = clave.length();
            int espacios = 5 - largoClave;
            for (int i = 1; i <= espacios; i++) {
                clave = " " + clave;
            }
        } catch (Exception e) {
        }
        return clave;
    }

    public String idLote(String clave, String lote, String fec_cad, String cant, double costo, String origen, String fec_fab) {
        String idLote = "";
        int exi = 0;
        double cos = 0;
        int ban = 0;
        try {
            /*consql.conectar();
             try {
             ResultSet rset = consql.consulta("select F_FolLot, F_ExiLot, F_CosLot from tb_lote where F_ClaPro = '" + clave + "' and F_ClaLot = '" + lote + "' ");
             while (rset.next()) {
             idLote = rset.getString("F_FolLot");
             exi = rset.getInt("F_ExiLot");
             cos = rset.getDouble("F_CosLot");
             ban = 1;
             }
             } catch (SQLException e) {
             }
             if (ban == 0) {
             ResultSet rset = consql.consulta("select F_IL from TB_Indice ");
             while (rset.next()) {
             idLote = rset.getString("F_IL");
             consql.actualizar("update TB_Indice set F_IL = '" + (Integer.parseInt(idLote) + 1) + "' ");
             }
             consql.insertar("insert into tb_lote values ('" + lote + "', '" + clave + "', '" + fec_cad + "', '" + cant + "', '" + costo + "', '" + idLote + "', '" + origen + "', '0000', '" + fec_fab + "') ");
             } else {
             int texi = exi + Integer.parseInt(cant);
             double totcos = cos + costo;
             consql.actualizar("update tb_lote set F_ExiLot = '" + texi + "', F_CosLot = '" + totcos + "' where F_FolLot = '" + idLote + "' ");
             }
             consql.cierraConexion();
             */
        } catch (Exception e) {
        }
        return idLote;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
