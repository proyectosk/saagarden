/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import conn.ConectionDB;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelos.InventarioCarga;

/**
 *
 * @author Mario
 */
public class capturarInventario extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String proveedor = "722252";
        String usuario = "sistemas";
        String destino = "/capturaInventario.jsp";
        float costo = 0;
        int folLot = 0;

        try {
            String accion, clave, descripcion, consulta, claveR = "", descR = "", lote, caducidad, cantidad, cb, costoS;
            accion = request.getParameter("accion");
            ConectionDB con = new ConectionDB();
            con.conectar();
            Connection conexion = con.getConn();
            PreparedStatement ps;
            ResultSet rs;

            if (accion.equals("buscarT")) {
                clave = request.getParameter("clave");
                descripcion = request.getParameter("descripcion");
                consulta = "SELECT F_ClaPro,F_DesPro FROM tb_medica as m WHERE F_StsPro = 'A' AND (F_ClaPro=? OR F_DesPro=?)  GROUP BY F_ClaPro";
                ps = conexion.prepareStatement(consulta);
                ps.setString(1, clave);
                ps.setString(2, descripcion);
                rs = ps.executeQuery();
                if (rs.next()) {
                    claveR = rs.getString(1);
                    descR = rs.getString(2);
                }
                if (!claveR.isEmpty()) {
                    consulta = "SELECT lote,cadu,cantidad,cb from inventario_excel WHERE clave=? AND status=0 ";
                    ps = conexion.prepareStatement(consulta);
                    ps.setString(1, claveR);
                    rs = ps.executeQuery();
                    if (rs.next()) {
                        request.setAttribute("clave", claveR);
                        request.setAttribute("descripcion", descR);
                        request.setAttribute("lote", rs.getString("lote"));
                        request.setAttribute("caducidad", rs.getString("cadu"));
                        request.setAttribute("cantidad", rs.getString("cantidad"));
                        request.setAttribute("cb", rs.getString("cb"));
                    }
                }
            }
            if (accion.equals("buscar")) {
                clave = request.getParameter("clave");
                descripcion = request.getParameter("descripcion");
                consulta = "SELECT F_ClaPro,F_DesPro FROM tb_medica as m WHERE F_StsPro = 'A' AND (F_ClaPro=? OR F_DesPro=?)  GROUP BY F_ClaPro";
                ps = conexion.prepareStatement(consulta);
                ps.setString(1, clave);
                ps.setString(2, descripcion);
                rs = ps.executeQuery();
                if (rs.next()) {
                    claveR = rs.getString(1);
                    descR = rs.getString(2);
                }
                if (!claveR.isEmpty()) {
                    request.setAttribute("clave", claveR);
                    request.setAttribute("descripcion", descR);
                } else {
                    request.setAttribute("noencontrada", "Clave no encontrada");
                    request.setAttribute("claveRep", clave);
                    request.setAttribute("descRep", descripcion);
                }

            }
            if (accion.equals("capturar")) {

                clave = request.getParameter("claveRO");
                if (clave == null | clave.isEmpty()) {
                    request.setAttribute("sinclave", "Ingrese la clave a capturar primero");
                } else {
                    lote = request.getParameter("lote");
                    caducidad = request.getParameter("caducidad");
                    cantidad = request.getParameter("cantidad");
                    cb = request.getParameter("cb");
                    costoS = request.getParameter("costo");

                    consulta = "SELECT cantidad,id FROM inventario_captura WHERE clave=? AND lote=? AND cadu=?";
                    ps = conexion.prepareStatement(consulta);
                    ps.setString(1, clave);
                    ps.setString(2, lote);
                    ps.setString(3, caducidad);
                    rs = ps.executeQuery();

                    if (rs.next()) {
                        //update
                        consulta = "UPDATE inventario_captura SET cantidad=? WHERE id=?";
                        ps = conexion.prepareStatement(consulta);
                        ps.setInt(1, Integer.parseInt(cantidad) + rs.getInt(1));
                        ps.setInt(2, rs.getInt(2));
                        ps.execute();
                        request.setAttribute("capturado", "Insumo capturado correctamente!");
                    } else {
                        //insert
                        consulta = "INSERT INTO inventario_captura VALUES(?,?,?,?,?,?,CURDATE(),0,0)";
                        ps = conexion.prepareStatement(consulta);
                        ps.setString(1, clave);
                        ps.setString(2, lote);
                        ps.setString(3, caducidad);
                        ps.setString(4, cb);
                        ps.setString(5, cantidad);
                        ps.setString(6, costoS);
                        ps.execute();
                        request.setAttribute("capturado", "Insumo capturado correctamente!");
                    }

                }

                try {

                    List<InventarioCarga> listaInvCaptura = new ArrayList<InventarioCarga>();

                    PreparedStatement psInt;
                    ResultSet rsInt;
                    descripcion = "";

                    consulta = "SELECT ic.clave, ic.lote, ic.cadu, ic.cb, ic.cantidad, ic.costo, ic.fecha, ic.`status`, ic.id FROM inventario_captura AS ic ORDER BY ic.id DESC LIMIT 1";
                    ps = conexion.prepareStatement(consulta);
                    rs = ps.executeQuery();
                    InventarioCarga invCapt;
                    while (rs.next()) {
                        consulta = "SELECT F_DesPro FROM tb_medica as m WHERE F_StsPro = 'A' AND F_ClaPro=?  GROUP BY F_ClaPro";
                        psInt = conexion.prepareStatement(consulta);
                        psInt.setString(1, rs.getString("clave"));
                        rsInt = psInt.executeQuery();
                        while (rsInt.next()) {
                            descripcion = rsInt.getString("F_DesPro");
                        }
                        invCapt = new InventarioCarga();
                        invCapt.setId(rs.getString("id"));
                        invCapt.setClave(rs.getString("clave"));
                        invCapt.setDesc(descripcion);
                        invCapt.setLote(rs.getString("lote"));
                        try {
                            String currentDate = rs.getString("cadu");
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                            Date tempDate = simpleDateFormat.parse(currentDate);
                            SimpleDateFormat outputDateFormat = new SimpleDateFormat("dd/MM/YYYY");
                            invCapt.setCaducidad(outputDateFormat.format(tempDate));
                        } catch (ParseException ex) {
                            Logger.getLogger(comparativoInventario.class.getName()).log(Level.SEVERE, null, ex);
                        }                        invCapt.setCb(rs.getString("cb"));
                        invCapt.setCantidad(rs.getString("cantidad"));
                        invCapt.setCosto(rs.getString("costo"));
                        listaInvCaptura.add(invCapt);
                    }

                    request.setAttribute("invCap", listaInvCaptura);

                } catch (SQLException ex) {
                    Logger.getLogger(comparativoInventario.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            if (accion.equals("editar")) {

                clave = request.getParameter("claveMod");
                if (clave == null | clave.isEmpty()) {
                    request.setAttribute("sinclave", "Ingrese la clave a capturar primero");
                } else {
                    lote = request.getParameter("loteMod");
                    caducidad = request.getParameter("caduMod");
                    cantidad = request.getParameter("cantMod");
                    cb = request.getParameter("cbMod");
                    String id = request.getParameter("idMod");

                    consulta = "UPDATE inventario_captura set lote=?,cadu=?,cb=?,cantidad=? where id=?";
                    ps = conexion.prepareStatement(consulta);
                    ps.setString(1, lote);
                    ps.setString(2, caducidad);
                    ps.setString(3, cb);
                    ps.setString(4, cantidad);
                    ps.setString(5, id);
                    System.out.println(ps);

                    ps.execute();
                    request.setAttribute("editado", "Insumo capturado Editado!");
                    destino = "/comparativoInventario";
                }

            }
            if (accion.equals("editar2")) {

                clave = request.getParameter("claveMod");
                if (clave == null | clave.isEmpty()) {
                    request.setAttribute("sinclave", "Ingrese la clave a capturar primero");
                } else {
                    lote = request.getParameter("loteMod");
                    caducidad = request.getParameter("caduMod");
                    cantidad = request.getParameter("cantMod");
                    cb = request.getParameter("cbMod");
                    String id = request.getParameter("idMod");

                    consulta = "UPDATE inventario_captura set lote=?,cadu=?,cb=?,cantidad=? where id=?";
                    ps = conexion.prepareStatement(consulta);
                    ps.setString(1, lote);
                    ps.setString(2, caducidad);
                    ps.setString(3, cb);
                    ps.setString(4, cantidad);
                    ps.setString(5, id);
                    System.out.println(ps);

                    ps.execute();
                    request.setAttribute("editado", "Insumo capturado Editado!");
                }

                try {

                    List<InventarioCarga> listaInvCaptura = new ArrayList<InventarioCarga>();

                    PreparedStatement psInt;
                    ResultSet rsInt;
                    descripcion = "";

                    consulta = "SELECT ic.clave, ic.lote, ic.cadu, ic.cb, ic.cantidad, ic.costo, ic.fecha, ic.`status`, ic.id FROM inventario_captura AS ic ORDER BY ic.id DESC LIMIT 1";
                    ps = conexion.prepareStatement(consulta);
                    rs = ps.executeQuery();
                    InventarioCarga invCapt;
                    while (rs.next()) {
                        consulta = "SELECT F_DesPro FROM tb_medica as m WHERE F_StsPro = 'A' AND F_ClaPro=?  GROUP BY F_ClaPro";
                        psInt = conexion.prepareStatement(consulta);
                        psInt.setString(1, rs.getString("clave"));
                        rsInt = psInt.executeQuery();
                        while (rsInt.next()) {
                            descripcion = rsInt.getString("F_DesPro");
                        }
                        invCapt = new InventarioCarga();
                        invCapt.setId(rs.getString("id"));
                        invCapt.setClave(rs.getString("clave"));
                        invCapt.setDesc(descripcion);
                        invCapt.setLote(rs.getString("lote"));
                        try {
                            String currentDate = rs.getString("cadu");
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                            Date tempDate = simpleDateFormat.parse(currentDate);
                            SimpleDateFormat outputDateFormat = new SimpleDateFormat("dd/MM/YYYY");
                            invCapt.setCaducidad(outputDateFormat.format(tempDate));
                        } catch (ParseException ex) {
                            Logger.getLogger(comparativoInventario.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        invCapt.setCb(rs.getString("cb"));
                        invCapt.setCantidad(rs.getString("cantidad"));
                        invCapt.setCosto(rs.getString("costo"));
                        listaInvCaptura.add(invCapt);
                    }

                    request.setAttribute("invCap", listaInvCaptura);

                } catch (SQLException ex) {
                    Logger.getLogger(comparativoInventario.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            if (accion.equals("eliminar")) {

                String id = request.getParameter("idEli");

                consulta = "DELETE FROM inventario_captura where id=?";
                ps = conexion.prepareStatement(consulta);
                ps.setString(1, id);
                System.out.println(ps);
                System.out.println(ps);

                ps.execute();
                request.setAttribute("eliminado", "Insumo Eliminado!");
                destino = "/comparativoInventario";

            }
            if (accion.equals("eliminar2")) {

                String id = request.getParameter("idEli");

                consulta = "DELETE FROM inventario_captura where id=?";
                ps = conexion.prepareStatement(consulta);
                ps.setString(1, id);
                System.out.println(ps);
                System.out.println(ps);

                ps.execute();
                request.setAttribute("eliminado", "Insumo Eliminado!");
                try {

                    List<InventarioCarga> listaInvCaptura = new ArrayList<InventarioCarga>();

                    PreparedStatement psInt;
                    ResultSet rsInt;
                    descripcion = "";

                    consulta = "SELECT ic.clave, ic.lote, ic.cadu, ic.cb, ic.cantidad, ic.costo, ic.fecha, ic.`status`, ic.id FROM inventario_captura AS ic ORDER BY ic.id DESC LIMIT 1";
                    ps = conexion.prepareStatement(consulta);
                    rs = ps.executeQuery();
                    InventarioCarga invCapt;
                    while (rs.next()) {
                        consulta = "SELECT F_DesPro FROM tb_medica as m WHERE F_StsPro = 'A' AND F_ClaPro=?  GROUP BY F_ClaPro";
                        psInt = conexion.prepareStatement(consulta);
                        psInt.setString(1, rs.getString("clave"));
                        rsInt = psInt.executeQuery();
                        while (rsInt.next()) {
                            descripcion = rsInt.getString("F_DesPro");
                        }
                        invCapt = new InventarioCarga();
                        invCapt.setId(rs.getString("id"));
                        invCapt.setClave(rs.getString("clave"));
                        invCapt.setDesc(descripcion);
                        invCapt.setLote(rs.getString("lote"));
                        invCapt.setCaducidad(rs.getString("cadu"));
                        invCapt.setCb(rs.getString("cb"));
                        invCapt.setCantidad(rs.getString("cantidad"));
                        invCapt.setCosto(rs.getString("costo"));
                        listaInvCaptura.add(invCapt);
                    }

                    request.setAttribute("invCap", listaInvCaptura);

                } catch (SQLException ex) {
                    Logger.getLogger(comparativoInventario.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (accion.equals("IngresarLote")) {

                clave = request.getParameter("claveRO");
                lote = request.getParameter("lote");
                caducidad = request.getParameter("caducidad");
                cantidad = request.getParameter("cantidad");
                cb = request.getParameter("cb");

                consulta = "SELECT MAX(F_FolLot) from tb_lote";
                ps = conexion.prepareStatement(consulta);
                rs = ps.executeQuery();

                if (rs.next()) {
                    folLot = rs.getInt(1) + 1;
                } else {
                    folLot++;
                }

                consulta = "SELECT F_ClaPro,F_DesPro,F_Costo FROM tb_medica as m WHERE F_StsPro = 'A' AND F_ClaPro=?  GROUP BY F_ClaPro";
                ps = conexion.prepareStatement(consulta);
                ps.setString(1, clave);
                rs = ps.executeQuery();
                if (rs.next()) {
                    costo = rs.getFloat(3);
                }

                consulta = "INSERT INTO tb_lote VALUES(0,?,?,?,?,?,?,'NUEVA','1990-01-01',?,1,2,?,13)";
                ps = conexion.prepareStatement(consulta);
                ps.setString(1, clave);
                ps.setString(2, lote);
                ps.setString(3, caducidad);
                ps.setString(4, cantidad);
                ps.setInt(5, folLot);
                ps.setString(6, proveedor);
                ps.setString(7, cb);
                ps.setString(8, proveedor);
                ps.execute();

                consulta = "INSERT INTO tb_tb_movinv VALUES(0,CURDATE(),1,15,?,?,?,?,1,?,'NUEVA0',?,CURTIME(),?)";
                ps = conexion.prepareStatement(consulta);
                ps.setString(1, clave);
                ps.setString(2, cantidad);
                ps.setFloat(3, costo);
                ps.setFloat(4, costo * Integer.parseInt(cantidad));
                ps.setInt(5, folLot);
                ps.setString(6, proveedor);
                ps.setString(7, usuario);
                ps.execute();
            }
            con.cierraConexion();
            request.getRequestDispatcher(destino).forward(request, response);

        } catch (SQLException ex) {
            Logger.getLogger(capturarInventario.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
