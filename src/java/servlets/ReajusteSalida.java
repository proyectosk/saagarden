/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import dao.ReajusteDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelos.Producto;

/**
 *
 * @author Mario
 */
public class ReajusteSalida extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String accion = request.getParameter("accion");
        RequestDispatcher view = request.getRequestDispatcher("SalidaAjuste.jsp");
        HttpSession sesion = request.getSession(true);
        String usuario = (String) sesion.getAttribute("nombre");
        String msg = "";
        ReajusteDao dao = new ReajusteDao();

        /**
         * En base al parametro accion se realiza una tarea
         */
        if (accion != null) {
            String clave, lote, cadu;
            if (accion.equals("buscarClave")) {
                clave = request.getParameter("Clave");
                if (clave != null) {
                    request.setAttribute("Clave", clave);

                    List<Producto> lotes = dao.getAllLotesByClave(clave);
                    if (!lotes.isEmpty()) {
                        List<Producto> caducidades = dao.getAllCadusByClave(clave);
                        request.setAttribute("lotes", lotes);
                        request.setAttribute("caducidades", caducidades);
                        msg = "Encontrado: Seleccione el lote y la caducidad";

                    } else {
                        msg = "ADVERTENCIA: Producto no encontrado";
                        System.out.println("Producto no encontrado");
                    }
                } else {
                    sesion.setAttribute("advertencia", "Advertencia: La clave esta vacia");

                }
                request.setAttribute("justifi", dao.getJustificacion());

            }
            if (accion.equals("buscarDesc")) {

                String desc = request.getParameter("Desc");
                request.setAttribute("desc", desc);

                clave = dao.getClaveByDesc(desc);
                request.setAttribute("Clave", clave);

                List<Producto> lotes2 = dao.getAllLotesByClave(clave);
                if (!lotes2.isEmpty()) {
                    List<Producto> caducidades = dao.getAllCadusByClave(clave);
                    request.setAttribute("lotes", lotes2);
                    request.setAttribute("caducidades", caducidades);
                    msg = "Encontrado: Seleccione el lote y la caducidad";

                } else {
                    System.out.println("Producto no encontrado");
                    msg = "ADVERTENCIA: Producto no encontrado";
                }
            }
            if (accion.equals("filtrar")) {

                clave = request.getParameter("Clave");
                lote = request.getParameter("Lote");
                cadu = request.getParameter("Cadu");

                Producto producto = dao.getProductoByCLC(clave, lote, cadu);
                if (producto != null) {
                    request.setAttribute("prod", producto);
                } else {
                    System.out.println("Producto no encontrado");
                    msg = "ADVERTENCIA: Producto no encontrado";
                }
            }
            if (accion.equals("Aplicar")) {

                String cantidad = request.getParameter("Cantidad");
                String exis = request.getParameter("hExis");
                clave = request.getParameter("hClave");
                lote = request.getParameter("hLote");
                cadu = request.getParameter("hCadu");
                String justi = request.getParameter("justi");

                if (cantidad != null) {

                    try {
                        int cantInt;
                        cantInt = Integer.parseInt(cantidad);
                        if (cantInt < 0) {
                            msg = "ADVERTENCIA: La cantidad no es valida";
                        } else {
                            if (exis != null) {
                                int exisInt = Integer.parseInt(exis);

                                msg = dao.updateTBLOTE(clave, lote, cadu, cantInt, exisInt, justi, usuario);
                                if (!msg.contains("ERROR")) {
                                    msg = "Hecho: El ajuste se realizo con exito";
                                }

                            } else {
                                msg = "ADVERTENCIA: No ha seleccionado un producto";
                            }
                        }

                    } catch (Exception e) {
                        msg = "ADVERTENCIA: La cantidad no es valida";

                    }

                } else {
                    msg = "ADVERTENCIA: No ha ingresado la cantidad";
                }
                Producto productoRecarga = dao.getProductoByCLC(clave, lote, cadu);
                if (productoRecarga != null) {
                    request.setAttribute("prod", productoRecarga);
                } else {
                    System.out.println("Producto no encontrado");
                    msg = "ADVERTENCIA: Producto no encontrado";
                }
            }

            System.out.println("Mensaje: " + msg);
            if (msg.contains("ERROR")) {
                sesion.removeAttribute("error");
                sesion.setAttribute("error", msg);
            } else {
                if (msg.contains("ADVERTENCIA")) {
                    sesion.removeAttribute("advertencia");
                    sesion.setAttribute("advertencia", msg);
                } else {
                    sesion.removeAttribute("mensaje");
                    sesion.setAttribute("mensaje", msg);
                }
            }
            dao.close();

            view.forward(request, response);

        } else {
            sesion.removeAttribute("error");
            sesion.removeAttribute("advertencia");
            sesion.removeAttribute("mensaje");
            dao.close();

            view.forward(request, response);

        }

    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
