/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import conn.ConectionDB;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Correo.CorreoAltaUsuario;
import Correo.CorreoRecUsuario;

/**
 *
 * @author Mario
 */
public class ControlUsuarios extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ConectionDB con = new ConectionDB();
        String salida = "crearCredenciales.jsp";
        System.out.println("Pagina:");
        System.out.println(request.getAttribute("javax.servlet.forward.request_uri"));
        try {

            if (request.getParameter("accion").equals("crear")) {
                String usuario = request.getParameter("usuario");
                String email = request.getParameter("email");
                String password = request.getParameter("password");
                String nombre = request.getParameter("nombre");
                String apellido = request.getParameter("apellido");
                String apellidomat = request.getParameter("apellidomat");
                String rol = request.getParameter("rol");
                String passAdmin = request.getParameter("passwordAdmin");

                con.conectar();

                java.sql.PreparedStatement ps = con.getConn().prepareStatement("SELECT F_Usu FROM tb_usuario where F_Pass=PASSWORD(?) and F_TipUsu=8 ");
                ps.setString(1, passAdmin);
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    java.sql.PreparedStatement ps3 = con.getConn().prepareStatement("SELECT F_Usu FROM tb_usuario where F_Usu=? ");
                    ps3.setString(1, usuario);
                    ResultSet rs2 = ps3.executeQuery();
                    System.out.println(ps3);
                    if (rs2.next()) {
                        //Ya existe un usuario con ese nombre de usuario
                        request.setAttribute("usuarioYa", "El nombre de usuario no esta disponible");
                        request.setAttribute("nom", nombre);
                        request.setAttribute("apellido", apellido);
                        request.setAttribute("rol", rol);
                        request.setAttribute("email", email);

                    } else {

                        java.sql.PreparedStatement psmail = con.getConn().prepareStatement("SELECT F_Usu FROM tb_usuario where email=? ");
                        psmail.setString(1, email);
                        ResultSet rsmail = psmail.executeQuery();
                        System.out.println(psmail);
                        if (rsmail.next()) {
                            //Ya existe un usuario con ese nombre de usuario
                            request.setAttribute("usuarioYa", "El correo ya esta registrado");
                            request.setAttribute("nom", nombre);
                            request.setAttribute("apellido", apellido);
                            request.setAttribute("rol", rol);
                            request.setAttribute("email", email);

                        } else {
                            java.sql.PreparedStatement ps2 = con.getConn().prepareStatement("INSERT INTO `tb_usuario` (`F_Usu`, `F_Pass`, `F_Nombre`, `F_Apellido`, `F_Status`, `F_TipUsu`,`F_Apellidomat`,`email`) "
                                    + "VALUES (?, PASSWORD(?), ?, ?, 'A', ?,?,?)");
                            ps2.setString(1, usuario);
                            ps2.setString(2, password);
                            ps2.setString(3, nombre);
                            ps2.setString(4, apellido);
                            ps2.setString(5, rol);
                            ps2.setString(6, apellidomat);
                            ps2.setString(7, email);
                            System.out.println(ps2);
                            ps2.execute();
                            request.setAttribute("usuarioOK", "Usuario dado de alta exitosamente");

                            //Mandar correo
                            CorreoAltaUsuario.enviaCorreo(usuario, password, email);
                        }

                    }

                } else {
                    request.setAttribute("usuarioYa", "La contraseña del administrador no es valida");
                    request.setAttribute("nom", nombre);
                    request.setAttribute("usuario", usuario);
                    request.setAttribute("apellido", apellido);
                    request.setAttribute("apellidomat", apellidomat);
                    request.setAttribute("rol", rol);
                    request.setAttribute("email", email);
                }
                con.cierraConexion();

            }
            if (request.getParameter("accion").equals("recuperar")) {
                salida = "recuperarCredenciales.jsp";
                String email = request.getParameter("email");
                String pass = request.getParameter("password");

                con.conectar();

                java.sql.PreparedStatement ps = con.getConn().prepareStatement("SELECT F_Usu FROM tb_usuario where email=?");
                ps.setString(1, email);
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    java.sql.PreparedStatement ps2 = con.getConn().prepareStatement("UPDATE tb_usuario set F_Pass=PASSWORD(?) where email=?");
                    ps2.setString(1, pass);
                    ps2.setString(2, email);
                    ps2.execute();

                    //Mandar correo
                    CorreoRecUsuario.enviaCorreo(rs.getString("F_Usu"), pass, email);
                    request.setAttribute("emailOk", "Se ha enviado un correo con las credenciales");
                    request.setAttribute("email", email);
                } else {
                    request.setAttribute("emailNo", "El correo no coincide con ningun usuario, verfiquelo e intente de nuevo");
                    request.setAttribute("email", email);
                }
                con.cierraConexion();

            }
        } catch (SQLException ex) {
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
        }
        RequestDispatcher view = request.getRequestDispatcher(salida);

        view.forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
