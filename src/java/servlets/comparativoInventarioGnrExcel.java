/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import conn.ConectionDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelos.InventarioCarga;

/**
 *
 * @author Mario
 */
public class comparativoInventarioGnrExcel extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws java.io.IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String excel = request.getParameter("gnr");
        String salida = "gnrExcelExcel.jsp";
        if (excel.equals("capt")) {
            salida = "gnrExcelCapt.jsp";
        }

        try {

            List<InventarioCarga> listaInvExcel = new ArrayList<InventarioCarga>();
            List<InventarioCarga> listaInvCaptura = new ArrayList<InventarioCarga>();
            ConectionDB con = new ConectionDB();
            con.conectar();
            Connection conexion = con.getConn();

            PreparedStatement ps, psInt;
            ResultSet rs, rsInt;
            String consulta, descripcion = "";

            consulta = "SELECT * FROM inventario_excel";
            ps = conexion.prepareStatement(consulta);
            rs = ps.executeQuery();
            InventarioCarga invExcel;
            while (rs.next()) {
                consulta = "SELECT F_DesPro FROM tb_medica as m WHERE F_StsPro = 'A' AND F_ClaPro=?  GROUP BY F_ClaPro";
                psInt = conexion.prepareStatement(consulta);
                psInt.setString(1, rs.getString("clave"));
                rsInt = psInt.executeQuery();
                while (rsInt.next()) {
                    descripcion = rsInt.getString("F_DesPro");
                }

                invExcel = new InventarioCarga();
                invExcel.setClave(rs.getString("clave"));
                invExcel.setDesc(descripcion);
                invExcel.setLote(rs.getString("lote"));
                invExcel.setCaducidad(rs.getString("cadu"));
                invExcel.setCb(rs.getString("cb"));
                invExcel.setCantidad(rs.getString("cantidad"));
                invExcel.setCosto(rs.getString("costo"));
                listaInvExcel.add(invExcel);
            }
            consulta = "SELECT * FROM inventario_captura";
            ps = conexion.prepareStatement(consulta);
            rs = ps.executeQuery();
            InventarioCarga invCapt;
            while (rs.next()) {
                consulta = "SELECT F_DesPro FROM tb_medica as m WHERE F_StsPro = 'A' AND F_ClaPro=?  GROUP BY F_ClaPro";
                psInt = conexion.prepareStatement(consulta);
                psInt.setString(1, rs.getString("clave"));
                rsInt = psInt.executeQuery();
                while (rsInt.next()) {
                    descripcion = rsInt.getString("F_DesPro");
                }
                invCapt = new InventarioCarga();
                invCapt.setClave(rs.getString("clave"));
                invCapt.setDesc(descripcion);
                invCapt.setLote(rs.getString("lote"));
                invCapt.setCaducidad(rs.getString("cadu"));
                invCapt.setCb(rs.getString("cb"));
                invCapt.setCantidad(rs.getString("cantidad"));
                invCapt.setCosto(rs.getString("costo"));
                listaInvCaptura.add(invCapt);
            }

            request.setAttribute("invCap", listaInvCaptura);
            request.setAttribute("invExcel", listaInvExcel);

            con.cierraConexion();

        } catch (SQLException ex) {
            Logger.getLogger(comparativoInventario.class.getName()).log(Level.SEVERE, null, ex);
        }

        request.getRequestDispatcher("/" + salida).forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
