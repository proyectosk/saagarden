/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import dao.ModificacionOcDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelos.OrdenCompra;
import modelos.Producto;

/**
 *
 * @author Mario
 */
public class ModificacionOC extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String accion = request.getParameter("accion");
        RequestDispatcher view = request.getRequestDispatcher("ModificacionCompra.jsp");
        HttpSession sesion = request.getSession(true);
        String usuario = (String) sesion.getAttribute("nombre");
        String msg = "";
        List<Producto> productos;
        int total;
        OrdenCompra orden;
        /**
         * En base al parametro accion se realiza una tarea
         */
        ModificacionOcDao dao = new ModificacionOcDao();

        String oc, folio;

        if (accion != null) {
            String clave, lote, cadu;
            System.out.println(accion);
            if (accion.equals("buscarFolios")) {

                clave = request.getParameter("orden");
                if (clave != null) {
                    request.setAttribute("orden", clave);

                    List<String> folios = dao.getFoliosRemisionByOc(clave);
                    if (!folios.isEmpty()) {
                        request.setAttribute("folios", folios);
                        msg = "Encontrado: Seleccione el folio";
                    } else {
                        msg = "ADVERTENCIA: Producto no encontrado";
                        System.out.println("Producto no encontrado");
                    }
                } else {
                    sesion.setAttribute("advertencia", "Advertencia: La Orden de reposición de inventario esta vacia");
                }
            }
            if (accion.equals("buscarFolios")) {
                oc = request.getParameter("orden");
                folio = request.getParameter("folio");
                request.setAttribute("orden", oc);

                productos = dao.getDetalleOcByOcFolioRem(oc, folio);
                request.setAttribute("productos", productos);

                total = 0;
                for (Producto producto : productos) {
                    total += producto.getExistencia();
                }
                orden = dao.getOcByFolio(oc, folio, total);
                request.setAttribute("oc", orden);
            }
            if (accion.equals("buscarFolios")) {
                System.out.println("Vamos a editar");

                String cantidad = request.getParameter("cantMod");
                String exis = request.getParameter("exisOri");
                String id = request.getParameter("idMod");
                String justi = request.getParameter("justi");

                folio = dao.getFolioRemById(id);
                oc = dao.getOcById(id);

                if (cantidad != null) {

                    try {
                        int cantInt;
                        cantInt = Integer.parseInt(cantidad);
                        int exisInt = Integer.parseInt(exis);
                        if (cantInt < 0) {
                            msg = "ADVERTENCIA: La cantidad no es valida";
                        } else {
                            if (exisInt != cantInt) {
                                msg = dao.EditarCantidad(id, cantInt, justi, usuario);
                                if (!msg.contains("ERROR")) {
                                    msg = "Hecho: El ajuste se realizo con exito";
                                }
                            } else {
                                msg = "ADVERTENCIA: No se realizo ningun cambio, la cantidad no cambio";
                            }
                        }

                    } catch (Exception e) {
                        msg = "ADVERTENCIA: La cantidad no es valida";

                    }

                } else {
                    msg = "ADVERTENCIA: No ha ingresado la cantidad";
                }
                productos = dao.getDetalleOcByOcFolioRem(oc, folio);
                request.setAttribute("productos", productos);

                total = 0;
                for (Producto producto : productos) {
                    total += producto.getExistencia();
                }
                orden = dao.getOcByFolio(oc, folio, total);
                request.setAttribute("oc", orden);
            }

            if (msg.contains("ERROR")) {
                sesion.removeAttribute("error");
                sesion.setAttribute("error", msg);
            } else {
                if (msg.contains("ADVERTENCIA")) {
                    sesion.removeAttribute("advertencia");
                    sesion.setAttribute("advertencia", msg);
                } else {
                    sesion.removeAttribute("mensaje");
                    sesion.setAttribute("mensaje", msg);
                }
            }
            dao.close();

            view.forward(request, response);

        } else {
            sesion.removeAttribute("error");
            sesion.removeAttribute("advertencia");
            sesion.removeAttribute("mensaje");
            dao.close();

            view.forward(request, response);

        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
