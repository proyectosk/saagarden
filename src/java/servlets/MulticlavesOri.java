/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import conn.ConectionDB;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mario
 */
public class MulticlavesOri extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            HttpSession sesion = request.getSession(true);

            String accion, clave, descripcion, consulta, claveR = "", descR = "", lote, caducidad, cantidad, cb, costoS;
            accion = request.getParameter("accion");
            ConectionDB con = new ConectionDB();
            con.conectar();
            Connection conexion = con.getConn();
            PreparedStatement ps;
            ResultSet rs;
            /**
             * Para buscar una clave
             */
            if (request.getParameter("accion").equals("descripcion")) {
                /**
                 * Se busca la clave y se agregan los datos a variables de
                 * sesión
                 */
                String NoCompra = (String) sesion.getAttribute("NoCompra");
                int ban = 0;
                con.conectar();
                try {
                    List<String> claves = new ArrayList<String>();
                    ResultSet rset = con.consulta("select F_ClaPro, F_DesPro from tb_medica where F_DesPro = '" + request.getParameter("descripcion") + "'");
                    while (rset.next()) {
                        claves.add(rset.getString(1));
                        sesion.setAttribute("clave", rset.getString(1));
                        sesion.setAttribute("descripcion", rset.getString(2));
                    }

                    if (claves.size() == 1) {
                        ban = 1;
                        sesion.setAttribute("desProv", request.getParameter("des_pro"));
                        sesion.setAttribute("proveedor", request.getParameter("Proveedor"));
                        sesion.setAttribute("fec_entrega", request.getParameter("Fecha"));
                        sesion.setAttribute("hor_entrega", request.getParameter("Hora"));
                        sesion.setAttribute("NoCompra", request.getParameter("NoCompra"));
                    } else {
                        request.setAttribute("masdeuno", "si");
                        request.setAttribute("claves", claves);
                        sesion.setAttribute("clave", "");
                        sesion.setAttribute("descripcion", "");
                    }

                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                con.cierraConexion();
                /**
                 * Si existe
                 */
                if (ban == 1) {
                    try {
                        if (NoCompra.isEmpty()) {
                            try {
                                /**
                                 * Bandera de si existe ese número de compra
                                 */
                                int ban2 = 0;
                                con.conectar();
                                ResultSet rset = con.consulta("select F_IdIsem from tb_pedidoisem where F_NoCompra = '" + request.getParameter("NoCompra") + "'");
                                while (rset.next()) {
                                    ban2 = 1;
                                }
                                con.cierraConexion();
                                if (ban2 == 1) {
                                    /**
                                     * Si existe se avisa, y se limpian las
                                     * variables de sesión
                                     */
                                    sesion.setAttribute("NoCompra", "");
                                    sesion.setAttribute("clave", "");
                                    sesion.setAttribute("descripcion", "");
                                }
                            } catch (Exception e) {
                                System.out.println(e.getMessage());
                            }
                        }
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }

                } else {
                    /**
                     * Si no existe la clave se limpian las variables de sesión
                     */
                    sesion.setAttribute("clave", "");
                    sesion.setAttribute("descripcion", "");
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(MulticlavesOri.class.getName()).log(Level.SEVERE, null, ex);
        }
        request.getRequestDispatcher("capturaItu.jsp").forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
