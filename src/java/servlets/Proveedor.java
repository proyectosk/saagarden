/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import conn.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Americo
 *
 *
 * Clase para proveedores
 */
public class Proveedor extends HttpServlet {

    ConectionDB con = new ConectionDB();
    //ConectionDB_SQLServer consql = new ConectionDB_SQLServer();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession sesion = request.getSession(true);
        try {
            /*
             *Para actualizar Registros
             */
            if (request.getParameter("accion").equals("actualizar")) {
                /**
                 * Para actualizar los registro de los proveedores, se tienen
                 * comentadas las lineas que se utilizaban para SGW
                 *
                 */
                //consql.conectar();
                con.conectar();
                try {
                    String clave = request.getParameter("Clave");
                    try {
                        int largoClave = request.getParameter("Clave").length();
                        int espacios = 5 - largoClave;
                        for (int i = 1; i <= espacios; i++) {
                            clave = " " + clave;
                        }
                    } catch (Exception e) {
                    }
                    //consql.actualizar("update TB_Provee set F_ClaPrv='" + clave + "', F_NomPrv='" + request.getParameter("Nombre").toUpperCase() + "', F_Dir='" + request.getParameter("Direccion").toUpperCase() + "', F_Col='" + request.getParameter("Colonia").toUpperCase() + "', F_Pob='" + request.getParameter("Poblacion").toUpperCase() + "', F_CP='" + request.getParameter("CP").toUpperCase() + "', F_RFC='" + request.getParameter("RFC").toUpperCase() + "', F_Con='" + request.getParameter("CON").toUpperCase() + "', F_Cls='" + request.getParameter("CLS").toUpperCase() + "', F_Tel='" + request.getParameter("Telefono").toUpperCase() + "', F_Fax='" + request.getParameter("FAX").toUpperCase() + "', F_Mail='" + request.getParameter("Mail").toUpperCase() + "', F_Obs='" + request.getParameter("Observaciones").toUpperCase() + "' where F_ClaPrv='" + request.getParameter("id").toUpperCase() + "';");

                    con.actualizar("update provee_all set F_ClaPrv='" + clave + "', F_nomprov='" + request.getParameter("Nombre").toUpperCase() + "', F_Dir='" + request.getParameter("Direccion").toUpperCase() + "', F_Col='" + request.getParameter("Colonia").toUpperCase() + "', F_Pob='" + request.getParameter("Poblacion").toUpperCase() + "', F_CP='" + request.getParameter("CP").toUpperCase() + "', F_RFC='" + request.getParameter("RFC").toUpperCase() + "', F_Con='" + request.getParameter("CON").toUpperCase() + "', F_Cls='" + request.getParameter("CLS").toUpperCase() + "', F_Tel='" + request.getParameter("Telefono").toUpperCase() + "', F_Fax='" + request.getParameter("FAX").toUpperCase() + "', F_Mail='" + request.getParameter("Mail").toUpperCase() + "', F_Obs='" + request.getParameter("Observaciones").toUpperCase() + "' where F_ClaPrv='" + request.getParameter("id").toUpperCase() + "';");

                } catch (Exception e) {
                    System.out.println(e.getMessage());

                    out.println("<script>alert('Ya esta registrado ese proveedor')</script>");
                    out.println("<script>window.location='editar_proveedor.jsp'</script>");
                }
                con.cierraConexion();
                //consql.cierraConexion();

                out.println("<script>alert('Proveedor actualizado correctamente.')</script>");
                out.println("<script>window.location='catalogo.jsp'</script>");
            }
            /*
             *Manda al jsp el id del registro a editar
             */
            if (request.getParameter("accion").equals("editar")) {
                /**
                 * Manda el ID para editar el proveedor
                 */
                request.getSession().setAttribute("id", request.getParameter("id"));
                response.sendRedirect("editar_proveedor.jsp");
            }
            /*
             *Para eliminar registro
             */
            if (request.getParameter("accion").equals("eliminar")) {
                /**
                 * Para eliminar el proveedor
                 */
                //consql.conectar();
                con.conectar();
                try {
                    //consql.insertar("delete from TB_Provee where F_ClaPrv = '" + request.getParameter("id") + "' ");
                    con.insertar("delete from provee_all where F_ClaPrv = '" + request.getParameter("id") + "' ");
                } catch (SQLException e) {
                    System.out.println(e.getMessage());

                    out.println("<script>alert('Error al eliminar')</script>");
                    out.println("<script>window.location='catalogo.jsp'</script>");
                }
                con.cierraConexion();
                //consql.cierraConexion();

                out.println("<script>alert('Se elimino el proveedor correctamente')</script>");
                out.println("<script>window.location='catalogo.jsp'</script>");
            }
            /*
             *Guarda Registros
             */
            if (request.getParameter("accion").equals("guardar")) {
                /**
                 * Para guardar el proveedor
                 */
                try {
                    String Nombre = "";
                    //consql.conectar();
                    con.conectar();
                    try {
                        ResultSet rst_prov = con.consulta("SELECT F_NomPro FROM tb_proveedor WHERE F_ClaProve='" + request.getParameter("Clave") + "'");
                        while (rst_prov.next()) {
                            Nombre = rst_prov.getString("F_NomPro");
                        }
                        if (!(Nombre.equals(""))) {
                            out.println("<script>alert('La clave proporcionada ya ésta utilizada por otro proveedor, !Favor de proporcionar otra Clave¡')</script>");
                            out.println("<script>window.history.back()</script>");

                        } else {
                            con.insertar("insert into tb_proveedor values ('" + request.getParameter("Clave") + "','A','" + request.getParameter("Nombre").toUpperCase() + "', '" + request.getParameter("RFC").toUpperCase() + "', '" + request.getParameter("Direccion").toUpperCase() + "', '" + request.getParameter("noint").toUpperCase() + "', '" + request.getParameter("noext").toUpperCase() + "', '" + request.getParameter("cruza").toUpperCase() + "','', '" + request.getParameter("Colonia").toUpperCase() + "', '" + request.getParameter("CP").toUpperCase() + "', '" + request.getParameter("loca").toUpperCase() + "', '" + request.getParameter("mpio").toUpperCase() + "', '" + request.getParameter("estado").toUpperCase() + "','MX','MX','', '" + request.getParameter("Telefono").toUpperCase() + "', '', '" + request.getParameter("FAX").toUpperCase() + "', '" + request.getParameter("Mail").toUpperCase() + "','','');");
                            //consql.insertar("insert into TB_Provee values ('" + request.getParameter("Nombre").toUpperCase() + "', '" + request.getParameter("Direccion").toUpperCase() + "', '" + request.getParameter("Colonia").toUpperCase() + "', '" + request.getParameter("CP").toUpperCase() + "', '" + request.getParameter("Telefono").toUpperCase() + "', '" + request.getParameter("RFC").toUpperCase() + "', '" + request.getParameter("FAX").toUpperCase() + "', '" + request.getParameter("Mail").toUpperCase() + "','');");
                            out.println("<script>alert('Proveedor capturado correctamente.')</script>");
                            out.println("<script>window.location='catalogo.jsp'</script>");
                        }
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }
                    con.cierraConexion();
                    // consql.cierraConexion();

                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

            }

            /*
             *Guarda Registros cliente proveedores
             */
            if (request.getParameter("accion").equals("guardarProve")) {
                /**
                 * Para guardar el proveedor
                 */
                try {
                    String Nombre = "";
                    //consql.conectar();
                    con.conectar();
                    try {
                        ResultSet rst_prov = con.consulta("SELECT F_NomPro FROM tb_proveedor WHERE F_ClaProve='" + request.getParameter("Clave") + "'");
                        while (rst_prov.next()) {
                            Nombre = rst_prov.getString("F_NomPro");
                        }
                        if (!(Nombre.equals(""))) {
                            out.println("<script>alert('La clave proporcionada ya ésta utilizada por otro proveedor, !Favor de proporcionar otra Clave¡')</script>");
                            out.println("<script>window.history.back()</script>");

                        } else {
                            con.insertar("insert into tb_proveedor values ('" + request.getParameter("Clave") + "','A','" + request.getParameter("Nombre").toUpperCase() + "', '" + request.getParameter("RFC").toUpperCase() + "', '" + request.getParameter("Direccion").toUpperCase() + "', '" + request.getParameter("noint").toUpperCase() + "', '" + request.getParameter("noext").toUpperCase() + "', '" + request.getParameter("cruza").toUpperCase() + "','', '" + request.getParameter("Colonia").toUpperCase() + "', '" + request.getParameter("CP").toUpperCase() + "', '" + request.getParameter("loca").toUpperCase() + "', '" + request.getParameter("mpio").toUpperCase() + "', '" + request.getParameter("estado").toUpperCase() + "','MX','MX','', '" + request.getParameter("Telefono").toUpperCase() + "', '', '" + request.getParameter("FAX").toUpperCase() + "', '" + request.getParameter("Mail").toUpperCase() + "','','');");
                            //consql.insertar("insert into TB_Provee values ('" + request.getParameter("Nombre").toUpperCase() + "', '" + request.getParameter("Direccion").toUpperCase() + "', '" + request.getParameter("Colonia").toUpperCase() + "', '" + request.getParameter("CP").toUpperCase() + "', '" + request.getParameter("Telefono").toUpperCase() + "', '" + request.getParameter("RFC").toUpperCase() + "', '" + request.getParameter("FAX").toUpperCase() + "', '" + request.getParameter("Mail").toUpperCase() + "','');");
                            out.println("<script>alert('Proveedor capturado correctamente.')</script>");
                            out.println("<script>window.location='catalogo1.jsp'</script>");
                        }
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }
                    con.cierraConexion();
                    // consql.cierraConexion();

                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

            }
            /*
             *Guarda Registros Unidades
             */
            if (request.getParameter("accion").equals("guardarU")) {
                /**
                 * Para guardar el proveedor
                 */
                try {
                    String Nombre = "";
                    //consql.conectar();
                    con.conectar();
                    int FolUni = 0;
                    try {
                        ResultSet ClaUni = con.consulta("SELECT F_ClaCli FROM tb_uniatn ORDER BY F_ClaCli+0");
                        while (ClaUni.next()) {
                            FolUni = ClaUni.getInt(1);
                        }
                        FolUni = FolUni + 1;
                        con.insertar("insert into tb_uniatn values ('" + FolUni + "','" + request.getParameter("Nombre").toUpperCase() + "','A', '" + request.getParameter("RFC").toUpperCase() + "', '" + request.getParameter("calle").toUpperCase() + "','" + request.getParameter("noint").toUpperCase() + "', '" + request.getParameter("noext").toUpperCase() + "', '" + request.getParameter("Colonia").toUpperCase() + "', '" + request.getParameter("CP").toUpperCase() + "', '" + request.getParameter("estado").toUpperCase() + "','" + request.getParameter("Telefono").toUpperCase() + "','" + request.getParameter("Direccion").toUpperCase() + "','" + request.getParameter("Clave").toUpperCase() + "','" + request.getParameter("Contacto").toUpperCase() + "','" + request.getParameter("Transporte").toUpperCase() + "','" + request.getParameter("DirPaq").toUpperCase() + "');");

                        //consql.insertar("insert into TB_Provee values ('" + request.getParameter("Nombre").toUpperCase() + "', '" + request.getParameter("Direccion").toUpperCase() + "', '" + request.getParameter("Colonia").toUpperCase() + "', '" + request.getParameter("CP").toUpperCase() + "', '" + request.getParameter("Telefono").toUpperCase() + "', '" + request.getParameter("RFC").toUpperCase() + "', '" + request.getParameter("FAX").toUpperCase() + "', '" + request.getParameter("Mail").toUpperCase() + "','');");
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }
                    con.cierraConexion();
                    // consql.cierraConexion();
                    out.println("<script>alert('Unidad capturado correctamente.')</script>");
                    out.println("<script>window.location='catalogoUnidades.jsp'</script>");
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

            }

            /*
             *Guarda Registros Unidades cliente
             */
            if (request.getParameter("accion").equals("guardarUnidad")) {
                /**
                 * Para guardar el proveedor
                 */
                try {
                    String Nombre = "";
                    //consql.conectar();
                    con.conectar();
                    int FolUni = 0;
                    try {
                        ResultSet ClaUni = con.consulta("SELECT F_ClaCli FROM tb_uniatn ORDER BY F_ClaCli+0");
                        while (ClaUni.next()) {
                            FolUni = ClaUni.getInt(1);
                        }
                        FolUni = FolUni + 1;
                        con.insertar("insert into tb_uniatn values ('" + FolUni + "','" + request.getParameter("Nombre").toUpperCase() + "','A', '" + request.getParameter("RFC").toUpperCase() + "', '" + request.getParameter("calle").toUpperCase() + "','" + request.getParameter("noint").toUpperCase() + "', '" + request.getParameter("noext").toUpperCase() + "', '" + request.getParameter("Colonia").toUpperCase() + "', '" + request.getParameter("CP").toUpperCase() + "', '" + request.getParameter("estado").toUpperCase() + "','" + request.getParameter("Telefono").toUpperCase() + "','" + request.getParameter("Direccion").toUpperCase() + "','" + request.getParameter("Clave").toUpperCase() + "','" + request.getParameter("Contacto").toUpperCase() + "','" + request.getParameter("Transporte").toUpperCase() + "','" + request.getParameter("DirPaq").toUpperCase() + "');");

                        //consql.insertar("insert into TB_Provee values ('" + request.getParameter("Nombre").toUpperCase() + "', '" + request.getParameter("Direccion").toUpperCase() + "', '" + request.getParameter("Colonia").toUpperCase() + "', '" + request.getParameter("CP").toUpperCase() + "', '" + request.getParameter("Telefono").toUpperCase() + "', '" + request.getParameter("RFC").toUpperCase() + "', '" + request.getParameter("FAX").toUpperCase() + "', '" + request.getParameter("Mail").toUpperCase() + "','');");
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }
                    con.cierraConexion();
                    // consql.cierraConexion();
                    out.println("<script>alert('Unidad capturado correctamente.')</script>");
                    out.println("<script>window.location='catalogoUnidades1.jsp'</script>");
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

            }

            /*
             *Modificar Registros Unidades
             */
            if (request.getParameter("accion").equals("ModificarU")) {
                /**
                 * Para guardar el proveedor
                 */
                try {
                    String Clave2 = "";
                    //consql.conectar();
                    con.conectar();
                    try {
                        Clave2 = request.getParameter("Clave2");
                        con.actualizar("UPDATE tb_uniatn SET F_NomCli='" + request.getParameter("Nombre").toUpperCase() + "',F_StsCli='" + request.getParameter("radiosts").toUpperCase() + "',F_Rfc='" + request.getParameter("RFC").toUpperCase() + "',F_Calle='" + request.getParameter("calle").toUpperCase() + "',F_NoInt='" + request.getParameter("noint").toUpperCase() + "',F_NoExt='" + request.getParameter("noext").toUpperCase() + "',F_Colonia='" + request.getParameter("Colonia").toUpperCase() + "',F_Cp='" + request.getParameter("CP").toUpperCase() + "',F_Estado='" + request.getParameter("estado").toUpperCase() + "',F_Tel='" + request.getParameter("Telefono").toUpperCase() + "',F_Direc='" + request.getParameter("Direccion").toUpperCase() + "',F_Contacto='" + request.getParameter("Contacto").toUpperCase() + "',F_Transporte='" + request.getParameter("Transporte").toUpperCase() + "',F_DirTrans='" + request.getParameter("DirPaq").toUpperCase() + "' WHERE F_ClaCli='" + Clave2 + "'");
                        //con.insertar("insert into tb_uniatn values (,'A',);");

                        //consql.insertar("insert into TB_Provee values ('" + request.getParameter("Nombre").toUpperCase() + "', '" + request.getParameter("Direccion").toUpperCase() + "', '" + request.getParameter("Colonia").toUpperCase() + "', '" + request.getParameter("CP").toUpperCase() + "', '" + request.getParameter("Telefono").toUpperCase() + "', '" + request.getParameter("RFC").toUpperCase() + "', '" + request.getParameter("FAX").toUpperCase() + "', '" + request.getParameter("Mail").toUpperCase() + "','');");
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }
                    con.cierraConexion();
                    // consql.cierraConexion();
                    out.println("<script>alert('Unidad Modificada correctamente.')</script>");
                    out.println("<script>window.location='catalogoUnidades.jsp'</script>");
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

            }
            
            /*
             *Modificar cliente Registros Unidades
             */
            if (request.getParameter("accion").equals("ModificarUnidad")) {
                /**
                 * Para guardar el proveedor
                 */
                try {
                    String Clave2 = "";
                    //consql.conectar();
                    con.conectar();
                    try {
                        Clave2 = request.getParameter("Clave2");
                        con.actualizar("UPDATE tb_uniatn SET F_NomCli='" + request.getParameter("Nombre").toUpperCase() + "',F_StsCli='" + request.getParameter("radiosts").toUpperCase() + "',F_Rfc='" + request.getParameter("RFC").toUpperCase() + "',F_Calle='" + request.getParameter("calle").toUpperCase() + "',F_NoInt='" + request.getParameter("noint").toUpperCase() + "',F_NoExt='" + request.getParameter("noext").toUpperCase() + "',F_Colonia='" + request.getParameter("Colonia").toUpperCase() + "',F_Cp='" + request.getParameter("CP").toUpperCase() + "',F_Estado='" + request.getParameter("estado").toUpperCase() + "',F_Tel='" + request.getParameter("Telefono").toUpperCase() + "',F_Direc='" + request.getParameter("Direccion").toUpperCase() + "',F_Contacto='" + request.getParameter("Contacto").toUpperCase() + "',F_Transporte='" + request.getParameter("Transporte").toUpperCase() + "',F_DirTrans='" + request.getParameter("DirPaq").toUpperCase() + "' WHERE F_ClaCli='" + Clave2 + "'");
                        //con.insertar("insert into tb_uniatn values (,'A',);");

                        //consql.insertar("insert into TB_Provee values ('" + request.getParameter("Nombre").toUpperCase() + "', '" + request.getParameter("Direccion").toUpperCase() + "', '" + request.getParameter("Colonia").toUpperCase() + "', '" + request.getParameter("CP").toUpperCase() + "', '" + request.getParameter("Telefono").toUpperCase() + "', '" + request.getParameter("RFC").toUpperCase() + "', '" + request.getParameter("FAX").toUpperCase() + "', '" + request.getParameter("Mail").toUpperCase() + "','');");
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }
                    con.cierraConexion();
                    // consql.cierraConexion();
                    out.println("<script>alert('Unidad Modificada correctamente.')</script>");
                    out.println("<script>window.location='catalogoUnidades1.jsp'</script>");
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

            }
            
            if (request.getParameter("accion").equals("obtieneProvee")) {
                /**
                 * Se utilizaba para mandar los proveedores a SGW (YA no se usa)
                 */
                try {
                    proveedores();
                    out.println("<script>alert('Se obtuvieron los Proveedores Correctamente')</script>");
                } catch (Exception e) {
                    out.println("<script>alert('Error al obtener proveedores')</script>");
                }
                out.println("<script>window.location='catalogo.jsp'</script>");
            }
            /**
             * *Modifica datos de los proveedores**
             */
            if (request.getParameter("accion").equals("Modificar")) {
                try {
                    con.conectar();
                    String Clave = "", Descripcion = "", radiosts = "", Direc = "", Tel = "";

                    Clave = request.getParameter("Clave");
                    Descripcion = request.getParameter("Descripcion");
                    radiosts = request.getParameter("radiosts");
                    //radiorigen = request.getParameter("radiorigen");
                    //radiocat = request.getParameter("radiocat");
                    Direc = request.getParameter("Direc");
                    Tel = request.getParameter("Tel");

                    if ((Clave != "") && (Descripcion != "") && (radiosts != "") && (Direc != "") && (Tel != "")) {

                        con.actualizar("UPDATE tb_proveedor SET F_NomPro='" + Descripcion + "',F_Stst='" + radiosts + "',F_Calle='" + Direc + "',F_Tel='" + Tel + "' WHERE F_ClaProve='" + Clave + "'");
                        out.println("<script>alert('Proveedor Modificado correctamente.')</script>");
                        out.println("<script>window.location='catalogo.jsp'</script>");
                    } else {
                        out.println("<script>alert('Favor de llenar todos los campos')</script>");
                        out.println("<script>window.location='ModiProvee.jsp?Clave=" + Clave + "'</script>");
                    }
                    con.cierraConexion();

                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

            }
            /**
             * *Modifica datos clientes de los proveedores**
             */
            if (request.getParameter("accion").equals("ModificarProvee")) {
                try {
                    con.conectar();
                    String Clave = "", Descripcion = "", radiosts = "", Direc = "", Tel = "";

                    Clave = request.getParameter("Clave");
                    Descripcion = request.getParameter("Descripcion");
                    radiosts = request.getParameter("radiosts");
                    //radiorigen = request.getParameter("radiorigen");
                    //radiocat = request.getParameter("radiocat");
                    Direc = request.getParameter("Direc");
                    Tel = request.getParameter("Tel");

                    if ((Clave != "") && (Descripcion != "") && (radiosts != "") && (Direc != "") && (Tel != "")) {

                        con.actualizar("UPDATE tb_proveedor SET F_NomPro='" + Descripcion + "',F_Stst='" + radiosts + "',F_Calle='" + Direc + "',F_Tel='" + Tel + "' WHERE F_ClaProve='" + Clave + "'");
                        out.println("<script>alert('Proveedor Modificado correctamente.')</script>");
                        out.println("<script>window.location='catalogo1.jsp'</script>");
                    } else {
                        out.println("<script>alert('Favor de llenar todos los campos')</script>");
                        out.println("<script>window.location='ModiProvee1.jsp?Clave=" + Clave + "'</script>");
                    }
                    con.cierraConexion();

                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

            }

        } catch (SQLException e) {

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public void proveedores() {
        try {
            //consql.conectar();
            con.conectar();
            try {
                /*con.insertar("truncate table provee_all");
                 ResultSet rset = consql.consulta("select * from TB_Provee;");
                 while (rset.next()) {
                 con.insertar("insert into provee_all values ('" + rset.getString(1) + "', '" + rset.getString(2) + "', '" + rset.getString(3) + "', '" + rset.getString(4) + "', '" + rset.getString(5) + "', '" + rset.getString(6) + "', '" + rset.getString(7) + "', '" + rset.getString(8) + "' ,'" + rset.getString(9) + "', '" + rset.getString(10) + "', '" + rset.getString(11) + "', '" + rset.getString(12) + "', '" + rset.getString(13) + "')");
                 }*/
            } catch (Exception e) {
            }
            con.cierraConexion();
            //consql.cierraConexion();
        } catch (Exception e) {
        }
    }

}
