/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import Correo.CorreoPedido2;
import conn.ConectionDB;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mario
 */
public class ValidarPedidoAuto extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher view = request.getRequestDispatcher("verificarPedido.jsp");

        String accion;

        accion = request.getParameter("accion");

        if (accion == null) {
            accion = "";
        }

        ConectionDB con = new ConectionDB();
        HttpSession sesion = request.getSession();

        if (accion.equals("GuardarVerifica")) {

            try {
                con.conectar();
                Connection c = con.getConn();

                String FechaE="",Cajas="0",Kg="0";
                
                //Cajas = request.getParameter("Cajas");
                //Kg = request.getParameter("KG");
                
                int FolioFactura = 0;
                ResultSet FolioFact = con.consulta("SELECT F_IndFact FROM tb_indice");
                while (FolioFact.next()) {
                    FolioFactura = Integer.parseInt(FolioFact.getString("F_IndFact"));
                }
                int FolFact = FolioFactura + 1;
                con.actualizar("update tb_indice set F_IndFact='" + FolFact + "'");

                String idFact = request.getParameter("pedido");
                String qryFact = "SELECT l.F_ClaPro, m.F_DesPro, l.F_IdLote, l.F_FecCad, ft.F_Cant, l.F_ExiLot, ft.F_Id, ft.F_ClaCli, l.F_Ubica,l.F_FolLot,l.F_ClaOrg,ft.F_FecEnt FROM tb_facttemp ft, tb_uniatn u, tb_lote l, tb_medica m WHERE ltrim(ft.F_ClaCli) = u.F_ClaCli AND l.F_IdLote = ft.F_IdLot AND l.F_ClaPro = m.F_ClaPro AND ft.F_StsFact = 0 AND ft.F_IdFact=?;";
                PreparedStatement ps = c.prepareStatement(qryFact);
                ps.setString(1, idFact);
                ResultSet rset = ps.executeQuery();
                while (rset.next()) {
                    String Clave = rset.getString(1);
                    String FolioLote = rset.getString("F_FolLot");
                    String IdLote = rset.getString("F_IdLote");
                    String Ubicacion = rset.getString("F_Ubica");
                    String ClaProve = rset.getString("F_ClaOrg");
                    String F_Id = rset.getString("F_Id");
                    String claUni = rset.getString("ft.F_ClaCli");
                    FechaE = rset.getString("ft.F_FecEnt");
                    int existencia = rset.getInt("F_ExiLot");
                    int cantidad = rset.getInt("F_Cant");

                    int Diferencia = existencia - cantidad;

                    if (Diferencia >= 0) {
                        if (cantidad > 0) {
                            if (Diferencia == 0) {
                                con.actualizar("UPDATE tb_lote SET F_ExiLot='0' WHERE F_IdLote='" + IdLote + "'");
                            } else {
                                con.actualizar("UPDATE tb_lote SET F_ExiLot='" + Diferencia + "' WHERE F_IdLote='" + IdLote + "'");
                            }
                            //Inserciones
                            con.insertar("insert into tb_movinv values(0,curdate(),'" + FolioFactura + "','51','" + Clave + "','" + cantidad + "','0.0','0.0','-1','" + FolioLote + "','" + Ubicacion + "','" + ClaProve + "',curtime(),'" + sesion.getAttribute("nombre") + "')");
                            con.insertar("insert into tb_factura values(0,'" + FolioFactura + "','" + claUni + "','A',curdate(),'" + Clave + "','" + cantidad + "','" + cantidad + "','0.0','0.0','0.0','" + FolioLote + "','" + FechaE + "',curtime(),'" + sesion.getAttribute("nombre") + "','" + Ubicacion + "','','')");
                            con.actualizar("update tb_facttemp set F_StsFact='5' where F_Id='" + F_Id + "'");
                            

                        }

                    } else {
                        //Error enviando mas de lo existente
                    }

                }
                con.insertar("INSERT INTO tb_cajaskg VALUES('"+FolioFactura+"','"+Cajas+"','"+Kg+"','Pendiente','0000-00-00','00:00:00');");
                
                con.insertar("INSERT INTO tb_foliosval VALUES('"+FolioFactura+"','"+idFact+"','PENDIENTE','',NOW(),'0000-00-00','','0000-00-00 00:00:00');");
                
                CorreoPedido2.enviaCorreo(idFact,FolioFactura+"");
                
                //Fi naliza
                //consql.cierraConexion();
                con.cierraConexion();
                sesion.setAttribute("ClaCliFM", "");
                sesion.setAttribute("FechaEntFM", "");
                sesion.setAttribute("ClaProFM", "");
                sesion.setAttribute("DesProFM", "");
                sesion.setAttribute("F_IndGlobal", null);
                //Aqui tenemos que poner en nulo la variable de folio de dactura
                //Salimos

            } catch (SQLException e) {
                System.out.println("Error: " + e);
            } catch (NumberFormatException e) {
                System.out.println("Error: " + e);
            }
            response.sendRedirect("verificarPedido.jsp");

        }
        /**
         * En la parte de verificar las Órdenes de Reposición de Inventario,
         * para eliminarlas.
         */
        if (accion.equals("EliminarVerifica")) {
            try {
                con.conectar();
                con.insertar("delete from tb_facttemp where F_IdFact = '" + request.getParameter("pedido") + "'");
                con.cierraConexion();
                sesion.setAttribute("pedido", "");
                response.sendRedirect("verificarPedido.jsp");
            } catch (SQLException e) {
            } catch (IOException e) {
            }
        }
        if (accion.equals("modificarPedido")) {
            /**
             * Mandar el parametro de que se modificara por parte de los
             * auditores en el proceso de entrada
             */
            request.getSession().setAttribute("id", request.getParameter("id"));
            response.sendRedirect("editaClavePedido.jsp");
        }
        if (accion.equals("eliminarVerificaPed")) {

            /**
             * Eliminar por parte de verificardor ciertos insumos
             */
            try {
                con.conectar();
                con.borrar2("delete from tb_facttemp where F_Id = '" + request.getParameter("id") + "'");
                System.out.println("delete from tb_facttemp where F_Id = '" + request.getParameter("id") + "'");
                request.setAttribute("NoPedido", request.getParameter("pedidoM"));
                con.cierraConexion();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            view.forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
