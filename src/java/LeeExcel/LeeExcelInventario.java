package LeeExcel;

import conn.ConectionDB;
import java.io.FileInputStream;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Para insertar los requerimientos en la tabla tb_unireq
 *
 * @author Indra Hidayatulloh
 */
public class LeeExcelInventario {

    public boolean obtieneArchivo(String path, String file) {
        Vector vectorDataExcelXLSX;
        String excelXLSXFileName = path + "/exceles/" + file;
        vectorDataExcelXLSX = readDataExcelXLSX(excelXLSXFileName);
        displayDataExcelXLSX(vectorDataExcelXLSX);
        return true;
    }

    public Vector readDataExcelXLSX(String fileName) {
        Vector vectorData = new Vector();

        try {
            FileInputStream fileInputStream = new FileInputStream(fileName);

            XSSFWorkbook xssfWorkBook = new XSSFWorkbook(fileInputStream);

            // Read data at sheet 0
            XSSFSheet xssfSheet = xssfWorkBook.getSheetAt(0);

            Iterator rowIteration = xssfSheet.rowIterator();

            // Looping every row at sheet 0
            while (rowIteration.hasNext()) {
                XSSFRow xssfRow = (XSSFRow) rowIteration.next();
                Iterator cellIteration = xssfRow.cellIterator();

                Vector vectorCellEachRowData = new Vector();

                // Looping every cell in each row at sheet 0
                while (cellIteration.hasNext()) {
                    XSSFCell xssfCell = (XSSFCell) cellIteration.next();
                    vectorCellEachRowData.addElement(xssfCell);
                }

                vectorData.addElement(vectorCellEachRowData);
            }
        } catch (Exception ex) {
        }

        return vectorData;
    }

    /**
     * En este metodo es onde se arma el query y se ejecutan las inserciones
     *
     * @param vectorData
     */
    public void displayDataExcelXLSX(Vector vectorData) {
        // Looping every row data in vector
        ConectionDB con = new ConectionDB();
        for (Object vectorData1 : vectorData) {
            Vector vectorCellEachRowData = (Vector) vectorData1;
            /**
             * Creación del query para hacer las inserciones
             */
            String qry = "insert into inventario_excel values (";
            // looping every cell in each row
            /**
             * Se lee cada una de las celdas, se utiliza el ciclo FOR para que
             * recorra las celdas 0-3
             */
            for (int j = 0; j < 6; j++) {

                /**
                 * Si es 0 quiere decir que es la clave
                 */
                switch (j) {
                    case 0:
                        String Clave = (vectorCellEachRowData.get(j).toString());
                        try {
                            Clave = Clave.substring(0, Clave.lastIndexOf("."));
                        } catch (Exception e) {

                        }

                        qry = qry + "'" + Clave + "' , ";
                        break;
                    case 1:
                        String lote = (vectorCellEachRowData.get(j).toString());
                        qry = qry + "'" + lote + "' , ";
                        break;
                    case 2:
                        DateFormat df3 = new SimpleDateFormat("dd-MMM-yyyy");
                        DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
                        String caducidad = (vectorCellEachRowData.get(j).toString());
                         {
                            try {
                                caducidad = df2.format(df3.parse(caducidad));
                            } catch (ParseException ex) {

                                caducidad = "1970-01-01";
                                Logger.getLogger(LeeExcelInventario.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }

                        qry = qry + "'" + caducidad + "' , ";
                        break;
                    case 3:

                        String cb = (vectorCellEachRowData.get(j).toString());
                        try {
                            cb = cb.substring(0, cb.lastIndexOf("."));
                        } catch (Exception e) {

                        }
                        qry = qry + "'" + cb + "',";
                        break;
                    case 4:
                        String cantidad = (vectorCellEachRowData.get(j).toString());

                        try {
                            cantidad = cantidad.substring(0, cantidad.lastIndexOf("."));
                        } catch (Exception e) {

                        }
                        qry = qry + "'" + cantidad + "',";
                        break;
                    case 5:
                        String costo = (vectorCellEachRowData.get(j).toString());

                        qry = qry + "'" + costo + "'";
                        break;
                }
            }
            try {
                /**
                 * Se finaliza el query agregando campos de fecha y status
                 */
                qry = qry + ",curdate(), 0,0)"; // agregar campos fuera del excel
            } catch (NumberFormatException e) {
                System.out.println(e);
            }
            try {
                con.conectar();
                try {
                    /**
                     * Ejecución del query
                     */
                    con.insertar(qry);
                } catch (SQLException e) {
                    System.out.println(e.getMessage());
                }
                con.cierraConexion();
            } catch (SQLException e) {
            }
        }
    }

}
