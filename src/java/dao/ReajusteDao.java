/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import conn.ConectionDBMario;
import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import modelos.Producto;

/**
 * Colexxion de Query utiles para el reajuste por salida
 *
 * @author Mario
 */
public class ReajusteDao {

    private final Connection connection;

    /**
     * Constructor en el que se establece la conexion
     */
    public ReajusteDao() {
        connection = ConectionDBMario.getConnection();
    }

    /**
     * Se encarga de cerrar la conexion
     */
    public void close() {
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(ReajusteDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param clave Clave del producto a consultar
     * @return Lista con todos los lotes del producto que se encuentran en la
     * Ubiciacion REJA_DEVOL
     */
    public List<Producto> getAllLotesByClave(String clave) {
        List<Producto> productos = new ArrayList<Producto>();
        try {

            PreparedStatement ps = connection.prepareStatement("select F_ClaLot "
                    + "from tb_lote "
                    + "where F_ClaPro=? and F_Ubica='REJA_DEVOL' "
                    + "GROUP BY F_ClaLot");
            ps.setString(1, clave);
            System.out.println(ps);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                Producto producto = new Producto();
                producto.setLote(rs.getString("F_ClaLot"));
                productos.add(producto);

            }

        } catch (Exception e) {
            Logger.getLogger(ReajusteDao.class.getName()).log(Level.SEVERE, null, e);
        }
        return productos;
    }

    /**
     *
     * @param clave Clave del producto a consultar
     * @return Lista con todos las caducidades del producto que se encuentran en
     * la Ubiciacion REJA_DEVOL
     */
    public List<Producto> getAllCadusByClave(String clave) {
        List<Producto> productos = new ArrayList<Producto>();
        try {
            PreparedStatement ps = connection.prepareStatement("select F_FecCad "
                    + "from tb_lote "
                    + "where F_ClaPro=? and F_Ubica='REJA_DEVOL' "
                    + "GROUP BY F_FecCad");
            ps.setString(1, clave);
            System.out.println(ps);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                Producto producto = new Producto();
                producto.setCaducidad(convertirFechaFromSQL(rs.getString("F_FecCad")));
                productos.add(producto);

            }

        } catch (Exception e) {
            Logger.getLogger(ReajusteDao.class.getName()).log(Level.SEVERE, null, e);
        }
        return productos;
    }

    /**
     *
     * @param clave Clave del producto a consultar
     * @param lote
     * @return Lista con todos las caducidades del producto que se encuentran en
     * la Ubiciacion REJA_DEVOL
     */
    public List<String> getAllCadusByClaveString(String clave, String lote) {
        List<String> productos = new ArrayList<String>();
        try {
            PreparedStatement ps = connection.prepareStatement("select F_FecCad "
                    + "from tb_lote "
                    + "where F_ClaPro=? and F_ClaLot=? and F_Ubica='REJA_DEVOL' "
                    + "GROUP BY F_FecCad");
            ps.setString(1, clave);
            ps.setString(2, lote);
            System.out.println(ps);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                productos.add(convertirFechaFromSQL(rs.getString("F_FecCad")));

            }

        } catch (Exception e) {
            Logger.getLogger(ReajusteDao.class.getName()).log(Level.SEVERE, null, e);
        }
        return productos;

    }

    /**
     * Obtiene un producto en base a la clave,lote y caducidad
     *
     * @param clave La clave del producto
     * @param lote El lote del producto
     * @param caducidad La caducidad del producto
     * @return Objeto del producto
     */
    public Producto getProductoByCLC(String clave, String lote, String caducidad) {
        Producto producto = null;
        try {
            PreparedStatement ps = connection.prepareStatement("select l.F_ExiLot as exis,m.F_DesPro as des "
                    + "from tb_lote as l, tb_medica as m "
                    + "where l.F_ClaPro=m.F_ClaPro and l.F_ClaPro=? and l.F_ClaLot=? and l.F_FecCad=? and l.F_Ubica='REJA_DEVOL'");
            ps.setString(1, clave);
            ps.setString(2, lote);
            ps.setString(3, convertirFechaToSQL(caducidad));
            System.out.println(ps);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {

                producto = new Producto();
                producto.setExistencia(rs.getInt("exis"));
                producto.setDescripcion(rs.getString("des"));
                producto.setClave(clave);
                producto.setCaducidad(caducidad);
                producto.setLote(lote);

            }

        } catch (Exception e) {
            Logger.getLogger(ReajusteDao.class.getName()).log(Level.SEVERE, null, e);
        }

        return producto;
    }

    /**
     * Obtiene una lista de con las descripciones de los productos
     *
     * @return Lista de los productos
     */
    public List<String> getAllDesc() {
        List<String> desc = new ArrayList<String>();
        try {
            PreparedStatement ps = connection.prepareStatement("select F_DesPro "
                    + "from tb_medica ");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                desc.add(rs.getString("F_DesPro"));

            }

        } catch (Exception e) {
            Logger.getLogger(ReajusteDao.class.getName()).log(Level.SEVERE, null, e);
        }
        return desc;
    }

    /**
     * Obtiene la clave en base a la descripcion del producto
     *
     * @param desc La descripcion del producto
     * @return La clave del producto
     */
    public String getClaveByDesc(String desc) {
        String clave = null;
        try {
            PreparedStatement ps = connection.prepareStatement("select F_ClaPro "
                    + "from tb_medica "
                    + "where F_DesPro=?");
            ps.setString(1, desc);
            System.out.println(ps);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {

                clave = rs.getString("F_ClaPro");

            }

        } catch (Exception e) {
            Logger.getLogger(ReajusteDao.class.getName()).log(Level.SEVERE, null, e);
        }

        return clave;
    }

    /**
     * Convierte de YYYY-mm-dd a dd/mm/YYYY
     *
     * @param fecha
     * @return outFecha
     */
    public String convertirFechaFromSQL(String fecha) {
        String[] fechaArray = fecha.split("-");
        String outFecha = fechaArray[2] + "/" + fechaArray[1] + "/" + fechaArray[0];
        return outFecha;
    }

    /**
     * Convierte de YYYY-mm-dd a dd/mm/YYYY
     *
     * @param fecha
     * @return outFecha
     */
    public String convertirFechaToSQL(String fecha) {
        String[] fechaArray = fecha.split("/");
        String outFecha = fechaArray[2] + "/" + fechaArray[1] + "/" + fechaArray[0];
        return outFecha;
    }

    /**
     * Se encarga de realizar las transacciones para el ajuste por salida
     *
     * @param clave La clave del producto
     * @param lote El lote del producto
     * @param cadu La caducidad del producto
     * @param cantidad La cantidad del producto
     * @param exis La existencia del producto
     * @param justi La justificacion por la que se realiza el ajuste
     * @param usuario El usuario que realiza el ajuste
     * @return Mensaje obtenido
     */
    public String updateTBLOTE(String clave, String lote, String cadu, int cantidad, int exis, String justi, String usuario) {
        String msg = "";
        cadu = convertirFechaToSQL(cadu);

        String SQL_INSERT = "UPDATE tb_lote "
                + "SET F_ExiLot=? "
                + "WHERE F_ClaPro=? AND F_ClaLot=? AND F_FecCad=? AND F_Ubica='REJA_DEVOL'";

        try {

            PreparedStatement statement = connection.prepareStatement(SQL_INSERT,
                    Statement.RETURN_GENERATED_KEYS);

            statement.setInt(1, cantidad);
            statement.setString(2, clave);
            statement.setString(3, lote);
            statement.setString(4, cadu);

            statement.executeUpdate();
            statement.close();

        } catch (Exception e) {
            msg = "ERROR: Acurrio un error, contacte con sistemas (001)";
            Logger.getLogger(ReajusteDao.class.getName()).log(Level.SEVERE, null, e);
        }
        int folLot = 0, claOrg = 0;
        try {
            PreparedStatement ps = connection.prepareStatement("select F_FolLot,F_ClaOrg "
                    + "from tb_lote "
                    + "where F_ClaPro=?");
            ps.setString(1, clave);
            System.out.println(ps);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {

                folLot = rs.getInt("F_FolLot");
                claOrg = rs.getInt("F_ClaOrg");

            }

        } catch (Exception e) {
            Logger.getLogger(ReajusteDao.class.getName()).log(Level.SEVERE, null, e);
            msg = "ERROR: Acurrio un error, contacte con sistemas (002)";
        }

        float costo = getCostoByClave(clave);
        SQL_INSERT = "insert into tb_movinv(`F_FecMov`, `F_DocMov`, `F_ConMov`, `F_ProMov`, `F_CantMov`, `F_CostMov`, `F_TotMov`, `F_SigMov`"
                + ", `F_LotMov`, `F_UbiMov`, `F_ClaProve`, `F_hora`, `F_User`)"
                + " values(curdate(),0,?,?,?,?,?,?,?,?,?,curtime(),?)";
        String id_generado = null;
        try {

            PreparedStatement statement = connection.prepareStatement(SQL_INSERT,
                    Statement.RETURN_GENERATED_KEYS);
            int movimiento = -1;
            int nuevaCan = exis - cantidad;
            int movimientoCode = 58;
            if (nuevaCan < 0) {
                nuevaCan *= -1;
                movimiento = 1;
                movimientoCode = 57;

            }
            statement.setInt(1, movimientoCode);
            statement.setString(2, clave);
            statement.setInt(3, nuevaCan);
            statement.setFloat(4, costo);
            statement.setFloat(5, costo * cantidad);
            statement.setFloat(6, movimiento);
            statement.setInt(7, folLot);
            statement.setString(8, "REJA_DEVOL");
            statement.setInt(9, claOrg);
            statement.setString(10, usuario);
            // ...
            System.out.println(statement);
            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Creating Reporte failed, no rows affected.");
            }

            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                id_generado = "" + generatedKeys.getLong(1);
            } else {
                throw new SQLException("Creating Reporte failed, no ID obtained.");
            }

            statement.close();

        } catch (Exception e) {
            Logger.getLogger(ReajusteDao.class.getName()).log(Level.SEVERE, null, e);
            msg = "ERROR: currio un error, contacte con sistemas (003)";
        }

        if (id_generado != null) {
            SQL_INSERT = "insert into tb_justireajustesalida(`F_IdMov`, `F_Justi`)"
                    + " values(?,?)";
            try {

                PreparedStatement statement = connection.prepareStatement(SQL_INSERT,
                        Statement.RETURN_GENERATED_KEYS);
                statement.setString(1, id_generado);
                statement.setString(2, justi);
                // ...

                statement.executeUpdate();

                statement.close();

            } catch (Exception e) {
                Logger.getLogger(ReajusteDao.class.getName()).log(Level.SEVERE, null, e);
                msg = "ERROR: Acurrio un error, contacte con sistemas (004)";
            }
        }

        return msg;
    }

    /**
     * Obtiene el costo del producto por su clave
     *
     * @param clave Clave del producto
     * @return Costo del producto
     */
    public float getCostoByClave(String clave) {
        float costo = 0;
        try {
            PreparedStatement ps = connection.prepareStatement("select F_Costo "
                    + "from tb_medica "
                    + "where F_ClaPro=?");
            ps.setString(1, clave);
            System.out.println(ps);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {

                costo = rs.getFloat("F_Costo");

            }

        } catch (Exception e) {
            Logger.getLogger(ReajusteDao.class.getName()).log(Level.SEVERE, null, e);
        }

        return costo;
    }

    public String getJustificacion() {
        String costo = null;
        try {
            PreparedStatement ps = connection.prepareStatement("select F_Justi "
                    + "from tb_justireajustesalida "
                    + "where F_IdJust=10");
            System.out.println(ps);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {

                costo = new String(rs.getString("F_Justi").getBytes(), "UTF-8");

            }

        } catch (Exception e) {
            Logger.getLogger(ReajusteDao.class.getName()).log(Level.SEVERE, null, e);
        }

        return costo;
    }

}
