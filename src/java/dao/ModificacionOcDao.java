/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conn.ConectionDBMario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelos.OrdenCompra;
import modelos.Producto;

/**
 *
 * @author Mario
 */
public class ModificacionOcDao {

    private final Connection connection;

    /**
     * Constructor en el que se establece la conexion
     */
    public ModificacionOcDao() {
        connection = ConectionDBMario.getConnection();
    }

    /**
     * Se encarga de cerrar la conexion
     */
    public void close() {
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(ReajusteDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<String> getFoliosRemisionByOc(String oc) {
        List<String> folios = new ArrayList<String>();

        try {
            PreparedStatement ps = connection.prepareStatement("SELECT F_FolRemi "
                    + "FROM tb_compra "
                    + "WHERE F_OrdCom=? "
                    + "GROUP BY F_FolRemi");
            ps.setString(1, oc);
            System.out.println(ps);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                folios.add(rs.getString("F_FolRemi"));

            }

        } catch (SQLException e) {
            Logger.getLogger(ModificacionOcDao.class.getName()).log(Level.SEVERE, null, e);
        }

        return folios;
    }

    public OrdenCompra getOcByFolio(String oc, String folio, int total) {
        OrdenCompra orden = null;

        try {
            PreparedStatement ps = connection.prepareStatement("select p.F_NomPro,c.F_FecApl "
                    + "from tb_compra as c , tb_proveedor as p "
                    + "where p.F_ClaProve=c.F_ProVee AND F_FolRemi=? AND F_OrdCom=? ");

            ps.setString(1, folio);
            ps.setString(2, oc);

            System.out.println(ps);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                orden = new OrdenCompra();
                orden.setOc(oc);
                orden.setFecha(rs.getString("c.F_FecApl"));
                orden.setFolioRem(folio);
                orden.setTotal(total);
                orden.setProveedor(rs.getString("p.F_NomPro"));

            }

        } catch (SQLException e) {
            Logger.getLogger(ModificacionOcDao.class.getName()).log(Level.SEVERE, null, e);
        }
        return orden;

    }

    public List<Producto> getDetalleOcByOcFolioRem(String oc, String folioRem) {
        List<Producto> productos = new ArrayList<Producto>();

        try {
            PreparedStatement ps = connection.prepareStatement("Select c.F_IdCom,c.F_ClaPro,l.F_ClaLot,l.F_FecCad,m.F_DesMar,c.F_CanCom "
                    + "from tb_lote as l, tb_compra as c,tb_marca as m "
                    + "where l.F_FolLot = c.F_Lote AND m.F_ClaMar=l.F_ClaMar AND F_FolRemi=? AND F_OrdCom=? "
                    + "GROUP BY F_IdCom");

            ps.setString(2, oc);
            ps.setString(1, folioRem);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                Producto producto = new Producto();
                producto.setClave(rs.getString("c.F_ClaPro"));
                producto.setExistencia(rs.getInt("c.F_CanCom"));
                producto.setMarca(rs.getString("m.F_DesMar"));
                producto.setLote(rs.getString("l.F_ClaLot"));
                producto.setCaducidad(rs.getString("l.F_FecCad"));
                producto.setId(rs.getString("c.F_IdCom"));
                productos.add(producto);

            }

        } catch (SQLException e) {
            Logger.getLogger(ModificacionOcDao.class.getName()).log(Level.SEVERE, null, e);
        }

        return productos;
    }

    /**
     *
     * Actualiza la existencia
     *
     * @param idCom
     * @param nuevaCantidad
     * @param justi
     * @param usu
     * @return
     */
    public String EditarCantidad(String idCom, int nuevaCantidad, String justi, String usu) {
        String msg = "";

        String SQL_INSERT = "UPDATE tb_compra "
                + "SET F_CanCom=? "
                + "WHERE F_IdCom=?";

        try {

            PreparedStatement statement = connection.prepareStatement(SQL_INSERT);
            statement.setInt(1, nuevaCantidad);
            statement.setString(2, idCom);

            System.out.println(statement);

            statement.executeUpdate();
            statement.close();

        } catch (Exception e) {
            msg = "ERROR: Acurrio un error, contacte con sistemas (001)";
            Logger.getLogger(ReajusteDao.class.getName()).log(Level.SEVERE, null, e);
        }

        SQL_INSERT = "INSERT INTO tb_justimodoc(`F_IdMod`, `F_Justi`, `F_Usu`,`F_FecHora`)"
                + " VALUES(?,?,?,NOW())";
        try {

            PreparedStatement statement = connection.prepareStatement(SQL_INSERT);
            statement.setString(1, idCom);
            statement.setString(2, justi);
            statement.setString(3, usu);

            statement.executeUpdate();

            statement.close();

        } catch (Exception e) {
            Logger.getLogger(ReajusteDao.class.getName()).log(Level.SEVERE, null, e);
            msg = "ERROR: Acurrio un error, contacte con sistemas (004)";
        }

        return msg;

    }

    /**
     * Se obtiene la Orden de reposición de inventario en base al id de la
     * compra
     *
     * @param id Id de la compra
     * @return Orden de reposición de inventario
     */
    public String getOcById(String id) {
        String oc = null;
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT F_OrdCom "
                    + "FROM tb_compra "
                    + "WHERE F_IdCom=? ");
            ps.setString(1, id);
            System.out.println(ps);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                oc = (rs.getString("F_OrdCom"));

            }

        } catch (SQLException e) {
            Logger.getLogger(ModificacionOcDao.class.getName()).log(Level.SEVERE, null, e);
        }

        return oc;
    }

    /**
     * Se obtiene el folio de la remision en base al id de la compra
     *
     * @param id Id de la compra
     * @return Folio de remicion
     */
    public String getFolioRemById(String id) {
        String oc = null;
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT F_FolRemi "
                    + "FROM tb_compra "
                    + "WHERE F_IdCom=? ");
            ps.setString(1, id);
            System.out.println(ps);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                oc = (rs.getString("F_FolRemi"));

            }

        } catch (SQLException e) {
            Logger.getLogger(ModificacionOcDao.class.getName()).log(Level.SEVERE, null, e);
        }

        return oc;
    }

}
