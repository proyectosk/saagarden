/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

/**
 *
 * @author Mario
 */
public class Maximos {
    
    
    String clave,desc,maximo,rec,porc,cpmSol;
    String total;
    String asf;

    public String getCpmSol() {
        return cpmSol;
    }

    public void setCpmSol(String cpmSol) {
        this.cpmSol = cpmSol;
    }

    
    
    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getAsf() {
        return asf;
    }

    public void setAsf(String asf) {
        this.asf = asf;
    }
    
    

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getMaximo() {
        return maximo;
    }

    public void setMaximo(String maximo) {
        this.maximo = maximo;
    }

    public String getRec() {
        return rec;
    }

    public void setRec(String rec) {
        this.rec = rec;
    }

    public String getPorc() {
        return porc;
    }

    public void setPorc(String porc) {
        this.porc = porc;
    }
    
    
    
    
}
