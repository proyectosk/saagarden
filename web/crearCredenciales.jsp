<%-- 
    Document   : SalidaAjuste
    Created on : 14/07/2015, 03:49:42 PM
    Author     : Mario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%

    HttpSession sesion = request.getSession();
    String info = null;

    String usua = "", tipo = "";
    if (sesion.getAttribute("nombre") != null) {
        usua = (String) sesion.getAttribute("nombre");
        tipo = (String) sesion.getAttribute("Tipo");
    } else {
        response.sendRedirect("index.jsp");
    }
    /*String usua = "";
     if (sesion.getAttribute("nombre") != null) {
     usua = (String) sesion.getAttribute("nombre");
     System.out.println("login ok");
     } else {
     System.out.println("sin nombre");
     request.setAttribute("mensaje", "la sesion a terminado.  Por favor ingrese de nuevo sus credenciales.");
     RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
     rd.forward(request, response);
     }*/

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Estilos CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="css/cupertino/jquery-ui-1.10.3.custom.css" />
        <link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap.css">
        <!---->
        <title>SIALSS</title>
    </head>
    <body>
        <div class="container">

            <h1>SIALSS</h1>
            <h4>Módulo - Sistema de Creaci&oacute;n y Mantenimiento de Credenciales (SCMC)</h4>
            <div style="width: 90%; margin: auto;">
                <div>
                    <h3>Creaci&oacute;n de credenciales</h3>
                </div>

                <div class="panel-body">

                    <div class="panel">

                        <c:if test="${usuarioYa!=null}">
                            <div class="row">
                                <div class="alert alert-warning" role="alert">
                                    <span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span>
                                    <span class="sr-only">Advertencia:</span>
                                    <c:out value="${usuarioYa}"></c:out>
                                    </div>
                                </div>
                        </c:if>
                        <c:if test="${usuarioOK!=null}">
                            <div class="row">
                                <div class="alert alert-success" role="alert">
                                    <span class="glyphicon glyphicon-check" aria-hidden="true"></span>
                                    <span class="sr-only">Hecho:</span>
                                    <c:out value="${usuarioOK}"></c:out>
                                    </div>
                                </div>
                        </c:if>

                        <form class="form-horizontal" name ="altaUsuarios" id="altaUsuarios" action="ControlUsuarios" method="post" data-fv-framework="bootstrap"
                              data-fv-icon-valid="glyphicon glyphicon-ok"
                              data-fv-icon-invalid="glyphicon glyphicon-remove"
                              data-fv-icon-validating="glyphicon glyphicon-refresh">

                            <!-- Default panel contents -->
                            <div class="panel-heading">DATOS DEL USUARIO</div>

                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-2 control-label">
                                        <b>NOMBRE(S):</b>
                                    </div>
                                    <div class="col-lg-4">
                                        <input class="form-control" name="nombre" required  value="${nom}" />
                                        <br>
                                    </div>

                                </div>
                                <br />
                            <div class="panel-body">
                                <div class="row">
                                    
                                    <div class="col-lg-2 control-label">
                                        <b>APELLIDO PAT.:</b>
                                    </div>
                                    <div class="col-lg-4">
                                        <input class="form-control" name="apellido" required  value="${apellido}" />
                                        <br>
                                    </div>

                                    <div class="col-lg-2 control-label">
                                        <b>APELLIDO MAT.:</b>
                                    </div>
                                    <div class="col-lg-4">
                                        <input class="form-control" name="apellidomat" required  value="${apellidomat}" />
                                        <br>
                                    </div>
                                </div>
                                <br />
                                <div class="row">

                                    <div class="col-lg-2 control-label" align="left">
                                        <b>USUARIO:</b>
                                    </div>
                                    <div class="col-lg-4">
                                        <input class="form-control" name="usuario" required value="${usuario}" />
                                        <br>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-lg-2 control-label">
                                        <b>CORREO ELECTRÓNICO:</b>
                                    </div>
                                    <div class="col-lg-4">
                                        <input  type="email" class="form-control" name="email" id="email" required value="${email}" />
                                        <br>
                                    </div>

                                </div>

                                <br>
                                <div class="row">

                                    <div class="col-lg-2 control-label">
                                        <b>CONTRASEÑA:</b>
                                    </div>
                                    <div class="col-lg-4">
                                        <input  type="password" class="form-control" name="password" id="password" required value="" />
                                        <br>
                                    </div>
                                    <div class="col-lg-2 control-label">
                                        <b>CONFIRMAR CONTRASEÑA:</b>
                                    </div>
                                    <div class="col-lg-4">
                                        <input  type="password" class="form-control" required value="" name="confirmPassword" id="confirmPassword"
                                                data-fv-identical="true"
                                                data-fv-identical-field="password"
                                                data-fv-identical-message="La contraseña no coincide"/>
                                        <br>
                                    </div>

                                </div>
                                <br />


                                <div class="row">
                                    <div class="col-lg-2 control-label">
                                        <b>ROL:</b>
                                    </div>
                                    <div class="col-lg-4">
                                        <select name="rol" class="form-control">
                                            <option value="8">Sistemas</option>
                                            <option value="7">Facturaci&oacute;n</option>
                                            <option value="6">Montacarguista</option>
                                            <option value="5">Consulta</option>
                                            <option value="4">Consulta</option>
                                            <option value="3">Administrador</option>
                                            <option value="2">Auditori&iacute;a</option>
                                            <option value="1">Recibo</option>
                                        </select>
                                        <br>
                                    </div>


                                </div>

                                <hr>

                                <div class="row">

                                    <div class="col-lg-2 control-label">
                                        <b>CONTRASEÑA ADMINISTRADOR:</b>
                                    </div>
                                    <div class="col-lg-4">
                                        <input  type="password" class="form-control" name="passwordAdmin" required value="${usuario.pass}" />
                                        <br>
                                    </div>
                                </div>


                                <br />
                                <button type="submit" name="accion" value="crear" class="btn btn-success">Crear credencial</button>
                            </div>

                        </form>
                    </div>

                </div>
                <br><br><br>
                <div class="navbar navbar-fixed-bottom navbar-inverse">
                    <div class="text-center text-muted">
                        GNK Logística || Desarrollo de Aplicaciones 2009 - 2015 <span class="glyphicon glyphicon-registration-mark"></span><br />
                        Todos los Derechos Reservados
                    </div>
                </div>
            </div>
        </div>
    </body> 
    <STYLE TYPE="text/css" media="all">
        .ui-autocomplete { 
            position: absolute; 
            cursor: default; 
            height: 200px; 
            overflow-y: scroll; 
            overflow-x: hidden;}
    </STYLE>





    <script src="js/jquery-1.9.1.js" type="text/javascript"></script>

    <script src="js/jquery-ui.js" type="text/javascript"></script>

    <script src="js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.js" type="text/javascript"></script>

    <script type="text/javascript">

 
        $("#altaUsuarios").validate({
            rules: {
                password: "required",
                confirmPassword: {
                    equalTo: "#password",
                    minlength: 5
                }

            },
            messages: {
                confirmPassword: {
                    minlength: "Tu contraseña debe de tener 8 caracteres al menos",
                    equalTo: "Por favor, ingrese la misma contraseña"
                }
            }
        });
    </script>
</html>
