<%-- 
    Document   : index
    Created on : 17/02/2014, 03:34:46 PM
    Author     : Americo
--%>

<%@page import="java.util.Date"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="conn.*" %>
<!DOCTYPE html>
<%java.text.DateFormat df2 = new java.text.SimpleDateFormat("yyyy-MM-dd"); %>
<%
    /**
     * Para seleccionar de donde se tomará el insumo en la facturación manual
     */
    HttpSession sesion = request.getSession();
    String usua = "", tipo = "", nomUsu = "";
    if (sesion.getAttribute("Usuario") != null) {
        usua = (String) sesion.getAttribute("Usuario");
        nomUsu = (String) sesion.getAttribute("NombreU");
    } else {
        response.sendRedirect("indexGarden.jsp");
    }
    ConectionDB con = new ConectionDB();

    String ClaCli = "", FechaEnt = "", ClaPro = "", DesPro = "", Cantidad = "",F_IndGlobal="";
    String nombrePAQ="",DirecPAQ="";
    try {
        ClaCli = (String) sesion.getAttribute("ClaCliFM");
        FechaEnt = (String) sesion.getAttribute("FechaEntFM");
        ClaPro = (String) sesion.getAttribute("ClaProFM");
        DesPro = (String) sesion.getAttribute("DesProFM");
        Cantidad = (String) request.getAttribute("Cantidad");
        F_IndGlobal = (String) sesion.getAttribute("F_IndGlobal");
        nombrePAQ = (String) sesion.getAttribute("nombrePAQ");
        DirecPAQ = (String) sesion.getAttribute("DirecPAQ");
    } catch (Exception e) {

    }
    if (ClaCli == null) {
        ClaCli = "";
    }
    if (FechaEnt == null) {
        FechaEnt = "";
    }
    if (ClaPro == null) {
        ClaPro = "";
    }
    if (DesPro == null) {
        DesPro = "";
    }
    if (Cantidad == null) {
        Cantidad = "";
    }

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Estilos CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="css/cupertino/jquery-ui-1.10.3.custom.css" />
        <!--link href="css/navbar-fixed-top.css" rel="stylesheet"-->
        <!---->
        <title>SIALSS</title>
    </head>
    <body>
        <div class="container">
            <h1>SIALSS</h1>
            <h4>Módulo - Sistema de Administración de Almacenes (SAA)</h4>

            <%@include file="jspf/menuPrincipal.jspf"%>

            <div class="row">
                <div class="col-sm-12">
                    <h2>Facturación Manual</h2>
                </div>
            </div>
            <hr/>
            <form action="PedidoManual" method="post">
                <div class="row">
                    <div class="col-sm-1">
                        <h4>Unidad:</h4>
                    </div>
                    <div class="col-sm-5">
                        <select class="form-control" name="ClaCli" id="ClaCli">
                            <option value="">-Seleccione Unidad-</option>
                            <%                                try {
                                    con.conectar();
                                    ResultSet rset = con.consulta("select F_ClaCli, F_NomCli from tb_uniatn");
                                    while (rset.next()) {
                            %>
                            <option value="<%=rset.getString(1)%>"
                                    <%
                                        if (rset.getString(1).equals(ClaCli)) {
                                            out.println("selected");
                                        }
                                    %>
                                    ><%=rset.getString(2)%></option>
                            <%
                                    }
                                    con.cierraConexion();
                                } catch (Exception e) {

                                }
                            %>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <h4>Fecha de Entrega</h4>
                    </div>
                    <div class="col-sm-2">
                        <input type="date" class="form-control" name="FechaEnt" id="FechaEnt" min="<%=df2.format(new Date())%>" value="<%=FechaEnt%>"/>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">

                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-1">
                                <h4>Clave:</h4>
                            </div>
                            <div class="col-sm-2">
                                <input class="form-control" readonly="" value="<%=ClaPro%>"/>
                            </div>
                            <div class="col-sm-2">
                                <h4>Descripción:</h4>
                            </div>
                            <div class="col-sm-7">
                                <textarea class="form-control" readonly=""><%=DesPro%></textarea>
                            </div>
                        </div>
                        <br/>

                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-sm-2">
                                <h4>Cantidad a Facturar:</h4>
                            </div>
                            <div class="col-sm-2">
                                <input class="form-control" name="Cantidad" id="Cantidad" value="<%=Cantidad%>" readonly=""/>
                            </div>
                            <div class="col-sm-2 col-sm-offset-6">
                                <a class="btn btn-block btn-default" href="pedidoManual2.jsp">Regresar</a>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
            <form action="PedidoManual" method="post">
                <input class="form-control" id="unidad1" name="unidad1" type="hidden" value="<%=Cantidad%>" />
                <input class="form-control" id="folio" name="folio" type="hidden" value="<%=F_IndGlobal%>" />
                <input class="form-control" id="ClaCli" name="ClaCli" type="hidden" value="<%=ClaCli%>" />
                <input class="form-control" id="FechaEnt" name="FechaEnt" type="hidden" value="<%=FechaEnt%>" />
                <input class="form-control" id="usua" name="usua" type="hidden" value="<%=usua%>" />
                
            <table class="table table-condensed table-striped table-bordered table-responsive">
                <tr>
                    <td>Clave</td>
                    <td>Lote</td>
                    <!--td>Caducidad</td-->
                    <!--td>Ubicación</td-->
                    <td>Cantidad</td>
                    <!--td>Seleccionar</td-->
                    <td></td>
                </tr>
                <%
                    /**
                     * Listado de insumos para seleccionar. NO se puede tomar de
                     * la ubicación reja_devol
                     */
                    try {
                        con.conectar();
                        String cantTemp1 ="",Concatenar="";
                        
                                                
                        ResultSet rset = con.consulta("select F_ClaPro, F_ClaLot, DATE_FORMAT(F_FecCad, '%d/%m/%Y'), SUM(F_ExiLot), F_FolLot,F_FecCad from tb_lote where F_ClaPro = '" + ClaPro + "' and F_ExiLot!=0 GROUP BY F_ClaPro, F_ClaLot,F_FecCad,F_FolLot order by F_FecCad,F_ClaLot asc   ");
                        while (rset.next()) {
                            
                            int cant = 0, cantTemp = 0;
                            int cantLot = rset.getInt(4);
                            /**
                             * Se resta la cantidad apartada para el insumo en
                             * específico
                             */
                            ResultSet rset2 = con.consulta("select SUM(F_Cant) from tb_facttemp f INNER JOIN tb_lote l on f.F_IdLot=l.F_IdLote where F_FolLot = '" + rset.getString("F_FolLot") + "' and F_StsFact <5 ");
                            while (rset2.next()) {
                                cantTemp1 = rset2.getString(1);
                            }
                            if ((cantTemp1 =="") || (cantTemp1 == null)){
                                cantTemp=0;
                            }else{
                                
                                cantTemp = Integer.parseInt(cantTemp1);
                            }
                            cant = cantLot - cantTemp;
                            Concatenar = rset.getString(1)+";"+rset.getString(2)+":"+rset.getString(6)+"&"+Cantidad;
                            /**
                             * Se muestran los registros de donde se puede tomar
                             * el insumo, no se puede tomar de uno que tenga
                             * menos de lo que se solicita
                             */
                            if(cant > 0){
                %>
                <tr>
                    <td><%=rset.getString(1)%></td>
                    <td><%=rset.getString(2)%></td>
                    <!--td><%//=rset.getString(3)%></td-->
                    <!--td><%//=rset.getString(4)%></td-->
                    <td><%=cant%></td>
                    <!--td>
                        <form action="PedidoManual" method="post">
                            <input name="FolLot" value="<%//=rset.getString(7)%>" class="hidden" readonly=""/>
                            <input name="IdLot" value="<%//=rset.getString(6)%>" class="hidden" readonly=""/>
                            <input class="hidden" name="Cant" id="Cant<%//=rset.getString(6)%>" value=""/>
                            <input class="hidden" name="CantAlm_<%//=rset.getString(6)%>" id="CantAlm_<%//=rset.getString(6)%>" value="<%//=cant%>"/>
                            <button name="accion" value="AgregarClave" id="<%//=rset.getString(6)%>" class="btn btn-block btn-success" onclick="return validaCantidad(this.id);"><span class="glyphicon glyphicon-ok"></span></button>
                        </form>
                    </td-->
                    <td>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="checkRemis" onchange="activarBtnReCal();" value="<%=Concatenar%>">
                            </label>
                        </div>
                    </td>
                </tr>
                <%
                        }
                        }
                        con.cierraConexion();
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                %>
            </table>
            <div class="row">
                <label class="col-sm-2 text-left">
                    <h4>Observaciones:</h4>
                </label>
                <div class="col-sm-10">
                    <textarea id="Observaciones" name="Observaciones" class="form-control" rows="4"></textarea>
                </div>
            </div>
            <br />
              <button type="submit" class="btn btn-warning btn-block" id="btnImpMult" name="accion" value="SelectMultples" >Selección Multiples</button>            
        </form>
        </div>
        <br><br><br>
        <div class="navbar navbar-fixed-bottom navbar-inverse">
            <div class="text-center text-muted">
                GNK Logística || Desarrollo de Aplicaciones 2009 - 2016 <span class="glyphicon glyphicon-registration-mark"></span><br />
                Todos los Derechos Reservados
            </div>
        </div>
        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="js/jquery-1.9.1.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery-ui-1.10.3.custom.js"></script>
        <script>
                                function cambiaLoteCadu(elemento) {
                                    var indice = elemento.selectedIndex;
                                    document.getElementById('SelectCadu').selectedIndex = indice;
                                }

                                function validaCantidad(e) {
                                    /**
                                     * Aquí se compara el solicitado contra lo de almacén para ver que se exceda
                                     */
                                    var cantidadSol = document.getElementById('Cantidad').value;
                                    document.getElementById('Cant' + e).value = cantidadSol;
                                    var cantidadAlm = document.getElementById('CantAlm_' + e).value;
                                    if (parseInt(cantidadSol) > parseInt(cantidadAlm)) {
                                        alert('La cantidad a facturar no puede ser mayor a la cantidad de esa ubicación');
                                        return false;
                                    }

                                    /*var confirma = confirm('Seguro de usar esta ubicación?');
                                     if (confirma === false) {
                                     return false;
                                     }*/
                                }
        </script>
    </body>
</html>

