<%-- 
    Document   : index
    Created on : 17/02/2014, 03:34:46 PM
    Author     : Americo
--%>

<%@page import="java.util.Date"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="conn.*" %>
<!DOCTYPE html>
<%
    /**
     * Para verificar las compras que están en proceso de ingreso
     */
    DecimalFormat formatter = new DecimalFormat("#,###,###");
    DecimalFormat formatterDecimal = new DecimalFormat("#,###,##0.00");
    DecimalFormatSymbols custom = new DecimalFormatSymbols();
    custom.setDecimalSeparator('.');
    custom.setGroupingSeparator(',');
    formatter.setDecimalFormatSymbols(custom);
    formatterDecimal.setDecimalFormatSymbols(custom);
    HttpSession sesion = request.getSession();
    String usua = "";
    String tipo = "";
    String pedido = "";

    //Verifica que este logeado el usuario
    if (sesion.getAttribute("nombre") != null) {
        usua = (String) sesion.getAttribute("nombre");
        tipo = (String) sesion.getAttribute("Tipo");
    } else {
        response.sendRedirect("index.jsp");
    }

    ConectionDB con = new ConectionDB();

    try {
        pedido = request.getParameter("NoPedido");
    } catch (Exception e) {
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Estilos CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/datepicker3.css" rel="stylesheet">
        <!---->
        <title>SIALSS</title><link type="image/x-icon" href="imagenes/favicon.png" rel="icon" />
    </head>
    <body>
        <div class="container">
            <h1>SIALSS</h1>
            <h4>Módulo - Sistema de Administraci&oacute;n de Almacenes (SAA)</h4>

            <%@include file="jspf/menuPrincipal.jspf"%>
            <form action="verificarPedido.jsp" method="post">
                <br/>
                <div class="row">
                    <label class="col-sm-2">
                        <h4>Pedidos: </h4>
                    </label>
                    <div class="col-sm-9">
                        <select class="form-control" name="NoPedido" onchange="this.form.submit();">
                            <option value="">-- Punto de Entrega -- Pedido --</option>
                            <%                                try {
                                    con.conectar();
                                    ResultSet rset = con.consulta("SELECT ft.F_IdFact, u.F_NomCli FROM tb_facttemp ft, tb_uniatn u WHERE ltrim(ft.F_ClaCli) = u.F_ClaCli AND ft.F_StsFact NOT IN (3,5) GROUP BY ft.F_IdFact");
                                    while (rset.next()) {
                            %>
                            <option value="<%=rset.getString(1)%>"><%=rset.getString(2)%> - <%=rset.getString(1)%></option>
                            <%
                                    }
                                    con.cierraConexion();
                                } catch (Exception e) {

                                }
                            %>
                        </select>
                    </div>
                </div>
                <br/>
            </form>
        </div>
        <div style="width: 90%; margin: auto;">
            <br/>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <form action="VerificarPedido" method="post" name="formulario1">
                        <%
                            try {
                                con.conectar();
                                ResultSet rset = con.consulta("SELECT ft.F_IdFact, u.F_NomCli,ft.F_FecEnt FROM tb_facttemp ft, tb_uniatn u WHERE ltrim(ft.F_ClaCli) = u.F_ClaCli AND ft.F_IdFact=" + pedido + " GROUP BY ft.F_IdFact;");
                                while (rset.next()) {
                        %>
                        <div class="row">
                            <h4 class="col-sm-2">Folio PEDIDO:</h4>
                            <div class="col-sm-2"><input class="form-control" value="<%=pedido%>" readonly="" name="folio" id="folio" onkeypress="return tabular(event, this)" /></div>
                        </div>
                        <div class="row">
                            <h4 class="col-sm-12">Punto de Entrega: <%=rset.getString(2)%></h4>
                        </div>
                        <div class="row">
                            <h4 class="col-sm-9">Fecha de Entrega: <%=rset.getString(3)%></h4>
                        </div>
                        <%
                                }
                                con.cierraConexion();
                            } catch (Exception e) {
                                e.getMessage();
                            }
                        %>
                    </form>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tr>
                                <td>Clave</td>
                                <td>Descripci&oacute;n</td>                       
                                <td>Lote</td>
                                <!--td>Caducidad</td-->                        
                                <td>Cantidad</td>                      
                                <td></td>
                            </tr>
                            <%
                                int banBtn = 0;
                                try {
                                    /**
                                     * Se busca con base en la ORDEN DE
                                     * REPOSICIÓN DE INVENTARIO y la remisión y
                                     * que el estado sea 2 para poder validar su
                                     * entrada a almacén
                                     */
                                    con.conectar();
                                    ResultSet rset = con.consulta("SELECT l.F_ClaPro,m.F_DesPro,l.F_FolLot,l.F_FecCad,ft.F_Cant,ft.F_Id,ft.F_IdFact,l.F_ClaLot FROM tb_facttemp ft, tb_uniatn u,tb_lote l,tb_medica m WHERE LTRIM(ft.F_ClaCli) = u.F_ClaCli AND l.F_IdLote=ft.F_IdLot AND l.F_ClaPro=m.F_ClaPro AND ft.F_StsFact=0 AND ft.F_IdFact=" + pedido + ";");
                                    while (rset.next()) {
                                        banBtn = 1;
                            %>
                            <tr>
                                <td><%=rset.getString(1)%></td>
                                <td><%=rset.getString(2)%></td>
                                <td><%=rset.getString(8)%></td>
                                <!--td><%//=rset.getString(4)%></td-->
                                <td><%=rset.getString(5)%></td>
                                <td>
                                    <form method="get" action="ValidarPedidoAuto">
                                        <input name="id" type="text" style="" class="hidden" value="<%=rset.getString("ft.F_Id")%>" />
                                        <input name="pedidoM" type="text" style="" class="hidden" value="<%=rset.getString("ft.F_IdFact")%>" />
                                        <!--button class="btn btn-warning" name="accion" value="modificarPedido"><span class="glyphicon glyphicon-pencil" ></span></button-->
                                        <!--button class="btn btn-danger" onclick="return confirm('¿Seguro de que desea eliminar?');" name="accion" value="eliminarVerificaPed"><span class="glyphicon glyphicon-remove"></span></button-->
                                    </form>
                                </td>
                            </tr>
                            <%
                                    }
                                    con.cierraConexion();
                                } catch (Exception e) {

                                }

                            %>
                            <tr>
                                <td colspan="12">

                                </td>
                            </tr>

                        </table>
                    </div>
                    <hr/>
                </div>
                <%                    //
                    /**
                     * Para mostrar el botón de liberación
                     */
                    if (banBtn == 1) {

                %>

                <div class="panel-body table-responsive">
                    <div class="row">
                        <form action="ValidarPedidoAuto" method="post" onsubmit="desactivaBotones()">
                            <!--div class="container">
                                <div class="row">
                                    <label>No. Cajas:</label>
                                    <input name="Cajas" id="Cajas" type="text" required="" />&nbsp;&nbsp;&nbsp;
                                    <label>Peso Total:</label>
                                    <input name="KG" id="KG" type="text" required="" />
                                </div>
                            </div>
                            <br /-->
                            <input name="pedido" type="text" style="" class="hidden" value='<%=pedido%>' />
                            
                            <div class="col-lg-3 col-lg-offset-3">
                                <button id="EliminarVerifica" value="EliminarVerifica" name="accion" class="btn btn-danger btn-block" onclick="return confirm('¿Seguro que desea eliminar el pedido?');">Cancelar Pedido</button>
                            </div>
                            <div class="col-lg-3">
                                <button id="GuardarVerifica" value="GuardarVerifica" name="accion" class="btn btn-success  btn-block" onclick="return confirm('¿Seguro que desea verificar el pedido?');
                                        return validaCompra();">Confirmar Pedido</button>
                            </div>
                        </form>
                    </div>
                </div>
                <%
                    }
                %>
            </div>
        </div>


        <br><br><br>
        <%@include file="jspf/piePagina.jspf" %>
        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="js/jquery-1.9.1.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
    </body>
</html>
