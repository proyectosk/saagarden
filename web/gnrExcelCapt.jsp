<%-- 
    Document   : InventarioExcel
    Created on : 12/10/2015, 04:45:46 PM
    Author     : Mario
--%>

<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<%
    /**
     * Generar Excel
     */
    response.setContentType("application/vnd.ms-excel");
    response.setHeader("Content-Disposition", "attachment;filename=\"InventarioCap.xls\"");
%>
<table  class="table table-bordered table-striped" id="tablaExcel">
    <thead>
        <tr>
            <th>Clave</th>
            <th>Descripci&oacute;n</th>
            <th>lote</th>
            <th>Caducidad</th>
            <th>CB</th>
            <th>Cantidad</th>
            <th>Costo</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach items="${invCap}" var="detalle">
            <tr data-toggle="tooltip" data-placement="top" title="${detalle.clave}">
                <td><c:out value="${detalle.clave}" /></td>
                <td><c:out value="${detalle.desc}" /></td>
                <td><c:out value="${detalle.lote}" /></td>
                <td><c:out value="${detalle.caducidad}" /></td>
                <td><c:out value="${detalle.cb}" /></td>
                <td><c:out value="${detalle.cantidad}" /></td>
                <td><c:out value="${detalle.costo}" /></td>
            </tr>
        </c:forEach>
    </tbody>
</table>


