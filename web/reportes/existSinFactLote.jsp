<%-- 
    Document   : exist
    Created on : 02-jul-2014, 23:24:11
    Author     : wence
--%>

<%@page import="conn.ConectionDB"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    /**
     * Muesta las existencias sin lo que se tiene en proceso de facturación
     */
    DecimalFormat formatter = new DecimalFormat("#,###,###");
    DecimalFormatSymbols custom = new DecimalFormatSymbols();
    custom.setDecimalSeparator(',');
    formatter.setDecimalFormatSymbols(custom);
    String Claves = "";
    ResultSet rset;
    int Cantidad = 0;
    int cantExi=0;
    int cantTotal = 0, cantApar = 0;

    ConectionDB con = new ConectionDB();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Estilos CSS -->
        <link href="../css/bootstrap.css" rel="stylesheet">
        <!---->
        <title>SIALSS</title><link type="image/x-icon" href="../imagenes/favicon.png" rel="icon" />
    </head>
    <body>
        <div class="container">
            <h1>SIALSS</h1>
            <h4>SISTEMA INTEGRAL DE ADMINISTRACIÓN Y LOGÍSTICA PARA SERVICIOS DE SALUD</h4>
            <%@include file="../jspf/menuCliente.jspf"%>
        </div>
        <div class="container">
            <hr/>
            <h3>Insumo Dispoible en CEDIS</h3>
            <form action="existSinFactLote.jsp" method="post">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-sm-2">
                                <input type="text" name="clave" id="clave" placeholder="Clave" class="form-control input-sm" >
                            </div>
                            <div class="col-sm-2">
                                <button class="btn btn-sm btn-success" id="btn-buscar2">
                                    BUSCAR &nbsp;
                                    <label class="glyphicon glyphicon-search"></label>
                                </button>
                            </div>
                            <div class="col-sm-2">
                                <a href="gnrExistSinFactLote.jsp" class="btn btn-default">Descargar<label class="glyphicon glyphicon-download"></label></a>
                            </div>
                            <div class="col-sm-2">
                                <a href="existSinFactLote.jsp" class="btn btn-default">Actualizar&nbsp;<label class="glyphicon glyphicon-refresh"></label></a>
                            </div>
                        </div>

                    </div>

                    <div class="panel-footer">
                        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="datosProv">
                            <thead>
                                <tr>
                                    <td>Clave</td>
                                    <td>Descripción</td>
                                    <td>Lote</td>
                                    <td>Caducidad</td>
                                    <td>Cantidad</td>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    try {
                                        con.conectar();

                                        Claves = request.getParameter("clave");
                                        if (Claves == null) {
                                            Claves = "";
                                        }

                                        /**
                                         * Para consultar la existencia de las
                                         * claves
                                         */
                                        if (Claves.equals("")) {
                                            rset = con.consulta("SELECT l.F_ClaPro, m.F_DesPro, l.F_ClaLot, DATE_FORMAT(l.F_FecCad, '%d/%m/%Y') AS F_FecCad, l.F_Ubica, l.F_Cb, SUM(F_ExiLot), u.F_DesUbi, (m.F_Costo * SUM(l.F_ExiLot)) AS monto, m.F_Costo, F_DesMar, l.F_FecCad AS F_FechaCad FROM tb_marca mar, tb_lote l, tb_medica m, tb_ubica u WHERE mar.F_ClaMar = l.F_ClaMar AND m.F_ClaPro = l.F_ClaPro AND l.F_Ubica = u.F_ClaUbi AND F_ExiLot != 0 GROUP BY l.F_ClaPro, l.F_ClaLot, l.F_FecCad");
                                        } else {
                                            rset = con.consulta("SELECT l.F_ClaPro, m.F_DesPro, l.F_ClaLot, DATE_FORMAT(l.F_FecCad, '%d/%m/%Y') AS F_FecCad, l.F_Ubica, l.F_Cb, SUM(F_ExiLot), u.F_DesUbi, (m.F_Costo * SUM(l.F_ExiLot)) AS monto, m.F_Costo, F_DesMar, l.F_FecCad AS F_FechaCad FROM tb_marca mar, tb_lote l, tb_medica m, tb_ubica u WHERE mar.F_ClaMar = l.F_ClaMar AND m.F_ClaPro = l.F_ClaPro AND l.F_Ubica = u.F_ClaUbi AND F_ExiLot != 0 AND l.F_ClaPro='" + Claves + "' GROUP BY l.F_ClaPro, l.F_ClaLot, l.F_FecCad");
                                        }
                                        while (rset.next()) {
                                            cantExi = rset.getInt(7);
                                            cantTotal = 0;
                                            cantApar = 0;

                                            /**
                                             * Se obtiene lo apartado para esa
                                             * clave, (cualquiera que no se haya
                                             * remisionado)
                                             */
                                            ResultSet rset3 = con.consulta("select SUM(F_Cant) as F_Cant, SUM(c.F_Cant * m.F_Costo) as Importe from clavefact c, tb_medica m WHERE m.F_ClaPro = c.F_ClaPro and c.F_ClaPro = '" + rset.getString("F_ClaPro") + "' and c.F_ClaLot = '" + rset.getString("F_ClaLot") + "' and c.F_FecCad = '" + rset.getString("F_FechaCad") + "' and F_StsFact<5 ");
                                            while (rset3.next()) {
                                                cantApar = rset3.getInt("F_Cant");
                                            }

                                            /**
                                             * Calculo de las existencias
                                             * disponibles y el monto de estas
                                             */
                                            cantTotal = cantExi - cantApar;
                                            if (cantTotal < 0) {
                                                cantTotal = 0;
                                            }
                                            Cantidad = Cantidad + cantTotal;
                                %>
                                <tr>
                                    <td><%=rset.getString(1)%></td>
                                    <td><%=rset.getString(2)%></td>
                                    <td><%=rset.getString("F_ClaLot")%></td>
                                    <td><%=rset.getString("F_FecCad")%></td>
                                    <td><%=formatter.format(cantTotal)%></td>
                                </tr>
                                <%
                                        }
                                        con.cierraConexion();

                                    } catch (Exception e) {
                                        out.println(e);
                                    }
                                %>
                            </tbody>

                        </table>
                        <h3>Total Piezas = <%=formatter.format(Cantidad)%>&nbsp;&nbsp;&nbsp;</h3>
                    </div>
                </div>
            </form>
        </div>
        <br><br><br>
        <div class="navbar navbar-fixed-bottom navbar-inverse">
            <div class="text-center text-muted">
                GNK Logística || Desarrollo de Aplicaciones 2009 - 2015 <span class="glyphicon glyphicon-registration-mark"></span><br />
                Todos los Derechos Reservados
            </div>
        </div>

        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="../js/jquery-1.9.1.js" type="text/javascript"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery.dataTables.js" type="text/javascript"></script>
        <script src="../js/dataTables.bootstrap.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                $('#datosProv').dataTable();
            });
        </script>

    </body>
</html>
