<%-- 
    Document   : exist
    Created on : 02-jul-2014, 23:24:11
    Author     : wence
--%>

<%@page import="conn.ConectionDB"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    /**
     * PAra descargar las existencias disponibles
     */
    DecimalFormat formatter = new DecimalFormat("#,###,###");
    DecimalFormatSymbols custom = new DecimalFormatSymbols();
    custom.setDecimalSeparator(',');
    formatter.setDecimalFormatSymbols(custom);
    String Claves = "";
    ResultSet rset;

    ConectionDB con = new ConectionDB();
    /**
     * Para generar el excel
     */
    response.setContentType("application/vnd.ms-excel");
    response.setHeader("Content-Disposition", "attachment; filename=Existencias en CEDIS sin Pedido.xls");
%>
<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="datosProv">
    <thead>
        <tr>
            <td>Clave</td>
            <td>Descripci&oacute;n</td>
            <td>Cantidad</td>
        </tr>
    </thead>
    <tbody>
        <%
            try {
                con.conectar();

                Claves = request.getParameter("clave");
                if (Claves == null) {
                    Claves = "";
                }

                /**
                 * Para consultar la existencia de las claves
                 */
                if (Claves.equals("")) {
                    rset = con.consulta("SELECT l.F_ClaPro, m.F_DesPro, l.F_ClaLot, DATE_FORMAT(l.F_FecCad, '%d/%m/%Y') AS F_FecCad, l.F_Ubica, l.F_Cb, SUM(F_ExiLot), "
                            + "u.F_DesUbi,(m.F_Costo*SUM(l.F_ExiLot)) as monto,m.F_Costo, F_DesMar, l.F_FecCad as F_FechaCad "
                            + "FROM tb_marca mar, tb_lote l, tb_medica m, tb_ubica u "
                            + "WHERE mar.F_ClaMar = l.F_ClaMar and m.F_ClaPro = l.F_ClaPro AND l.F_Ubica = u.F_ClaUbi AND F_ExiLot != 0 GROUP BY l.F_ClaPro");
                } else {
                    rset = con.consulta("SELECT l.F_ClaPro, m.F_DesPro, l.F_ClaLot, DATE_FORMAT(l.F_FecCad, '%d/%m/%Y') AS F_FecCad, l.F_Ubica, l.F_Cb, SUM(F_ExiLot), "
                            + "u.F_DesUbi,(m.F_Costo*SUM(l.F_ExiLot)) as monto,m.F_Costo, F_DesMar, l.F_FecCad as F_FechaCad "
                            + "FROM tb_marca mar, tb_lote l, tb_medica m, tb_ubica u "
                            + "WHERE mar.F_ClaMar = l.F_ClaMar and m.F_ClaPro = l.F_ClaPro AND l.F_Ubica = u.F_ClaUbi AND F_ExiLot != 0 and l.F_ClaPro='" + Claves + "' GROUP BY l.F_ClaPro");
                }
                while (rset.next()) {
                    int cantExi = rset.getInt(7);
                    int cantTotal = 0, cantApar = 0;

                    /**
                     * Se obtiene lo apartado para esa clave, (cualquiera que no
                     * se haya remisionado)
                     */
                    ResultSet rset3 = con.consulta("select SUM(F_Cant) as F_Cant, SUM(c.F_Cant * m.F_Costo) as Importe from clavefact c, tb_medica m where m.F_ClaPro = c.F_ClaPro and c.F_ClaPro = '" + rset.getString("F_ClaPro") + "' and F_StsFact<5 ");
                    while (rset3.next()) {
                        cantApar = rset3.getInt("F_Cant");
                    }

                    /**
                     * Calculo de las existencias disponibles y el monto de
                     * estas
                     */
                    cantTotal = cantExi - cantApar;
                    if (cantTotal < 0) {
                        cantTotal = 0;
                    }
        %>
        <tr>
            <td><%=rset.getString(1)%></td>
            <td><%=rset.getString(2)%></td>
            <td><%=formatter.format(cantTotal)%></td>
        </tr>
        <%
                }
                con.cierraConexion();

            } catch (Exception e) {
                out.println(e);
            }
        %>
    </tbody>

</table>
