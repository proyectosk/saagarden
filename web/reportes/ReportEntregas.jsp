<%-- 
    Document   : index
    Created on : 17/02/2014, 03:34:46 PM
    Author     : Americo
--%>

<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="conn.*" %>
<!DOCTYPE html>
<%
    /**
     * Listado de concentrados globales para su reimpresión
     */
    HttpSession sesion = request.getSession();
    String usua = "", tipo = "";
    if (sesion.getAttribute("Usuario") != null) {
        usua = (String) sesion.getAttribute("Usuario");
    } else {
        response.sendRedirect("indexGarden.jsp");
    }
    ConectionDB con = new ConectionDB();
    String Hora="",Minuto="",Segundo="";
    String fol_gnkl = "", fol_remi = "", orden_compra = "", fecha = "";
    try {
        if (request.getParameter("accion").equals("buscar")) {
            fol_gnkl = request.getParameter("fol_gnkl");
            fol_remi = request.getParameter("fol_remi");
            orden_compra = request.getParameter("orden_compra");
            fecha = request.getParameter("fecha");
        }
    } catch (Exception e) {

    }
    if (fol_gnkl == null) {
        fol_gnkl = "";
        fol_remi = "";
        orden_compra = "";
        fecha = "";
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Estilos CSS -->
        <link href="../css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/cupertino/jquery-ui-1.10.3.custom.css" />
        <!--link href="css/navbar-fixed-top.css" rel="stylesheet"-->
        <link href="../css/datepicker3.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="../css/dataTables.bootstrap.css">
        <!---->
        <title>SIALSS</title>
    </head>
    <body>
        <div class="container">
            <h1>SIALSS</h1>
            <h4>SISTEMA INTEGRAL DE ADMINISTRACIÓN Y LOGÍSTICA PARA SERVICIOS DE SALUD</h4>
            <%@include file="../jspf/menuCliente.jspf"%>

            <div>
                <h3>Reporte de Entregas</h3>

                <br />
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <div class="panel panel-primary">
                        <div class="panel-body table-responsive">
                            <div style="width:100%; height:400px; overflow:auto;">
                        <table class="table table-bordered table-striped" id="datosCompras">
                            <thead> 
                                <tr>
                                    <td>No. Folio Validado</td>
                                    <!--td>No.Pre-pedido</td-->
                                    <td>Punto de entrega</td>
                                    <td>Fecha Captura</td>
                                    <td>Fecha Validación</td>
                                    <td>Fecha Entrega</td>
                                    <td>Hora Captura</td>
                                    <td>Hora Validación</td>
                                    <td>Tiempo Generado Validar</td>                                    
                                    <td>Entrega</td>
                                    <td>Paqueteria</td>
                                    <td>No. Gu&iacute;a</td>
                                    <td>No. Factura</td>
                                    <td>Fecha Entrega Cliente</td>
                                    <td>Tiempo Generado Validado Vs Env&iacute;o Factura</td>
                                    <td>D&iacute;as Transcurrido entrega Cliente</td>
                                </tr>
                            </thead>
                            <tbody>
                                <%                                    
                                    try {
                                        con.conectar();
                                        try {
                                            /**
                                             * Listado de concentrados
                                             */
                                            //
                                            String F_Paqueteria="",F_TipoEnvio="",Dias="";
                                            ResultSet rset = con.consulta("SELECT * FROM view_reportes;");
                                            while (rset.next()) {
                                %>
                                <tr>
                                    <td><%=rset.getString(1)%></td>
                                    <!--td><%//=rset.getString(2)%></td-->
                                    <td><%=rset.getString(3)%></td>
                                    <td><%=rset.getString(4)%></td>
                                    <td><%=rset.getString(5)%></td>
                                    <td><%=rset.getString(6)%></td>
                                    <td><%=rset.getString(7)%></td>
                                    <td><%=rset.getString(8)%></td>
                                    <%
                                        F_Paqueteria = rset.getString(9);
                                        if((F_Paqueteria.equals("GNKL")) || (F_Paqueteria.equals("GNK"))){
                                            F_TipoEnvio="DIRECTA";
                                            F_Paqueteria="";
                                        }else{
                                            F_TipoEnvio="PAQUETERIA";
                                        }
                                        
                                        Dias =  rset.getString(15);
                                        if(Dias == null){
                                            Dias = "";
                                        }
                                        
                                      ResultSet HoraDif = con.consulta("SELECT  HOUR(SEC_TO_TIME(TIMESTAMPDIFF(SECOND, '"+rset.getString(12)+"','"+rset.getString(13)+"'))), MINUTE(SEC_TO_TIME(TIMESTAMPDIFF(SECOND, '"+rset.getString(12)+"','"+rset.getString(13)+"'))), SECOND(SEC_TO_TIME(TIMESTAMPDIFF(SECOND, '"+rset.getString(12)+"','"+rset.getString(13)+"'))) ;");          
                                     if(HoraDif.next()){
                                          Hora = HoraDif.getString(1);
                                         Minuto = HoraDif.getString(2);
                                         Segundo = HoraDif.getString(3);
                                         if(Hora == null){Hora = "00";}
                                         if(Minuto == null){Minuto = "00";}
                                         if(Segundo == null){Segundo = "00";}
                                    %>
                                    <td><%=Hora+":"+Minuto+":"+Segundo%></td>
                                    <%}%>  
                                    <td><%=F_TipoEnvio%></td>
                                    <td><%=F_Paqueteria%></td>
                                    <td><%=rset.getString(10)%></td>
                                    <td><%=rset.getString(16)%></td>
                                    <td><%=rset.getString(11)%></td>
                                    <%
                                        ResultSet HoraDif2 = con.consulta("SELECT  HOUR(SEC_TO_TIME(TIMESTAMPDIFF(SECOND, '"+rset.getString(17)+"','"+rset.getString(13)+"'))), MINUTE(SEC_TO_TIME(TIMESTAMPDIFF(SECOND, '"+rset.getString(17)+"','"+rset.getString(13)+"'))), SECOND(SEC_TO_TIME(TIMESTAMPDIFF(SECOND, '"+rset.getString(17)+"','"+rset.getString(13)+"'))) ;");          
                                     if(HoraDif2.next()){
                                          Hora = HoraDif2.getString(1);
                                         Minuto = HoraDif2.getString(2);
                                         Segundo = HoraDif2.getString(3);
                                         if(Hora == null){Hora = "00";}
                                         if(Minuto == null){Minuto = "00";}
                                         if(Segundo == null){Segundo = "00";}
                                    %>
                                    <td><%=Hora+":"+Minuto+":"+Segundo%></td>
                                    <%}%>  
                                    <td><%=Dias%></td>                         
                                </tr>
                                <%         
                                    F_TipoEnvio="";
                                    F_Paqueteria="";
                                            }
                                        } catch (Exception e) {

                                        }
                                        con.cierraConexion();
                                    } catch (Exception e) {

                                    }
                                %>
                            </tbody>
                        </table>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                           
        <br><br><br>
        <div class="navbar navbar-fixed-bottom navbar-inverse">
            <div class="text-center text-muted">
                GNK Logística || Desarrollo de Aplicaciones 2009 - 2015 <span class="glyphicon glyphicon-registration-mark"></span><br />
                Todos los Derechos Reservados
            </div>
        </div>

        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="../js/jquery-1.9.1.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery-ui-1.10.3.custom.js"></script>
        <script src="../js/bootstrap-datepicker.js"></script>
        <script src="../js/jquery.dataTables.js"></script>
        <script src="../js/dataTables.bootstrap.js"></script>
        <script  type="text/javascript">
            $(document).ready(function () {
                $('#datosCompras').dataTable();
            });
        
        $(".rowButton").click(function () {
            var $row = $(this).closest("tr");    // Find the row
            var $folio = $row.find("td.Documento").text(); // Find the text             
            var $guia = $row.find("td.NoGuia").text(); // Find the text
            var $envio = $row.find("td.StsEnvio").text(); // Find the text
            

            $("#Folio").val($folio);
            $("#Guia").val($guia);
            $("#Envio").val($envio);           

        });
            
           
        </script>
        <script>
            $(function () {
                $("#fecha").datepicker();
                $("#fecha").datepicker('option', {dateFormat: 'dd/mm/yy'});
            });
            
        </script>
    </body>
</html>