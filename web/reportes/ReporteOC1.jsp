<%-- 
    Document   : cambioFechas
    Created on : 14/04/2015, 12:58:35 PM
    Author     : Americo
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="javax.print.PrintServiceLookup"%>
<%@page import="javax.print.PrintService"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="conn.*" %>
<!DOCTYPE html>
<%java.text.DateFormat df = new java.text.SimpleDateFormat("yyyyMMddhhmmss"); %>
<%java.text.DateFormat df2 = new java.text.SimpleDateFormat("yyyy-MM-dd"); %>
<%java.text.DateFormat df3 = new java.text.SimpleDateFormat("dd/MM/yyyy"); %>

<%
    DecimalFormat myFormatter = new DecimalFormat("###,###,###,###,###");

    HttpSession sesion = request.getSession();
    String usua = "", tipo = "";
    if (sesion.getAttribute("nombre") != null) {
        usua = (String) sesion.getAttribute("nombre");
        tipo = (String) sesion.getAttribute("Tipo");
    } else {
        response.sendRedirect("index.jsp");
    }
    ConectionDB con = new ConectionDB();
    int F_CanCom=0;
    String fecha_ini = "", fecha_fin = "", radio = "";
    try {
        fecha_ini = request.getParameter("fecha_ini");
        fecha_fin = request.getParameter("fecha_fin");
        radio = request.getParameter("radio");
    } catch (Exception e) {

    }
    if (fecha_ini == null) {
        fecha_ini = "";
    }
    if (fecha_fin == null) {
        fecha_fin = "";
    }
    if (radio == null) {
        radio = "si";
    }

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Estilos CSS -->
        <link href="../css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/cupertino/jquery-ui-1.10.3.custom.css" />
        <link href="../css/navbar-fixed-top.css" rel="stylesheet">
        <link href="../css/datepicker3.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="../css/dataTables.bootstrap.css">
        <!---->
        <title>SIE Sistema de Ingreso de Entradas</title><link type="image/x-icon" href="../imagenes/favicon.png" rel="icon" />
    </head>
    <body>
        <div class="container">
            <h1>SIALSS</h1>
            <h4>SISTEMA INTEGRAL DE ADMINISTRACIÓN Y LOGÍSTICA PARA SERVICIOS DE SALUD</h4>
            <%@include file="../jspf/menuPrincipal.jspf"%>
            <div class="panel-heading">
                <h3 class="panel-title">Reportes OC/FOLIOS</h3>
            </div>
            <form action="ReporteOC1.jsp" method="post">
                <div class="panel-footer">
                    <div class="row">

                        <label class="control-label col-sm-1" for="fecha_ini">Fechas</label>
                        <div class="col-sm-2">
                            <input class="form-control" id="fecha_ini" name="fecha_ini" type="date" onchange="habilitar(this.value);"/>
                        </div>
                        <div class="col-sm-2">
                            <input class="form-control" id="fecha_fin" name="fecha_fin" type="date" onchange="habilitar(this.value);"/>
                        </div>
                        <div class="col-sm-2">
                            <input type="radio" id="radio" name="radio" checked="true" value="si" > OC
                            <input type="radio" id="radio" name="radio" value="no"> Folios
                        </div>
                    </div>   
                </div>
                <div class="panel-body">
                    <div class="row">
                        <button class="btn btn-block btn-success" id="btn_capturar" onclick="return confirma();">MOSTRAR&nbsp;<label class="glyphicon glyphicon-search"></label></button>                        
                    </div>
                </div>  
            </form>
            <%                int Contar = 0;
                try {
                    con.conectar();
                    ResultSet rset = null;
                    try {
                        if (radio.equals("si")) {
                            rset = con.consulta("SELECT COUNT(F_ClaDoc) FROM tb_compra WHERE F_FecApl BETWEEN '" + fecha_ini + "' AND '" + fecha_fin + "';");
                        } else {
                            rset = con.consulta("SELECT COUNT(F_ClaDoc) FROM tb_factura WHERE F_FecEnt BETWEEN '" + fecha_ini + "' AND '" + fecha_fin + "' AND F_StsFact='A';");
                        }

                        if (rset.next()) {
                            Contar = rset.getInt(1);
                        }
                    } catch (Exception e) {

                    }
                    con.cierraConexion();
                } catch (Exception e) {

                }

            %>

            <%                    if (Contar > 0) {
            %>
            <div class="row">                    
                <input class="form-control" id="radio1" name="radio1" type="hidden" value="<%=radio%>" />                    
                <input class="form-control" id="fecha_ini1" name="fecha_ini1" type="hidden" value="<%=fecha_ini%>" />
                <input class="form-control" id="fecha_fin1" name="fecha_fin1" type="hidden" value="<%=fecha_fin%>" />

                <br />
                <div class="row">    
                    <div class="col-sm-2 col-sm-offset-2">                       
                        <a class="btn btn-info btn-block" href="gnrROC.jsp?F1=<%=fecha_ini%>&F2=<%=fecha_fin%>&R=<%=radio%>" >Exportar</a>
                    </div>

                </div>
            </div>
            <%}%>


            <div>
                <div class="panel panel-primary">
                    <div class="panel-body table-responsive">
                        <div style="width:100%; height:400px; overflow:auto;">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <%if (radio.equals("si")) {%>
                                        <td>No. OC</td>
                                        <td>Nombre Proveedor</td>
                                        <%} else {%>
                                        <td>No. Folio</td>
                                        <td>Nombre Cliente</td>
                                        <%}%>
                                        <td>Clave</td>
                                        <td>Descripción</td>
                                        <td>Lote</td>
                                        <td>Caducidad</td>
                                        <td>Cantidad</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        try {
                                            con.conectar();
                                            try {
                                                ResultSet rset = null;
                                                if (radio.equals("si")) {
                                                    //rset = con.consulta("SELECT C.F_OrdCom,P.F_NomPro,C.F_ClaPro,M.F_DesPro,L.F_ClaLot,DATE_FORMAT(F_FecCad,'%d/%m/%Y') AS F_FecCad,FORMAT(SUM(C.F_CanCom),0) AS F_CanCom1,SUM(C.F_CanCom) AS F_CanCom FROM tb_compra C INNER JOIN tb_lote L ON C.F_Lote=L.F_FolLot AND C.F_ClaPro=L.F_ClaPro INNER JOIN tb_medica M ON C.F_ClaPro=M.F_ClaPro INNER JOIN tb_proveedor P ON C.F_ProVee=P.F_ClaProve WHERE C.F_FecApl BETWEEN '"+fecha_ini+"' AND '"+fecha_fin+"' GROUP BY L.F_ClaPro,L.F_FolLot,C.F_OrdCom,C.F_ProVee;");
                                                    rset = con.consulta("SELECT C.F_OrdCom,P.F_NomPro,C.F_ClaPro,M.F_DesPro,FORMAT(SUM(C.F_CanCom),0) AS F_CanCom1,SUM(C.F_CanCom) AS F_CanCom,C.F_Lote FROM tb_compra C INNER JOIN tb_medica M ON C.F_ClaPro=M.F_ClaPro INNER JOIN tb_proveedor P ON C.F_ProVee=P.F_ClaProve WHERE C.F_FecApl BETWEEN '" + fecha_ini + "' AND '" + fecha_fin + "' GROUP BY C.F_ClaPro,C.F_OrdCom,C.F_ProVee,C.F_Lote;");
                                                    while (rset.next()) {
                                                        F_CanCom = F_CanCom + rset.getInt(6);

                                                        ResultSet RsetDatosL = con.consulta("SELECT F_ClaLot,DATE_FORMAT(F_FecCad,'%d/%m/%Y') AS F_FecCad FROM tb_lote WHERE F_FolLot='" + rset.getString(7) + "' AND F_ClaPro='" + rset.getString(3) + "';");
                                                        if (RsetDatosL.next()) {

                                    %>
                                    <tr>
                                        <td><%=rset.getString(1)%></td>
                                        <td><%=rset.getString(2)%></td>
                                        <td><%=rset.getString(3)%></td>
                                        <td><%=rset.getString(4)%></td>
                                        <td><%=RsetDatosL.getString(1)%></td>
                                        <td><%=RsetDatosL.getString(2)%></td>
                                        <td><%=rset.getString(5)%></td>
                                    </tr>
                                    <%

                                            }
                                        }

                                    } else {
                                        rset = con.consulta("SELECT F.F_ClaDoc,U.F_NomCli,L.F_ClaPro,M.F_DesPro,L.F_ClaLot,DATE_FORMAT(L.F_FecCad,'%d/%m/%Y') AS F_FecCad,FORMAT(SUM(F_CantSur),0) AS F_CantSur1,SUM(F_CantSur) AS F_CantSur FROM tb_factura F INNER JOIN tb_lote L ON F.F_Lote=L.F_FolLot AND F.F_ClaPro=L.F_ClaPro AND F.F_Ubicacion=L.F_Ubica INNER JOIN tb_medica M ON L.F_ClaPro=M.F_ClaPro INNER JOIN tb_uniatn U ON F.F_ClaCli=U.F_ClaCli WHERE F_FecEnt BETWEEN '" + fecha_ini + "' AND '" + fecha_fin + "' AND F_StsFact='A' GROUP BY F.F_ClaDoc,L.F_ClaPro,L.F_ClaLot,L.F_FecCad;");

                                        while (rset.next()) {
                                            F_CanCom = F_CanCom + rset.getInt(8);


                                    %>
                                    <tr>
                                        <td><%=rset.getString(1)%></td>
                                        <td><%=rset.getString(2)%></td>
                                        <td><%=rset.getString(3)%></td>
                                        <td><%=rset.getString(4)%></td>
                                        <td><%=rset.getString(5)%></td>
                                        <td><%=rset.getString(6)%></td>
                                        <td><%=rset.getString(7)%></td>
                                    </tr>
                                    <%}
                                                }
                                            } catch (Exception e) {

                                            }
                                            con.cierraConexion();
                                        } catch (Exception e) {

                                        }
                                    %>
                                </tbody>
                            </table>                                
                        </div>
                    </div>
                </div>
                <div class="row">
                    <h3>Total Piezas:   <%=myFormatter.format(F_CanCom)%></h3>
                </div>

            </div>

        </div>
        <%@include file="../jspf/piePagina.jspf" %>


        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="../js/jquery-1.9.1.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery-ui-1.10.3.custom.js"></script>
        <script src="../js/bootstrap-datepicker.js"></script>
        <script src="../js/jquery.dataTables.js"></script>
        <script src="../js/dataTables.bootstrap.js"></script>
        <script>
                            $(document).ready(function () {
                                $('#datosCompras').dataTable();
                                $("#fecha").datepicker();
                                $("#fecha").datepicker('option', {dateFormat: 'dd/mm/yy'});

                                //$('#btnRecalendarizar').attr('disabled', true);
                                //$('#btnImpMult').attr('disabled', true);
                            });



        </script>


    </body>
</html>

