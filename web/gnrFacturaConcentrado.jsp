<%-- 
    Document   : index
    Created on : 17/02/2014, 03:34:46 PM
    Author     : Americo
--%>

<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="conn.*" %>
<!DOCTYPE html>
<%java.text.DateFormat df = new java.text.SimpleDateFormat("yyyyMMddhhmmss"); %>
<%java.text.DateFormat df2 = new java.text.SimpleDateFormat("yyyy-MM-dd"); %>
<%java.text.DateFormat df3 = new java.text.SimpleDateFormat("dd/MM/yyyy"); %>
<%

    /**
     * Para generar el Excel del concentrado de facturas por año
     *
     *
     */
    DecimalFormat formatter = new DecimalFormat("#,###,###");
    DecimalFormat formatterDecimal = new DecimalFormat("#,###,##0.00");
    DecimalFormatSymbols custom = new DecimalFormatSymbols();
    custom.setDecimalSeparator('.');
    custom.setGroupingSeparator(',');
    formatter.setDecimalFormatSymbols(custom);
    formatterDecimal.setDecimalFormatSymbols(custom);
    HttpSession sesion = request.getSession();
    String usua = "";
    if (sesion.getAttribute("nombre") != null) {
        usua = (String) sesion.getAttribute("nombre");
    } else {
        //response.sendRedirect("index.jsp");
    }
    ConectionDB con = new ConectionDB();

    String fol_gnkl = "", fol_remi = "", orden_compra = "", fecha = "";
    fecha = request.getParameter("fecha");
    try {
        if (request.getParameter("accion").equals("buscar")) {
            fol_gnkl = request.getParameter("fol_gnkl");
            fol_remi = request.getParameter("fol_remi");
            orden_compra = request.getParameter("orden_compra");
        }
    } catch (Exception e) {

    }
    if (fol_gnkl == null) {
        fol_gnkl = "";
        fol_remi = "";
        orden_compra = "";
        fecha = "";
    }

    response.setContentType("application/vnd.ms-excel");
    response.setHeader("Content-type", "application/xls");
    response.setHeader("Content-Disposition", "attachment;filename=\"RemisionGlobal.xls\"");
%>
<html>
    <head>
        <style>
            .num {
                mso-number-format:General;
            }
            .text{
                mso-number-format:"\@";/*force text*/
            }
        </style>
    </head>
    <body>
        <div>
            <h4>Global de Remisiones</h4>

            <br />
            <div class="panel panel-primary">
                <div class="panel-body">
                    <table class="table table-bordered table-striped" id="datosCompras" border="1">
                        <thead>
                            <tr>
                                <td>Cliente</td>
                                <td>FechaEnt</td>
                                <td>Remision</td>
                                <td>Clave</td>
                                <td>Descripción</td>
                                <td>Lote</td>
                                <td>Caducidad</td>
                                <td>Marca</td>
                                <td>FecFab</td>
                                <td>Req.</td>
                                <td>Ubicación</td>
                                <td>Ent.</td>
                                <td>Costo U</td>
                                <td>Importe</td>
                                <td>Status</td>
                                <td>No Entrega</td>
                                <td>Tipo</td>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                String where = "";

                                try {
                                    con.conectar();
                                    try {
                                        /**
                                         * Dependiendo del año se cambia el
                                         * query de consulta, la información se
                                         * obtiene de la vista facturas
                                         */
                                        ResultSet rset = con.consulta("SELECT f.F_NomCli,f.F_FecEnt,f.F_ClaDoc,f.F_ClaPro,f.F_DesPro,f.F_ClaLot,f.F_FecCad,f.F_CantReq,f.F_CantSur,f.F_Costo,f.F_Monto, f.F_Ubicacion, f.F_StsFact, m.F_DesMar, DATE_FORMAT(f.F_FecFab,'%d/%m/%Y') AS F_FecFab, f.F_Req, f.F_Tipo, f.F_FactTemp, ft.F_IdReq FROM facturas f, tb_marca m, tb_facttemp ft WHERE f.F_ClaMar = m.F_ClaMar and f.F_FactTemp=ft.F_IdFact and f.F_Fecha between '2014-01-01' and '2014-12-31' group by f.F_IdFact;");

                                        if (fecha.equals("2015")) {
                                            rset = con.consulta("SELECT f.F_NomCli,f.F_FecEnt,f.F_ClaDoc,f.F_ClaPro,f.F_DesPro,f.F_ClaLot,f.F_FecCad,f.F_CantReq,f.F_CantSur,f.F_Costo,f.F_Monto, f.F_Ubicacion, f.F_StsFact, m.F_DesMar, DATE_FORMAT(f.F_FecFab,'%d/%m/%Y') AS F_FecFab, f.F_Req, f.F_Tipo, f.F_FactTemp, ft.F_IdReq FROM facturas f, tb_marca m, tb_facttemp ft WHERE f.F_ClaMar = m.F_ClaMar and f.F_FactTemp=ft.F_IdFact and f.F_Fecha between '2015-01-01' and '2015-12-31' group by f.F_IdFact");
                                        }
                                        while (rset.next()) {
                            %>
                            <tr>
                                <td><%=rset.getString(1)%></td>
                                <td><%=rset.getString(2)%></td>
                                <td><%=rset.getString(3)%></td>
                                <td><%=rset.getString(4)%></td>
                                <td><%=rset.getString(5)%></td>
                                <td class="text"><%=rset.getString(6)%></td>
                                <td><%=rset.getString(7)%></td>

                                <td><%=rset.getString("F_DesMar")%></td>
                                <td><%=rset.getString("F_FecFab")%></td>

                                <td><%=rset.getString(8)%></td>
                                <td><%=rset.getString(12)%></td>
                                <td><%=rset.getString(9)%></td>
                                <td><%=rset.getString(10)%></td>
                                <td><%=rset.getString(11)%></td>
                                <td><%=rset.getString("F_StsFact")%></td>
                                <td><%=rset.getString("F_Req")%></td>
                                <td><%=rset.getString("F_Tipo")%></td>
                                <td><%=rset.getString("F_FactTemp")%></td>
                                <td><%=rset.getString("F_IdReq")%></td>
                            </tr>
                            <%
                                        }
                                    } catch (Exception e) {

                                    }
                                    con.cierraConexion();
                                } catch (Exception e) {

                                }
                            %>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
