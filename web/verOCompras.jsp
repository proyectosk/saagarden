<%-- 
    Document   : index
    Created on : 17/02/2014, 03:34:46 PM
    Author     : Americo
--%>

<%@page import="conn.ConectionDB"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    /**
     * Listado de concentrados globales para su reimpresión
     */
    HttpSession sesion = request.getSession();
    String usua = "";
    String tipo = "";
    if (sesion.getAttribute("Usuario") != null) {
        usua = (String) sesion.getAttribute("Usuario");
    } else {
        response.sendRedirect("indexGarden.jsp");
    }
    ConectionDB con = new ConectionDB();

    String fol_gnkl = "", fol_remi = "", orden_compra = "", fecha = "";
    try {
        if (request.getParameter("accion").equals("buscar")) {
            fol_gnkl = request.getParameter("fol_gnkl");
            fol_remi = request.getParameter("fol_remi");
            orden_compra = request.getParameter("orden_compra");
            fecha = request.getParameter("fecha");
        }
    } catch (Exception e) {

    }
    if (fol_gnkl == null) {
        fol_gnkl = "";
        fol_remi = "";
        orden_compra = "";
        fecha = "";
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Estilos CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap.css">
        <!---->
        <title>SIALSS</title><link type="image/x-icon" href="imagenes/favicon.png" rel="icon" />
    </head>
    <body>
        <div class="container">
            <h1>SIALSS</h1>
            <h4>SISTEMA INTEGRAL DE ADMINISTRACIÓN Y LOGÍSTICA PARA SERVICIOS DE SALUD</h4>
            <%@include file="jspf/menuCliente.jspf"%>

            <div>
                <h3>OC ingresados</h3>
                <br />
                <div class="panel">
                    <div class="panel-body">
                        <table class="table table-bordered table-striped" id="datosCompras">
                            <thead>
                                <tr>
                                    <td>No. OC</td>
                                    <td>Proveedor</td>
                                    <td>Fecha OC</td>
                                    <td>Estado</td>
                                    <td>Acciones</td>
                                </tr>
                            </thead>
                            <tbody>
                                <%                                    try {
                                        con.conectar();
                                        try {
                                            /**
                                             * Listado de concentrados
                                             */
                                            String Estatus="";
                                            int F_Contar=0,F_StsPed=0,F_Contar0=0,F_Contar1=0,Diferencia=0;
                                            ResultSet OC = con.consulta("SELECT F_NoCompra,F_Provee,pr.F_NomPro,DATE_FORMAT(F_FecSur,'%d/%m/%Y') AS F_FecSur1,F_FecSur,COUNT(F_NoCompra) AS F_Contar FROM tb_pedidoisem p INNER JOIN tb_proveedor pr on p.F_Provee=pr.F_ClaProve GROUP BY F_NoCompra,F_Provee,F_FecSur");
                                            while (OC.next()){
                                                F_Contar = OC.getInt(6);                                                                                            
                                            
                                                ResultSet Sts = con.consulta("SELECT F_StsPed FROM tb_pedidoisem WHERE F_NoCompra='"+OC.getString(1)+"' AND F_Provee='"+OC.getString(2)+"' AND F_FecSur='"+OC.getString(5)+"' GROUP BY F_StsPed; ");
                                                while (Sts.next()) {
                                                    F_StsPed = Sts.getInt(1);
                                                }
                                                if(F_StsPed == 2){
                                                    Estatus = "CANCELADO";
                                                }else{                                                   
                                                    ResultSet Contar0 = con.consulta("SELECT COUNT(F_Recibido) FROM tb_pedidoisem WHERE F_NoCompra='"+OC.getString(1)+"' AND F_Provee='"+OC.getString(2)+"' AND F_FecSur='"+OC.getString(5)+"' AND F_StsPed='1' AND F_Recibido='0'");
                                                    if(Contar0.next()){
                                                        F_Contar0 = Contar0.getInt(1);
                                                    }
                                                    ResultSet Contar1 = con.consulta("SELECT COUNT(F_Recibido) FROM tb_pedidoisem WHERE F_NoCompra='"+OC.getString(1)+"' AND F_Provee='"+OC.getString(2)+"' AND F_FecSur='"+OC.getString(5)+"' AND F_StsPed='1' AND F_Recibido='1'");
                                                    if(Contar1.next()){
                                                        F_Contar1 = Contar1.getInt(1);
                                                    }
                                                    if(F_Contar1 > 0){
                                                        Diferencia = F_Contar - F_Contar1;
                                                        if (Diferencia == 0){
                                                            Estatus = "CERRADO";
                                                        }else{
                                                            Estatus = "ABIERTO";
                                                        }                                                                                                            
                                                    }else{
                                                        Estatus = "EN ESPERA";
                                                    }
                                                    
                                                }
                                %>
                                <tr>

                                    <td><%=OC.getString(1)%></td>
                                    <td><%=OC.getString(3)%></td>
                                    <td><%=OC.getString(4)%></td>
                                    <td><%=Estatus%></td>
                                    <td>
                                        <a class="btn btn-warning " href="verOC.jsp?oc=<%=OC.getString(1)%>&prove=<%=OC.getString(2)%>&fecha=<%=OC.getString(5)%>"><span class="glyphicon glyphicon-search" ></span>Ver</a>
                                    </td>
                                </tr>
                                <%               F_StsPed = 0;     
                                            }
                                        } catch (Exception e) {

                                        }
                                        con.cierraConexion();
                                    } catch (Exception e) {

                                    }
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <br><br><br>
        <%@include file="jspf/piePagina.jspf" %>

        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="js/jquery-1.9.1.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery.dataTables.js"></script>
        <script src="js/dataTables.bootstrap.js"></script>
        <script  type="text/javascript">
            $(document).ready(function () {
                $('#datosCompras').dataTable();
            });
        </script>

    </body>
</html>