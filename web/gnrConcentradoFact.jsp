<%-- 
    Document   : gnrConcentrado
    Created on : 4/09/2014, 11:10:51 AM
    Author     : Americo
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="conn.ConectionDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%

    /**
     * Para generar excel del concentrado
     */
    ConectionDB con = new ConectionDB();
    String cli = "", fec = "";

    response.setContentType("application/vnd.ms-excel");
    response.setHeader("Content-Disposition", "attachment;filename=\"PedidoFact.xls\"");
%>
<table border="1">
    <tr>
        <td>Clave de Punto de Entrega</td>
        <td>Punto de Entrega</td>
        <td>Fecha de Entrega</td>
        <td>Clave</td>
        <td>Descripci&oacute;n</td>
        <td>Lote</td>
        <td>Caducidad</td>
        <td>Cantidad</td>
    </tr>
    <%        try {
            con.conectar();
            try {
                ResultSet rset = con.consulta("SELECT u.F_ClaCli,u.F_NomCli, DATE_FORMAT(f.F_FecEnt, '%d/%m/%Y') AS FecEnt, f.F_ClaPro,m.F_DesPro, l.F_ClaLot, DATE_FORMAT(l.F_FecCad, '%d/%m/%Y') AS FecCad, (f.F_CantSur) AS F_Cant, l.F_Ubica, f.F_ClaDoc, l.F_Cb,m.F_DesPro, f.F_IdFact, f.F_FecEnt  FROM tb_factura f INNER JOIN tb_lote l on f.F_ClaPro=l.F_ClaPro AND f.F_Lote= l.F_FolLot AND f.F_Ubicacion=l.F_Ubica INNER JOIN tb_uniatn u on f.F_ClaCli=u.F_ClaCli INNER JOIN tb_medica m on f.F_ClaPro=m.F_ClaPro WHERE f.F_ClaDoc ='" + request.getParameter("fol_gnkl") + "';");
                while (rset.next()) {
                    cli = rset.getString("F_NomCli");
                    fec = rset.getString("F_FecEnt");
    %>
    <tr>
        <td><%=rset.getString("F_ClaCli")%></td>
        <td><%=rset.getString("F_NomCli")%></td>
        <td><%=rset.getString("FecEnt")%></td>
        <td><%=rset.getString("F_ClaPro")%></td>
        <td><%=rset.getString("F_DesPro")%></td>
        <td><%=rset.getString("F_ClaLot")%></td>
        <td><%=rset.getString("FecCad")%></td>
        <td><%=rset.getString("F_Cant")%></td>
    </tr>
    <%
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            con.cierraConexion();
        } catch (Exception e) {

            System.out.println(e.getMessage());
        }

    %>
</table>