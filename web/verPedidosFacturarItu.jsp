<%-- 
    Document   : index
    Created on : 17/02/2014, 03:34:46 PM
    Author     : Americo
--%>

<%@page import="conn.ConectionDB"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    /**
     * Listado de concentrados globales para su reimpresión
     */
    HttpSession sesion = request.getSession();
    String usua = "", tipo = "";
    if (sesion.getAttribute("Usuario") != null) {
        usua = (String) sesion.getAttribute("Usuario");
    } else {
        response.sendRedirect("indexGarden.jsp");
    }
    ConectionDB con = new ConectionDB();

    String fol_gnkl = "";
    try {
        if (request.getParameter("accion").equals("buscar")) {
            fol_gnkl = request.getParameter("fol_gnkl");
        }
    } catch (Exception e) {

    }
    if (fol_gnkl == null) {
        fol_gnkl = "";
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Estilos CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap.css">
        <!---->
        <title>SIALSS</title><link type="image/x-icon" href="imagenes/favicon.png" rel="icon" />
    </head>
    <body>
        <div class="container">
            <h1>SIALSS</h1>
            <h4>Módulo - Sistema de Órdenes de Compra, Pedidos y Facturación (SOCPF)</h4>
            <%@include file="jspf/menuCliente.jspf"%>

            <div>
                <h3>Revisión de Concentrados por Proveedor</h3>
                <h4>Seleccione</h4>

                <br />
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <table class="table table-bordered table-striped" id="datosCompras">
                            <thead>
                                <tr>
                                    <td>No.Prepedido</td>
                                    <td>No. Folio Validado</td>
                                    <td>Punto de entrega</td>
                                    <td>Sts Factura</td>
                                    <td>Fecha de entrega</td>
                                    <!--td>Transporte</td>
                                    <td>Sts Entrega</td>
                                    <td>No. Guía</td-->
                                    <td>Excel</td>
                                    <td>Envio de Factura</td>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <%                                    try {
                                        con.conectar();
                                        try {
                                            /**
                                             * Listado de concentrados
                                             */
                                            //
                                            String F_FolPre="",F_StsEnvio="",F_Guia="",F_Paqueteria="";
                                            ResultSet rset = con.consulta("SELECT f.F_ClaDoc, u.F_NomCli, DATE_FORMAT(f.F_FecEnt, '%d/%m/%Y') AS FecEnt, (f.F_CantSur + 0) AS F_Cant, f.F_IdFact,f.F_StsFact,fv.F_FolPre,u.F_Transporte,fv.F_StsEnvio,fv.F_Guia FROM tb_factura f INNER JOIN tb_uniatn u ON  f.F_ClaCli = u.F_ClaCli LEFT JOIN tb_foliosval fv on f.F_ClaDoc=fv.F_FolVal GROUP BY f.F_ClaDoc;");
                                            while (rset.next()) {
                                                F_FolPre = rset.getString("F_FolPre");
                                                F_StsEnvio = rset.getString("F_StsEnvio");
                                                F_Guia = rset.getString("F_Guia");
                                                F_Paqueteria = rset.getString("F_Transporte");
                                                if (F_FolPre == null){F_FolPre = "";}
                                                if(F_StsEnvio == null){F_StsEnvio="";}
                                                if(F_Guia == null){F_Guia="";}
                                %>
                                <tr>
                                    <td><%=F_FolPre%></td>
                                    <td class="Folios"><%=rset.getString("F_ClaDoc")%></td>
                                    <td><%=rset.getString("F_NomCli")%></td>
                                    <td><%=rset.getString("F_StsFact")%></td> 
                                    <td><%=rset.getString("FecEnt")%></td>
                                    <!--td><%//=rset.getString("F_Transporte")%></td>   
                                    <td><%//=F_StsEnvio%></td>
                                    <%//if((F_Paqueteria.equals("GNK"))||(F_Paqueteria.equals("CLIENTE RECOGE")) || (F_Paqueteria.equals("GNKL"))){%>
                                    <td class="NoGuia">N/A</td>
                                    <%//}else{%>
                                    <td class="NoGuia"><%//=F_Guia%></td-->
                                    <%//}%>
                                    <td>
                                        <a class="btn btn-block btn-success" href="gnrConcentradoFact.jsp?fol_gnkl=<%=rset.getString("F_ClaDoc")%>" target="_blank"><span class="glyphicon glyphicon-download"></span>&nbspDescargar</a>
                                    </td>
                                    <td>
                                        <button class="btn btn-warning rowButton" data-toggle="modal" data-target="#ModiOc"><span class="glyphicon glyphicon-envelope" ></span></button>                                        
                                    </td>
                                    
                                </tr>
                                <%
                                            }
                                        } catch (Exception e) {

                                        }
                                        con.cierraConexion();
                                    } catch (Exception e) {

                                    }
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div id="ModiOc" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Envío de Factura</h4>
                    </div>

                    <!--form method="post" class="jumbotron"  action="FileUploadServletCorreo" enctype="multipart/form-data" name="form1"-->
                        <form name="formEditOC" action="PedidoManual" method="Post">

                        <div class="modal-body">
                            <input class="form-control hidden" name="idMod" id="idMod" type="text" value="" readonly />
                            <input class="form-control hidden" name="F_NoCompra" id="F_NoCompra" type="" value="" readonly />

                            <div class="row">
                                <h4 class="col-sm-2">Folio:</h4>
                                <div class="col-sm-4">
                                    <input class="form-control" name="Folio" id="Folio" type="text" value="" readonly required/>
                                </div>                                
                            </div>
                            
                            <div class="row">                                
                                <h4 class="col-sm-2">N° Factura:</h4>
                                <div class="col-sm-4">
                                    <input class="form-control" name="Factu" id="Factu" type="text" value="" required=""/>
                                </div>
                            </div>
                            <!--div class="row">
                                <h6 class="col-sm-2">Archivo 1:</h6>                                
                                <div class="col-sm-4">
                                    <input class="form-control" name="file1" id="file1" type="file" value="" required="" onchange="nombre1(this.value)"/>
                                </div>
                                <h6 class="col-sm-2">Nombre Archivo:</h6>                                
                                <div class="col-sm-4">
                                    <input type="text" value="" placeholder="" id="ima1" name="ima1" readonly="" required="" />
                                </div>
                            </div-->
                            <!--div class="row">
                                <h6 class="col-sm-2">Archivo 2:</h6>                                
                                <div class="col-sm-4">
                                    <input class="form-control" name="file2" id="file2" type="file" value="" required="" onchange="nombre2(this.value)"/>
                                </div>
                                <h6 class="col-sm-2">Nombre Archivo:</h6>                                
                                <div class="col-sm-4">
                                    <input type="text" value="" placeholder="" id="ima2" name="ima2" readonly="" />
                                </div>
                            </div>
                            <div class="row">
                                <h6 class="col-sm-2">Archivo 3:</h6>                                
                                <div class="col-sm-4">
                                    <input class="form-control" name="file3" id="file3" type="file" value="" required="" onchange="nombre3(this.value)"/>
                                </div>
                                <h6 class="col-sm-2">Nombre Archivo:</h6>                                
                                <div class="col-sm-4">
                                    <input type="text" value="" placeholder="" id="ima3" name="ima3" readonly="" />
                                </div>
                            </div>
                            <div class="row">
                                <h6 class="col-sm-2">Archivo 4:</h6>                                
                                <div class="col-sm-4">
                                    <input class="form-control" name="file4" id="file4" type="file" value="" required="" onchange="nombre4(this.value)"/>
                                </div>
                                <h6 class="col-sm-2">Nombre Archivo:</h6>                                
                                <div class="col-sm-4">
                                    <input type="text" value="" placeholder="" id="ima4" name="ima4" readonly="" />
                                </div>
                            </div-->
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-default" name="accion" value="EnvioCorreo">Guardar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        </div>

                    </form>

                </div>

            </div>
        </div>                    
        <br><br><br>
        <%@include file="jspf/piePagina.jspf" %>

        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="js/jquery-1.9.1.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery-ui-1.10.3.custom.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/jquery.dataTables.js"></script>
        <script src="js/dataTables.bootstrap.js"></script>
        <script  type="text/javascript">
            $(document).ready(function () {
                $('#datosCompras').dataTable();
            });
            function EditarConcentrado(idCon, distri, fecha) {
                /**
                 * 
                 * jala informacion de la compra
                 */
                $('#hdIdCon').val(idCon);
                $('#lbIdCon').html("  " + idCon);
                $('#lbPun').html("  " + distri);
                $('#lbFecha').html("  " + fecha);
                $('#txtId').val("");
                $('#lbClave').html("");
                $('#lbLote').html("");
                $('#lbCaducidad').html("");
                $('#lbClave').html("");
                $('#lbCantidad').html("");
                $('#btnModificar').addClass("disabled");
                $('#txtCant').val("");
            }
            $('#btnBuscarId').click(function () {
                /**
                 * 
                 * Busca del id seleccionado los datos para editarlos
                 */
                var dir = "Concentrados";
                var id = $('#txtId').val();
                $.ajax({
                    url: dir,
                    data: {que: "b", id: id},
                    success: function (data) {
                        var json = JSON.parse(data);
                        $('#btnModificar').removeClass("disabled");
                        $('#txtId').val("");
                        $('#hdIdCla').val(json.id);
                        $('#lbClave').html(json.clave);
                        $('#lbLote').html(json.lote);
                        $('#lbCaducidad').html(json.caduc);
                        $('#lbClave').html(json.clave);
                        $('#lbCantidad').html(json.cant);
                        if (json.mgs === "0") {
                            $('#btnModificar').addClass("disabled");
                            alert("Error al consultar");
                        }
                    },
                    error: function () {
                        $('#txtId').val("");
                        alert("Ocurrió un error");
                    }
                });
            });
            $('#btnModificar').click(function () {
                /**
                 * Para la edición de la cantidad de los solicitado, ('solo menor')
                 */
                if ($('#txtCant').val() === "" || parseInt($('#txtCant').val()) > parseInt($('#lbCantidad').html())) {
                    alert("La cantida no puede ser cero o mayor a la actual");
                    $('#txtCant').focus();
                    return false;
                }
                var dir = "Concentrados";
                var id = $('#hdIdCla').val();
                var cant = $('#txtCant').val();
                $.ajax({
                    url: dir,
                    data: {que: "mod", id: id, cant: cant},
                    success: function (data) {
                        var json = JSON.parse(data);
                        if (json.msg === "1") {
                            alert('Actualizado Correctamene');
                            $('#lbLote').html("");
                            $('#lbCaducidad').html("");
                            $('#lbClave').html("");
                            $('#lbCantidad').html("");
                            $('#txtCant').val("");
                        } else {
                            alert('Error al Actualizar');
                            $('#lbLote').html("");
                            $('#lbCaducidad').html("");
                            $('#lbClave').html("");
                            $('#lbCantidad').html("");
                            $('#txtCant').val("");
                        }
                    },
                    error: function () {
                        $('#txtId').val("");
                        alert("Ocurrió un error");
                    }
                });
            });
        </script>
        <script>
            $(function () {
                $("#fecha").datepicker();
                $("#fecha").datepicker('option', {dateFormat: 'dd/mm/yy'});
            });
            
            $(".rowButton").click(function () {
            var $row = $(this).closest("tr");    // Find the row
            var $folio = $row.find("td.Folios").text(); // Find the text             
                      

            $("#Folio").val($folio);
            $("#Factu").val("");

        });
        </script>
        <script>
            function nombre1(fic) {
                fic = fic.split('\\');
                $('#ima1').val(fic[fic.length-1]);
            }
            function nombre2(fic) {
                fic = fic.split('\\');
                $('#ima2').val(fic[fic.length-1]);
            }
            function nombre3(fic) {
                fic = fic.split('\\');
                $('#ima3').val(fic[fic.length-1]);
            }
            function nombre4(fic) {
                fic = fic.split('\\');
                $('#ima4').val(fic[fic.length-1]);
            }
       </script>
    </body>
</html>