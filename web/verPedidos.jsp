<%-- 
    Document   : index
    Created on : 17/02/2014, 03:34:46 PM
    Author     : Americo
--%>

<%@page import="java.util.Date"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="conn.*" %>
<!DOCTYPE html>
<%
    /**
     * Listado de concentrados globales para su reimpresión
     */
    HttpSession sesion = request.getSession();
    String usua = "", tipo = "";
    if (sesion.getAttribute("nombre") != null) {
        usua = (String) sesion.getAttribute("nombre");
        tipo = (String) sesion.getAttribute("Tipo");
    } else {
        response.sendRedirect("index.jsp");
    }
    ConectionDB con = new ConectionDB();
    String Hora="",Minuto="",Segundo="";
    String fol_gnkl = "", fol_remi = "", orden_compra = "", fecha = "";
    try {
        if (request.getParameter("accion").equals("buscar")) {
            fol_gnkl = request.getParameter("fol_gnkl");
            fol_remi = request.getParameter("fol_remi");
            orden_compra = request.getParameter("orden_compra");
            fecha = request.getParameter("fecha");
        }
    } catch (Exception e) {

    }
    if (fol_gnkl == null) {
        fol_gnkl = "";
        fol_remi = "";
        orden_compra = "";
        fecha = "";
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Estilos CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="css/cupertino/jquery-ui-1.10.3.custom.css" />
        <!--link href="css/navbar-fixed-top.css" rel="stylesheet"-->
        <link href="css/datepicker3.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap.css">
        <!---->
        <title>SIALSS</title><link type="image/x-icon" href="imagenes/favicon.png" rel="icon" />
    </head>
    <body>
        <div class="container">
            <h1>SIALSS</h1>
            <h4>Módulo - Sistema de Administración de Almacenes (SAA)</h4>

            <%@include file="jspf/menuPrincipal.jspf"%>

            <div>
                <h3>Revisión de Concentrados por Proveedor</h3>
                <h4>Seleccione</h4>

                <br />
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <table class="table table-bordered table-striped" id="datosCompras">
                            <thead>
                                <tr>
                                    <td>No. Folio</td>
                                    <td>Punto de entrega</td>
                                    <td>Fecha de entrega</td>
                                    <td>Estado</td>
                                    <td>Observaciones</td>
                                    <td>Concentrado</td>
                                    <td>Marbetes</td>
                                    <td>Excel</td>
                                    <!--td>Fecha Creación</td-->
                                    <td>Hora Confirmado</td>
                                    <td>Hora Validado</td>
                                    <td>Tiempo Transcurrido</td>
                                </tr>
                            </thead>
                            <tbody>
                                <%                                    try {
                                        con.conectar();
                                        try {
                                            /**
                                             * Listado de concentrados
                                             */
                                            //
                                            String F_TerminoCap="",F_TerminoVal="",HoraPre="",HoraVal="",DifeHora="",TipoEnvio="",TipoCobro="";
                                            
                                            ResultSet rset = con.consulta("SELECT u.F_NomCli, DATE_FORMAT(f.F_FecEnt, '%d/%m/%Y') AS FecEnt, (f.F_Cant + 0) AS F_Cant, f.F_IdFact,(case when (f.F_StsFact = 0) then 'Por Validar' WHEN (f.F_StsFact = 5) THEN 'Validado' WHEN (f.F_StsFact = 3) THEN 'En Captura' end) AS estado,f.F_Obs,DATE_FORMAT(f.F_TerminoCap,'%Y-%m-%d %T') as F_TerminoCap,DATE_FORMAT(fv.F_TerminoVal,'%Y-%m-%d %T') as F_TerminoVal,DATE_FORMAT(F_TerminoCap,'%H:%i:%s') AS HoraPre,DATE_FORMAT(F_TerminoVal,'%H:%i:%s') AS HoraVal,DATE_FORMAT(f.F_TerminoCap, '%d/%m/%Y') AS FechaPre,f.F_TipoEnvio,f.F_TipoCobro  FROM tb_facttemp f INNER JOIN tb_uniatn u  ON LTRIM(f.F_ClaCli) = u.F_ClaCli LEFT JOIN tb_foliosval fv on f.F_IdFact=fv.F_FolPre GROUP BY f.F_IdFact;");
                                            while (rset.next()) {
                                                HoraVal = rset.getString("HoraVal");
                                                if(HoraVal == null){HoraVal="";}                                                
                                                F_TerminoCap=rset.getString("F_TerminoCap");                                              
                                                F_TerminoVal=rset.getString("F_TerminoVal");
                                                TipoEnvio = rset.getString("F_TipoEnvio");
                                                TipoCobro = rset.getString("F_TipoCobro");
                                                if(F_TerminoVal == null){
                                                ResultSet Fecha = con.consulta("SELECT NOW()");
                                                if(Fecha.next()){
                                                    F_TerminoVal=Fecha.getString(1);
                                                }
                                                }
                                                
                                                System.out.println("Folio: "+rset.getString("F_IdFact"));
                                                
                                %>
                                <tr>

                                    <td><%=rset.getString("F_IdFact")%></td>
                                    <td><%=rset.getString("F_NomCli")%></td>
                                    <td><%=rset.getString("FecEnt")%></td>
                                    <td><%=rset.getString("estado")%></td>
                                    <td><%=rset.getString("F_Obs")%></td>
                                    <%if((TipoEnvio.equals("PRE-PAGADO"))  || (TipoCobro.equals("PRE-PAGADO-OCURRE")) || (TipoCobro.equals("PRE-PAGADO"))){%>
                                    <td>
                                        <form action="ImprimirConcentrado" target="_blank">
                                            <input class="hidden" name="fol_gnkl" value="<%=rset.getString("F_IdFact")%>">
                                            <input class="hidden" name="tipo" value="PRE-PAGADO">
                                            <button class="btn btn-block btn-success glyphicon glyphicon-print"></button>
                                        </form>
                                    </td>
                                    <%}else{%>
                                    <td>
                                        <form action="ImprimirConcentrado" target="_blank">
                                            <input class="hidden" name="fol_gnkl" value="<%=rset.getString("F_IdFact")%>">
                                            <input class="hidden" name="tipo" value="">
                                            <button class="btn btn-block btn-success glyphicon glyphicon-print"></button>
                                        </form>
                                    </td>
                                    <%}%>                                    
                                    <td>
                                        <form action="ImprimirMarbete" target="_blank">
                                            <input class="hidden" name="fol_gnkl" value="<%=rset.getString("F_IdFact")%>">
                                            <button class="btn btn-block btn-warning glyphicon glyphicon-print"></button>
                                        </form>
                                    </td>
                                    <td>
                                        <a class="btn btn-block btn-danger glyphicon glyphicon-download-alt" href="gnrConcentrado.jsp?fol_gnkl=<%=rset.getString("F_IdFact")%>" target="_blank"></a>
                                    </td>
                                    <!--td><%//=rset.getString("FechaPre")%></td-->
                                    <td><%=rset.getString("HoraPre")%></td>
                                    <td><%=HoraVal%></td>
                                    <%
                                      ResultSet HoraDif = con.consulta("SELECT  HOUR(SEC_TO_TIME(TIMESTAMPDIFF(SECOND, '"+F_TerminoCap+"','"+F_TerminoVal+"'))), MINUTE(SEC_TO_TIME(TIMESTAMPDIFF(SECOND, '"+F_TerminoCap+"','"+F_TerminoVal+"'))), SECOND(SEC_TO_TIME(TIMESTAMPDIFF(SECOND, '"+F_TerminoCap+"','"+F_TerminoVal+"'))) ;");          
                                     if(HoraDif.next()){                                    
                                    Hora = HoraDif.getString(1);
                                         Minuto = HoraDif.getString(2);
                                         Segundo = HoraDif.getString(3);
                                         if(Hora == null){Hora = "00";}
                                         if(Minuto == null){Minuto = "00";}
                                         if(Segundo == null){Segundo = "00";}
                                    %>
                                    <td><%=Hora+":"+Minuto+":"+Segundo%></td>
                                    <%}%>
                                </tr>
                                <%
                                                F_TerminoCap="";
                                                F_TerminoVal="";
                                                //DifeHora = "";
                                            }
                                        } catch (Exception e) {

                                        }
                                        con.cierraConexion();
                                    } catch (Exception e) {

                                    }
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <br><br><br>
        <%@include file="jspf/piePagina.jspf" %>

        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="js/jquery-1.9.1.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery-ui-1.10.3.custom.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/jquery.dataTables.js"></script>
        <script src="js/dataTables.bootstrap.js"></script>
        <script  type="text/javascript">
            $(document).ready(function () {
                $('#datosCompras').dataTable();
            });
            function EditarConcentrado(idCon, distri, fecha) {
                /**
                 * 
                 * jala informacion de la compra
                 */
                $('#hdIdCon').val(idCon);
                $('#lbIdCon').html("  " + idCon);
                $('#lbPun').html("  " + distri);
                $('#lbFecha').html("  " + fecha);
                $('#txtId').val("");
                $('#lbClave').html("");
                $('#lbLote').html("");
                $('#lbCaducidad').html("");
                $('#lbClave').html("");
                $('#lbCantidad').html("");
                $('#btnModificar').addClass("disabled");
                $('#txtCant').val("");
            }
            $('#btnBuscarId').click(function () {
                /**
                 * 
                 * Busca del id seleccionado los datos para editarlos
                 */
                var dir = "Concentrados";
                var id = $('#txtId').val();
                $.ajax({
                    url: dir,
                    data: {que: "b", id: id},
                    success: function (data) {
                        var json = JSON.parse(data);
                        $('#btnModificar').removeClass("disabled");
                        $('#txtId').val("");
                        $('#hdIdCla').val(json.id);
                        $('#lbClave').html(json.clave);
                        $('#lbLote').html(json.lote);
                        $('#lbCaducidad').html(json.caduc);
                        $('#lbClave').html(json.clave);
                        $('#lbCantidad').html(json.cant);
                        if (json.mgs === "0") {
                            $('#btnModificar').addClass("disabled");
                            alert("Error al consultar");
                        }
                    },
                    error: function () {
                        $('#txtId').val("");
                        alert("Ocurrió un error");
                    }
                });
            });
            $('#btnModificar').click(function () {
                /**
                 * Para la edición de la cantidad de los solicitado, ('solo menor')
                 */
                if ($('#txtCant').val() === "" || parseInt($('#txtCant').val()) > parseInt($('#lbCantidad').html())) {
                    alert("La cantida no puede ser cero o mayor a la actual");
                    $('#txtCant').focus();
                    return false;
                }
                var dir = "Concentrados";
                var id = $('#hdIdCla').val();
                var cant = $('#txtCant').val();
                $.ajax({
                    url: dir,
                    data: {que: "mod", id: id, cant: cant},
                    success: function (data) {
                        var json = JSON.parse(data);
                        if (json.msg === "1") {
                            alert('Actualizado Correctamene');
                            $('#lbLote').html("");
                            $('#lbCaducidad').html("");
                            $('#lbClave').html("");
                            $('#lbCantidad').html("");
                            $('#txtCant').val("");
                        } else {
                            alert('Error al Actualizar');
                            $('#lbLote').html("");
                            $('#lbCaducidad').html("");
                            $('#lbClave').html("");
                            $('#lbCantidad').html("");
                            $('#txtCant').val("");
                        }
                    },
                    error: function () {
                        $('#txtId').val("");
                        alert("Ocurrió un error");
                    }
                });
            });
        </script>
        <script>
            $(function () {
                $("#fecha").datepicker();
                $("#fecha").datepicker('option', {dateFormat: 'dd/mm/yy'});
            });
        </script>
    </body>
</html>