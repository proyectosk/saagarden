
<%-- 
    Document   : index
    Created on : 17/02/2014, 03:34:46 PM
    Author     : Americo
--%>

<%@page import="ISEM.CapturaPedidos"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="conn.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<%java.text.DateFormat df2 = new java.text.SimpleDateFormat("yyyy-MM-dd"); %>
<%
    /**
     * Para facturación manual
     */
    HttpSession sesion = request.getSession();
    String usua = "", tipo = "";

    ConectionDB con = new ConectionDB();

    String ClaCli = "", DesCli = "", FechaEnt = "",Hora="", ClaPro = "", DesPro = "", existencia = "", claGenCliente = "", nombrePE = "";
    /**
     * Se obtiene el numero de folio
     */
    String F_IndGlobal = (String) sesion.getAttribute("F_IndGlobal");
    String noPedido;
    if (F_IndGlobal == null) {
        noPedido = "";
    } else {
        noPedido = F_IndGlobal;
    }
    try {
        nombrePE = (String) sesion.getAttribute("nombrePE");
        ClaCli = (String) sesion.getAttribute("ClaCliFM");
        claGenCliente = (String) sesion.getAttribute("ClaGenCliFM");
        DesCli = (String) sesion.getAttribute("DesCliFM");
        FechaEnt = (String) sesion.getAttribute("FechaEntFM");
        ClaPro = (String) sesion.getAttribute("ClaProFM");
        DesPro = (String) sesion.getAttribute("DesProFM");
        existencia = (String) sesion.getAttribute("ExistenciaFM");
        //Hora = (String) sesion.getAttribute("HoraEnt");
    } catch (Exception e) {

    }

    if (DesCli == null) {
        DesCli = "";
    }

    if (nombrePE == null) {
        nombrePE = "";
    }
    if (claGenCliente == null) {
        claGenCliente = "";
    }

    if (ClaCli == null) {
        ClaCli = "";
    }
    if (existencia == null) {
        existencia = "";
    }
    if (FechaEnt == null) {
        FechaEnt = "";
    }
    if (ClaPro == null) {
        ClaPro = "";
    }
    if (DesPro == null) {
        DesPro = "";
    }
    /**
     * Captura de ordenes de reposicion de inventario por ISEM
     */
    DecimalFormat formatter = new DecimalFormat("#,###,###");
    DecimalFormat formNoCom = new DecimalFormat("000");
    DecimalFormatSymbols custom = new DecimalFormatSymbols();
    custom.setDecimalSeparator('.');
    custom.setGroupingSeparator(',');
    formatter.setDecimalFormatSymbols(custom);
    String nomUsu = "";
    if (sesion.getAttribute("Usuario") != null) {
        usua = (String) sesion.getAttribute("Usuario");
        nomUsu = (String) sesion.getAttribute("NombreU");
    } else {
        response.sendRedirect("indexGarden.jsp");
    }
    CapturaPedidos indice = new CapturaPedidos();
    String proveedor = "", fecEnt = "", claPro = "", desPro = "", NoCompra = "";
    try {
        NoCompra = (String) sesion.getAttribute("NoCompra");
        proveedor = (String) sesion.getAttribute("proveedor");
        fecEnt = (String) sesion.getAttribute("fec_entrega");        
        claPro = (String) sesion.getAttribute("clave");
        desPro = (String) sesion.getAttribute("descripcion");
    } catch (Exception e) {
    }
    if (proveedor == null) {
        proveedor = "";
        fecEnt = "";
    }
    if (claPro == null) {
        claPro = "";
        desPro = "";
    }


%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Estilos CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <!---->
        <link href="css/cupertino/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css"/>

        <title>SIALSS</title>
    </head>
    <body>
        <div class="container">
            <h1>SIALSS</h1>
            <h4>Módulo - Sistema de Administración de Almacenes (SAA)</h4>

            <%@include file="jspf/menuCliente.jspf"%>

            <div class="row">
                <div class="col-sm-12">
                    <h2>Pedido Manual</h2>
                </div>
            </div>
            <hr/>
            <form action="PedidoManual" method="post">
                <div class="row">
                    <div class="col-sm-2">
                        <input type="text" class="form-control input-sm" id="NoCompra" name="NoCompra" value="<%=noPedido%>" readonly=""  />
                    </div>
                </div>
                <br>
                <div class="row">

                    <label class="col-sm-1 text-left">
                        <h4>Clave Cliente:</h4>
                    </label>
                    <div class="col-sm-2">  
                        <input class="form-control input-sm" placeholder="" name="ClaCli" id="ClaCli" value="<%=ClaCli%>" required/>
                    </div>
                    <label class="col-sm-1 text-left">
                        <h4>Nombre Cliente</h4>
                    </label>
                    <div class="col-sm-5">
                        <input class="form-control input-sm" placeholder="" name="desCli" id="desCli" value="<%=DesCli%>"  required/>
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-primary btn-block" name="accion" value="buscarCliente" id="btnCli">Buscar</button>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-1 text-left">
                        <h4>Punto de Entrega:</h4>
                    </label>
                    <div class="col-sm-5">

                        <select name="puntosEntrega" id="puntosEntrega">
                            <c:forEach items="${puntosEntrega}" var="detalle">
                                <option value="${detalle[0]}" <c:if test="${claGenCliente==detalle[0]}" >selected</c:if>>${detalle[1]}</option>
                            </c:forEach>
                        </select>
                    </div>
                    
                    <div class="col-sm-2" hidden>
                        <input class="form-control input-sm" placeholder="" name="clavereal" id="clavereal" value="<%=claGenCliente%>"  readonly/>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-1 text-left">
                        <h4>Transporte:</h4>
                    </label>
                    <div class="col-sm-5">

                        <select name="transporte" id="transporte">
                            
                        </select>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-1 text-left">
                        <h4>Paq. Dirección:</h4>
                    </label>
                    <div class="col-sm-5">
                        <select name="paqdir" id="paqdir">                            
                        </select>
                    </div>
                </div>    
                <div class="row">
                    <label class="col-sm-1 text-left">
                        <h4>Nombre PE:</h4>
                    </label>
                    <div class="col-sm-9">
                        <input class="form-control input-sm" placeholder="" name="nombrePE" id="nombrePE" value="<%=nombrePE%>"  readonly/>
                    </div>
                    <div class="col-sm-2">
                        <input class="form-control input-sm" placeholder="" name="nombrePE" id="nombrePE" value="<%=claGenCliente%>"  readonly/>
                    </div>
                </div>
                <div class="row">

                    <!--div class="col-sm-2">
                        <h4>Punto de Entrega</h4>
                    </div>
                    <div class="col-sm-5">
                        <select class="form-control" name="ClaCli" id="ClaCli">
                            <option value="">-Seleccione Punto de Entrega-</option>
                    <%                                try {
                            con.conectar();
                            ResultSet rset = con.consulta("select F_ClaCli, F_NomCli from tb_uniatn");
                            while (rset.next()) {
                    %>
                    <option value="<%=rset.getString(1)%>"
                    <%
                        if (rset.getString(1).equals(ClaCli)) {
                            out.println("selected");
                        }
                    %>
                    ><%=rset.getString(2)%></option>
                    <%
                            }
                            con.cierraConexion();
                        } catch (Exception e) {

                        }
                    %>
                </select>
            </div-->
                    <div class="col-sm-2">
                        <h4>Fecha de Entrega</h4>
                    </div>
                    <div class="col-sm-2">
                        <input type="date" class="form-control" name="FechaEnt" id="FechaEnt" min="<%=df2.format(new Date())%>" value="<%=FechaEnt%>"/>
                    </div>
                    <label class="col-sm-2">
                        <h4>Hora de Entrega:</h4>
                    </label>
                    <div class="col-sm-2">
                        <input type="time" class="form-control" name="hora" id="hora" value="<%=FechaEnt%>"/>

                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-sm-2">
                                <h4>Ingrese la Clave:</h4>
                            </div>
                            <div class="col-sm-2">
                                <input class="form-control" name="ClaPro" id="ClaPro" autocomplete="off" data-provide="typeahead"/>
                            </div>
                            <div class="col-sm-2">
                                <button class="btn btn-primary btn-block" name="accion" value="buscar" id="btnClave" onclick="return validaBuscar();">Buscar</button>
                            </div>

                        </div>
                        <div class="row">
                            <label class="col-sm-2 text-left">
                                <h4>Descripci&oacute;n:</h4>
                            </label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control typeahead" id="descripcion" name="descripcion" data-provide="typeahead" autocomplete="off" />
                            </div>
                            <div class="col-sm-2">
                                <button class="btn btn-primary btn-block" name="accion" value="buscarDesc" onclick="return validaClaDes(this);">Descripción</button>
                            </div>
                        </div>
                        <c:if test="${masdeuno!=null}">

                            <div class="row">
                                <form>
                                    <select name="claves">
                                        <c:forEach items="${claves}" var="detalle">
                                            <option value="${detalle}">${detalle}</option>
                                        </c:forEach>
                                    </select>
                                    <div class="col-sm-1">
                                        <button class="btn btn-primary btn-block" name="accion" value="selectClave">Buscar</button>
                                    </div>
                                </form>
                            </div>
                        </c:if>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-1">
                                <h4>Clave:</h4>
                            </div>
                            <div class="col-sm-2">
                                <input class="form-control" readonly="" value="<%=ClaPro%>" id="ClaveSel" />
                            </div>
                            <div class="col-sm-2">
                                <h4>Descripción:</h4>
                            </div>
                            <div class="col-sm-7">
                                <textarea class="form-control" readonly="" id="DesSel"><%=DesPro%></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-1">
                                <h4>Existencia:</h4>
                            </div>
                            <div class="col-sm-2">
                                <input class="form-control" readonly="" value="<%=existencia%>" id="ClaveSel" name="ClaveSel"/>
                            </div>

                        </div>
                        <br/>

                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-sm-2">
                                <h4>Cantidad:</h4>
                            </div>
                            <div class="col-sm-2">
                                <input class="form-control" name="Cantidad" id="Cantidad" onKeyPress="return justNumbers(event);"/>
                            </div>
                            <div class="col-sm-2">
                                <button class="btn btn-block btn-success" name="accion" value="SeleccionaLote" onclick="return validaSeleccionar();">Seleccionar</button>
                            </div>
                            <div class="col-sm-2">
                                <button class="btn btn-block btn-info" name="accion" value="Transferir" >Transferir</button>
                            </div>
                        </div>

                    </div>
                </div>
                <%
                    if (F_IndGlobal != null) {
                %>
                <input name="F_IndGlob" value="<%=F_IndGlobal%>" class="" hidden>
                <table class="table table-condensed table-striped table-bordered table-responsive">
                    <tr>
                        <td>Clave</td>
                        <td>Lote</td>
                        <td>Caducidad</td>
                        <td>Ubicación</td>
                        <td>Cantidad</td>
                        <td>Remover</td>
                    </tr>
                    <%
                        int banBtn = 0;
                        try {
                            con.conectar();
                            ResultSet rset = con.consulta("SELECT l.F_ClaPro, l.F_ClaLot, DATE_FORMAT(l.F_FecCad, '%d/%m/%Y'), f.F_Cant, l.F_Ubica, f.F_IdFact, f.F_Id FROM tb_facttemp f, tb_lote l WHERE f.F_IdLot = l.F_IdLote and F_ClaCli = '" + claGenCliente + "' and F_StsFact=3;");
                            while (rset.next()) {
                                banBtn = 1;
                    %>
                    <tr>
                        <td><%=rset.getString(1)%></td>
                        <td><%=rset.getString(2)%></td>
                        <td><%=rset.getString(3)%></td>
                        <td><%=rset.getString(5)%></td>
                        <td><%=rset.getString(4)%></td>
                        <td>
                            <button class="btn btn-block btn-danger" name="accionEliminar3" value="<%=rset.getString("F_Id")%>" onclick="return confirm('Seguro que desea eliminar esta clave?')"><span class="glyphicon glyphicon-remove"></span></button>
                        </td>
                    </tr>
                    <%
                            }
                            con.cierraConexion();
                        } catch (Exception e) {

                        }
                    %>
                </table>
                <%
                    if (banBtn == 1) {
                %>
                <div class="row">
                    <div class="col-sm-6">
                        <button class="btn btn-block btn-primary" name="accion" value="ConfirmarFactura" onclick="return confirm('Seguro de confirmar la Factura?')">Confirmar Pedido</button>
                    </div>
                    <div class="col-sm-6">
                        <button class="btn btn-block btn-danger" name="accion" value="CancelarFactura" onclick="return confirm('Seguro de CANCELAR la Factura?')">Cancelar Pedido</button>
                    </div>
                </div>

                <%
                        }
                    }
                %>

            </form>
        </div>
        <br><br><br>
        <div class="navbar navbar-fixed-bottom navbar-inverse">
            <div class="text-center text-muted">
                GNK Logística || Desarrollo de Aplicaciones 2009 - 2015 <span class="glyphicon glyphicon-registration-mark"></span><br />
                Todos los Derechos Reservados
            </div>
        </div> 
        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="js/jquery.js" type="text/javascript"></script>
        <script src="js/jquery-ui_1.js" type="text/javascript"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/bootstrap3-typeahead.js" type="text/javascript"></script>

        <script>
                            $(document).ready(function () {
                                $("#ClaPro").typeahead({
                                    source: function (request, response) {

                                        $.ajax({
                                            url: "AutoCompleteProdExist",
                                            dataType: "json",
                                            data: request,
                                            success: function (data, textStatus, jqXHR) {
                                                console.log(data);
                                                var items = data;
                                                response(items);
                                            },
                                            error: function (jqXHR, textStatus, errorThrown) {
                                                console.log(textStatus);
                                            }
                                        });
                                    }

                                });
                                $("#descripcion").typeahead({
                                    source: function (request, response) {

                                        $.ajax({
                                            url: "AutoCompleteProdExistDesc",
                                            dataType: "json",
                                            data: request,
                                            success: function (data, textStatus, jqXHR) {
                                                console.log(data);
                                                var items = data;
                                                response(items);
                                            },
                                            error: function (jqXHR, textStatus, errorThrown) {
                                                console.log(textStatus);
                                            }
                                        });
                                    }

                                });
                            });
        </script>
        
        <script>
                            $(function () {
                                $("#puntosEntrega").keyup(function () {
                                var PEn = $("#puntosEntrega").text();
                                alert("Hp"+PEn);
                                
                                
                            });
                             });
        </script>
        
        <script>
            function justNumbers(e)
            {
                var keynum = window.event ? window.event.keyCode : e.which;
                if ((keynum === 8) || (keynum === 46))
                    return true;
                return /\d/.test(String.fromCharCode(keynum));
            }

            function validaBuscar() {
                var Unidad = document.getElementById('ClaCli').value;
                if (Unidad === "") {
                    alert('Seleccione el cliente');
                    return false;
                }

                var FechaEnt = document.getElementById('FechaEnt').value;
                if (FechaEnt === "") {
                    alert('Seleccione Fecha de Entrega');
                    return false;
                }
                var clave = document.getElementById('ClaPro').value;
                if (clave === "") {
                    alert('Escriba una Clave');
                    return false;
                }
            }


            function validaSeleccionar() {
                var DesSel = document.getElementById('DesSel').value;
                if (DesSel === "") {
                    alert('Favor de Capturar Toda la información');
                    return false;
                }
                var cantidad = document.getElementById('Cantidad').value;
                if (cantidad === "") {
                    alert('Escriba una cantidad');
                    return false;
                }

            }

            $(function () {
                $("#ClaCli").keyup(function () {
                    var nombre = $("#ClaCli").val();
                    $("#ClaCli").autocomplete({
                        source: "AutoCompleteClientes?cla_pro=" + nombre + "&tipo=1",
                        selectFirst: true, //here
                        minLength: 0,
                        select: function (event, ui) {
                            $("#ClaCli").val(ui.item.cla_pro);
                            $("#desCli").val(ui.item.des_pro);
                            return false;
                        }
                    }).data("ui-autocomplete")._renderItem = function (ul, item) {
                        return $("<li>")
                                .data("ui-autocomplete-item", item)
                                .append("<a>" + item.cla_pro + "</a>")
                                .appendTo(ul);
                    };
                });
            });
            
            
            
            $(function () {
                $("#desCli").keyup(function () {
                    var nombre = $("#desCli").val();
                    $("#desCli").autocomplete({
                        source: "AutoCompleteClientes?cla_pro=" + nombre + "&tipo=2",
                        selectFirst: true, //here
                        minLength: 0,
                        select: function (event, ui) {
                            $("#ClaCli").val(ui.item.cla_pro);
                            $("#desCli").val(ui.item.des_pro);
                            return false;
                        }
                    }).data("ui-autocomplete")._renderItem = function (ul, item) {
                        return $("<li>")
                                .data("ui-autocomplete-item", item)
                                .append("<a>" + item.des_pro + "</a>")
                                .appendTo(ul);
                    };
                });
            });
            function llenarentregas() {

                $.post("AutoCompletePuntos?cla_pro=" + nombre + "&tipo=1", {
                    'idcategory': idc},
                function (data) {
                    var sel = $("#puntosEntrega");
                    sel.empty();
                    for (var i = 0; i < data.length; i++) {
                        sel.append('<option value="' + data[i].id + '">' + data[i].desc + '</option>');
                    }
                }, "json");
            }
        </script>
    </body>

</html>

