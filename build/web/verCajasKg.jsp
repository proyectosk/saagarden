<%-- 
    Document   : index
    Created on : 17/02/2014, 03:34:46 PM
    Author     : Americo
--%>

<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="conn.*" %>
<!DOCTYPE html>
<%
    /**
     * Listado de concentrados globales para su reimpresión
     */
    HttpSession sesion = request.getSession();
    String usua = "", tipo = "";
    if (sesion.getAttribute("nombre") != null) {
        usua = (String) sesion.getAttribute("nombre");
        tipo = (String) sesion.getAttribute("Tipo");
    } else {
        response.sendRedirect("index.jsp");
    }
    ConectionDB con = new ConectionDB();
    String Hora = "", Minuto = "", Segundo = "";
    String fol_gnkl = "", fol_remi = "", orden_compra = "", fecha = "", Estatus = "";
    String Fecha1 = "", Fecha11 = "";
    try {
        if (request.getParameter("accion").equals("buscar")) {
            fol_gnkl = request.getParameter("fol_gnkl");
            fol_remi = request.getParameter("fol_remi");
            orden_compra = request.getParameter("orden_compra");
            fecha = request.getParameter("fecha");
        }
    } catch (Exception e) {

    }
    try {
        Fecha1 = request.getParameter("fecha_ini");
        Estatus = request.getParameter("estatus");
    } catch (Exception e) {
        System.out.println(e.getMessage());
    }
    try {
        con.conectar();
        ResultSet RFecha = con.consulta("SELECT CURDATE() AS fecha;");
        if (RFecha.next()) {
            Fecha11 = RFecha.getString(1);

        }
        con.cierraConexion();
    } catch (Exception e) {

    }
    if ((Fecha1 == "") || (Fecha1 == null)) {
        Fecha1 = Fecha11;
    } else {
        Fecha1 = Fecha1;
    }
    
    System.out.println("Sts"+Fecha1);
    
    if((Estatus == null) || (Estatus.equals("")) || (Estatus.equals("--Seleccione--"))){
        Estatus = "Pendiente";
    }

    if (fol_gnkl == null) {
        fol_gnkl = "";
        fol_remi = "";
        orden_compra = "";
        fecha = "";
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Estilos CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="css/cupertino/jquery-ui-1.10.3.custom.css" />
        <!--link href="css/navbar-fixed-top.css" rel="stylesheet"-->
        <link href="css/datepicker3.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap.css">
        <!---->
        <title>SIALSS</title>
    </head>
    <body>
        <div class="container">
            <h1>SIALSS</h1>
            <h4>Módulo - Sistema de Administración de Almacenes (SAA)</h4>

            <%@include file="jspf/menuPrincipal.jspf"%>

            <div>
                <form name="forma1" id="forma1" action="verCajasKg.jsp" method="post">
                    <h3>Cajas Kg</h3>
                    <h4>Seleccione*</h4>

                    <div class="panel-footer">
                        <div class="row">
                            <label class="control-label col-sm-1" for="fecha_ini">Fecha:</label>
                            <div class="col-sm-2">
                                <input class="form-control" id="fecha_ini" name="fecha_ini" value="<%=Fecha1%>" type="date" onchange="habilitar(this.value);"/>
                            </div>
                            <div class="row">
                                <label class="control-label col-sm-1" for="estatus">Estatus:</label>
                                <select name="estatus" id="estatus">
                                    <option>--Seleccione--</option>
                                    <option>Entregado</option>
                                    <option>Pendiente</option>
                                </select>
                            </div>
                        </div>   
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <button class="btn btn-block btn-success" name="accion" value="mostrar" >MOSTRAR&nbsp;<label class="glyphicon glyphicon-search"></label></button>                        
                            </div>
                            <%if(Estatus.equals("Entregado")){%>
                            <div class="col-sm-3">
                                <a href="reportes/ReportLista.jsp?Fecha=<%=Fecha1%>" target="_black" class="btn btn-block btn-primary">Imprimir</a>
                            </div>
                            <%}%>
                        </div>
                    </div>

                    <br />                
                    <div class="panel panel-primary">                    
                        <div class="panel-body table-responsive">
                            <table class="table table-bordered table-striped" id="datosCompras">
                                <thead>
                                    <tr>                                    
                                        <td>No. Folio Validado</td>
                                        <td>Punto de entrega</td>
                                        <td>Sts Factura</td>
                                        <td>Fecha de entrega</td>
                                        <td>Cajas</td>
                                        <td>Peso Kg</td>
                                        <td>Estatus</td>
                                        <td>Modificar</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%                                    try {
                                            con.conectar();
                                            try {
                                                ResultSet rset = null;
                                                
                                                if(Estatus.equals("Entregado")){

                                                rset = con.consulta("SELECT f.F_ClaDoc, u.F_NomCli, DATE_FORMAT(f.F_FecEnt, '%d/%m/%Y') AS FecEnt, f.F_StsFact,cj.F_Cajas,cj.F_Kg,cj.F_Sts FROM tb_factura f INNER JOIN tb_uniatn u ON f.F_ClaCli = u.F_ClaCli INNER JOIN tb_cajaskg cj on f.F_ClaDoc=cj.F_Cladoc WHERE cj.F_Sts='"+Estatus+"' AND cj.F_Fecha='"+Fecha1+"'GROUP BY f.F_ClaDoc,f.F_StsFact;");
                                                }else{
                                                    rset = con.consulta("SELECT f.F_ClaDoc, u.F_NomCli, DATE_FORMAT(f.F_FecEnt, '%d/%m/%Y') AS FecEnt, f.F_StsFact,cj.F_Cajas,cj.F_Kg,cj.F_Sts FROM tb_factura f INNER JOIN tb_uniatn u ON f.F_ClaCli = u.F_ClaCli INNER JOIN tb_cajaskg cj on f.F_ClaDoc=cj.F_Cladoc WHERE cj.F_Sts='"+Estatus+"' GROUP BY f.F_ClaDoc,f.F_StsFact;");
                                                }
                                                while (rset.next()) {

                                    %>
                                    <tr>
                                        <td class="Documento"><%=rset.getString(1)%></td>
                                        <td><%=rset.getString(2)%></td>
                                        <td><%=rset.getString(4)%></td>                                
                                        <td><%=rset.getString(3)%></td>                                       
                                        <td class="cajas"><%=rset.getString(5)%></td>                                       
                                        <td class="kilo"><%=rset.getString(6)%></td>                                       
                                        <td class="sts"><%=rset.getString(7)%></td>                                       
                                        <td><button class="btn btn-warning rowButton" data-toggle="modal" data-target="#ModiOc"><span class="glyphicon glyphicon-pencil" ></span></button></td>                                    
                                    </tr>
                                    <%

                                                }
                                            } catch (Exception e) {

                                            }
                                            con.cierraConexion();
                                        } catch (Exception e) {

                                        }
                                    %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- Modal -->
        <div id="ModiOc" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modificar Cajas y/o Kg</h4>
                    </div>

                    <form name="formEditOC" action="PedidoManual" method="Post">

                        <div class="modal-body">
                            <input class="form-control hidden" name="idMod" id="idMod" type="text" value="" readonly />
                            <input class="form-control hidden" name="F_NoCompra" id="F_NoCompra" type="" value="" readonly />

                            <div class="row">
                                <h4 class="col-sm-1">Folio:</h4>
                                <div class="col-sm-2">
                                    <input class="form-control" name="Folio" id="Folio" type="text" value="" readonly required/>
                                </div>                                
                            </div>
                            <div class="row">
                                <h4 class="col-sm-1">Cajas:</h4>
                                <div class="col-sm-2">
                                    <input class="form-control" name="Cajas" id="Cajas" type="text" value="" readonly required/>
                                </div>
                                <h4 class="col-sm-1">Por:</h4>
                                <div class="col-sm-2">
                                    <input class="form-control" name="Cajas2" id="Cajas2" type="text" value=""/>
                                </div>
                            </div>
                            <div class="row">
                                <h4 class="col-sm-1">Kg:</h4>
                                <div class="col-sm-2">
                                    <input class="form-control" name="KG" id="KG" type="text" value="" readonly required/>
                                </div>
                                <h4 class="col-sm-1">Por:</h4>
                                <div class="col-sm-2">
                                    <input class="form-control" name="KG2" id="KG2" type="text" value=""/>
                                </div>
                            </div>  
                            <div class="row">
                                <h4 class="col-sm-1">Sts:</h4>
                                <div class="col-sm-3">
                                    <input class="form-control" name="STS" id="STS" type="text" value="" readonly required/>
                                </div>
                                <h4 class="col-sm-1">Por:</h4>                                
                                <div class="col-sm-3">
                                    <select name="STS2" id="STS2">
                                        <option>--Seleccione--</option>
                                        <option>Entregado</option>
                                        <option>Pendiente</option>
                                    </select>
                                </div>
                            </div>  
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-default" name="accion" value="CambiOCKG">Guardar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        </div>

                    </form>

                </div>

            </div>
        </div>                    
        <br><br><br>
        <div class="navbar navbar-fixed-bottom navbar-inverse">
            <div class="text-center text-muted">
                GNK Logística || Desarrollo de Aplicaciones 2009 - 2016 <span class="glyphicon glyphicon-registration-mark"></span><br />
                Todos los Derechos Reservados
            </div>
        </div>

        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="js/jquery-1.9.1.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery-ui-1.10.3.custom.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/jquery.dataTables.js"></script>
        <script src="js/dataTables.bootstrap.js"></script>
        <script  type="text/javascript">
                                    $(document).ready(function() {
                                        $('#datosCompras').dataTable();
                                    });

                                    $(".rowButton").click(function() {
                                        var $row = $(this).closest("tr");    // Find the row
                                        var $folio = $row.find("td.Documento").text(); // Find the text             
                                        var $cajas = $row.find("td.cajas").text(); // Find the text
                                        var $kg = $row.find("td.kilo").text(); // Find the text
                                        var $Sts = $row.find("td.sts").text(); // Find the text


                                        $("#Folio").val($folio);
                                        $("#Cajas").val($cajas);
                                        $("#KG").val($kg);
                                        $("#STS").val($Sts);

                                    });


        </script>
        <script>
            $(function() {
                $("#fecha").datepicker();
                $("#fecha").datepicker('option', {dateFormat: 'dd/mm/yy'});
            });

        </script>
    </body>
</html>