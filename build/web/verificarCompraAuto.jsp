<%-- 
    Document   : index
    Created on : 17/02/2014, 03:34:46 PM
    Author     : Americo
--%>

<%@page import="java.util.Date"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="conn.*" %>
<!DOCTYPE html>
<%java.text.DateFormat df2 = new java.text.SimpleDateFormat("yyyy-MM-dd"); %>
<%java.text.DateFormat df3 = new java.text.SimpleDateFormat("dd/MM/yyyy"); %>
<%
    /**
     * Para verificar las compras que están en proceso de ingreso
     */
    DecimalFormat formatter = new DecimalFormat("#,###,###");
    DecimalFormat formatterDecimal = new DecimalFormat("#,###,##0.00");
    DecimalFormatSymbols custom = new DecimalFormatSymbols();
    custom.setDecimalSeparator('.');
    custom.setGroupingSeparator(',');
    formatter.setDecimalFormatSymbols(custom);
    formatterDecimal.setDecimalFormatSymbols(custom);
    HttpSession sesion = request.getSession();
    String usua = "", tipo = "";
    if (sesion.getAttribute("nombre") != null) {
        usua = (String) sesion.getAttribute("nombre");
        tipo = (String) sesion.getAttribute("Tipo");
    } else {
        response.sendRedirect("index.jsp");
    }
    ConectionDB con = new ConectionDB();
    String vOrden = "", vRemi = "";

    try {
        vOrden = (String) sesion.getAttribute("vOrden");
        vRemi = (String) sesion.getAttribute("vRemi");
    } catch (Exception e) {
    }

    try {
        String Folio = "";
        String folio[] = null;
        Folio = request.getParameter("NoCompra");
        /**
         * Se obtiene la compra, a esta se le aplica un split para hacer la
         * busqeda en los 2 campos de la tabla
         */
        if (!Folio.equals("")) {
            folio = Folio.split(",");
            sesion.setAttribute("vOrden", folio[0]);
            sesion.setAttribute("vRemi", folio[1]);
            vOrden = folio[0];
            vRemi = folio[1];
        }
    } catch (Exception e) {
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Estilos CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/datepicker3.css" rel="stylesheet">
        <link rel="stylesheet" href="css/cupertino/jquery-ui-1.10.3.custom.css" />
        <!---->
        <!---->
        <title>SIALSS</title><link type="image/x-icon" href="imagenes/favicon.png" rel="icon" />
    </head>
    <body>
        <div class="container">
            <h1>SIALSS</h1>
            <h4>Módulo - Sistema de Administración de Almacenes (SAA)</h4>            
            <%@include file="jspf/menuPrincipal.jspf"%>
            <form action="verificarCompraAuto.jsp" method="get">
                <br/>
                <div class="row">
                    <label class="col-sm-2">
                        <h4>&Oacute;rdenes de Compra: </h4>
                    </label>
                    <div class="col-sm-9">
                        <select class="form-control" name="NoCompra" onchange="this.form.submit();">
                            <option value="">-- Proveedor -- ORDEN DE REPOSICIÓN DE INVENTARIO --</option>
                            <%                                try {
                                    con.conectar();
                                    ResultSet rset = con.consulta("SELECT c.F_OrdCom, p.F_NomPro, c.F_FolRemi FROM tb_compratemp c, tb_proveedor p WHERE c.F_Provee = p.F_ClaProve and c.F_Estado = '2' GROUP BY c.F_OrdCom, c.F_FolRemi");
                                    while (rset.next()) {
                            %>
                            <option value="<%=rset.getString(1)%>,<%=rset.getString(3)%>"><%=rset.getString(2)%> - <%=rset.getString(1)%> - <%=rset.getString(3)%></option>
                            <%
                                    }
                                    con.cierraConexion();
                                } catch (Exception e) {

                                }
                            %>
                        </select>
                    </div>
                </div>
                <br/>
            </form>
        </div>
        <div style="width: 90%; margin: auto;">
            <br/>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <form action="CompraAutomatica" method="get" name="formulario1">
                        <%
                            try {
                                con.conectar();
                                ResultSet rset = con.consulta("select i.F_NoCompra, DATE_FORMAT(i.F_FecSur, '%d/%m/%Y') as F_FecSur, i.F_HorSur, p.F_NomPro, p.F_ClaProve from tb_pedidoisem i, tb_proveedor p where i.F_Provee = p.F_ClaProve and F_StsPed = '1' and F_NoCompra = '" + vOrden + "'  and F_recibido='0' group by F_NoCompra");
                                while (rset.next()) {
                        %>
                        <div class="row">
                            <h4 class="col-sm-2">Folio ORDEN DE REPOSICIÓN DE INVENTARIO:</h4>
                            <div class="col-sm-2"><input class="form-control" value="<%=vOrden%>" readonly="" name="folio" id="folio" onkeypress="return tabular(event, this)" /></div>
                            <h4 class="col-sm-1">Remisión:</h4>
                            <div class="col-sm-2"><input class="form-control" value="<%=vRemi%>" readonly="" name="folio" id="folio" onkeypress="return tabular(event, this)" /></div>
                        </div>
                        <div class="row">
                            <h4 class="col-sm-12">Proveedor: <%=rset.getString("p.F_NomPro")%></h4>
                        </div>
                        <div class="row">
                            <h4 class="col-sm-9">Fecha y Hora de Entrega: <%=rset.getString("F_FecSur")%> <%=rset.getString("i.F_HorSur")%></h4>
                            <div class="col-sm-2">
                                <a class="btn btn-default" href="compraAuto2.jsp">Agregar Clave al Inventario</a>
                            </div>
                        </div>
                        <%
                                }
                                con.cierraConexion();
                            } catch (Exception e) {
                                e.getMessage();
                            }
                        %>
                    </form>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tr>
                                <td>Remisión</td>
                                <td>Clave</td>
                                <td>Descripción</td>                       
                                <td>Lote</td>                        
                                <td>Cantidad</td>                      
                                <td>Costo U</td>                     
                                <td>IVA</td>                       
                                <td>Importe</td>
                                <td>Observaciones</td>
                                <td></td>
                            </tr>
                            <%
                                int banBtn = 0;
                                try {
                                    /**
                                     * Se busca con base en la ORDEN DE REPOSICIÓN DE INVENTARIO y
                                     * la remisión y que el estado sea 2 para
                                     * poder validar su entrada a almacén
                                     */
                                    con.conectar();
                                    ResultSet rset = con.consulta("SELECT C.F_Cb,C.F_ClaPro,M.F_DesPro,C.F_Lote,C.F_FecCad,C.F_Pz,F_IdCom, C.F_Costo, C.F_ImpTo, C.F_ComTot, C.F_FolRemi, C.F_Obser FROM tb_compratemp C INNER JOIN tb_medica M ON C.F_ClaPro=M.F_ClaPro WHERE F_OrdCom='" + vOrden + "' and F_FolRemi = '" + vRemi + "'  and F_Estado = '2'");
                                    while (rset.next()) {
                                        banBtn = 1;
                            %>
                            <tr>
                                <td><%=rset.getString("C.F_FolRemi")%></td>
                                <td><%=rset.getString(2)%></td>
                                <td><%=rset.getString(3)%></td>
                                <td><%=rset.getString(4)%></td>
                                <td><%=formatter.format(rset.getDouble(6))%></td>           
                                <td><%=formatterDecimal.format(rset.getDouble("C.F_Costo"))%></td>
                                <td><%=formatterDecimal.format(rset.getDouble("C.F_ImpTo"))%></td>          
                                <td><%=formatterDecimal.format(rset.getDouble("C.F_ComTot"))%></td>   
                                <td><%=rset.getString("C.F_Obser")%></td>
                                <td>
                                    <form method="get" action="Modificaciones">
                                        <input name="id" type="text" style="" class="hidden" value="<%=rset.getString(7)%>" />
                                        <!--button class="btn btn-warning" name="accion" value="modificarVerifica"><span class="glyphicon glyphicon-pencil" ></span></button-->
                                        <button class="btn btn-danger" onclick="return confirm('¿Seguro de que desea eliminar?');" name="accion" value="eliminarVerifica"><span class="glyphicon glyphicon-remove"></span></button>
                                    </form>
                                </td>
                            </tr>
                            <%
                                    }
                                    con.cierraConexion();
                                } catch (Exception e) {

                                }

                            %>
                            <tr>
                                <td colspan="12">

                                </td>
                            </tr>

                        </table>
                    </div>
                    <hr/>
                </div>
                <%                                if (banBtn == 1) {
                        /**
                         * Para mostrar el botón de liberación
                         */
                %>

                <div class="panel-body table-responsive">
                    <div class="row">
                        <form action="nuevoAutomaticaLotes" method="post" onsubmit="desactivaBotones()">
                            <input name="vOrden" type="text" style="" class="hidden" value='<%=vOrden%>' />
                            <input name="vRemi" type="text" style="" class="hidden" value='<%=vRemi%>' />
                            <div class="col-lg-3 col-lg-offset-3">
                                <button id="EliminarVerifica" value="EliminarVerifica" name="accion" class="btn btn-danger btn-block" onclick="return confirm('Seguro que desea eliminar la compra?');">Cancelar Remisión</button>
                            </div>
                            <div class="col-lg-3">
                                <button id="GuardarAbiertaVerifica" value="GuardarAbiertaVerifica" name="accion" class="btn btn-warning  btn-block" onclick="return confirm('Seguro que desea realizar la compra?');
                                        return validaCompra();">Remisión Abierta</button>
                            </div>
                            <div class="col-lg-3">
                                <button id="GuardarVerifica" value="GuardarVerifica" name="accion" class="btn btn-success  btn-block" onclick="return confirm('Seguro que desea realizar la compra?');
                                    return validaCompra();">Confirmar Remisi&oacute;n</button>
                            </div>
                        </form>
                    </div>
                </div>
                <%
                    }
                %>
            </div>
        </div>


        <br><br><br>
        <%@include file="jspf/piePagina.jspf" %>
        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="js/jquery-1.9.1.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
    </body>
</html>
