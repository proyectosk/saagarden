<%-- 
    Document   : index
    Created on : 17/02/2014, 03:34:46 PM
    Author     : Americo
--%>

<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="conn.*" %>
<!DOCTYPE html>
<%
    /**
     * Listado de concentrados globales para su reimpresión
     */
    HttpSession sesion = request.getSession();
    String usua = "", tipo = "";
    if (sesion.getAttribute("nombre") != null) {
        usua = (String) sesion.getAttribute("nombre");
        tipo = (String) sesion.getAttribute("Tipo");
    } else {
        response.sendRedirect("index.jsp");
    }
    ConectionDB con = new ConectionDB();
    String Hora="",Minuto="",Segundo="";
    String fol_gnkl = "", fol_remi = "", orden_compra = "", fecha = "";
    try {
        if (request.getParameter("accion").equals("buscar")) {
            fol_gnkl = request.getParameter("fol_gnkl");
            fol_remi = request.getParameter("fol_remi");
            orden_compra = request.getParameter("orden_compra");
            fecha = request.getParameter("fecha");
        }
    } catch (Exception e) {

    }
    if (fol_gnkl == null) {
        fol_gnkl = "";
        fol_remi = "";
        orden_compra = "";
        fecha = "";
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Estilos CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="css/cupertino/jquery-ui-1.10.3.custom.css" />
        <!--link href="css/navbar-fixed-top.css" rel="stylesheet"-->
        <link href="css/datepicker3.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap.css">
        <!---->
        <title>SIALSS</title><link type="image/x-icon" href="imagenes/favicon.png" rel="icon" />
    </head>
    <body>
        <div class="container">
            <h1>SIALSS</h1>
            <h4>Módulo - Sistema de Administración de Almacenes (SAA)</h4>

            <%@include file="jspf/menuPrincipal.jspf"%>

            <div>
                <h3>Revisión de Concentrados por Proveedor</h3>
                <h4>Seleccione*</h4>

                <br />
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <div class="panel panel-primary">
                        <div class="panel-body table-responsive">
                            <div style="width:100%; height:700px; overflow:auto;">
                        <table class="table table-bordered table-striped" id="datosCompras">
                            <thead>
                                <tr>
                                    <td>No.Pre-pedido</td>
                                    <td>No. Folio Validado</td>
                                    <td>Punto de entrega</td>
                                    <td>Sts Factura</td>
                                    <td>Fecha de entrega</td>                                    
                                    <td>Imprimir</td>
                                    <td>Pre-pagado</td>
                                    <td>Transporte</td>
                                    <td>Sts Entrega</td>
                                    <td>No. Guía</td>
                                    <td>Tiempo Generado</td>
                                    <td>Feche Entrego Paq.</td>
                                    <td>Modificar</td>
                                    <% if ((usua.equals("ricardo")) || (usua.equals("jose")) || (usua.equals("ramon")) || (usua.equals("sistemas"))) {
                                            //out.println("<td>Reintegrar Insumo</td>");
                                            out.println("<td>Devoluciones</td>");
                                        }
                                    %>                                    
                                </tr>
                            </thead>
                            <tbody>
                                <%                                    try {
                                        con.conectar();
                                        try {
                                            /**
                                             * Listado de concentrados
                                             */
                                            //
                                            String F_FolPre="",F_Paqueteria="",F_StsEnvio="",F_Guia="",F_TipoEnvio="",F_TerminoCap="",F_TerminoVal="",TipoCobro="";
                                            ResultSet rset = con.consulta("SELECT f.F_ClaDoc, u.F_NomCli, DATE_FORMAT(f.F_FecEnt, '%d/%m/%Y') AS FecEnt, (f.F_CantSur + 0) AS F_Cant, f.F_IdFact,f.F_StsFact,fv.F_FolPre,u.F_Transporte,fv.F_StsEnvio,fv.F_Guia,DATE_FORMAT(fv.F_TerminoVal,'%Y-%m-%d %T') as F_TerminoVal,DATE_FORMAT(fv.F_FecCliente, '%d/%m/%Y') AS F_FecCliente  FROM tb_factura f INNER JOIN tb_uniatn u ON f.F_ClaCli = u.F_ClaCli LEFT JOIN tb_foliosval fv on f.F_ClaDoc=fv.F_FolVal GROUP BY f.F_ClaDoc,f.F_StsFact;");
                                            while (rset.next()) {
                                                F_FolPre = rset.getString("F_FolPre");
                                                F_Paqueteria = rset.getString("F_Transporte");
                                                F_StsEnvio = rset.getString("F_StsEnvio");
                                                F_Guia = rset.getString("F_Guia");
                                                F_TerminoVal=rset.getString("F_TerminoVal");
                                                if(F_TerminoVal == null){
                                                ResultSet Fecha = con.consulta("SELECT NOW()");
                                                if(Fecha.next()){
                                                    F_TerminoVal=Fecha.getString(1);
                                                }
                                                }
                                                if (F_FolPre == null){F_FolPre = "";}
                                                if(F_StsEnvio == null){F_StsEnvio="";}
                                                if(F_Guia == null){F_Guia="";}
                                                
                                                if(F_FolPre.equals("")){
                                                    F_FolPre="0";
                                                }
                                                
                                                ResultSet PrePaq = con.consulta("SELECT F_TipoEnvio,DATE_FORMAT(F_TerminoCap,'%Y-%m-%d %T') as F_TerminoCap,F_TipoCobro FROM tb_facttemp WHERE F_IdFact='"+F_FolPre+"' GROUP BY F_TipoEnvio");
                                                if(PrePaq.next()){
                                                    F_TipoEnvio = PrePaq.getString(1);
                                                    F_TerminoCap=PrePaq.getString(2);
                                                    TipoCobro = PrePaq.getString("F_TipoCobro");
                                                }
                                                
                                                
                                                
                                %>
                                <tr>
                                    <td><%=F_FolPre%></td>
                                    <td class="Documento"><%=rset.getString("F_ClaDoc")%></td>
                                    <td><%=rset.getString("F_NomCli")%></td>
                                    <td><%=rset.getString("F_StsFact")%></td>                                
                                    <td><%=rset.getString("FecEnt")%></td>                                       
                                    <td>
                                        <a href="reportes/ReportVal.jsp?Documento=<%=rset.getString("F_ClaDoc")%>&tipo=" target="_black" class="btn btn-block btn-primary">Imprimir</a>
                                            
                                    </td>
                                    <%if((F_TipoEnvio.equals("PRE-PAGADO"))  || (TipoCobro.equals("PRE-PAGADO-OCURRE")) || (TipoCobro.equals("PRE-PAGADO"))){%>
                                    <td>
                                        <a href="reportes/ReportVal.jsp?Documento=<%=rset.getString("F_ClaDoc")%>&tipo=PRE-PAGADO" target="_black" class="btn btn-block btn-primary">Imprimir</a>
                                            
                                    </td> 
                                    <%}else{%>
                                    <td></td>
                                    <%}%>
                                    <td><%=rset.getString("F_Transporte")%></td>                                    
                                    <td class="StsEnvio"><%=F_StsEnvio%></td>
                                    <%if((F_Paqueteria.equals("GNK"))||(F_Paqueteria.equals("CLIENTE RECOGE")) || (F_Paqueteria.equals("GNKL"))){%>
                                    <td class="NoGuia">N/A</td>
                                    <%}else{%>
                                    <td class="NoGuia"><%=F_Guia%></td>
                                    <%}
                                      ResultSet HoraDif = con.consulta("SELECT  HOUR(SEC_TO_TIME(TIMESTAMPDIFF(SECOND, '"+F_TerminoCap+"','"+F_TerminoVal+"'))), MINUTE(SEC_TO_TIME(TIMESTAMPDIFF(SECOND, '"+F_TerminoCap+"','"+F_TerminoVal+"'))), SECOND(SEC_TO_TIME(TIMESTAMPDIFF(SECOND, '"+F_TerminoCap+"','"+F_TerminoVal+"'))) ;");          
                                     if(HoraDif.next()){
                                          Hora = HoraDif.getString(1);
                                         Minuto = HoraDif.getString(2);
                                         Segundo = HoraDif.getString(3);
                                         if(Hora == null){Hora = "00";}
                                         if(Minuto == null){Minuto = "00";}
                                         if(Segundo == null){Segundo = "00";}
                                    %>
                                    <td><%=Hora+":"+Minuto+":"+Segundo%></td>
                                    <%}%> 
                                    <td><%=rset.getString("F_FecCliente")%></td>
                                    <td><button class="btn btn-warning rowButton" data-toggle="modal" data-target="#ModiOc"><span class="glyphicon glyphicon-pencil" ></span></button></td>
                                    <%
                                        if ((usua.equals("ricardo")) || (usua.equals("jose")) || (usua.equals("ramon")) || (usua.equals("sistemas"))) {
                                    %>
                                    <td>
                                        <form action="DevolucionGlobal" method="post">
                                            <input class="hidden" name="fol_gnkl" value="<%=rset.getString("F_ClaDoc")%>">
                                            <button class="btn btn-block btn-danger" name="accion" value="DevoGlobal"><span class="glyphicon glyphicon-arrow-left"></span></button>
                                        </form>
                                    </td>
                                    <%
                                        }
                                    %>
                                </tr>
                                <%
                                           F_TipoEnvio="";
                                            }
                                        } catch (Exception e) {

                                        }
                                        con.cierraConexion();
                                    } catch (Exception e) {

                                    }
                                %>
                            </tbody>
                        </table>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div id="ModiOc" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modificar Sts Envío y Guía</h4>
                    </div>

                    <form name="formEditOC" action="PedidoManual" method="Post">

                        <div class="modal-body">
                            <input class="form-control hidden" name="idMod" id="idMod" type="text" value="" readonly />
                            <input class="form-control hidden" name="F_NoCompra" id="F_NoCompra" type="" value="" readonly />

                            <div class="row">
                                <h4 class="col-sm-2">Folio:</h4>
                                <div class="col-sm-4">
                                    <input class="form-control" name="Folio" id="Folio" type="text" value="" readonly required/>
                                </div>                                
                            </div>
                            <div class="row">
                                <h4 class="col-sm-2">Sts Envío:</h4>
                                <div class="col-sm-4">
                                    <input class="form-control" name="Envio" id="Envio" type="text" value="" readonly required/>
                                </div>
                                <h4 class="col-sm-2">Por:</h4>
                                <div class="col-sm-4">
                                    <select name="Envio1" id="Envio1">
                                        <option value="">--Seleccione--</option>
                                        <option value="ENTREGADO">Entregado</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <h4 class="col-sm-2">No Guía:</h4>
                                <div class="col-sm-4">
                                    <input class="form-control" name="Guia" id="Guia" type="text" value="" readonly required/>
                                </div>
                                <h4 class="col-sm-2">Por:</h4>
                                <div class="col-sm-4">
                                    <input class="form-control" name="Guia1" id="Guia1" type="text" value=""/>
                                </div>
                            </div>
                            <div class="row">
                                <h4 class="col-sm-2">Fecha Entrega Cliente:</h4>                                
                                <div class="col-sm-4">
                                    <input class="form-control" name="fechacli" id="fechacli" type="date" value=""/>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-default" name="accion" value="CambiO">Guardar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        </div>

                    </form>

                </div>

            </div>
        </div>                    
        <br><br><br>
        <%@include file="jspf/piePagina.jspf" %>

        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="js/jquery-1.9.1.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery-ui-1.10.3.custom.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/jquery.dataTables.js"></script>
        <script src="js/dataTables.bootstrap.js"></script>
        <script  type="text/javascript">
            $(document).ready(function () {
                $('#datosCompras').dataTable();
            });
        
        $(".rowButton").click(function () {
            var $row = $(this).closest("tr");    // Find the row
            var $folio = $row.find("td.Documento").text(); // Find the text             
            var $guia = $row.find("td.NoGuia").text(); // Find the text
            var $envio = $row.find("td.StsEnvio").text(); // Find the text
            

            $("#Folio").val($folio);
            $("#Guia").val($guia);
            $("#Envio").val($envio);           

        });
            
           
        </script>
        <script>
            $(function () {
                $("#fecha").datepicker();
                $("#fecha").datepicker('option', {dateFormat: 'dd/mm/yy'});
            });
            
        </script>
    </body>
</html>