
<%-- 
    Document   : index
    Created on : 17/02/2014, 03:34:46 PM
    Author     : Americo
--%>

<%@page import="ISEM.CapturaPedidos"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="conn.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<%java.text.DateFormat df2 = new java.text.SimpleDateFormat("yyyy-MM-dd"); %>
<%
    /**
     * Para facturación manual
     */
    HttpSession sesion = request.getSession();
    String usua = "", tipo = "";

    ConectionDB con = new ConectionDB();

    String ClaCli = "", DesCli = "", FechaEnt = "",Hora="", ClaPro = "", DesPro = "", existencia = "", claGenCliente = "", nombrePE = "";
    /**
     * Se obtiene el numero de folio
     */
    String F_IndGlobal = (String) sesion.getAttribute("F_IndGlobal");
    String noPedido;
    if (F_IndGlobal == null) {
        noPedido = "";
    } else {
        noPedido = F_IndGlobal;
    }
    try {
        nombrePE = (String) sesion.getAttribute("nombrePE");
        ClaCli = (String) sesion.getAttribute("ClaCliFM");        
        DesCli = (String) sesion.getAttribute("DesCliFM");
      
        //Hora = (String) sesion.getAttribute("HoraEnt");
    } catch (Exception e) {

    }

    if (DesCli == null) {
        DesCli = "";
    }

    if (nombrePE == null) {
        nombrePE = "";
    }
    if (claGenCliente == null) {
        claGenCliente = "";
    }

    if (ClaCli == null) {
        ClaCli = "";
    }
    if (existencia == null) {
        existencia = "";
    }
    if (FechaEnt == null) {
        FechaEnt = "";
    }
    if (ClaPro == null) {
        ClaPro = "";
    }
    if (DesPro == null) {
        DesPro = "";
    }
    /**
     * Captura de ordenes de reposicion de inventario por ISEM
     */
    DecimalFormat formatter = new DecimalFormat("#,###,###");
    DecimalFormat formNoCom = new DecimalFormat("000");
    DecimalFormatSymbols custom = new DecimalFormatSymbols();
    custom.setDecimalSeparator('.');
    custom.setGroupingSeparator(',');
    formatter.setDecimalFormatSymbols(custom);
    String nomUsu = "";
    if (sesion.getAttribute("Usuario") != null) {
        usua = (String) sesion.getAttribute("Usuario");
        nomUsu = (String) sesion.getAttribute("NombreU");
    } else {
        response.sendRedirect("indexGarden.jsp");
    }
    CapturaPedidos indice = new CapturaPedidos();
    String proveedor = "", fecEnt = "", claPro = "", desPro = "", NoCompra = "",Direc="",PaqDir="",Paquet="";
    try {
        NoCompra = (String) sesion.getAttribute("NoCompra");
        proveedor = (String) sesion.getAttribute("proveedor");
             
        
    } catch (Exception e) {
    }
    if (proveedor == null) {
        proveedor = "";
        fecEnt = "";
    }
    


%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Estilos CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <!---->
        <link href="css/cupertino/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css"/>

        <title>SIALSS</title>
    </head>
    <body>
        <div class="container">
            <h1>SIALSS</h1>
            <h4>Módulo - Sistema de Administración de Almacenes (SAA)</h4>

            <%@include file="jspf/menuCliente.jspf"%>

            <div class="row">
                <div class="col-sm-12">
                    <h2>Pedido Manual</h2>
                </div>
            </div>
            <hr/>
            <form action="PedidoManual" method="post">                
                <br>
                <div class="row">

                    <label class="col-sm-1 text-left">
                        <h4>Clave Cliente:</h4>
                    </label>
                    <div class="col-sm-2">  
                        <input class="form-control input-sm" placeholder="" name="ClaCli" id="ClaCli" value="<%=ClaCli%>" required/>
                    </div>
                    <label class="col-sm-1 text-left">
                        <h4>Nombre Cliente</h4>
                    </label>
                    <div class="col-sm-5">
                        <input class="form-control input-sm" placeholder="" name="desCli" id="desCli" value="<%=DesCli%>"  required/>
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-primary btn-block" name="accion" value="buscarCliente" id="btnCli">Buscar</button>
                    </div>
                </div>
                 <div class="row">
                      <label class="col-sm-1 text-left">
                        <h4>Dirección</h4>
                    </label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" placeholder="" name="Direc" id="Direc" value="<%=Direc%>" readonly=""  required/>
                    </div>
                    <div class="col-sm-offset-1">
                        <select id="selectdir" name="selectdir">                                       
                            <option>--Seleccione--</option>
                            <%
                                try{
                                    con.conectar();
                                    
                                ResultSet Dir = con.consulta("SELECT F_Direc FROM tb_uniatn WHERE F_ClaveGen='"+ClaCli+"'");
                                while (Dir.next()) {
                            %>
                            <option value="<%=Dir.getString(1)%>"><%=Dir.getString(1)%></option>
                            <%}
                                con.cierraConexion();
                                }catch(Exception e){
                                    
                                }
                            %>
                        </select>
                    </div>
                 </div>
                 <div class="row">
                      <label class="col-sm-1 text-left">
                        <h4>Paquetería</h4>
                    </label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" placeholder="" name="paq" id="paq" value="<%=Paquet%>" readonly=""  required/>
                    </div>
                    <div class="col-sm-offset-1">
                        <select id="selectpaq" name="selectpaq">                                       
                            <option>--Seleccione--</option>
                            <%
                                //while (Rset_Clave.next()) {
                            %>
                            <option value="<%//=Rset_Clave.getString(1)%>"><%//=Rset_Clave.getString(1)%></option>
                            <%//}%>
                        </select>
                    </div>
                 </div>    
                 <div class="row">
                      <label class="col-sm-1 text-left">
                        <h4>Dirección Paquetería</h4>
                    </label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" placeholder="" name="paqdir" id="paqdir" value="<%=PaqDir%>" readonly=""  required/>
                    </div>
                    <div class="col-sm-offset-1">
                        <select id="selectpaqdir" name="selectpaqdir">                                       
                            <option>--Seleccione--</option>
                            <%
                                //while (Rset_Clave.next()) {
                            %>
                            <option value="<%//=Rset_Clave.getString(1)%>"><%//=Rset_Clave.getString(1)%></option>
                            <%//}%>
                        </select>
                    </div>
                 </div>           
            </form>
        </div>
        <br><br><br>
        <div class="navbar navbar-fixed-bottom navbar-inverse">
            <div class="text-center text-muted">
                GNK Logística || Desarrollo de Aplicaciones 2009 - 2015 <span class="glyphicon glyphicon-registration-mark"></span><br />
                Todos los Derechos Reservados
            </div>
        </div> 
        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="js/jquery.js" type="text/javascript"></script>
        <script src="js/jquery-ui_1.js" type="text/javascript"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/bootstrap3-typeahead.js" type="text/javascript"></script>

        <script>
                            $(document).ready(function () {
                                $("#selectdir").click(function() {
                                $("#selectpaq").empty();                                
                                $("#selectpaq").append($("<option></option>").text("--Seleccione--").val("--Seleccione--"));
                                var Direcc = $("#selectdir").val();
                                var ClaCli = $("#ClaCli").val();
                                
                                if (Direcc != "--Seleccione--") {
                                    var dir = 'jsp/consultasInv.jsp?ban=1&clave=' + ClaCli + '&Direcc=' + Direcc + ''
                                }
                                
                                $.ajax({
                                    url: dir,
                                    type: 'json',
                                    async: false,
                                    success: function(data) {
                                        MostrarDir(data);
                                    },
                                    error: function() {
                                        alert("Ha ocurrido un error");
                                    }

                                });
                                function MostrarDir(data) {

                                    var json = JSON.parse(data);
                                    alert(json);
                                    for (var x = 0; x < json.length; x++) {
                                        var F_Transporte = json[x].F_Transporte;
                                        $("#selectpaq").append($("<option></option>").text(F_Transporte).val(F_Transporte));

                                    }

                                }


                            });
                                
                                
                                $('#selectdir').change(function() {
                                var valor = $('#selectdir').val();
                                $('#Direc').val(valor);
                                });
                                $('#selectpaq').change(function() {
                                var valor = $('#selectpaq').val();
                                $('#paq').val(valor);
                                });
                                $('#selectpaqdir').change(function() {
                                var valor = $('#selectpaqdir').val();
                                $('#paqdir').val(valor);
                                });
                                
                            });
        </script>
        
        
        
        <script>
            function justNumbers(e)
            {
                var keynum = window.event ? window.event.keyCode : e.which;
                if ((keynum === 8) || (keynum === 46))
                    return true;
                return /\d/.test(String.fromCharCode(keynum));
            }

            function validaBuscar() {
                var Unidad = document.getElementById('ClaCli').value;
                if (Unidad === "") {
                    alert('Seleccione el cliente');
                    return false;
                }

                var FechaEnt = document.getElementById('FechaEnt').value;
                if (FechaEnt === "") {
                    alert('Seleccione Fecha de Entrega');
                    return false;
                }
                var clave = document.getElementById('ClaPro').value;
                if (clave === "") {
                    alert('Escriba una Clave');
                    return false;
                }
            }


            function validaSeleccionar() {
                var DesSel = document.getElementById('DesSel').value;
                if (DesSel === "") {
                    alert('Favor de Capturar Toda la información');
                    return false;
                }
                var cantidad = document.getElementById('Cantidad').value;
                if (cantidad === "") {
                    alert('Escriba una cantidad');
                    return false;
                }

            }

            $(function () {
                $("#ClaCli").keyup(function () {
                    var nombre = $("#ClaCli").val();
                    $("#ClaCli").autocomplete({
                        source: "AutoCompleteClientes?cla_pro=" + nombre + "&tipo=1",
                        selectFirst: true, //here
                        minLength: 0,
                        select: function (event, ui) {
                            $("#ClaCli").val(ui.item.cla_pro);
                            $("#desCli").val(ui.item.des_pro);
                            return false;
                        }
                    }).data("ui-autocomplete")._renderItem = function (ul, item) {
                        return $("<li>")
                                .data("ui-autocomplete-item", item)
                                .append("<a>" + item.cla_pro + "</a>")
                                .appendTo(ul);
                    };
                });
            });
            
            
            
            $(function () {
                $("#desCli").keyup(function () {
                    var nombre = $("#desCli").val();
                    $("#desCli").autocomplete({
                        source: "AutoCompleteClientes?cla_pro=" + nombre + "&tipo=2",
                        selectFirst: true, //here
                        minLength: 0,
                        select: function (event, ui) {
                            $("#ClaCli").val(ui.item.cla_pro);
                            $("#desCli").val(ui.item.des_pro);
                            return false;
                        }
                    }).data("ui-autocomplete")._renderItem = function (ul, item) {
                        return $("<li>")
                                .data("ui-autocomplete-item", item)
                                .append("<a>" + item.des_pro + "</a>")
                                .appendTo(ul);
                    };
                });
            });
            function llenarentregas() {

                $.post("AutoCompletePuntos?cla_pro=" + nombre + "&tipo=1", {
                    'idcategory': idc},
                function (data) {
                    var sel = $("#puntosEntrega");
                    sel.empty();
                    for (var i = 0; i < data.length; i++) {
                        sel.append('<option value="' + data[i].id + '">' + data[i].desc + '</option>');
                    }
                }, "json");
            }
        </script>
    </body>

</html>

