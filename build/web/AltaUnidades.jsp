<%-- 
    Document   : index
    Created on : 17/02/2014, 03:34:46 PM
    Author     : Americo
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="conn.*" %>
<!DOCTYPE html>
<%java.text.DateFormat df = new java.text.SimpleDateFormat("yyyyMMddhhmmss"); %>
<%java.text.DateFormat df2 = new java.text.SimpleDateFormat("yyyy-MM-dd"); %>
<%java.text.DateFormat df3 = new java.text.SimpleDateFormat("dd/MM/yyyy"); %>
<%
    /**
     * Cat�logo de proveedores
     */
    HttpSession sesion = request.getSession();
    String usua = "",nomUsu="";
    if (sesion.getAttribute("Usuario") != null) {
        usua = (String) sesion.getAttribute("Usuario");
        nomUsu = (String) sesion.getAttribute("NombreU");
    } else {
        response.sendRedirect("indexGarden.jsp");
    }
    ConectionDB con = new ConectionDB();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Estilos CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="css/cupertino/jquery-ui-1.10.3.custom.css" />
        <!--link href="css/navbar-fixed-top.css" rel="stylesheet"-->
        <link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap.css">
        <!---->
        <title>SIALSS</title><link type="image/x-icon" href="imagenes/favicon.png" rel="icon" />
    </head>
    <body>
        <div class="container">
            <h1>SIALSS</h1>
            <h4>SISTEMA INTEGRAL DE ADMINISTRACI�N Y LOG�STICA PARA SERVICIOS DE SALUD</h4>

            <hr/>
        </div>
        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">ALTA CLIENTE</h3>
                </div>
                <div class="panel-body ">
                    <form class="form-horizontal" role="form" name="formulario1" id="formulario1" method="post" action="Proveedores">
                        <div class="form-group">
                            <div class="panel-body ">
                                <div class="col-sm-6">
                                    <a href="catalogoUnidades.jsp" class="btn btn-block btn-info col-sm-6">Regresar&nbsp;<span class="glyphicon glyphicon-hand-left"></span></a>
                                </div>       
                            </div>                            
                            <div class="form-group">                                
                                <label for="Clave" class="col-sm-1 control-label">Clave*</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" id="Clave" name="Clave" placeholder="Clave" maxlength="5" onKeyPress="return isNumberKey(event, this);return tabular(event, this);" autofocus >
                                </div>
                                <label for="Nombre" class="col-xs-1 control-label">Nombre*</label>
                                <div class="col-xs-3">
                                    <input type="text" class="form-control" id="Nombre" name="Nombre" maxlength="60" placeholder="Nombre" onKeyPress="return tabular(event, this)" />
                                </div>
                                <label for="Telefono" class="col-xs-1 control-label">Tel�fono* </label>
                                <div class="col-xs-2">
                                    <input name="Telefono" type="text" class="form-control" id="Telefono" placeholder="Telefono" onKeyPress="LP_data();
                                            anade(this);
                                            return isNumberKey(event, this);" maxlength="14" /><h6>(XXX) XXX-XXXX</h6></div>
                                <div class="col-lg-1"></div>
                                
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label for="Direccion" class="col-xs-1 control-label">Direcci�n</label>
                                <div class="col-xs-3">
                                    <input type="text" class="form-control" id="Direccion" maxlength="50" name="Direccion" placeholder="Direccion" onKeyPress="return tabular(event, this)" />
                                </div>
                                <label for="NoInt" class="col-xs-1 control-label">No. Int</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" id="noint" name="noint" maxlength="40" placeholder="No. Interior" onKeyPress="return tabular(event, this)" />
                                </div>
                                <label for="NoExt" class="col-xs-1 control-label">No. Ext</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" id="noext" name="noext" maxlength="40" placeholder="No. Exterior" onKeyPress="return tabular(event, this)" />
                                </div>                                
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="form-group">                                
                                <label for="Colonia" class="col-xs-1 control-label">Colonia</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" id="Colonia" name="Colonia" maxlength="40" placeholder="Colonia" onKeyPress="return tabular(event, this)" />
                                </div>
                                <label for="Calle" class="col-xs-1 control-label">Calle</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" id="calle" name="calle" placeholder="Calle" onKeyPress="return tabular(event, this)" />
                                </div>                                
                                <label for="CP" class="col-xs-2 control-label">C.P.</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" id="CP" name="CP" placeholder="CP" maxlength="5" onKeyPress="return isNumberKey(event, this);" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group">                                
                                <label for="Loca" class="col-xs-1 control-label">Estado</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" id="estado" name="estado" placeholder="Estado" />
                                </div>
                                <label for="Muni" class="col-xs-1 control-label">RFC</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" id="RFC" name="RFC" maxlength="15" placeholder="RFC" onKeyPress="return tabular(event, this)" />
                                </div>
                                <label for="Est" class="col-xs-1 control-labell">Nombre Contacto</label>
                                <div class="col-xs-3">
                                    <input type="text" class="form-control" id="Contacto" name="Contacto" placeholder="Contacto" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label for="RFC" class="col-xs-1 control-label">Nombre Paq:</label>
                                <div class="col-xs-3">
                                    <input type="text" class="form-control" id="Transporte" name="Transporte" placeholder="Transporte" onKeyPress="return tabular(event, this)" />
                                </div>      
                                <label for="FAX" class="col-xs-1 control-label">Direcci�n Paq:</label>
                                <div class="col-xs-5">
                                    <input type="text" class="form-control" id="DirPaq" name="DirPaq" placeholder="Direcci�n Paq" onKeyPress="return tabular(event, this)"/>
                                </div>                                
                            </div>
                        </div>
                        <button class="btn btn-block btn-primary" type="submit" name="accion" value="guardarU" onclick="return valida_alta();"> Guardar</button> 

                    </form>
                    <div>
                        <h6>Los campos marcados con * son obligatorios</h6>
                    </div>
                </div>
                
            </div>
        </div>
        <br><br><br>
        <div class="navbar navbar-fixed-bottom navbar-inverse">
            <div class="text-center text-muted">
                GNK Log�stica || Desarrollo de Aplicaciones 2009 - 2015 <span class="glyphicon glyphicon-registration-mark"></span><br />
                Todos los Derechos Reservados
            </div>
        </div>



        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="js/jquery-1.9.1.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery-ui-1.10.3.custom.js"></script>
        <script src="js/jquery.dataTables.js"></script>
        <script src="js/dataTables.bootstrap.js"></script>
        <script>
                            $(document).ready(function() {
                                $('#datosProv').dataTable();
                            });
        </script>
        <script>


            function isNumberKey(evt, obj)
            {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode === 13 || charCode > 31 && (charCode < 48 || charCode > 57)) {
                    if (charCode === 13) {
                        frm = obj.form;
                        for (i = 0; i < frm.elements.length; i++)
                            if (frm.elements[i] === obj)
                            {
                                if (i === frm.elements.length - 1)
                                    i = -1;
                                break
                            }
                        /*ACA ESTA EL CAMBIO*/
                        if (frm.elements[i + 1].disabled === true)
                            tabular(e, frm.elements[i + 1]);
                        else
                            frm.elements[i + 1].focus();
                        return false;
                    }
                    return false;
                }
                return true;
            }


            function valida_alta() {
                /*var Clave = document.formulario1.Clave.value;*/
                var Nombre = document.formulario1.Nombre.value;
                var Telefono = document.formulario1.Telefono.value;
                if (Nombre === "" || Telefono === "") {
                    alert("Tiene campos vac�os, verifique.");
                    return false;
                }
            }
        </script>
        <script language="javascript">
            function justNumbers(e)
            {
                var keynum = window.event ? window.event.keyCode : e.which;
                if ((keynum == 8) || (keynum == 46))
                    return true;
                return /\d/.test(String.fromCharCode(keynum));
            }
            otro = 0;
            function LP_data() {
                var key = window.event.keyCode; //codigo de tecla. 
                if (key < 48 || key > 57) {//si no es numero 
                    window.event.keyCode = 0; //anula la entrada de texto. 
                }
            }
            function anade(esto) {
                if (esto.value === "(55") {
                    if (esto.value.length === 0) {
                        if (esto.value.length === 0) {
                            esto.value += "(";
                        }
                    }
                    if (esto.value.length > otro) {
                        if (esto.value.length === 3) {
                            esto.value += ") ";
                        }
                    }
                    if (esto.value.length > otro) {
                        if (esto.value.length === 9) {
                            esto.value += "-";
                        }
                    }
                    if (esto.value.length < otro) {
                        if (esto.value.length === 4 || esto.value.length === 9) {
                            esto.value = esto.value.substring(0, esto.value.length - 1);
                        }
                    }
                } else {
                    if (esto.value.length === 0) {
                        if (esto.value.length === 0) {
                            esto.value += "(";
                        }
                    }
                    if (esto.value.length > otro) {
                        if (esto.value.length === 4) {
                            esto.value += ") ";
                        }
                    }
                    if (esto.value.length > otro) {
                        if (esto.value.length === 9) {
                            esto.value += "-";
                        }
                    }
                    if (esto.value.length < otro) {
                        if (esto.value.length === 4 || esto.value.length === 9) {
                            esto.value = esto.value.substring(0, esto.value.length - 1);
                        }
                    }
                }
                otro = esto.value.length

            }


            function tabular(e, obj)
            {
                tecla = (document.all) ? e.keyCode : e.which;
                if (tecla != 13)
                    return;
                frm = obj.form;
                for (i = 0; i < frm.elements.length; i++)
                    if (frm.elements[i] == obj)
                    {
                        if (i == frm.elements.length - 1)
                            i = -1;
                        break
                    }
                /*ACA ESTA EL CAMBIO*/
                if (frm.elements[i + 1].disabled == true)
                    tabular(e, frm.elements[i + 1]);
                else
                    frm.elements[i + 1].focus();
                return false;
            }

        </script> 

    </body>
</html>