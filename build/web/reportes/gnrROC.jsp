<%-- 
    Document   : exist
    Created on : 02-jul-2014, 23:24:11
    Author     : wence
--%>

<%@page import="conn.ConectionDB"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    /**
     * PAra descargar las existencias disponibles
     */
    DecimalFormat formatter = new DecimalFormat("#,###,###");
    DecimalFormatSymbols custom = new DecimalFormatSymbols();
    custom.setDecimalSeparator(',');
    formatter.setDecimalFormatSymbols(custom);
    String Claves = "";
    ResultSet rset;

    ConectionDB con = new ConectionDB();
    String fecha_ini="",fecha_fin="",radio="";
    int F_CanCom=0;
     try {
        fecha_ini = request.getParameter("F1");        
        fecha_fin = request.getParameter("F2");    
        radio = request.getParameter("R");
        
    } catch (Exception e) {

    }
    
    /**
     * Para generar el excel
     */
    response.setContentType("application/vnd.ms-excel");
    if(radio.equals("si")){
        response.setHeader("Content-Disposition", "attachment; filename=ReporteOC_"+fecha_ini+"_"+fecha_fin+".xls");
    }else{
        response.setHeader("Content-Disposition", "attachment; filename=ReporteFolio_"+fecha_ini+"_"+fecha_fin+".xls");
    }
    
%>
<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="datosProv">
    <thead>
        <tr>
            <%if(radio.equals("si")){%>
            <td>No. OC</td>
            <td>Nombre Proveedor</td>
            <%}else{%>
            <td>No. Folio</td>
            <td>Nombre Cliente</td>
            <%}%>
            <td>Clave</td>
            <td>Descripci&oacute;n</td>
            <td>Lote</td>
            <td>Caducidad</td>
            <td>Cantidad</td>
        </tr>
    </thead>
    <tbody>
        <%
            try {
                con.conectar();

                Claves = request.getParameter("clave");
                                                   
                if(radio.equals("si")){
                    //rset = con.consulta("SELECT C.F_OrdCom,P.F_NomPro,C.F_ClaPro,M.F_DesPro,L.F_ClaLot,DATE_FORMAT(F_FecCad,'%d/%m/%Y') AS F_FecCad,FORMAT(SUM(C.F_CanCom),0) AS F_CanCom1,SUM(C.F_CanCom) AS F_CanCom FROM tb_compra C INNER JOIN tb_lote L ON C.F_Lote=L.F_FolLot AND C.F_ClaPro=L.F_ClaPro INNER JOIN tb_medica M ON C.F_ClaPro=M.F_ClaPro INNER JOIN tb_proveedor P ON C.F_ProVee=P.F_ClaProve WHERE C.F_FecApl BETWEEN '"+fecha_ini+"' AND '"+fecha_fin+"' GROUP BY L.F_ClaPro,L.F_FolLot,C.F_OrdCom,C.F_ProVee;");
                    rset = con.consulta("SELECT C.F_OrdCom,P.F_NomPro,C.F_ClaPro,M.F_DesPro,FORMAT(SUM(C.F_CanCom),0) AS F_CanCom1,SUM(C.F_CanCom) AS F_CanCom,C.F_Lote FROM tb_compra C INNER JOIN tb_medica M ON C.F_ClaPro=M.F_ClaPro INNER JOIN tb_proveedor P ON C.F_ProVee=P.F_ClaProve WHERE C.F_FecApl BETWEEN '"+fecha_ini+"' AND '"+fecha_fin+"' GROUP BY C.F_ClaPro,C.F_OrdCom,C.F_ProVee,C.F_Lote;");
                    while (rset.next()) {
                    F_CanCom = F_CanCom + rset.getInt(6);

                        ResultSet RsetDatosL = con.consulta("SELECT F_ClaLot,DATE_FORMAT(F_FecCad,'%d/%m/%Y') AS F_FecCad FROM tb_lote WHERE F_FolLot='"+rset.getString(7)+"' AND F_ClaPro='"+rset.getString(3)+"';");
                        if(RsetDatosL.next()){

                            %>
                        <tr>
                            <td><%=rset.getString(1)%></td>
                            <td><%=rset.getString(2)%></td>
                            <td><%=rset.getString(3)%></td>
                            <td><%=rset.getString(4)%></td>
                            <td style='mso-number-format:"@"'><%=RsetDatosL.getString(1)%></td>
                            <td><%=RsetDatosL.getString(2)%></td>
                            <td><%=rset.getString(5)%></td>
                        </tr>
                        <%

                        }
                    }
                }else{
                    rset = con.consulta("SELECT F.F_ClaDoc,U.F_NomCli,L.F_ClaPro,M.F_DesPro,L.F_ClaLot,DATE_FORMAT(L.F_FecCad,'%d/%m/%Y') AS F_FecCad,FORMAT(SUM(F_CantSur),0) AS F_CantSur1,SUM(F_CantSur) AS F_CantSur FROM tb_factura F INNER JOIN tb_lote L ON F.F_Lote=L.F_FolLot AND F.F_ClaPro=L.F_ClaPro AND F.F_Ubicacion=L.F_Ubica INNER JOIN tb_medica M ON L.F_ClaPro=M.F_ClaPro INNER JOIN tb_uniatn U ON F.F_ClaCli=U.F_ClaCli WHERE F_FecEnt BETWEEN '"+fecha_ini+"' AND '"+fecha_fin+"' AND F_StsFact='A' GROUP BY F.F_ClaDoc,L.F_ClaPro,L.F_ClaLot,L.F_FecCad;");
                                                                    


                while (rset.next()) {
                    F_CanCom = F_CanCom + rset.getInt(8);
                /**
                 * Para consultar la existencia de las claves
                 */
                
        %>
        <tr>
            <td><%=rset.getString(1)%></td>
            <td><%=rset.getString(2)%></td>
            <td><%=rset.getString(3)%></td>
            <td><%=rset.getString(4)%></td>
            <td style='mso-number-format:"@"'><%=rset.getString(5)%></td>
            <td><%=rset.getString(6)%></td>
            <td><%=rset.getString(7)%></td>            
        </tr>
        <%
                }
                }
                con.cierraConexion();
            } catch (Exception e) {
                out.println(e);
            }

        %>
    </tbody>

</table>
