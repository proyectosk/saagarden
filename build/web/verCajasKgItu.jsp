<%-- 
    Document   : index
    Created on : 17/02/2014, 03:34:46 PM
    Author     : Americo
--%>

<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="conn.*" %>
<!DOCTYPE html>
<%
    /**
     * Listado de concentrados globales para su reimpresión
     */
    HttpSession sesion = request.getSession();
    String usua = "", tipo = "";
    if (sesion.getAttribute("Usuario") != null) {
        usua = (String) sesion.getAttribute("Usuario");
    } else {
        response.sendRedirect("indexGarden.jsp");
    }
    ConectionDB con = new ConectionDB();
    String Hora="",Minuto="",Segundo="";
    String fol_gnkl = "", fol_remi = "", orden_compra = "", fecha = "";
    try {
        if (request.getParameter("accion").equals("buscar")) {
            fol_gnkl = request.getParameter("fol_gnkl");
            fol_remi = request.getParameter("fol_remi");
            orden_compra = request.getParameter("orden_compra");
            fecha = request.getParameter("fecha");
        }
    } catch (Exception e) {

    }
    if (fol_gnkl == null) {
        fol_gnkl = "";
        fol_remi = "";
        orden_compra = "";
        fecha = "";
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Estilos CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="css/cupertino/jquery-ui-1.10.3.custom.css" />
        <!--link href="css/navbar-fixed-top.css" rel="stylesheet"-->
        <link href="css/datepicker3.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap.css">
        <!---->
        <title>SIALSS</title>
    </head>
    <body>
        <div class="container">
            <h1>SIALSS</h1>
            <h4>Módulo - Sistema de Administración de Almacenes (SAA)</h4>

            <%@include file="jspf/menuCliente.jspf"%>

            <div>
                <h3>Cajas Kg</h3>
                <h4>Seleccione*</h4>

                <br />
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <div class="panel panel-primary">
                        <div class="panel-body table-responsive">
                            <div style="width:100%; height:700px; overflow:auto;">
                        <table class="table table-bordered table-striped" id="datosCompras">
                            <thead>
                                <tr>                                    
                                    <td>No. Folio Validado</td>
                                    <td>Punto de entrega</td>
                                    <td>Sts Factura</td>
                                    <td>Fecha de entrega</td>
                                    <td>Cajas</td>
                                    <td>Peso Kg</td>
                                </tr>
                            </thead>
                            <tbody>
                                <%                                    try {
                                        con.conectar();
                                        try {
                                            /**
                                             * Listado de concentrados
                                             */
                                            //
                                            ResultSet rset = con.consulta("SELECT f.F_ClaDoc, u.F_NomCli, DATE_FORMAT(f.F_FecEnt, '%d/%m/%Y') AS FecEnt, f.F_StsFact,cj.F_Cajas,cj.F_Kg FROM tb_factura f INNER JOIN tb_uniatn u ON f.F_ClaCli = u.F_ClaCli INNER JOIN tb_cajaskg cj on f.F_ClaDoc=cj.F_Cladoc GROUP BY f.F_ClaDoc,f.F_StsFact;");
                                            while (rset.next()) {
                                                
                                                                                              
                                                                                                
                                                
                                                
                                %>
                                <tr>
                                    <td class="Documento"><%=rset.getString(1)%></td>
                                    <td><%=rset.getString(2)%></td>
                                    <td><%=rset.getString(4)%></td>                                
                                    <td><%=rset.getString(3)%></td>                                       
                                    <td class="cajas"><%=rset.getString(5)%></td>                                       
                                    <td class="kilo"><%=rset.getString(6)%></td>                                       
                                    
                                </tr>
                                <%
                                          
                                            }
                                        } catch (Exception e) {

                                        }
                                        con.cierraConexion();
                                    } catch (Exception e) {

                                    }
                                %>
                            </tbody>
                        </table>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div id="ModiOc" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modificar Cajas y/o Kg</h4>
                    </div>

                    <form name="formEditOC" action="PedidoManual" method="Post">

                        <div class="modal-body">
                            <input class="form-control hidden" name="idMod" id="idMod" type="text" value="" readonly />
                            <input class="form-control hidden" name="F_NoCompra" id="F_NoCompra" type="" value="" readonly />

                            <div class="row">
                                <h4 class="col-sm-2">Folio:</h4>
                                <div class="col-sm-4">
                                    <input class="form-control" name="Folio" id="Folio" type="text" value="" readonly required/>
                                </div>                                
                            </div>
                            <div class="row">
                                <h4 class="col-sm-2">Cajas:</h4>
                                <div class="col-sm-4">
                                    <input class="form-control" name="Cajas" id="Cajas" type="text" value="" readonly required/>
                                </div>
                                <h4 class="col-sm-2">Por:</h4>
                                <div class="col-sm-4">
                                    <input class="form-control" name="Cajas2" id="Cajas2" type="text" value=""/>
                                </div>
                            </div>
                            <div class="row">
                                <h4 class="col-sm-2">Kg:</h4>
                                <div class="col-sm-4">
                                    <input class="form-control" name="KG" id="KG" type="text" value="" readonly required/>
                                </div>
                                <h4 class="col-sm-2">Por:</h4>
                                <div class="col-sm-4">
                                    <input class="form-control" name="KG2" id="KG2" type="text" value=""/>
                                </div>
                            </div>                            
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-default" name="accion" value="CambiOCKG">Guardar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        </div>

                    </form>

                </div>

            </div>
        </div>                    
        <br><br><br>
        <div class="navbar navbar-fixed-bottom navbar-inverse">
            <div class="text-center text-muted">
                GNK Logística || Desarrollo de Aplicaciones 2009 - 2016 <span class="glyphicon glyphicon-registration-mark"></span><br />
                Todos los Derechos Reservados
            </div>
        </div>

        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="js/jquery-1.9.1.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery-ui-1.10.3.custom.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/jquery.dataTables.js"></script>
        <script src="js/dataTables.bootstrap.js"></script>
        <script  type="text/javascript">
            $(document).ready(function () {
                $('#datosCompras').dataTable();
            });
        
        $(".rowButton").click(function () {
            var $row = $(this).closest("tr");    // Find the row
            var $folio = $row.find("td.Documento").text(); // Find the text             
            var $cajas = $row.find("td.cajas").text(); // Find the text
            var $kg = $row.find("td.kilo").text(); // Find the text
            

            $("#Folio").val($folio);
            $("#Cajas").val($cajas);
            $("#KG").val($kg);           

        });
            
           
        </script>
        <script>
            $(function () {
                $("#fecha").datepicker();
                $("#fecha").datepicker('option', {dateFormat: 'dd/mm/yy'});
            });
            
        </script>
    </body>
</html>