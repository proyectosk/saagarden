<%-- 
    Document   : SalidaAjuste
    Created on : 14/07/2015, 03:49:42 PM
    Author     : Mario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%

    HttpSession sesion = request.getSession();
    String info = null;

    String usua = "", tipo = "";
    if (sesion.getAttribute("nombre") != null) {
        usua = (String) sesion.getAttribute("nombre");
        tipo = (String) sesion.getAttribute("Tipo");
    } else {
        response.sendRedirect("index.jsp");
    }
    /*String usua = "";
     if (sesion.getAttribute("nombre") != null) {
     usua = (String) sesion.getAttribute("nombre");
     System.out.println("login ok");
     } else {
     System.out.println("sin nombre");
     request.setAttribute("mensaje", "la sesion a terminado.  Por favor ingrese de nuevo sus credenciales.");
     RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
     rd.forward(request, response);
     }*/

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Estilos CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="css/cupertino/jquery-ui-1.10.3.custom.css" />
        <link href="css/navbar-fixed-top.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap.css">
        <!---->
        <title>SIALSS</title>
    </head>
    <body>
        <div class="container">

            <h1>SIALSS</h1>
            <h4>Módulo - Sistema de Administración de Almacenes (SAA)</h4>
            <div style="width: 90%; margin: auto;">
                <div>
                    <h3>Salida por Ajuste</h3>
                </div>



                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Criterios de Búsqueda
                    </div>
                    <div class="panel-body">

                        <%  info = (String) sesion.getAttribute("mensaje");
                            //out.print(info);
                            if (!(info == null || info.equals(""))) {
                        %>


                        <div class="alert alert-success" role="alert">
                            <span class="glyphicon glyphicon-check" aria-hidden="true"></span>
                            <span class="sr-only">Hecho:</span>
                            <%=info%>
                        </div>
                        <%
                                sesion.removeAttribute("mensaje");
                            }
                        %>

                        <%  info = (String) sesion.getAttribute("error");
                            //out.print(info);
                            if (!(info == null || info.equals(""))) {
                        %>


                        <div class="alert alert-error" role="alert">
                            <span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                            <%=info%>
                        </div>
                        <%
                                sesion.removeAttribute("error");
                            }
                        %>

                        <%  info = (String) sesion.getAttribute("advertencia");
                            //out.print(info);
                            if (!(info == null || info.equals(""))) {
                        %>


                        <div class="alert alert-warning" role="alert">
                            <span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span>
                            <span class="sr-only">Advertencia:</span>
                            <%=info%>
                        </div>
                        <%
                                sesion.removeAttribute("advertencia");
                            }
                        %>


                        <form name="formReajuste" action="ReajusteSalida" method="Post">
                            <div class="row">
                                <h4 class="col-sm-2">Clave</h4>
                                <div class="col-sm-4">
                                    <input class="form-control" name="Clave" id="Clave" type="text" value="${Clave}"/>
                                </div>
                                <div class="col-sm-2">
                                    <button class="btn btn-primary" name="accion" value="buscarClave">Por Clave</button>
                                </div>
                            </div>
                            <div class="row">
                                <h4 class="col-sm-2">Descripci&oacute;n</h4>
                                <div class="col-sm-4">
                                    <input class="form-control typeahead" name="Desc" id="Desc" type="text" value="${desc}" data-provide="typeahead"/>
                                </div>
                                <div class="col-sm-2">
                                    <button class="btn btn-primary" name="accion" value="buscarDesc">Por Descripci&oacute;n</button>
                                </div>
                            </div>
                            <div class="row">
                                <h4 class="col-sm-2">Lote</h4>
                                <div class="col-sm-2">
                                    <select class="form-control" name="Lote" id="Lote" onchange="cambiaLoteCadu(this);">
                                        <option value="">-Lote-</option>
                                        <c:forEach items="${lotes}" var="producto">
                                            <option>${producto.lote}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <h4 class="col-sm-2">Caducidad</h4>
                                <div class="col-sm-2">
                                    <select class="form-control" name="Cadu" id="Cadu">
                                        <option value="">-Caducidad-</option>
                                        <c:forEach items="${caducidades}" var="producto">
                                            <option>${producto.caducidad}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <button class="btn btn-primary" name="accion" value="filtrar">Buscar</button>
                                </div>

                            </div>

                        </form>
                        <hr/>
                        <form name="formReajusteEnv" action="ReajusteSalida" method="Post" >
                            <h3>Clave: &nbsp &nbsp${prod.clave}</h3>
                            <input class="form-control hidden" name="hClave" id="hClave" type="text" value="${prod.clave}"/>
                            <h4>Lote: &nbsp &nbsp${prod.lote}</h4>
                            <input class="form-control hidden" name="hLote" id="hLote" type="text" value="${prod.lote}"/>
                            <h4>Caducidad: &nbsp &nbsp${prod.caducidad}</h4> 
                            <input class="form-control hidden" name="hCadu" id="hCadu" type="text" value="${prod.caducidad}"/>
                            <h4>${prod.descripcion}</h4> 
                            <h4>Existencia Actual: &nbsp &nbsp${prod.existencia}</h4>
                            <input class="form-control hidden" name="hExis" id="hExis" type="text" value="${prod.existencia}"/>

                            <div class="row">
                                <div class="col-lg-2 control-label">
                                    <h4>Nueva Existencia</h4>

                                </div>
                                <div class="col-lg-4">
                                    <input class="form-control" id="Cantidad" name="Cantidad"  type="number" min="0" data-fv-integer-message="The value is not an integer"
                                           required/>
                                    <br>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-lg-2 control-label">
                                    <h4>Justificaci&oacute;n:</h4>

                                </div>
                                <div class="col-lg-4">
                                    
                                    
                                    <textarea class="form-control" rows="3" id="justi" name="justi" required></textarea>
                                    <br>
                                </div>
                                <div class="col-sm-2">
                                    <button class="btn btn-primary" id="btnEnviar" name="accion" value="Aplicar">Aplicar</button>
                                </div>


                            </div>
                            <br />
                        </form>


                    </div>
                </div>
                <br><br><br>
                <div class="navbar navbar-fixed-bottom navbar-inverse">
                    <div class="text-center text-muted">
                        GNK Logística || Desarrollo de Aplicaciones 2009 - 2015 <span class="glyphicon glyphicon-registration-mark"></span><br />
                        Todos los Derechos Reservados
                    </div>
                </div>
            </div>
        </div>
    </body> 
    <STYLE TYPE="text/css" media="all">
        .ui-autocomplete { 
            position: absolute; 
            cursor: default; 
            height: 200px; 
            overflow-y: scroll; 
            overflow-x: hidden;}
    </STYLE>
    <script src="http://code.jquery.com/jquery-1.7.js"
    type="text/javascript"></script>
    <script
        src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"
    type="text/javascript"></script>
    <link
        href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css"
        rel="stylesheet" type="text/css" />

    <script src="js/bootstrap3-typeahead.js" type="text/javascript"></script>

    <script type="text/javascript">

                                        //Obtener la caducidad cuando segun el lote
                                        $('#Lote').on('change', function () {
                                            var lote = $('#Lote').find(":selected").text();
                                            var clave = $('#Clave').val();

                                            var sel = $("#Cadu");
                                            sel.empty();

                                            $.ajax({
                                                url: "AutoCompleteDesc",
                                                dataType: "json",
                                                data: {lote: lote, clave: clave},
                                                success: function (data) {
                                                    console.log(data);
                                                    for (var i = 0; i < data.length; i++) {
                                                        sel.append('<option value="' + data[i] + '">' + data[i] + '</option>');
                                                    }
                                                },
                                                error: function (jqXHR, textStatus, errorThrown) {
                                                    console.log(textStatus);
                                                }
                                            });

                                        });



                                        $(document).ready(function () {
                                            var bla = $('#hExis').val();
                                            if (bla === "") {
                                                $('#btnEnviar').prop('disabled', true);
                                            } else {
                                                $('#btnEnviar').prop('disabled', false);
                                            }


                                            $("#Desc").typeahead({
                                                //width: 300,
                                                //max: 10,
                                                //delay: 100,
                                                //minLength: 1,
                                                //autoFocus: true,
                                                //cacheLength: 1,
                                                //scroll: true,
                                                //highlight: false,
                                                //limit: 10,
                                                source: function (request, response) {

                                                    $.ajax({
                                                        url: "AutoCompleteDesc",
                                                        dataType: "json",
                                                        data: request,
                                                        success: function (data, textStatus, jqXHR) {
                                                            console.log(data);
                                                            var items = data;
                                                            response(items);
                                                        },
                                                        error: function (jqXHR, textStatus, errorThrown) {
                                                            console.log(textStatus);
                                                        }
                                                    });
                                                }

                                            });
                                        });
    </script>
</html>
