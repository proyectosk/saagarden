<%-- 
    Document   : SalidaAjuste
    Created on : 14/07/2015, 03:49:42 PM
    Author     : Mario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%

    HttpSession sesion = request.getSession();
    String info = null;

    String usua = "", tipo = "";
    if (sesion.getAttribute("nombre") != null) {
        usua = (String) sesion.getAttribute("nombre");
        tipo = (String) sesion.getAttribute("Tipo");
    } else {
        response.sendRedirect("index.jsp");
    }
    /*String usua = "";
     if (sesion.getAttribute("nombre") != null) {
     usua = (String) sesion.getAttribute("nombre");
     System.out.println("login ok");
     } else {
     System.out.println("sin nombre");
     request.setAttribute("mensaje", "la sesion a terminado.  Por favor ingrese de nuevo sus credenciales.");
     RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
     rd.forward(request, response);
     }*/

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Estilos CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="css/cupertino/jquery-ui-1.10.3.custom.css" />
        <link href="css/navbar-fixed-top.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap.css">
        <!---->
        <title>SIALSS</title>
    </head>
    <body>
        <div class="container">

            <h1>SIALSS</h1>
            <h4>Módulo - Sistema de Administración de Almacenes (SAA)</h4>
            <div style="width: 90%; margin: auto;">
                <div>
                    <h3>Modificaci&oacute;n de Órdenes de Reposición de Inventario</h3>
                </div>



                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Criterios de Búsqueda
                    </div>
                    <div class="panel-body">

                        <%  info = (String) sesion.getAttribute("mensaje");
                            //out.print(info);
                            if (!(info == null || info.equals(""))) {
                        %>


                        <div class="alert alert-success" role="alert">
                            <span class="glyphicon glyphicon-check" aria-hidden="true"></span>
                            <span class="sr-only">Hecho:</span>
                            <%=info%>
                        </div>
                        <%
                                sesion.removeAttribute("mensaje");
                            }
                        %>

                        <%  info = (String) sesion.getAttribute("error");
                            //out.print(info);
                            if (!(info == null || info.equals(""))) {
                        %>


                        <div class="alert alert-error" role="alert">
                            <span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                            <%=info%>
                        </div>
                        <%
                                sesion.removeAttribute("error");
                            }
                        %>

                        <%  info = (String) sesion.getAttribute("advertencia");
                            //out.print(info);
                            if (!(info == null || info.equals(""))) {
                        %>


                        <div class="alert alert-warning" role="alert">
                            <span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span>
                            <span class="sr-only">Advertencia:</span>
                            <%=info%>
                        </div>
                        <%
                                sesion.removeAttribute("advertencia");
                            }
                        %>


                        <form name="formModOc" action="ModificacionOC" method="Post">
                            <div class="row">
                                <h4 class="col-sm-2">Ingrese ORI:</h4>
                                <div class="col-sm-2">
                                    <input class="form-control" name="orden" id="orden" type="text" value="${orden}" required=""/>
                                </div>
                                <div class="col-sm-2">
                                    <button class="btn btn-primary" name="accion" value="buscarFolios">Buscar ORI</button>
                                </div>
                                <h4 class="col-sm-2">Folio Remi.:</h4>
                                <div class="col-sm-2">
                                    <select class="form-control" name="folio" id="folio">
                                        <option value="">-Folio-</option>
                                        <c:forEach items="${folios}" var="folio">
                                            <option>${folio}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <button class="btn btn-primary" name="accion" value="filtrar">Buscar Folio</button>
                                </div>
                            </div>

                            <div class="row">
                                <h4 class="col-sm-2">ORI:</h4>
                                <div class="col-sm-2">
                                    <input class="form-control" name="ordenC" id="ordenC" type="text" value="${oc.oc}" readonly/>
                                </div>
                                <h4 class="col-sm-2">Folio:</h4>
                                <div class="col-sm-2">
                                    <input class="form-control" name="FolioRem" id="FolioRem" type="text" value="${oc.folioRem}" readonly/>
                                </div>


                            </div>
                            <div class="row">
                                <h4 class="col-sm-2">Proveedor:</h4>
                                <div class="col-sm-2">
                                    <input class="form-control" name="provee" id="provee" type="text" value="${oc.proveedor}" readonly/>
                                </div>
                                <h4 class="col-sm-2">Fecha:</h4>
                                <div class="col-sm-2">
                                    <input class="form-control" name="fecha" id="fecha" type="text" value="${oc.fecha}" readonly/>
                                </div>
                                <h4 class="col-sm-2">Total:</h4>
                                <div class="col-sm-2">
                                    <input class="form-control" name="total" id="total" type="text" value="${oc.total}" readonly/>
                                </div>

                            </div>

                        </form>
                        <hr/>
                        <table id="tablaDetalle" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="display:none;">ID"</th>
                                    <th>Clave</th>
                                    <th>Lote</th>
                                    <th>Caducidad</th>
                                    <th>Marca</th>
                                    <th>Cantidad</th>
                                    <th>Acciones</th>

                                </tr>
                            </thead>
                            <tbody>
                             
                                
                                <c:forEach items="${productos}" var="producto">
                                    <tr>
                                        <td style="display:none;" class="id"><c:out value="${producto.id}" /></td>
                                        <td class="clave"><c:out value="${producto.clave}" /></td>
                                        <td class="lote"><c:out value="${producto.lote}" /></td>
                                        <td class="caducidad"><c:out value="${producto.caducidad}" /></td>
                                        <td class="marca"><c:out value="${producto.marca}" /></td>
                                        <td class="cantidad"><c:out value="${producto.existencia}" /></td>
                                        <td>
                                            <button type="button" class="rowButton" data-toggle="modal" data-target="#ModiOc">Editar</button>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th style="display:none;">ID"</th>
                                    <th>Clave</th>
                                    <th>Lote</th>
                                    <th>Caducidad</th>
                                    <th>Marca</th>
                                    <th>Cantidad</th>
                                    <th>Acciones</th>

                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <br><br><br>


                <div class="navbar navbar-fixed-bottom navbar-inverse">
                    <div class="text-center text-muted">
                        GNK Logística || Desarrollo de Aplicaciones 2009 - 2015 <span class="glyphicon glyphicon-registration-mark"></span><br />
                        Todos los Derechos Reservados
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div id="ModiOc" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modificar Cantidad</h4>
                    </div>

                    <form name="formEditOC" action="ModificacionOC" method="Post">

                        <div class="modal-body">
                            <input class="form-control hidden" name="idMod" id="idMod" type="text" value="" readonly />

                            <div class="row">
                                <h4 class="col-sm-3">Clave:</h4>
                                <div class="col-sm-4">
                                    <input class="form-control" name="claveMod" id="claveMod" type="text" value="" readonly required/>
                                </div>
                            </div>
                            <div class="row">
                                <h4 class="col-sm-3">Lote:</h4>
                                <div class="col-sm-4">
                                    <input class="form-control" name="loteMod" id="loteMod" type="text" value="" readonly required/>
                                </div>
                            </div>
                            <div class="row">
                                <h4 class="col-sm-3">Caducidad:</h4>
                                <div class="col-sm-4">
                                    <input class="form-control" name="caduMod" id="caduMod" type="text" value="" readonly required/>
                                </div>
                            </div>
                            <div class="row">
                                <h4 class="col-sm-3">Cantidad</h4>
                                <div class="col-sm-4">
                                    <input class="form-control" name="cantMod" id="cantMod" type="number" value="" required/>
                                    <input class="form-control hidden" name="exisOri" id="exisOri" type="text" value="" readonly required/>
                                </div>
                            </div>
                            <div class="row">
                                <h4 class="col-sm-3">Justificaci&oacute;n</h4>
                                <div class="col-sm-4">
                                    <input class="form-control" name="justi" id="justi" type="text" value="" required/>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-default" name="accion" value="editOc">Guardar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        </div>

                    </form>

                </div>

            </div>
        </div>
    </body> 
    <STYLE TYPE="text/css" media="all">
        .ui-autocomplete { 
            position: absolute; 
            cursor: default; 
            height: 200px; 
            overflow-y: scroll; 
            overflow-x: hidden;}
    </STYLE>
    <script src="http://code.jquery.com/jquery-1.7.js"
    type="text/javascript"></script>
    <script
        src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"
    type="text/javascript"></script>
    <link
        href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css"
        rel="stylesheet" type="text/css" />

    <script src="js/bootstrap3-typeahead.js" type="text/javascript"></script>
    <script src="js/bootstrap.js" type="text/javascript"></script>

    <script type="text/javascript">

        //Obtener la caducidad cuando segun el lote
        $(".rowButton").click(function () {


            var $row = $(this).closest("tr");    // Find the row
            var $clave = $row.find("td.clave").text(); // Find the text             var $lote = $row.find("td.lote").text(); // Find the text
            var $cadu = $row.find("td.caducidad").text(); // Find the text
            var $cant = $row.find("td.cantidad").text(); // Find the text
            var $id = $row.find("td.id").text(); // Find the text
            var $lote = $row.find("td.lote").text(); // Find the text

            $("#claveMod").val($clave);
            $("#loteMod").val($lote);
            $("#caduMod").val($cadu);
            $("#cantMod").val($cant);
            $("#exisOri").val($cant);
            $("#idMod").val($id);

            //$('#ModiOc').modal('show')  
        });

        $('#Lote').on('change', function () {
            var lote = $('#Lote').find(":selected").text();
            var clave = $('#Clave').val();

            var sel = $("#Cadu");
            sel.empty();

            $.ajax({
                url: "AutoCompleteDesc",
                dataType: "json",
                data: {lote: lote, clave: clave},
                success: function (data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        sel.append('<option value="' + data[i] + '">' + data[i] + '</option>');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });

        });



        $(document).ready(function () {
            var bla = $('#hExis').val();
            if (bla === "") {
                $('#btnEnviar').prop('disabled', true);
            } else {
                $('#btnEnviar').prop('disabled', false);
            }

        });
    </script>
</html>
