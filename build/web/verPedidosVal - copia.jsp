<%-- 
    Document   : index
    Created on : 17/02/2014, 03:34:46 PM
    Author     : Americo
--%>

<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="conn.*" %>
<!DOCTYPE html>
<%
    /**
     * Listado de concentrados globales para su reimpresión
     */
    HttpSession sesion = request.getSession();
    String usua = "", tipo = "";
    if (sesion.getAttribute("nombre") != null) {
        usua = (String) sesion.getAttribute("nombre");
        tipo = (String) sesion.getAttribute("Tipo");
    } else {
        response.sendRedirect("index.jsp");
    }
    ConectionDB con = new ConectionDB();

    String fol_gnkl = "", fol_remi = "", orden_compra = "", fecha = "";
    try {
        if (request.getParameter("accion").equals("buscar")) {
            fol_gnkl = request.getParameter("fol_gnkl");
            fol_remi = request.getParameter("fol_remi");
            orden_compra = request.getParameter("orden_compra");
            fecha = request.getParameter("fecha");
        }
    } catch (Exception e) {

    }
    if (fol_gnkl == null) {
        fol_gnkl = "";
        fol_remi = "";
        orden_compra = "";
        fecha = "";
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Estilos CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="css/cupertino/jquery-ui-1.10.3.custom.css" />
        <!--link href="css/navbar-fixed-top.css" rel="stylesheet"-->
        <link href="css/datepicker3.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap.css">
        <!---->
        <title>SIALSS</title>
    </head>
    <body>
        <div class="container">
            <h1>SIALSS</h1>
            <h4>Módulo - Sistema de Administración de Almacenes (SAA)</h4>

            <%@include file="jspf/menuPrincipal.jspf"%>

            <div>
                <h3>Revisión de Concentrados por Proveedor</h3>
                <h4>Seleccione</h4>

                <br />
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <table class="table table-bordered table-striped" id="datosCompras">
                            <thead>
                                <tr>
                                    <td>No.Prepedido</td>
                                    <td>No. Folio Validado</td>
                                    <td>Punto de entrega</td>
                                    <td>Sts</td>
                                    <td>Fecha de entrega</td>                                    
                                    <td>Imprimir</td>
                                    <td>Paquetería</td>
                                    <td>Sts Entrega</td>
                                    <td>No. Guía</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <%                                    try {
                                        con.conectar();
                                        try {
                                            /**
                                             * Listado de concentrados
                                             */
                                            //
                                            String F_FolPre="",F_Paqueteria="",F_StsEnvio="",F_Guia="";
                                            ResultSet rset = con.consulta("SELECT f.F_ClaDoc, u.F_NomCli, DATE_FORMAT(f.F_FecEnt, '%d/%m/%Y') AS FecEnt, (f.F_CantSur + 0) AS F_Cant, f.F_IdFact,f.F_StsFact,fv.F_FolPre,u.F_Transporte,fv.F_StsEnvio,fv.F_Guia FROM tb_factura f INNER JOIN tb_uniatn u ON f.F_ClaCli = u.F_ClaCli LEFT JOIN tb_foliosval fv on f.F_ClaDoc=fv.F_FolVal GROUP BY f.F_ClaDoc;");
                                            while (rset.next()) {
                                                F_FolPre = rset.getString("F_FolPre");
                                                F_Paqueteria = rset.getString("F_Transporte");
                                                F_StsEnvio = rset.getString("F_StsEnvio");
                                                F_Guia = rset.getString("F_Guia");
                                                if (F_FolPre == null){F_FolPre = "";}
                                                if(F_StsEnvio == null){F_StsEnvio="";}
                                                if(F_Guia == null){F_Guia="";}
                                            
                                %>
                                <tr>
                                    <td><%=F_FolPre%></td>
                                    <td><%=rset.getString("F_ClaDoc")%></td>
                                    <td><%=rset.getString("F_NomCli")%></td>
                                    <td><%=rset.getString("F_StsFact")%></td>                                
                                    <td><%=rset.getString("FecEnt")%></td>                                       
                                    <td>
                                        <a href="reportes/ReportVal.jsp?Documento=<%=rset.getString("F_ClaDoc")%>" target="_black" class="btn btn-block btn-primary">Imprimir</a>
                                            
                                    </td>   
                                    <td><%=rset.getString("F_Transporte")%></td>                                    
                                    <td><%=F_StsEnvio%></td>
                                    <%if((F_Paqueteria.equals("GNK"))||(F_Paqueteria.equals("CLIENTE RECOGE"))){%>
                                    <td>N/A</td>
                                    <%}else{%>
                                    <td><%=F_Guia%></td>
                                    <%}%>
                                    <td><button type="button" class="btn btn-warning btn-block" data-toggle="modal" data-target="#modalCambio<%=rset.getString(1)%>" id="btnRecalendarizar" ><span class="glyphicon glyphicon-pencil"></span></button></td>
                                </tr>
                                <%
                                            }
                                        } catch (Exception e) {

                                        }
                                        con.cierraConexion();
                                    } catch (Exception e) {

                                    }
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
        <%
        try{
            con.conectar();
            ResultSet Datos=null;
            Datos = con.consulta("SELECT f.F_ClaDoc, u.F_NomCli, DATE_FORMAT(f.F_FecEnt, '%d/%m/%Y') AS FecEnt, (f.F_CantSur + 0) AS F_Cant, f.F_IdFact,f.F_StsFact,fv.F_FolPre,u.F_Transporte,fv.F_StsEnvio,fv.F_Guia FROM tb_factura f INNER JOIN tb_uniatn u ON f.F_ClaCli = u.F_ClaCli LEFT JOIN tb_foliosval fv on f.F_ClaDoc=fv.F_FolVal GROUP BY f.F_ClaDoc;");
            while(Datos.next()){
        %>                    
        <div class="modal fade" id="modalCambio<%=Datos.getString(1)%>" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form>
                        <div class="modal-header">
                            <div class="row">
                                <h4 class="col-sm-12">Actualizar Guía o Sts Entrega</h4>
                            </div>
                        </div>
                        <div class="modal-header">
                            <div class="row">
                                <label class="control-label col-sm-2" for="fecha">No. Folio</label>
                                <div class=" col-sm-4">
                                    <input type="text" class="form-control" required name="" id="ModalSecu" readonly="" value="<%=Datos.getString(1)%>" />
                                </div>                                                        
                            </div>
                        </div>
                        <div class="modal-body">
                            <h4 class="modal-title" id="myModalLabel">Sts Entrega:</h4>
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" required name="" id="ModalOr" readonly="" value="<%=Datos.getString(9)%>" />
                                </div>
                                <label class="control-label col-sm-1" for="fecha">Por</label>
                                <div class="col-sm-4">
                                   <input type="text" class="form-control" required name="" id="ModalOrigen2" />
                                </div>
                            </div>
                            <h4 class="modal-title" id="myModalLabel">No. Guía:</h4>
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" required name="" id="ModalCosto" readonly="" value="<%=Datos.getString(10)%>" />
                                </div>
                                <label class="control-label col-sm-1" for="fecha">Por</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" required name="" id="ModalCosto2" />
                                </div>
                            </div>
                            <div style="display: none;" class="text-center" id="Loader">
                                <img src="imagenes/ajax-loader-1.gif" height="150" />
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" onclick="return confirmaModal();" name="accion" value="CambiOrCost">Actualizar</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <%
            }
            con.cierraConexion();
        }catch(Exception e){}
        %>
        <!-- Modal -->                         
        <br><br><br>
        <div class="navbar navbar-fixed-bottom navbar-inverse">
            <div class="text-center text-muted">
                GNK Logística || Desarrollo de Aplicaciones 2009 - 2015 <span class="glyphicon glyphicon-registration-mark"></span><br />
                Todos los Derechos Reservados
            </div>
        </div>

        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="js/jquery-1.9.1.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery-ui-1.10.3.custom.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/jquery.dataTables.js"></script>
        <script src="js/dataTables.bootstrap.js"></script>
        
        <script>
            function confirmaModal() {
                var valida = confirm('Seguro que desea actualizar los Datos?');
                var Origen = $('#ModalOrigen2').val();
                var Costo = $('#ModalCosto2').val();
                //alert("Or: "+Origen+" Cos: "+Costo);
                if ((Origen === "") && (Costo === "")) {
                    alert('Ingrese Datos');
                    return false;
                } else {
                    if (valida) {
                        $('#Secuencial').val($('#ModalSecu').val());
                        $('#Origen').val($('#ModalOrigen2').val());
                        $('#Costo').val($('#ModalCosto2').val());
                        $('#formCambiOriCost').submit();
                    } else {
                        return false;
                    }
                }
            }
        </script>
    </body>
</html>