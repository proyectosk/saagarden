<%-- 
    Document   : comparativoClave
    Created on : 8/04/2015, 04:04:27 PM
    Author     : Americo
--%>

<%@page import="java.util.Date"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="conn.ConectionDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%java.text.DateFormat df2 = new java.text.SimpleDateFormat("yyyy-MM-dd"); %>
<%

    /**
     * COMPARATIVO DE ENTRADAS CONTRA SALIDAS POR CLAVE
     */
    ConectionDB con = new ConectionDB();
    response.setContentType("application/vnd.ms-excel");
    response.setHeader("Content-Disposition", "attachment; filename=ComparativoClave.xls");
%>

        <table class="table table-bordered table-condensed table-striped" id="diferencias">
            <thead>
                <tr>
                    <td>Clave</td>
                    <td>Entradas</td>
                    <td>Salidas</td>
                    <td>Total</td>
                    <td>Existencia</td>
                    <td>Dif</td>
                    <td>Devol</td>
                    <td>Dif 2</td>
                </tr>
            </thead>
            <tbody>
                <%
                    con.conectar();
                    /**
                     * RECORRIDO POR TODAS LAS CLAVES Y OBTENER SU EXISTENCIA
                     */
                    ResultSet rset = con.consulta("select F_ClaPro, SUM(F_ExiLot) as F_ExiLot from tb_lote group by F_ClaPro");
                    while (rset.next()) {
                        int cantComprada = 0;
                        int cantSurtida = 0;
                        int cantDevuelta = 0;
                        int existencia = rset.getInt("F_ExiLot");

                        /**
                         * Obtención de lo comprado
                         */
                        ResultSet rset2 = con.consulta("select SUM(F_CanCom) as F_CanCom from tb_compra where F_ClaPro = '" + rset.getString("F_ClaPro") + "' group by F_ClaPro");
                        while (rset2.next()) {
                            cantComprada = rset2.getInt("F_CanCom");
                        }

                        /**
                         * OBTENCION DE LO SURTIDO
                         */
                        rset2 = con.consulta("select F_ClaPro,sum(F_CantSur) as F_CantSur from tb_factura where F_ClaPro = '" + rset.getString("F_ClaPro") + "' GROUP BY F_ClaPro;");
                        while (rset2.next()) {
                            cantSurtida = rset2.getInt("F_CantSur");
                        }

                        /**
                         * Obtención de las devoluciones
                         */
                        rset2 = con.consulta("select F_ClaPro,sum(F_CantSur) as F_CantSur from tb_factura where F_StsFact='C' and F_ClaPro = '" + rset.getString("F_ClaPro") + "' GROUP BY F_ClaPro;");
                        while (rset2.next()) {
                            cantDevuelta = rset2.getInt("F_CantSur");
                        }

                        /**
                         * Calculo de de diferencias
                         */
                        int dif1 = ((existencia) - (cantComprada - cantSurtida));
                        int dif2 = dif1 - cantDevuelta;

                %>
                <tr>
                    <td><a href="comparativoClaveDetalle.jsp?F_ClaPro=<%=rset.getString("F_ClaPro")%>"><%=rset.getString("F_ClaPro")%></a></td>
                    <td class="text-right"><%=cantComprada%></td>
                    <td class="text-right"><%=cantSurtida%></td>
                    <td class="text-right"><%=cantComprada - cantSurtida%></td>
                    <td class="text-right"><%=existencia%></td>
                    <td class="text-right"><%=dif1%></td>
                    <td class="text-right"><%=cantDevuelta%></td>
                    <td class="text-right"><%=dif2%></td>
                </tr>
                <%

                    }
                    con.cierraConexion();

                %>
            </tbody>
        </table>