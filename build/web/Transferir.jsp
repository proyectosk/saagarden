<%-- 
    Document   : index
    Created on : 17/02/2014, 03:34:46 PM
    Author     : Americo
--%>

<%@page import="java.util.Date"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="conn.*" %>
<!DOCTYPE html>
<%java.text.DateFormat df2 = new java.text.SimpleDateFormat("yyyy-MM-dd"); %>
<%
    /**
     * Para seleccionar de donde se tomará el insumo en la facturación manual
     */
    HttpSession sesion = request.getSession();
    String usua = "", tipo = "", nomUsu = "";
    if (sesion.getAttribute("Usuario") != null) {
        usua = (String) sesion.getAttribute("Usuario");
        nomUsu = (String) sesion.getAttribute("NombreU");
    } else {
        response.sendRedirect("indexGarden.jsp");
    }
    ConectionDB con = new ConectionDB();

    String ClaCli = "", FechaEnt = "", ClaPro = "", DesPro = "",ClaPro2 = "", DesPro2 = "", Cantidad = "",F_IndGlobal="",existencia="";

    try {
        ClaCli = (String) sesion.getAttribute("ClaCliFM");
        FechaEnt = (String) sesion.getAttribute("FechaEntFM");
        ClaPro2 = (String) sesion.getAttribute("ClaProFM");
        DesPro2 = (String) sesion.getAttribute("DesProFM");
        ClaPro = (String) sesion.getAttribute("ClaProFM1");
        DesPro = (String) sesion.getAttribute("DesProFM1");
        Cantidad = (String) request.getAttribute("Cantidad");
        F_IndGlobal = (String) sesion.getAttribute("F_IndGlobal");
        existencia = (String) sesion.getAttribute("ExistenciaFM1");
    } catch (Exception e) {

    }
    if (ClaCli == null) {
        ClaCli = "";
    }
    if (FechaEnt == null) {
        FechaEnt = "";
    }
    if (ClaPro == null) {
        ClaPro = "";
    }
    if (DesPro == null) {
        DesPro = "";
    }
    if (Cantidad == null) {
        Cantidad = "";
    }
    if(existencia == null){
        existencia="";
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Estilos CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="css/cupertino/jquery-ui-1.10.3.custom.css" />
        <!--link href="css/navbar-fixed-top.css" rel="stylesheet"-->
        <!---->
        <title>SIALSS</title>
    </head>
    <body>
        <div class="container">
            <h1>SIALSS</h1>
            <h4>Módulo - Sistema de Administración de Almacenes (SAA)</h4>

            <%@include file="jspf/menuPrincipal.jspf"%>

            <div class="row">
                <div class="col-sm-12">
                    <h2>Facturación Manual</h2>
                </div>
            </div>
            <hr/>
            <form action="PedidoManual" method="post">
                <div class="row">
                    <div class="col-sm-1">
                        <h4>Unidad:</h4>
                    </div>
                    <div class="col-sm-5">
                        <select class="form-control" name="ClaCli" id="ClaCli">
                            <option value="">-Seleccione Unidad-</option>
                            <%                                try {
                                    con.conectar();
                                    ResultSet rset = con.consulta("select F_ClaCli, F_NomCli from tb_uniatn");
                                    while (rset.next()) {
                            %>
                            <option value="<%=rset.getString(1)%>"
                                    <%
                                        if (rset.getString(1).equals(ClaCli)) {
                                            out.println("selected");
                                        }
                                    %>
                                    ><%=rset.getString(2)%></option>
                            <%
                                    }
                                    con.cierraConexion();
                                } catch (Exception e) {

                                }
                            %>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <h4>Fecha de Entrega</h4>
                    </div>
                    <div class="col-sm-2">
                        <input type="date" class="form-control" name="FechaEnt" id="FechaEnt" min="<%=df2.format(new Date())%>" value="<%=FechaEnt%>"/>
                    </div>
                    <div class="col-sm-2">
                                <a class="btn btn-block btn-default" href="pedidoManual2.jsp">Regresar</a>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">

                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-1">
                                <h4>Para la Clave:</h4>
                            </div>
                            <div class="col-sm-2">
                                <input class="form-control" readonly="" value="<%=ClaPro2%>" id="ClaveSel2" name="ClaveSel2"/>
                            </div>
                            <div class="col-sm-2">
                                <h4>Descripción:</h4>
                            </div>
                            <div class="col-sm-7">
                                <textarea class="form-control" readonly=""><%=DesPro2%></textarea>
                            </div>
                        </div>
                        <br/>
                    </div>
                            
                </div>
                <div class="row">
                            <div class="col-sm-2">
                                <h4>De la Clave:</h4>
                            </div>
                            <div class="col-sm-2">
                                <input class="form-control" name="ClaPro" id="ClaPro" autocomplete="off" data-provide="typeahead"/>
                            </div>
                            <div class="col-sm-2">
                                <button class="btn btn-primary btn-block" name="accion" value="buscarT" id="btnClave" >Buscar</button>
                            </div>

                        </div>
                        <div class="row">
                            <label class="col-sm-2 text-left">
                                <h4>Descripci&oacute;n:</h4>
                            </label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control typeahead" id="descripcion" name="descripcion" data-provide="typeahead" autocomplete="off" />
                            </div>
                            <div class="col-sm-2">
                                <button class="btn btn-primary btn-block" name="accion" value="buscarDescT" onclick="return validaClaDes(this);">Descripción</button>
                            </div>
                        </div>
                        <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-1">
                                <h4>Clave:</h4>
                            </div>
                            <div class="col-sm-2">
                                <input class="form-control" readonly="" value="<%=ClaPro%>" id="ClaveSel" name="ClaveSel" />
                            </div>
                            <div class="col-sm-2">
                                <h4>Descripción:</h4>
                            </div>
                            <div class="col-sm-7">
                                <textarea class="form-control" readonly="" id="DesSel"><%=DesPro%></textarea>
                            </div>
                        </div>
                            <br />
                        <div class="row">
                            <div class="col-sm-1">
                                <h4>Existencia:</h4>
                            </div>
                            <div class="col-sm-2">
                                <input class="form-control" readonly="" value="<%=existencia%>" id="Exist" name="Exist"/>
                            </div>
                            <div class="col-sm-1">
                                <h4>Transferir:</h4>
                            </div>
                            <div class="col-sm-2">
                                <input type="number" min="1" class="form-control" id="CanTrans" name="CanTrans" onKeyPress="return justNumbers(event);"/>
                            </div>
                        </div>
                        <br/>
                        <div >
                            <input class="form-control" id="folio" name="folio" type="hidden" value="<%=F_IndGlobal%>" />
                            <input class="form-control" id="usua" name="usua" type="hidden" value="<%=usua%>" />
                                <button class="btn btn-success btn-block" name="accion" value="CantidadTrans" id="btnClave" onclick="return validaBuscar();">Transferir</button>
                            </div>
                    </div>    
            </form>
            
        </div>
        <br><br><br>
        <div class="navbar navbar-fixed-bottom navbar-inverse">
            <div class="text-center text-muted">
                GNK Logística || Desarrollo de Aplicaciones 2009 - 2015 <span class="glyphicon glyphicon-registration-mark"></span><br />
                Todos los Derechos Reservados
            </div>
        </div>
        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="js/jquery.js" type="text/javascript"></script>
        <script src="js/jquery-ui_1.js" type="text/javascript"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/bootstrap3-typeahead.js" type="text/javascript"></script>
        <script>
                                function cambiaLoteCadu(elemento) {
                                    var indice = elemento.selectedIndex;
                                    document.getElementById('SelectCadu').selectedIndex = indice;
                                }

                                function validaCantidad(e) {
                                    /**
                                     * Aquí se compara el solicitado contra lo de almacén para ver que se exceda
                                     */
                                    var cantidadSol = document.getElementById('Cantidad').value;
                                    document.getElementById('Cant' + e).value = cantidadSol;
                                    var cantidadAlm = document.getElementById('CantAlm_' + e).value;
                                    if (parseInt(cantidadSol) > parseInt(cantidadAlm)) {
                                        alert('La cantidad a facturar no puede ser mayor a la cantidad de esa ubicación');
                                        return false;
                                    }

                                    /*var confirma = confirm('Seguro de usar esta ubicación?');
                                     if (confirma === false) {
                                     return false;
                                     }*/
                                }
                                
          function validaBuscar() {                
                var clave = document.getElementById('ClaveSel').value;
                if (clave === "") {
                    alert('Escriba clave de Transferencias');
                    return false;
                }
                
                var clave2 = document.getElementById('ClaveSel2').value;
                if (clave2 === "") {
                    alert('Escriba clave transferencias2');
                    return false;
                }
                
                var CanTrans = document.getElementById('CanTrans').value;
                if (CanTrans === "") {
                    alert('Escriba Cantidad a transferir');
                    return false;
                }
                var Exist = document.getElementById('Exist').value;
                if (Exist === "0") {
                    alert('Existencia en 0');
                    return false;
                }
                
            }
            function justNumbers(e)
            {
                var keynum = window.event ? window.event.keyCode : e.which;
                if ((keynum === 8) || (keynum === 46))
                    return true;
                return /\d/.test(String.fromCharCode(keynum));
            }
        </script>
        <script>
                            $(document).ready(function () {
                                $("#ClaPro").typeahead({
                                    source: function (request, response) {

                                        $.ajax({
                                            url: "AutoCompleteProdExist",
                                            dataType: "json",
                                            data: request,
                                            success: function (data, textStatus, jqXHR) {
                                                console.log(data);
                                                var items = data;
                                                response(items);
                                            },
                                            error: function (jqXHR, textStatus, errorThrown) {
                                                console.log(textStatus);
                                            }
                                        });
                                    }

                                });
                                $("#descripcion").typeahead({
                                    source: function (request, response) {

                                        $.ajax({
                                            url: "AutoCompleteProdExistDesc",
                                            dataType: "json",
                                            data: request,
                                            success: function (data, textStatus, jqXHR) {
                                                console.log(data);
                                                var items = data;
                                                response(items);
                                            },
                                            error: function (jqXHR, textStatus, errorThrown) {
                                                console.log(textStatus);
                                            }
                                        });
                                    }

                                });
                            });
        </script>
        
    </body>
</html>

