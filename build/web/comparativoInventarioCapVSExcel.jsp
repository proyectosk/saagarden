<%-- 
    Document   : InventarioExcel
    Created on : 12/10/2015, 04:45:46 PM
    Author     : Mario
--%>

<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<%
    /**
     * Para cargar el excel del requerimiento
     */
    HttpSession sesion = request.getSession();
    String usua = "";
    String tipo = "";
    if (sesion.getAttribute("nombre") != null) {
        usua = (String) sesion.getAttribute("nombre");
        tipo = (String) sesion.getAttribute("Tipo");
    } else {
        response.sendRedirect("index.jsp");
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>SIALSS</title>
    </head>
    <body>
        <div class="container">
            <h1>SIALSS</h1>
            <h4>SISTEMA INTEGRAL DE ADMINISTRACIÓN Y LOGÍSTICA PARA SERVICIOS DE SALUD</h4>

            <%@include file="jspf/menuPrincipal.jspf"%>
        </div>
        <div class="panel container">
            <div class="panel-primary">
                <div class="panel-heading">
                    Inventario Subido Excel
                    <a href="comparativoInventarioGnrExcel?gnr=excel" class="btn btn-primary" target="_blank"><span class="glyphicon glyphicon-download"></span>Descargar</a>
                </div>
                <div class="panel-body ">
                    <c:if test="${noencontrada!=null}">
                        <div class="alert alert-warning" role="alert">
                            <span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span>
                            <span class="sr-only">Adverencia:</span>
                            ${noencontrada}
                        </div>
                    </c:if>
                    <table  class="table table-bordered table-striped" id="tablaExcel">
                        <thead>
                            <tr>
                                <th>Clave</th>
                                <th>Descripci&oacute;n</th>
                                <th>lote</th>
                                <th>Caducidad</th>
                                <th>CB</th>
                                <th>Cantidad</th>
                                <th>Costo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${invExcel}" var="detalle">
                                <tr data-toggle="tooltip" data-placement="top" title="${detalle.clave}">
                                    <td><c:out value="${detalle.clave}" /></td>
                                    <td><c:out value="${detalle.desc}" /></td>
                                    <td><c:out value="${detalle.lote}" /></td>
                                    <td><c:out value="${detalle.caducidad}" /></td>
                                    <td><c:out value="${detalle.cb}" /></td>
                                    <td><c:out value="${detalle.cantidad}" /></td>
                                    <td><c:out value="${detalle.costo}" /></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Clave</th>
                                <th>Descripci&oacute;n</th>
                                <th>lote</th>
                                <th>Caducidad</th>
                                <th>CB</th>
                                <th>Cantidad</th>
                                <th>Costo</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <br>
            <br>
            <div class="panel-success">
                <div class="panel-heading">
                    Inventario Capturado
                    <a href="comparativoInventarioGnrExcel?gnr=capt" class="btn btn-success" target="_blank"><span class="glyphicon glyphicon-download"></span>Descargar</a>
                </div>
                <div class="panel-body ">
                    <c:if test="${noencontrada!=null}">
                        <div class="alert alert-warning" role="alert">
                            <span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span>
                            <span class="sr-only">Adverencia:</span>
                            ${noencontrada}
                        </div>
                    </c:if>
                    <table class="table table-bordered table-striped" id="tablaCap">
                        <thead>
                            <tr>
                                <th>Clave</th>
                                <th>Descripci&oacute;n</th>
                                <th>lote</th>
                                <th>Caducidad</th>
                                <th>CB</th>
                                <th>Cantidad</th>
                                <!--th>Costo</th-->
                                <th></th>

                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${invCap}" var="detalle">
                                <tr data-toggle="tooltip" data-placement="top" title="${detalle.clave}">
                                    <td style="display:none;" class="id"><c:out value="${detalle.id}" /></td>
                                    <td class="clave"><c:out value="${detalle.clave}" /></td>
                                    <td class="desc"><c:out value="${detalle.desc}" /></td>
                                    <td class="lote"><c:out value="${detalle.lote}" /></td>
                                    <td class="cadu"><c:out value="${detalle.caducidad}" /></td>
                                    <td class="cb"><c:out value="${detalle.cb}" /></td>
                                    <td class="cantidad"><c:out value="${detalle.cantidad}" /></td>
                                    <!--td class="costo"><c:out value="${detalle.costo}" /></td-->
                                    <td>


                                        <button class="btn btn-warning rowButton" name="accion" value="modificarInv" data-toggle="modal" data-target="#ModiOc"><span class="glyphicon glyphicon-pencil" ></span></button>
                                        <button class="btn btn-danger rowButtonEli"  name="accion" value="eliminar" data-toggle="modal" data-target="#ModiEli"><span class="glyphicon glyphicon-remove"></span></button>
                                    </td>

                                </tr>
                            </c:forEach>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Clave</th>
                                <th>Descripci&oacute;n</th>
                                <th>lote</th>
                                <th>Caducidad</th>
                                <th>CB</th>
                                <th>Cantidad</th>
                                <!--th>Costo</th-->
                                <th></th>

                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <br><br><br>
        <div class="navbar navbar-fixed-bottom navbar-inverse">
            <div class="text-center text-muted">
                GNK Logística || Desarrollo de Aplicaciones 2009 - 2015 <span class="glyphicon glyphicon-registration-mark"></span><br />
                Todos los Derechos Reservados
            </div>
        </div>

        <!-- Modal -->
        <div id="ModiOc" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modificar Clave</h4>
                    </div>

                    <form name="formEditOC" action="capturarInventario" method="Post">

                        <div class="modal-body">
                            <input class="form-control hidden" name="idMod" id="idMod" type="text" value="" readonly />

                            <div class="row">
                                <h4 class="col-sm-2">Clave:</h4>
                                <div class="col-sm-3">
                                    <input class="form-control" name="claveMod" id="claveMod" type="text" value="" readonly required/>
                                </div>
                                <h4 class="col-sm-3">Descripción</h4>
                                <div class="col-sm-3">
                                    <input class="form-control" name="descMod" id="descMod" type="text" value="" readonly required/>
                                </div>
                            </div>
                            <div class="row">
                                <h4 class="col-sm-2">Lote:</h4>
                                <div class="col-sm-3">
                                    <input class="form-control" name="loteMod" id="loteMod" type="text" value="" required/>
                                </div>
                                <h4 class="col-sm-2">Caducidad:</h4>
                                <div class="col-sm-3">
                                    <input class="form-control" name="caduMod" id="caduMod" type="date" value="" required/>
                                </div>
                            </div>
                            <div class="row">
                                <h4 class="col-sm-2">Cantidad:</h4>
                                <div class="col-sm-3">
                                    <input class="form-control" name="cantMod" id="cantMod" type="number" value="" required/>
                                </div>
                                <!--h4 class="col-sm-2">Costo:</h4>
                                <div class="col-sm-3">
                                    <input class="form-control" name="costoMod" id="costoMod" type="number" step="any" value="" required/>
                                </div-->
                            </div>
                            <div class="row">
                                <h4 class="col-sm-3">CB:</h4>
                                <div class="col-sm-4">
                                    <input class="form-control" name="cbMod" id="cbMod" type="number" value="" required/>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-default" name="accion" value="editar">Guardar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        </div>

                    </form>

                </div>

            </div>
        </div>
        <div id="ModiEli" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Eliminar Clave</h4>
                    </div>

                    <form name="formEliminar" action="capturarInventario" method="Post">

                        <div class="modal-body">
                            <input class="form-control hidden" name="idEli" id="idEli" type="text" value="" readonly />

                            <div class="row">
                                <h4 class="col-sm-12">¿Seguro que desea eliminar la clave?</h4>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-default" name="accion" value="eliminar">Si</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                        </div>

                    </form>

                </div>

            </div>
        </div>
        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="js/jquery-1.9.1.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/bootstrap3-typeahead.js" type="text/javascript"></script>
        <script src="js/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/dataTables.bootstrap.js" type="text/javascript"></script>

        <script>
            $(document).ready(function () {

                $('#tablaExcel').dataTable();
                $('#tablaCap').dataTable();


                $("#clave").typeahead({
                    source: function (request, response) {

                        $.ajax({
                            url: "AutoCompleteMedicamentos",
                            dataType: "json",
                            data: request,
                            success: function (data, textStatus, jqXHR) {
                                console.log(data);
                                var items = data;
                                response(items);
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                console.log(textStatus);
                            }
                        });
                    }

                });
                $("#descripcion").typeahead({
                    source: function (request, response) {

                        $.ajax({
                            url: "AutoCompleteMedicamentosDesc",
                            dataType: "json",
                            data: request,
                            success: function (data, textStatus, jqXHR) {
                                console.log(data);
                                var items = data;
                                response(items);
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                console.log(textStatus);
                            }
                        });
                    }

                });
            });

            //Obtener la caducidad cuando segun el lote
            $(".rowButton").click(function () {

                var $row = $(this).closest("tr");    // Find the row
                var $clave = $row.find("td.clave").text(); // Find the text             
                var $lote = $row.find("td.lote").text(); // Find the text
                var $cadu = $row.find("td.cadu").text(); // Find the text
                var $cant = $row.find("td.cantidad").text(); // Find the text
                var $id = $row.find("td.id").text(); // Find the text
                var $desc = $row.find("td.desc").text(); // Find the text
                //var $costo = $row.find("td.costo").text(); // Find the text
                var $cb = $row.find("td.cb").text(); // Find the text

                $("#claveMod").val($clave);
                $("#descMod").val($desc);
                $("#loteMod").val($lote);
                $("#caduMod").val(formatDate($cadu));
                $("#cantMod").val($cant);
                //$("#costoMod").val($costo);
                $("#cbMod").val($cb);
                $("#idMod").val($id);

            });
            $(".rowButtonEli").click(function () {

                var $row = $(this).closest("tr");    // Find the row
                var $id = $row.find("td.id").text(); // Find the text
                $("#idEli").val($id);

            });

            function formatDate(input) {
                var datePart = input.match(/\d+/g),
                        year = datePart[2], // get only two digits
                        month = datePart[1], day = datePart[0];

                //return day + '/' + month + '/' + year;
                return year + '-' + month + '-' + day;
            }
        </script>
    </body>
</html>

