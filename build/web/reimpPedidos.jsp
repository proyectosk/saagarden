<%-- 
    Document   : index
    Created on : 17/02/2014, 03:34:46 PM
    Author     : Americo
--%>

<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="conn.*" %>
<!DOCTYPE html>
<%
    /**
     * Listado de concentrados globales para su reimpresión
     */
    HttpSession sesion = request.getSession();
    String usua = "";
    String tipo = "";
    if (sesion.getAttribute("Usuario") != null) {
        usua = (String) sesion.getAttribute("Usuario");
    } else {
        response.sendRedirect("indexIsem.jsp");
    }
    ConectionDB con = new ConectionDB();

    String fol_gnkl = "", fol_remi = "", orden_compra = "", fecha = "";
    try {
        if (request.getParameter("accion").equals("buscar")) {
            fol_gnkl = request.getParameter("fol_gnkl");
            fol_remi = request.getParameter("fol_remi");
            orden_compra = request.getParameter("orden_compra");
            fecha = request.getParameter("fecha");
        }
    } catch (Exception e) {

    }
    if (fol_gnkl == null) {
        fol_gnkl = "";
        fol_remi = "";
        orden_compra = "";
        fecha = "";
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Estilos CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap.css">
        <!---->
        <title>SIALSS</title><link type="image/x-icon" href="imagenes/favicon.png" rel="icon" />
    </head>
    <body>
        <div class="container">
            <h1>SIALSS</h1>
            <h4>Módulo - Sistema de Órdenes de Compra, Pedidos y Facturación (SOCPF)</h4>
            <%@include file="jspf/menuCliente.jspf"%>

            <div>
                <h3>Revisión de Concentrados por Proveedor</h3>
                <h4>Seleccione</h4>

                <br />
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <table class="table table-bordered table-striped" id="datosCompras">
                            <thead>
                                <tr>
                                    <td>No. Folio</td>
                                    <td>Punto de entrega</td>
                                    <td>Fecha de entrega</td>
                                    <td>Observaciones</td>
                                    <td>Concentrado</td>
                                    <td>Excel</td>
                                </tr>
                            </thead>
                            <tbody>
                                <%                                    try {
                                        con.conectar();
                                        try {
                                            /**
                                             * Listado de concentrados
                                             */
                                            String TipoEnvio = "", TipoCobro = "", Tipo = "";
                                            ResultSet rset = con.consulta("SELECT u.F_NomCli, DATE_FORMAT(f.F_FecEnt, '%d/%m/%Y') AS FecEnt, l.F_ClaPro, l.F_ClaLot, DATE_FORMAT(l.F_FecCad, '%d/%m/%Y'), (f.F_Cant + 0) AS F_Cant, l.F_Ubica, f.F_IdFact, l.F_Cb,f.F_Obs,f.F_TipoEnvio,f.F_TipoCobro FROM tb_facttemp f, tb_lote l, tb_uniatn u WHERE f.F_IdLot = l.F_IdLote AND f.F_ClaCli = u.F_ClaCli GROUP BY f.F_IdFact;");
                                            while (rset.next()) {
                                                TipoEnvio = rset.getString("F_TipoEnvio");
                                                TipoCobro = rset.getString("F_TipoCobro");
                                                if ((TipoEnvio.equals("PRE-PAGADO")) || (TipoCobro.equals("PRE-PAGADO-OCURRE")) || (TipoCobro.equals("PRE-PAGADO"))) {
                                                    Tipo = "PRE-PAGADO";
                                                } else {
                                                    Tipo = "";
                                                }
                                %>
                                <tr>

                                    <td><%=rset.getString("F_IdFact")%></td>
                                    <td><%=rset.getString("F_NomCli")%></td>
                                    <td><%=rset.getString("FecEnt")%></td>
                                    <td><%=rset.getString("F_Obs")%></td>
                                    <td>
                                        <form action="ImprimirConcentrado" target="_blank">
                                            <input class="hidden" name="fol_gnkl" value="<%=rset.getString("F_IdFact")%>">
                                            <input class="hidden" name="tipo" value="<%=Tipo%>">
                                            <button class="btn btn-block btn-primary">Imprimir</button>
                                        </form>
                                    </td>
                                    <td>
                                        <a class="btn btn-block btn-success" href="gnrConcentrado.jsp?fol_gnkl=<%=rset.getString("F_IdFact")%>" target="_blank">Descargar</a>
                                    </td>

                                </tr>
                                <%
                                            }
                                        } catch (Exception e) {

                                        }
                                        con.cierraConexion();
                                    } catch (Exception e) {

                                    }
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <br><br><br>
        <%@include file="jspf/piePagina.jspf" %>

        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="js/jquery-1.9.1.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery.dataTables.js"></script>
        <script src="js/dataTables.bootstrap.js"></script>
        <script  type="text/javascript">
            $(document).ready(function() {
                $('#datosCompras').dataTable();
            });
            function EditarConcentrado(idCon, distri, fecha) {
                /**
                 * 
                 * jala informacion de la compra
                 */
                $('#hdIdCon').val(idCon);
                $('#lbIdCon').html("  " + idCon);
                $('#lbPun').html("  " + distri);
                $('#lbFecha').html("  " + fecha);
                $('#txtId').val("");
                $('#lbClave').html("");
                $('#lbLote').html("");
                $('#lbCaducidad').html("");
                $('#lbClave').html("");
                $('#lbCantidad').html("");
                $('#btnModificar').addClass("disabled");
                $('#txtCant').val("");
            }
            $('#btnBuscarId').click(function() {
                /**
                 * 
                 * Busca del id seleccionado los datos para editarlos
                 */
                var dir = "Concentrados";
                var id = $('#txtId').val();
                $.ajax({
                    url: dir,
                    data: {que: "b", id: id},
                    success: function(data) {
                        var json = JSON.parse(data);
                        $('#btnModificar').removeClass("disabled");
                        $('#txtId').val("");
                        $('#hdIdCla').val(json.id);
                        $('#lbClave').html(json.clave);
                        $('#lbLote').html(json.lote);
                        $('#lbCaducidad').html(json.caduc);
                        $('#lbClave').html(json.clave);
                        $('#lbCantidad').html(json.cant);
                        if (json.mgs === "0") {
                            $('#btnModificar').addClass("disabled");
                            alert("Error al consultar");
                        }
                    },
                    error: function() {
                        $('#txtId').val("");
                        alert("Ocurrió un error");
                    }
                });
            });
            $('#btnModificar').click(function() {
                /**
                 * Para la edición de la cantidad de los solicitado, ('solo menor')
                 */
                if ($('#txtCant').val() === "" || parseInt($('#txtCant').val()) > parseInt($('#lbCantidad').html())) {
                    alert("La cantida no puede ser cero o mayor a la actual");
                    $('#txtCant').focus();
                    return false;
                }
                var dir = "Concentrados";
                var id = $('#hdIdCla').val();
                var cant = $('#txtCant').val();
                $.ajax({
                    url: dir,
                    data: {que: "mod", id: id, cant: cant},
                    success: function(data) {
                        var json = JSON.parse(data);
                        if (json.msg === "1") {
                            alert('Actualizado Correctamene');
                            $('#lbLote').html("");
                            $('#lbCaducidad').html("");
                            $('#lbClave').html("");
                            $('#lbCantidad').html("");
                            $('#txtCant').val("");
                        } else {
                            alert('Error al Actualizar');
                            $('#lbLote').html("");
                            $('#lbCaducidad').html("");
                            $('#lbClave').html("");
                            $('#lbCantidad').html("");
                            $('#txtCant').val("");
                        }
                    },
                    error: function() {
                        $('#txtId').val("");
                        alert("Ocurrió un error");
                    }
                });
            });
        </script>
        <script>
            $(function() {
                $("#fecha").datepicker();
                $("#fecha").datepicker('option', {dateFormat: 'dd/mm/yy'});
            });
        </script>
    </body>
</html>